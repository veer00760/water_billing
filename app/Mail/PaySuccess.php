<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PaySuccess extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('waterbills@valsadindustrialwaterservices.in')
        ->subject('Payment Status')
        ->markdown('emails.paymentstatus', [
            'status' =>$this->data['status'] ,
            'payid' =>$this->data['payid'] ,
            'amt' =>$this->data['amt'] ,

        ]);   
    }
}
