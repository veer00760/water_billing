<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class VerifyMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
         $this->data = $data;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {  
        $otp = $this->data['otp'];
        return $this->from('waterbills@valsadindustrialwaterservices.in')
        ->subject('Email Verification')
        ->markdown('emails.verifyemail', [
            'otp' =>$otp ,
        ]); 
      
    }
}
