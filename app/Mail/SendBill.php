<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendBill extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
         $this->data = $data;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('waterbills@valsadindustrialwaterservices.in')
        ->subject('New Bill')
        ->markdown('emails.sendbill', [
            'total' =>$this->data['total'] ,
            'paydate' =>$this->data['paydate'] ,
            'month' =>$this->data['month'] ,
            'conn' =>$this->data['conn'] ,
            'name' =>$this->data['name'] ,
            'unit' =>$this->data['unit'] ,
            'status' =>$this->data['status'] ,
            'url' =>$this->data['url'] ,

        ]);   
    
    }
}
