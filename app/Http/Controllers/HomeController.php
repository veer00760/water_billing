<?php

namespace App\Http\Controllers;

use App\Models\Meter;
use App\Models\Connection;
use Illuminate\Http\Request;
use DB;
use Auth;
use App\Models\User;
use App\Models\Party;
use Illuminate\Support\Facades\Hash;
use App\Models\CreateBill;
use App\Models\Settlement;
use App\Models\Payment;
use Illuminate\Support\Facades\Validator;
use App\Models\ExpencesBill;

// use App\Models\Meter;

class HomeController extends Controller
{

    public function __invoke()
    {
        // $this->middleware(['auth', '2fa']);
    }

    public function index()
    { 
        $meter = Meter::count();
        $conn = Connection::count();
        $party = Party::count();
        $active = Connection::where('status','active')->count();
        $active_meter = Meter::where('status','active')->count();
        $payments=[];
        $payments[] = CreateBill::where('paid_status','paid')->where('pay_by','cheque')->sum('paid_amount');
        $payments[] = CreateBill::where('paid_status','paid')->where('pay_by','cash')->sum('paid_amount');
        $payments[] = CreateBill::where('paid_status','paid')->where('pay_by','credit')->sum('paid_amount');
        $payments[] = CreateBill::where('paid_status','paid')->where('pay_by','online')->sum('paid_amount');
        $payments[] = CreateBill::where('paid_status','paid')->where('pay_by','settlement')->sum('paid_amount');
        $current_month = CreateBill::whereMonth('created_at', date('m'))->count();
        $last_month = CreateBill::whereMonth('created_at', date("m",strtotime("first day of previous month")))->count();

        $payment_charts = CreateBill::where('paid_status','paid')->get();


        //         $orders = CreateBill::select(
//             DB::raw('sum(paid_amount) as sums'), 
//             DB::raw("DATE_FORMAT(created_at,'%M %Y') as months"),
//             DB::raw("DATE_FORMAT(created_at,'%m') as monthKey")
//   )
//   ->groupBy('months', 'monthKey')
//   ->orderBy('created_at', 'ASC')
//   ->get();

$months = CreateBill::select(
    DB::raw("DATE_FORMAT(created_at,'%M') as monthKey")
)
->whereYear('created_at', date('Y'))
->where('paid_status','paid')
->groupBy('monthKey')
->orderBy('created_at', 'ASC')
->pluck('monthKey')->toArray();
$cash=[];
$bank=[];
$online=[];
$income=[];
$exp=[];
foreach($months as $month){
    $cash[]=CreateBill::where('paid_status','paid')->where('pay_by','cash')
    ->whereYear('created_at', date('Y'))
    ->whereMonth('created_at', date('m',strtotime($month)))
    ->sum('paid_amount');
    $bank[]=CreateBill::where('paid_status','paid')->where('pay_by','cheque')
    ->whereYear('created_at', date('Y'))
    ->whereMonth('created_at', date('m',strtotime($month)))
    ->sum('paid_amount');
    $online[]=CreateBill::where('paid_status','paid')->where('pay_by','online')
    ->whereYear('created_at', date('Y'))
    ->whereMonth('created_at', date('m',strtotime($month)))
    ->sum('paid_amount');
}
$expmonths = array(
    1=>'Jan',
    2=>'Feb',
    3=>'Mar',
    4=>'Apr',
    5=>'May',
    6=>'Jun',
    7=>'Jul',
    8=>'Aug',
    9=>'Sep',
    10=>'Oct',
    11=>'Nov',
    12=>'Dec',
);
// dd($expmonths);
foreach($expmonths as $expmonth){
    $income[]=CreateBill::where('paid_status','paid')
    ->whereYear('created_at', date('Y'))
    ->whereMonth('created_at', date('m',strtotime($expmonth)))
    ->sum('paid_amount');
    $exp[]=ExpencesBill::whereYear('created_at', date('Y'))
    ->whereMonth('created_at', date('m',strtotime($expmonth)))
    ->sum('amount');
}
// dd($exp,$income,$expmonths);
//  dd($cash);
// $income = CreateBill::select(
//     DB::raw('sum(paid_amount) as sums'), 
//     DB::raw("DATE_FORMAT(created_at,'%M') as monthKey")
// )
// ->whereYear('created_at', date('Y'))
// ->where('pay_by','cash')
// ->groupBy('monthKey')
// ->orderBy('created_at', 'ASC')
// ->pluck('sums','monthKey')->toArray();
// dd($income);

// dd($bill_by_month);
        return view('dashboard',['exp'=>$exp,'income'=>$income,'expmonths'=>$expmonths,'cash'=>$cash,'bank'=>$bank,'online'=>$online,'months'=>$months,'last_month'=>$last_month,'current_month'=>$current_month,'active_meter'=>$active_meter,'payment'=>$payments,'meter'=>$meter,'conn'=>$conn,'party'=>$party,'active'=>$active]);

    }

    public function customerdashboard()
    { 
        if(Auth::guard('customer')->user()->party_id){
         $conns =   Party::where('id',Auth::guard('customer')->user()->party_id)->first();
        $authority = 'Party Connection';
        } else{
            $conns =   Connection::where('id',Auth::guard('customer')->user()->connection_id)->first();
            $authority = 'Individual Connection';

        }

        return view('customer.dashboard',['authority'=>$authority,'conns'=>$conns]);

    }

    
    public function customerbill($id=null)
    { 
        if($id==null){
        if(Auth::guard('customer')->user()->party_id){
      $conns =   Connection::where('parties_id',Auth::guard('customer')->user()->party_id)->get();
           } else{
        $conns =   Connection::where('id',Auth::guard('customer')->user()->connection_id)->get();
           }
    $bill = CreateBill::where('connections_id',$conns[0]->id)->orderBy('id','DESC')->first();
    $id = $conns[0]->id;
   
    }
        else{
            $myconn =   Connection::where('id',$id)->first();
            if(Auth::guard('customer')->user()->party_id){
                $conns =   Connection::where('parties_id',Auth::guard('customer')->user()->party_id)->get();
                     } else{
                  $conns =   Connection::where('id',Auth::guard('customer')->user()->connection_id)->get();
                     }
             $bill = CreateBill::where('connections_id',$myconn->id)->orderBy('id','DESC')->first();
        }
        if(!empty($bill)){
        $predue = CreateBill::where('id', '<', $bill->id)
        ->where('paid_status', 'unpaid')
            ->orderBy('id', 'DESC')
            ->first();
        }else{
            $predue[]='';   
        }
        $prev_consumption = CreateBill::select('consume_unit','billing_month')->where('id', '<', $bill->id)
        ->orderBy('id', 'DESC')
        ->take(3)
        ->get();
    return view('customer.bill',['prev_consumption'=>$prev_consumption,'id'=>$id,'predue'=>$predue,'conns'=>$conns,'bill'=>$bill]);

    }

    public function viewprofile(){
    //    dd(Auth::user()->role);
       $user = User::find(Auth::user()->id); 
       return view('admin.profile',['user'=>$user,]);

    }

    public function updateprofile(Request $request){
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|email|unique:users,email,'.Auth::user()->id,
            // 'password' => 'string|c_password|min:8',
        ]);

       $user =  User::find(Auth::user()->id);
       if(isset($request->password)){
       if (Hash::check($request->cur_password, $user->password)) { 
        $user->password = Hash::make($request->password);     
        $request->session()->flash('success', 'Password changed');
        //  return redirect()->route('profile')->with('status', 'Password changed');
     
     } else {
         $request->session()->flash('error', 'Password does not match');
         return redirect()->route('profile')->with('status', 'Password does not match');
     }
    }
     $user->email = $request->email;     
     $user->name = $request->name;     
     $user->save();     
     return redirect()->route('profile')->with('status', 'Profile updated succesfully..');

    
        }


        public function transactions(){
            $trans = CreateBill::where('paid_status','paid')->where('paid_amount','!=',null)->orderBy('id', 'DESC')->get();
            $settlements = Settlement::orderBy('id', 'DESC')->get();
            $payments = Payment::where('status','failure')->orderBy('id', 'DESC')->get();

            return view('admin.transaction',['payments'=>$payments,'settlements'=>$settlements,'trans'=>$trans,]);

        }

        public function contactus(){

            return view('contactus');

        }

        public function terms(){

            return view('terms');

        }

        public function newconn(){

            return view('customer.newconnection');

        }

        public function viewrecipt($createBill)
        {
 
            $bill = CreateBill::find($createBill)->load('meter')
                ->load('conn');
                if($bill->paid_status=='unpaid'){
                 return redirect()->back();
 
                }
            $unpaid_bills = '';
            $predue = CreateBill::where('id', '<', $bill->id)
            ->where('paid_status', 'unpaid')
                ->orderBy('id', 'DESC')
                ->first();
            if ($bill->unpaid_bill_after_meter_deactivate <= 2)
            {
                $unpaid_bills = CreateBill::where('id', '<', $bill->id)
                    ->orderBy('id', 'DESC')
                    ->take(3)
                    ->get();
    
            }
            $prev_consumption = CreateBill::select('consume_unit','billing_month')->where('id', '<', $bill->id)
            ->orderBy('id', 'DESC')
            ->take(3)
            ->get();
            return view('customer.recipt', ['prev_consumption'=>$prev_consumption,'unpaid_bills' => $unpaid_bills, 'predue' => $predue, 'bill' => $bill]);
    
        }


}
