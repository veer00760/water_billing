<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Illuminate\Http\Request;
use App\Http\Requests\Auth\LoginRequest;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\Redirect;
use App\Models\Party;
use App\Models\Connection;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Contracts\Auth\Authenticatable;
use App\Models\CreateBill;
use Illuminate\Support\Facades\Mail;
use App\Mail\VerifyMail;
use App\Models\Otp;
use Validator;


class CustomerController extends Controller
{
    // use AuthenticatesUsers;
    protected $redirectTo = '/customer/dashboard';

    // public function guard()
    // {
    //  return Auth::guard('customer');
    // }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('customer.login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'party' => 'unique:customers,party_id',
            'mobile' => 'required|',
            'email' => 'required|string|email|max:255|unique:customers',
            'password' => 'required|string|confirmed|min:8',
            'connection_id' => 'unique:customers,connection_id',

        ]);
       
        $data=$request->all();
        session(['regdata' =>$data ]);
      
        // Mail::to('its.virendra.singh.ecb@gmail.com')->send(new VerifyMail($otp));
        // session(['otp' =>$otp ]);

        return redirect()->route('verifyemail');

        // $customer = Customer::create([
        //     'mobile' => $request->mobile,
        //     'email' => $request->email,
        //     'password' => Hash::make($request->password),
        //     'party_id' => $request->party,
        //     'connection_id' => $request->connection_id,

        // ]);

        // event(new Registered($user));

        return redirect('/login');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show(Customer $customer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function edit(Customer $customer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Customer $customer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Customer $customer)
    {
        //
    }

    public function login(LoginRequest $request){

        $request->validate([
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|',
        ]);
        if (Auth::guard('customer')->attempt(['email' => $request->email, 'password' => $request->password])) {
            $request->session()->regenerate();
            //   $cus =  new Customer;
            // Auth::guard('customer')->login($cus);
            // dd($request->session());
            return redirect()->intended('/customer/dashboard');
        }
         $errors = new MessageBag(['password' => ['Email and/or password invalid.']]); // if Auth::attempt fails (wrong credentials) create a new message bag instance.

        return Redirect::back()->withErrors($errors)->withInput($request->only('email')); // redirect back to the login page, using ->withErrors($errors) you send the error created above
     
    }

    public function logout(Request $request)
    {
        Auth::guard('customer')->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/');
    }

    public function registration()
    {
       $parties =  Party::all();
        return view('customer.registration',['parties'=>$parties]);

    }

    public function register()
    {
        return view('customer.register');

    }

    public function housing_register()
    {
       $conns =  Connection::all();
    //    dd($conns);
        return view('customer.housing_reg',['conns'=>$conns]);

    }

        public function records($id=null){
            if($id==null){ 
            if(Auth::guard('customer')->user()->party_id){
                $conns =   Connection::where('parties_id',Auth::guard('customer')->user()->party_id)->pluck('id')->toArray();
                $myconn =   Connection::where('parties_id',Auth::guard('customer')->user()->party_id)->get();
                $profile =   Party::where('id',Auth::guard('customer')->user()->party_id)->first();
            } else{
                   $conns =   Connection::where('id',Auth::guard('customer')->user()->connection_id)->pluck('id')->toArray();
                   $myconn =   Connection::where('id',Auth::guard('customer')->user()->connection_id)->get();
                   $profile =   Connection::where('id',Auth::guard('customer')->user()->connection_id)->first();

               }
               
                $id = $conns[0];

            }
            else{
                if(Auth::guard('customer')->user()->party_id){
                    // $conns =   Connection::where('parties_id',Auth::guard('customer')->user()->party_id)->pluck('id')->toArray();
                     $myconn =   Connection::where('parties_id',Auth::guard('customer')->user()->party_id)->get();
                     $profile =   Party::where('id',Auth::guard('customer')->user()->party_id)->first();

                    } else{
                    //    $conns =   Connection::where('id',Auth::guard('customer')->user()->connection_id)->pluck('id')->toArray();
                       $myconn =   Connection::where('id',Auth::guard('customer')->user()->connection_id)->get();
                       $profile =   Connection::where('id',Auth::guard('customer')->user()->connection_id)->first();

                   }
                 $conns =   Connection::where('id',$id)->pluck('id')->toArray();
                // $myconn =   Connection::where('id',$id)->get();

            }
            //  dd($conns);
               $bills = CreateBill::whereIn('connections_id', $conns)->where('paid_status','paid')->get();
               return view('customer.records',['profile'=>$profile,'id'=>$id,'conns'=>$myconn,'bills'=>$bills]);

        }

        public function profile()
        {
           $profile= Customer::find(Auth::guard('customer')->user()->id);
           if(Auth::guard('customer')->user()->party_id){
            $myconn =   Connection::where('parties_id',Auth::guard('customer')->user()->party_id)->get();
           } else{
               $myconn =   Connection::where('id',Auth::guard('customer')->user()->connection_id)->first();

           } 
           return view('customer.profile.view',['myconn'=>$myconn,'profile'=>$profile]);

        }

        public function updateprofile(Request $request)
        {
            //  dd($request);
            $id= Auth::guard('customer')->user()->id;
            $request->validate([
                'mobile' => 'required|string|max:255|unique:customers,mobile,'.$id,
                'email' => 'required|email|unique:customers,email,'.$id,
                // 'password' => 'string|c_password|min:8',
            ]);
            $user= Customer::find($id);
            if(isset($request->password)){
                if (Hash::check($request->cur_password, $user->password)) { 
                 $user->password = Hash::make($request->password);     
                $user->save();
                return redirect()->route('profile')->with('status', 'Password changed succesfully');

                 //  $request->session()->flash('success', 'Password changed');
                //   return redirect()->route('profile')->with('status', 'Password changed');
              
              } else {
                  $request->session()->flash('error', 'Password does not match');
                  return redirect()->route('profile')->with('status', 'Password does not match');
              }
             }
            //  if($request->email != $user->email){
            //     $otp['otp'] = rand(1000,9999);
            //     $email = $request->email;
                
            //     Otp::create([
            //     'email'=>$email,
            //     'otp'=>$otp['otp'],
            //     ]);
            //     Mail::to($email)->send(new VerifyMail($otp));
            //     return redirect()->route('verifymail', $email);
            //     // return redirect()->route('verifymail');
            //     // return view('customer.profile.verifyemail',['otp'=>$otp['otp']]);

            //  }

            //  if($request->mobile != $user->mobile){
            //     $otp['otp'] = rand(1000,9999);
            //     $mobile = $request->mobile;
                
            //     Otp::create([
            //     'mobile'=>$mobile,
            //     'otp'=>$otp['otp'],
            //     ]);
            //     $this->sendsms($mobile,$otp);
            //     return redirect()->route('verifymobile', $mobile);
               
            //  }

        }

        // public function verifymail($email)
        // {
        //     // $email= Auth::guard('customer')->user()->email;
        //     $otp =  Otp::where('email',$email)->orderBy('id','DESC')->first();
        // //  dd($email);
        //     return view('customer.profile.verifyemail',['otp'=>$otp->otp,'email'=>$email]);

        // }

        
        // public function verifymobile($mobile)
        // {
        //     // $email= Auth::guard('customer')->user()->email;
        //     $otp =  Otp::where('mobile',$mobile)->orderBy('id','DESC')->first();
        // //  dd($email);
        //     return view('customer.profile.verifymobile',['otp'=>$otp->otp,'mobile'=>$mobile]);

        // }

        public function updatedata(Request $request)
        {
            $id= Auth::guard('customer')->user()->id;
            $user= Customer::find($id);
            if($request->email){
           
                $user->email=$request->email;
                $user->save();

            }  

            if($request->mobile){
           
                $user->mobile=$request->mobile;
                $user->save();

            } 
        }

        public function resend(Request $request)
        {
            $id= Auth::guard('customer')->user()->id;
            $user= Customer::find($id);
            if($request->email){
                $otp['otp'] = rand(1000,9999);
                $email = $request->email;
                
                Otp::create([
                'email'=>$email,
                'otp'=>$otp['otp'],
                ]);
                Mail::to($email)->send(new VerifyMail($otp));
                return response()->json([
                    'otp' => $otp,
                ]);
            }  

            if($request->mobile){
                $otp['otp'] = rand(1000,9999);
                $mobile = $request->mobile;
                
                Otp::create([
                'mobile'=>$mobile,
                'otp'=>$otp['otp'],
                ]);
                $this->sendsms($mobile,$otp);
                return response()->json([
                    'otp' => $otp,
                ]);
            } 
        }

        public function sendsms($mobile,$otp){
            // Account details
            $apiKey = urlencode('OGJiNWQ2ZDhhOTI3OTRjNTllZDUwNGQ5YjUwYWU4MTA=');
            $numbers = $mobile;
            $sender = urlencode('VIAWSe');
             $message = rawurlencode("Dear Customer, your OTP for verification is $otp, From VIA.");
            // Prepare data for POST request
            $data = array('apikey' => $apiKey, 'numbers' => $numbers, "sender" => $sender, "message" => $message);
         
            // Send the POST request with cURL
            $ch = curl_init('https://api.textlocal.in/send/');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($ch);
            curl_close($ch);
            return $response;
                    }

        public function checkmail(Request $request){
            // $key = $request->email;
            // dd($key);
            $id= Auth::guard('customer')->user()->id;
            $request->validate([
                'email' => 'required|email|unique:customers,email,'.$id,
            ]);
            $user= Customer::find($id);
            if($request->email != $user->email){
                $otp['otp'] = rand(1000,9999);
                $email = $request->email;
                
                Otp::create([
                'email'=>$email,
                'otp'=>$otp['otp'],
                ]);
                Mail::to($email)->send(new VerifyMail($otp));
                // return redirect()->route('verifymail', $email);
                // // return redirect()->route('verifymail');
                 return view('customer.profile.verifyemail',['otp'=>$otp['otp'],'email'=>$request->email]);

             }else{
                return redirect()->route('profile');

             }
        }

        public function checkmob(Request $request){
            // $key = $request->email;
            // dd($key);
            $id= Auth::guard('customer')->user()->id;
            $request->validate([
                'mobile' => 'required|string|max:255|unique:customers,mobile,'.$id,
            ]);
            $user= Customer::find($id);
       
            if($request->mobile != $user->mobile){
                $otp['otp'] = rand(1000,9999);
                $mobile = $request->mobile;
                
                Otp::create([
                'mobile'=>$mobile,
                'otp'=>$otp['otp'],
                ]);
                // dd($mobile);
                $this->sendsms($mobile,$otp['otp']);
                return view('customer.profile.verifymobile',['otp'=>$otp['otp'],'mobile'=>$request->mobile]);
               
             }else{
                return redirect()->route('profile');

             }
        }


}
