<?php

namespace App\Http\Controllers;

use App\Models\Noc;
use Illuminate\Http\Request;
use Auth;
use App\Models\Connection;
use App\Models\Party;
use App\Models\Download;
use App\Models\Notification;
use App\Models\NocRecords;
use PDF;
use App\Mail\ApplyNoc;
use Illuminate\Support\Facades\Mail;
use App\Mail\ReadyNoc;
use App\Models\Customer;
use Carbon\Carbon;
use App\Models\CustomBill;

class NocController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(isset(Auth::guard('customer')->user()->party_id)){
            $party_id = Auth::guard('customer')->user()->party_id;
            $nocs = Noc::orderBy('id', 'DESC')->where('party_id',$party_id)->get();  
 
        } else{
        $conn = Auth::guard('customer')->user()->connection_id;
        $nocs = Noc::where('connection_id',$conn)->orderBy('id', 'DESC')->get();
        }
        // if($noc->status=='approve'){
        // $update = Carbon::parse($nocs->updated_at );
        // $sevendays =  date('Y-m-d H:i:s', strtotime($nocs->updated_at. " + 7 days"));
        // $seven = Carbon::parse($sevendays);
        // $check = $update->gt($seven);
        // }
        //  dd($chdate);
        return view('noc.noclist',['nocs'=>$nocs]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(isset(Auth::guard('customer')->user()->party_id)){
            $party_id = Auth::guard('customer')->user()->party_id;
            $noc = Noc::orderBy('id', 'DESC')->where('party_id',$party_id)->first();  
            if(!empty($noc)){
                if($noc->status=='pending' ||$noc->status=='checked' )
                    {
                       $noc = $noc;
                    }else{
                        $noc=null;
                    }
            }
            $debit=0;
            // $debit = Connection::where('parties_id',$party_id)->where('debit','>',0)->count();
           $record = Party::where('id',$party_id)->first();
           $myconn = Connection::where('parties_id',$party_id)->get();
        }else{
            $conn = Auth::guard('customer')->user()->connection_id;
            $noc = Noc::where('connection_id',$conn)->orderBy('id', 'DESC')->first();
            if(!empty($noc)){
                if($noc->status=='pending' ||$noc->status=='checked' )
                    {
                       $noc = $noc;
                    }else{
                        $noc=null;
                    }
            }
            $debit = Connection::where('id',$conn)->where('debit','>',0)->count();
            $record = Connection::where('id',$conn)->first();
           $myconn=null;
        }
        //  dd($noc);
// $debit=2;
        return view('noc.noc',['myconn'=>$myconn,'noc'=>$noc,'debit'=>$debit,'record'=>$record]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $debit = Connection::where('id',$request->conn_id)->where('debit','>',0)->count();
        $debit2 = CustomBill::where('connection_id',$request->conn_id)->where('paid_status','unpaid')->count();

        if($debit>0){
            return redirect()->back()->with('status', 'Please clear all dues before applying for NOC');
   
        }
        if($debit2>0){
            return redirect()->back()->with('status', 'Please clear all dues before applying for NOC');
   
        }
        if(isset(Auth::guard('customer')->user()->party_id)){
         $userid = Auth::guard('customer')->user()->party_id; 
         $conn_id=NULL;
         $name = Party::find($userid);
         $party_id=$name->id;
        }else{
            $userid = Auth::guard('customer')->user()->connection_id; 
           $name = Connection::find($userid);
        //    $party_id=$name->party->id;
        $party_id =NULL;
        $conn_id=$name->id;
        }
       $noc =  Noc::latest()->first();
if(empty($noc)){
    $mynoc = 0;
}else{
   $mynoc= $noc->id; 
}
        if($mynoc<=999){
            $num = $mynoc+1; 
            $str_length = 4; 

            // Left padding if number < $str_length 
            $str = substr("0000{$num}", -$str_length); 
            $vno =   sprintf($str); 
          } else{
             $vno = $noc->id+1;
          }
        $noc = Noc::create([
            'connection_id' => $request->conn_id,
            'party_id' => $party_id,
             'user_id' => $userid,
            'reason' => $request->reason,
            'status' => 'pending',
            'certificate_no' => $vno,

        ]);

        $not = Notification::create([
            'type' => 'noc',
            'message' => 'Application for noc: By'.$name->name.'industry',
        ]);

        $mydata=[];
$mydata['message'] ='Thank you for applying for NOC certificate, your reg No is :'.$vno.'.Your application is being initiated. Will be notified once approved ';
Mail::to($name->email)->send(new ApplyNoc($mydata));
$mobile = Auth::guard('customer')->user()->mobile;

            if(isset($mobile)){
                $this->sendsms($mobile,$vno);

            }
        return redirect()->route('noc.index')->with('status', 'Applied succesfully..');
       
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Noc  $noc
     * @return \Illuminate\Http\Response
     */
    public function show( $noc)
    {
        $noc = Noc::find($noc);
        if($noc->status != 'approve' && $noc->status != 'cancel' ){
        $noc->status= 'checked';
        $noc->save();
        }
        return view('noc.show',['noc'=>$noc]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Noc  $noc
     * @return \Illuminate\Http\Response
     */
    public function edit( $noc)
    {
     

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Noc  $noc
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Noc $noc)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Noc  $noc
     * @return \Illuminate\Http\Response
     */
    public function destroy(Noc $noc)
    {
        //
    }

    public function noclist()
    {
        $nocs = Noc::OrderBy('id','DESC')->get();
        $connections = Connection::orderBy('id', 'DESC')->get();

        return view('noc.list',['nocs'=>$nocs,'connections'=>$connections]);

    }

        // Generate PDF
        public function createPDF($noc) {
            $data = Noc::find($noc);
      
        $dwid = Download::create([
            'nocid' => $noc,
            'download_by' => Auth::user()->id,
            'counter'=>1,
        ]);

        $data->download= $data->download+1;
        $data->save();
        // share data to view
        // view()->share('noc',$data);
        // $pdf = PDF::loadView('nocpdf', $data);
  
        // download PDF file with download method
        //  return $pdf->download('noc.pdf');
        // return redirect()->route('shownoc',$data->id);
        //  
     }

     public function downloadnoc($noc) {
        $data = Noc::find($noc);
  
    // share data to view
    view()->share('noc',$data);
    $pdf = PDF::loadView('nocpdf', $data);
    // return $pdf->stream();
    // dd($pdf);

    // download PDF file with download method
     return $pdf->download('noc.pdf');
    return redirect()->route('noc');
    //  
 }

          public function update_noc(Request $request)
          {
             $noc =  Noc::find($request->nocid);
             $noc->status= $request->status;
             $noc->issued_by= $request->issued_by;
             $noc->save();
            if($request->status=='approve'){
                if(isset($noc->party_id)){
                    $name = Party::find($noc->party_id);
                    $data= Customer::where('party_id',$noc->party_id)->first();
                   }else{
                      $name = Connection::find($noc->connection_id);
                      $data= Customer::where('connection_id',$noc->connection_id)->first();
                   }
                $mydata=[];
            $mydata['message'] ='Your NOC certificate is approved with Ceritficate No :'.$noc->certificate_no.' NOC certificate is invalid until Signed and approved by Authority';
            if(isset($name->email)){

            Mail::to($name->email)->send(new ReadyNoc($mydata));
            }
            if(isset($data->mobile)){
                $this->sendverifysms($data->mobile,$noc->certificate_no);
               }
            }
             return redirect()->route('shownoc',$noc->id)->with('status', 'updated succesfully..');


          }

        
          public function seeall()
          {
              $notis = Notification::OrderBy('id','DESC')->get();
              return view('noc.notis',['notis'=>$notis,]);
      
          }

          public function updatenoti()
          {
              $values = Notification::where('status', '0')->update(['status'=>'1']);
             echo 'success'; exit;
          }

          public function customershownoc( $noc)
          {
              $noc = Noc::find($noc);
              return view('customer.noc',['noc'=>$noc]);
          }

          public function sendsms($mobile,$regid){
            // Account details
            $apiKey = urlencode('OGJiNWQ2ZDhhOTI3OTRjNTllZDUwNGQ5YjUwYWU4MTA=');

            $numbers = $mobile;
             $sender = urlencode('VIANOC');
            // Prepare data for POST request
            $message = rawurlencode("Hello, your application for NOC application is being registered, will be informed once approved. Your Reg. ID is $regid, Regards, VIA.");
            $data = array('apikey' => $apiKey, 'numbers' => $numbers, "sender" => $sender, "message" => $message);
         
            // Send the POST request with cURL
            $ch = curl_init('https://api.textlocal.in/send/');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($ch);
            curl_close($ch);
            return $response;
                    
        }

        public function sendverifysms($mobile,$regid){
            // Account details
            $apiKey = urlencode('OGJiNWQ2ZDhhOTI3OTRjNTllZDUwNGQ5YjUwYWU4MTA=');
            $numbers = $mobile;
             $sender = urlencode('VIANOC');
            // Prepare data for POST request
            $message = rawurlencode("Hello, your NOC application for $regid, has been approved, please collect your certificate from office. Remember, not valid until signed and stamped. Regards, VIA.");
            $data = array('apikey' => $apiKey, 'numbers' => $numbers, "sender" => $sender, "message" => $message);
         
            // Send the POST request with cURL
            $ch = curl_init('https://api.textlocal.in/send/');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($ch);
            curl_close($ch);
            return $response;
                    
        }

        public function applynoc(Request $request)
        {
            // dd($request);
            $debit = Connection::where('id',$request->conn_id)->where('debit','>',0)->count();
        $debit2 = CustomBill::where('connection_id',$request->conn_id)->where('paid_status','unpaid')->count();

        if($debit>0){
            return redirect()->back()->with('status', 'Please clear all dues before applying for NOC');
   
        }
        if($debit2>0){
            return redirect()->back()->with('status', 'Please clear all dues before applying for NOC');
   
        }

        $noc = Noc::orderBy('id', 'DESC')->where('connection_id',$request->conn_id)->first();  
            if(!empty($noc)){
                if($noc->status=='pending' ||$noc->status=='checked' )
                    {
                        return redirect()->back()->with('status', 'You have already applied for NOC...');
                    }
            }
        $conn = Connection::find($request->conn_id);
        $noc =  Noc::latest()->first();
        if(empty($noc)){
            $mynoc = 0;
        }else{
           $mynoc= $noc->id; 
        }
                if($mynoc<=999){
                    $num = $mynoc+1; 
                    $str_length = 4; 
        
                    // Left padding if number < $str_length 
                    $str = substr("0000{$num}", -$str_length); 
                    $vno =   sprintf($str); 
                  } else{
                     $vno = $noc->id+1;
                  }
                $noc = Noc::create([
                    'connection_id' => $request->conn_id,
                    'party_id' => $conn->parties_id,
                    'reason' => $request->reason,
                    'status' => 'pending',
                    'certificate_no' => $vno,
        
                ]);
        
                $not = Notification::create([
                    'type' => 'noc',
                    'message' => 'Application for noc: By'.$conn->name.'industry',
                ]);
        
                $mydata=[];
        $mydata['message'] ='Thank you for applying for NOC certificate, your reg No is :'.$vno.'.Your application is being initiated. Will be notified once approved ';
       if(isset($conn->email)){
        Mail::to($conn->email)->send(new ApplyNoc($mydata));
       }
                    if(isset($conn->phone)){
                        $this->sendsms($conn->phone,$vno);
        
                    }
                return redirect()->back()->with('status', 'Noc Applied succesfully..');
        
        
        }

}
