<?php

namespace App\Http\Controllers;

use App\Models\Connection;
use App\Models\ConnectionCharge;
use App\Models\Party;
use App\Models\Meter;
use App\Models\activity;
use App\Models\CreateBill;
use Illuminate\Http\Request;
use DB;
use Session;
use Carbon\Carbon;
use App\Models\Customer;

class ConnectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $connections = Connection::orderBy('id', 'DESC')->get();
        $active = Connection::orderBy('id', 'DESC')->where('status','active')->count();
        return view('connection.list',['active'=>$active,'connections'=>$connections]);
    }


        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function partybill()
    {
        //
        // $connections = Connection::orderBy('id', 'DESC')->get();
        return view('party.partywise');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $parties = Party::all();
        $partyTypes = DB::table('party_types')->get();
        $connectionTypes = DB::table('connection_types')->get();
// dd($connectionTypes);
        return view('connection.add',['partyTypes'=>$partyTypes,'parties'=>$parties,'connectionTypes'=>$connectionTypes]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
           
            'party' => 'required',
            'connection_types_id' => 'required',
            'onname' => 'required',
            'party_types_id' => 'required',
            // 'onphone' => 'digits:10',
            // 'onemail' => 'string|email|max:255|unique:connections,email',
            // 'onemail' => 'string|email|max:255',
            'onaddress' => 'required',
            'meter_no' => 'required',
            'innital_meter_reading' => 'required',
            'installation_date' => 'required',
            ]);

            // $meterid = Meter::create([
            //     'meter_no' => $request->meter_no,
            //     'installation_date' => date('y-m-d h:i:s',strtotime($request->installation_date)),
            //     'party_types_id' => $request->party_types_id,
            //     'status' => $request->status,
            //     'innital_meter_reading' => $request->innital_meter_reading,
            //     'meter_test_copy' => $request->meter_test_copy,
            //     'appliaction_copy' => $request->application_copy,
            // ]);
    
            // $connid = Connection::create([
            //     'conn_no' => rand(999,99999),
            //     'name' => $request->onname,
            //     'phone' => $request->onphone,
            //     'connection_types_id' => $request->connection_types_id,
            //     'parties_id' => $request->party,
            //     'meters_id' => $meterid->id,
            //     'email' => $request->onemail,
            //     'address' => $request->onaddress,
            // ]);
            
            // Meter::where('id', $meterid->id)
            // ->update(['connections_id' => $connid->id]);
            // // 

            $meter_test_copy='';
            $application_copy='';
            if($request->meter_test_copy)
            {
                $images = $request->file('meter_test_copy');
                $destinationPath = public_path('/meter_test_copy'); 
      
                 // $imageed = $request->file('pdffile')[$key];
                    $meter_test_copy = time().'.'.$images->getClientOriginalExtension(); 
                    $image->move($destinationPath, $meter_test_copy); 
      
            }
            if($request->application_copy)
            {
                $image = $request->file('application_copy');
                $destinationPath = public_path('/application_copy'); 
      
                 // $imageed = $request->file('pdffile')[$key];
                    $application_copy = time().'.'.$image->getClientOriginalExtension(); 
                    $image->move($destinationPath, $application_copy); 
      
            }
        
           $data=$request->all();
           $data['meter_test_copy'] = $meter_test_copy;
           $data['application_copy'] =$application_copy;
            session(['data' =>$data ]);


            // Session::put('data', $request->all());
            return redirect()->route('charge');
            // return redirect()->route('conn')->with('status', 'Data Save Succesfully..');

            // return back()->with('status', 'success');
    
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Connection  $connection
     * @return \Illuminate\Http\Response
     */
    public function show( $connection)
    {
        $reason = $_GET['deactivate_reason'];
        Connection::where('id', $connection)
        ->update(['status' => 'inactive','deactivate_reason'=>$reason]);
         activity::create([
            'connections_id' => $connection,
            'reason' => $reason,
            'type' => 'deactivate',
        ]);
        return redirect()->route('conn')->with('status', 'Connection status updated succesfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Connection  $connection
     * @return \Illuminate\Http\Response
     */
    public function edit( $connection)
    {  
        $connection = Connection::find($connection);
        $parties = Party::all();
        $partyTypes = DB::table('party_types')->get();
        $connectionTypes = DB::table('connection_types')->get();
 
        return view('connection.edit',['partyTypes'=>$partyTypes,'connection'=>$connection,'parties'=>$parties,'connectionTypes'=>$connectionTypes]);
 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Connection  $connection
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $connection)
    {
        $connection = Connection::find($connection);
        // dd($request);
        $request->validate([
           
            'party' => 'required',
            // 'connection_types_id' => 'required',
            'onname' => 'required',
            'party_types_id' => 'required',
            'onphone' => 'digits:10',
             'onemail' => "email|max:255|unique:connections,email,$connection->id,id",
            // 'onemail' => "required|string|email|max:255",
            'onaddress' => 'required',
            ]);
            $connection->parties_id = $request->party;
            $connection->name = $request->onname;
            $connection->email = $request->onemail;
            $connection->phone = $request->onphone;
            // $connection->connection_types_id = $request->connection_types_id;
            $connection->address = $request->onaddress;
            $connection->save();
            return redirect()->route('conn')->with('status', 'Data updated ..</br> Succesfully ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Connection  $connection
     * @return \Illuminate\Http\Response
     */
    public function destroy(Connection $connection)
    {
        dd($connection);
    }

    public function getconnection(Request $request)
    {
        // dd($request);
        $monthbill = CreateBill::where('connections_id',$request->value)->whereMonth('created_at', date('m'))
        ->whereYear('created_at', date('Y'))
        ->count();
        if($monthbill>=1){
        $error = 'error'; 
        // echo  json_encode($error);
        //  exit;
                 }
        $connection = Connection::where('id',$request->value)->orderBy('id', 'DESC')->first()
        ->load('party')->load('meter')
        ->load('conn_charge')
        ->load('bill')
        ->load('conn_type');
        
         $bills =   CreateBill::where('connections_id',$connection->id)->orderBy('id', 'ASC')->get();
          // $bills =   CreateBill::where('connections_id',$connection->id)->where('paid_status','unpaid')->orderBy('id', 'ASC')->get();
        // $mbills =   CreateBill::where('connections_id',$connection->id)->orderBy('id', 'DESC')->take(2)->get();
        $billdata = '';
        $totalunit=0;
        $alltotal=0;
        $unpaiadbill=0;
        $connection['avgtotal']=0; 
        $prevCapital=0;

        foreach($bills as $bill){
            if($bill->meter->status=='inactive'){
                $deactive_on = Carbon::parse($bill->meter->deactivate_date );
                $bill_created_on = Carbon::parse($bill->created_at );
                $result = $deactive_on->lt($bill_created_on);  //meter deactive is less then bill created date i.e bill generated after meetr deactive
            //    dd($deactive_on);
                if($result){
                    if($bill->paid_status=='unpaid'){
                    $unpaiadbill++;  
                    }                  
                   
                }

                $billdata .=' <span>'.$bill->consume_unit.'</span> '.'' .'/';  
                    $totalunit += $bill->consume_unit;
                 
            }
            else{
              
                $connection['avgtotal']=$bill->total; 
           
            }
            $alltotal = $bill->total;
            $connection['credit'] = $connection->credit;
            $connection['debit'] = $connection->debit;
            if($connection->bill->paid_status=='paid'){
                $paidon = Carbon::parse($connection->bill->paid_date );
                $chdate =  date('Y-m-d H:i:s', strtotime($connection->bill->created_at. " + 15 days"));
                $billon = Carbon::parse($chdate);
                $check = $paidon->gt($billon);
                // dd($chdate);
                if($check){                    
                $connection['latepayment'] =$connection->bill->total ;
                $connection['subtotal']=0;   
   
                }else{
                    $connection['subtotal']=0;   
                }
            }else{
                $connection['subtotal']=$alltotal;

            }

            //   if($bill->paid_status=='unpaid'){
            //    $prevCapital += $bill->bill_total;
            //   }

            }
            // Foreach Ends HERE
            // $connection['subtotal']=$alltotal; 
            // if(isset($connection->bill->remaning_amount)){
            //     $prevCapital += $connection->bill->remaning_amount;
    
            // } 
            
                $prevCapital = $connection->capital;
    
               
            $connection['totalbill']=$bills->count();
            $connection['unpaidbill']=$unpaiadbill;
            $connection['prevCapital'] =$prevCapital;
                // if($unpaiadbill==1){
                // $avgtotal = $totalunit*9;                    
                // $connection['avgtotal']=$avgtotal;
                // $connection['avgunit']=$totalunit;
                // $connection['ttlunit']= $totalunit;

                // }elseif($unpaiadbill==2){
                   
                //     $avgunit = $totalunit/2;
                //     $avgtotal = $avgunit*9;
                //     $connection['avgunit']=$avgunit;
                //     $connection['avgtotal']=$avgtotal;
                //     $connection['ttlunit']= $avgunit;

                // }
                $connection['avgtotal']=0;

                if($bills->count()>=3 && $unpaiadbill < 2){
                    $mybills =   CreateBill::where('connections_id',$connection->id)->orderBy('id', 'DESC')->take(3)->get();
                    $mytotalunit=0;
                    $billdata='';
                    foreach($mybills as $mybill){
                        $mytotalunit +=$mybill->consume_unit; 
                        $billdata .=' <span>'.$mybill->consume_unit.'</span> '.'' .'/';  
                    $totalunit += $bill->consume_unit;   
                    }
                    
                    $avgunit = round($mytotalunit/3);
                        $avgtotal = $avgunit*$connection->conn_type->charge;
                        $connection['avgunit']=$avgunit;
                        $connection['avgtotal']=$avgtotal;
                        $connection['ttlunit']= $avgunit;   
                }
                elseif($unpaiadbill==2){
                    $mybills =   CreateBill::where('connections_id',$connection->id)->orderBy('id', 'DESC')->take(3)->get();
                    $mytotalunit=0;
                    $billdata='';
                    $connection['aftertext']='';
                    foreach($mybills as $mybill){
                        $mytotalunit +=$mybill->consume_unit; 
                        $billdata .=' <span>'.$mybill->consume_unit.'</span> '.'' .'/';  
                        $connection['aftertext'] .=' <span>'.$mybill->bill_total.'</span> '.'' .'/';
                    $plenty[] = $mybill->bill_total;
                    }
                    $threemonthtotal=0;
                    $threemonth = $mytotalunit*$connection->conn_type->charge;
                    // dd($threemonth);
                    $avgunit = round($mytotalunit/3);
                    // dd($totalunit);
                    $threemonthtotal = round((($plenty[0]+$plenty[1]) / 100) *50 ,2);
                    $connection['avgunit']=$avgunit;
                    $connection['avgtotal']=$threemonth;
                    $connection['ttm']= $threemonthtotal;

                }
                elseif($unpaiadbill==3 || $unpaiadbill>=4){
                    //  dd("hello");
                    $threemonthtotal=0;
                    $mybills =   CreateBill::where('connections_id',$connection->id)->orderBy('id', 'DESC')->take(3)->get();
                    $mytotalunit=0;
                    $billdata='';
                    $threemonth=0;
                    $connection['aftertext']='';
                    foreach($mybills as $mybill){
                        $mytotalunit +=$mybill->consume_unit; 
                        $billdata .=' <span>'.$mybill->consume_unit.'</span> '.'' .'/';  
                        $connection['aftertext'] .=' <span>'.$mybill->bill_total.'</span> '.'' .'/';
                        $threemonth +=$mybill->bill_total; 
                        $plenty[] = $mybill->bill_total;


                    }

                     $threemonth = $mytotalunit*$connection->conn_type->charge;
                    $avgunit = round($mytotalunit/3);
                    $threemonthtotal = round(($threemonth / 100) *70,2);
                    $connection['avgunit']=$avgunit;
                    $connection['avgtotal']=$threemonth;
                    $connection['ttm']= $threemonthtotal ;

                }
                elseif($unpaiadbill==5){
                    $threemonthtotal=0;
                    $mybills =   CreateBill::where('connections_id',$connection->id)->orderBy('id', 'DESC')->take(3)->get();
                    $mytotalunit=0;
                    $billdata='';
                    $connection['aftertext']='';
                    foreach($mybills as $mybill){
                        $mytotalunit +=$mybill->consume_unit; 
                        $billdata .=' <span>'.$mybill->consume_unit.'</span> '.'' .'/';  
                        $connection['aftertext'] .=' <span>'.$mybill->bill_total.'</span> '.'' .'/';
                       $threemonthtotal +=$mybill->bill_total; 
                    }
                     $threemonth = $mytotalunit*$connection->conn_type->charge;
                    $avgunit = round($mytotalunit/3);

                    // $threemonthtotal = $threemonth;
                    $connection['avgunit']=$avgunit;
                    $connection['avgtotal']=$threemonth;
                    $connection['ttm']= $threemonthtotal;

                }
               
                if($unpaiadbill>5){
                    $mybills =   CreateBill::where('connections_id',$connection->id)->orderBy('id', 'DESC')->take(3)->get();
                    $mytotalunit=0;
                    $billdata='';
                    $connection['aftertext']='';
                    foreach($mybills as $mybill){
                        $mytotalunit +=$mybill->consume_unit; 
                        $billdata .=' <span>'.$mybill->consume_unit.'</span> '.'' .'/';  
                        $connection['aftertext'] .=' <span>'.$mybill->bill_total.'</span> '.'' .'/';
  
                    }
                    $avgunit = round($mytotalunit/3);

                    $connection['avgunit']=0;
                    $connection['avgtotal']=5000;
                    $connection['ttm']= 5000;
  
                }
                
            
        $connection['type']= $connection->connType($connection->connection_types_id);
        $connection['mbills'] = $billdata;
        echo  json_encode($connection);
    }
    public function getallconnection(Request $request)
    {
        
        //   dd($request);
        // DB::enableQueryLog();
        $conn = new Connection;
        if ($request->party_id != null) {
            $party = Party::find($request->party_id);
            $conn = Connection::where('parties_id',$party->id);

        }
        if ($request->conn_id != null) {
            $conn = Connection::where('id',$request->conn_id);
        }
        if ($request->conn_type != null) {
            $conn = Connection::orwhere('party_types_id',$request->conn_type);

        }

          if ($request->from_date != null) {
            $bills =  CreateBill::select('connections_id')->where('created_at', '>=', $request->from_date)->get();
            $conn = $conn->orWhereIn('id',$bills->toArray());
        }

        if ($request->to_date != null) {
            $bills = CreateBill::select('connections_id')->where('created_at','=<', $request->to_date)->get();
           $conn = $conn->orWhereIn('id',$bills->toArray());
        }
        // if ($request->paid_type != null) {
        //     $bills = CreateBill::select('connections_id')->where('created_at','=<', $request->to_date)->get();
        //    $conn = $conn->orWhereIn('id',$bills->toArray());
        // }
       
        if ($request->amount != null) {
            $conn =  $conn->orWhere('debit','>=', $request->amount);
            $conn = $conn->orWhere('credit','>=', $request->amount);

        }
        // if ($request->party_id != null) {
            $data =  $conn->get()->load('party');
            // $data = $conn->get()->load('custom_bills');

        // }else{
        //     $data = $conn->get();
        // }


// and then you can get query log

        echo  json_encode($data);
         exit;
      
    }
    public function getconnectionbyparty(Request $request)
    {
        //
        
        $connection = Party::select('id','name')->where('name','like','%'.$request->value.'%')->get()->toArray();
        
        echo  json_encode($connection);
    }

    public function getemailbyconn(Request $request)
    {
        //
        
        $connection = Customer::select('email','mobile')->where('party_id',$request->conn_id)->first()->toArray();
        
        echo  json_encode($connection);
    }

    public function getconnectionbyconn(Request $request)
    {
        //
        
        $connection = Connection::select('id','conn_no')->where('conn_no','like','%'.$request->value.'%')->get()->toArray();
        echo  json_encode($connection);
    }

    public function details( $connection)
    {
       $connection =  Connection::find($connection);
       $charges = ConnectionCharge::where('connections_id',$connection->id)->get();
      
       return view('connection.show',['connection'=>$connection,'charges'=>$charges]);
    //    dd($connection);
    }

    public function ledger()
    {
        //
        $connections = Connection::orderBy('id', 'DESC')->get();
        return view('connection.ledger',['connections'=>$connections]);
    }

}
