<?php

namespace App\Http\Controllers;

use App\Models\CustomBill;
use App\Models\Party;
use App\Models\Connection;
use App\Models\Meter;
use Illuminate\Http\Request;
use DB;
use App\Models\CreateBill;
use App\Models\Settlement;

use Illuminate\Support\Facades\Validator;
class PartyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $parties = Party::orderBy('id', 'DESC')->get();
        return view('party.list',['parties'=>$parties]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $partyTypes = DB::table('party_types')->get();
        $connectionTypes = DB::table('connection_types')->get();
        return view('party.add',['partyTypes'=>$partyTypes,'connectionTypes'=>$connectionTypes]);


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // dd($request);
        Validator::make($request->all(), [
            'name' => 'required|string|max:255|unique:parties,name',
            //  'email' => 'email|string|max:255',
            'party_types_id' => 'required',
            'address' => 'required',
            'connection_types_id' => 'required',
            'onname' => 'required',
            // 'onphone' => 'unique:connections,phone|digits:10',
            // 'onemail' => 'string|email|max:255|unique:connections,email',
            'onaddress' => 'required|',
            'meter_no' => 'required|unique:meters',
            'innital_meter_reading' => 'required',
            'installation_date' => 'required',
            ])->validate();

            $meter_test_copy='';
            $application_copy='';
            if($request->meter_test_copy)
            {
                $images = $request->file('meter_test_copy');
                $destinationPath = public_path('/meter_test_copy'); 
      
                 // $imageed = $request->file('pdffile')[$key];
                    $meter_test_copy = time().'.'.$images->getClientOriginalExtension(); 
                    $image->move($destinationPath, $meter_test_copy); 
      
            }
            if($request->application_copy)
            {
                $image = $request->file('application_copy');
                $destinationPath = public_path('/application_copy'); 
      
                 // $imageed = $request->file('pdffile')[$key];
                    $application_copy = time().'.'.$image->getClientOriginalExtension(); 
                    $image->move($destinationPath, $application_copy); 
      
            }
        
           $data=$request->all();
           $data['meter_test_copy'] = $meter_test_copy;
           $data['application_copy'] =$application_copy;
            session(['data' =>$data ]);

        // $partyid = Party::create([
        //     'name' => $request->name,
        //     'email' => $request->email,
        //     'party_types_id' => $request->party_types_id,
        //     'address' => $request->address,
        // ]);

        // $meterid = Meter::create([
        //     'meter_no' => $request->meter_no,
        //     'installation_date' => date('y-m-d h:i:s',strtotime($request->installation_date)),
        //     'party_types_id' => $request->party_types_id,
        //     'status' => $request->status,
        //     'innital_meter_reading' => $request->innital_meter_reading,
        //     'meter_test_copy' => $request->meter_test_copy,
        //     'appliaction_copy' => $request->application_copy,
        // ]);

        // $connid = Connection::create([
        //     'conn_no' => rand(999,99999),
        //     'name' => $request->onname,
        //     'phone' => $request->onphone,
        //     'connection_types_id' => $request->connection_types_id,
        //     'parties_id' => $partyid->id,
        //     'meters_id' => $meterid->id,
        //     'email' => $request->onemail,
        //     'address' => $request->onaddress,
        // ]);

        // Meter::where('id', $meterid->id)
        // ->update(['connections_id' => $connid->id]);
        // //  return redirect()->route('party');
        return redirect()->route('charge');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Party  $party
     * @return \Illuminate\Http\Response
     */
    public function show(Party $party)
    {
        // dd($party);
    return view('party.show',['party'=>$party]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Party  $party
     * @return \Illuminate\Http\Response
     */
    public function edit(Party $party)
    {
        $partyTypes = DB::table('party_types')->get();
        $connectionTypes = DB::table('connection_types')->get();
        return view('party.edit',['party'=>$party,'partyTypes'=>$partyTypes,'connectionTypes'=>$connectionTypes]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Party  $party
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Party $party)
    {
        $request->validate([
            'name' => "required|string|max:255|unique:parties,name,$party->id,id",
            // 'email' => "required|string|email|max:255|unique:parties,email,$party->id,id",
            // 'email' => "required|string|email|max:255",
            // 'party_types_id' => 'required',
            'address' => 'required',
            ]);
            $party->name = $request->name;
            $party->email = $request->email;
            // $party->party_types_id = $request->party_types_id;
            $party->address = $request->address;
            $party->save();
            // return redirect()->route('party');

            return redirect()->route('party')->with('status', 'Party status updated succesfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Party  $party
     * @return \Illuminate\Http\Response
     */
    public function destroy(Party $party)
    {
        //
    }


    public function getdictonarydata()
    {
       $data =  $_GET['data'];
       $mydata = DB::table('dictonary')
       ->where('hword', 'like', "$data%")
       ->get()->toArray();

       return response()->json(['status'=>200,'data'=>$mydata], 200);       


    }

    public function getalldata()
    {
       $mydata = DB::table('dictonary')
       ->get()->toArray();

       return response()->json(['status'=>200,'data'=>$mydata], 200);       


    }

    public function settlement(Request $request){
        // dd($request);
      $party =   Party::find($request->id);

// $debit = 0;
// $credit = 0;
            foreach($party->conn as $connection){
              $myconn = Connection::find($connection->id);
            //   $debit += $myconn->debit;
            //   $credit += $myconn->credit;

              $myconn->debit = 0;
               $myconn->credit=0;
              $myconn->save();
               
            }
            foreach($party->conn as $connection){
            $bill_datas = CustomBill::where('connection_id',$connection->id)->where('paid_status','unpaid')->get();
            // dd($bill_datas);
            foreach($bill_datas as $bill_data){
            $mydata = CustomBill::find($bill_data->id);
            $mydata->pay_by = $request->paid_by;
            $mydata->pay_amount = $mydata->total;
            // $bill_data->debit = $request->debit;
            // $bill_data->credit = $request->credit;
            $mydata->status = 'payment clear by settlement';
            $mydata->check_no = $request->check_no;
            $mydata->paid_date = date('Y-m-d H:i:s');
            $mydata->paid_status = 'paid';
            $mydata->rem_amount = 0;
            $mydata->save();
            }
            }
            foreach($party->conn as $cuspay){
                $manualbills = CreateBill::where('connections_id',$cuspay->id)->where('paid_status','unpaid')->get();
                foreach($manualbills as $manualbill){
                    $myoldbill = CreateBill::find($manualbill->id);

                    $myoldbill->pay_by ="settlement";
                    $myoldbill->paid_amount = $myoldbill->total;
                    //    $bill_data->debit = $request->debit;
                    //    $bill_data->credit = $request->credit;
                    $myoldbill->status = 'paid by settlement';
                    $myoldbill->check_no = $request->check_no;
                    $myoldbill->paid_date = date('Y-m-d H:i:s');
                    $myoldbill->paid_status = 'paid';
                    // $manualbill->remaning_amount =0;
                    // $bill_data->pre_bill_due=$rem;
                    $myoldbill->bill_to_pay=0;
                    $myoldbill->save();
                    }
            }

            $set = Settlement::create([
                'party_id' => $party->id,
                'paid_by' => $request->paid_by,
                'cheque_no' => $request->cheque_no,
                'amount' => $request->amount,
                'debit' => $request->debit,
                'credit' => $request->credit,
            ]);
            return redirect()->route('party')->with('status', 'Settlement done succesfully..');
            // Settlement::where('id', $set->id)
            // ->update(['debit' => $debit,'credit'=>$credit]);
    }

public function settlementdetail($id){
   $party =  Party::find($id);
   $settlements = Settlement::where('party_id',$id)->get();
   return view('party.settlement',['party'=>$party,'settlements'=>$settlements]);

}

}
