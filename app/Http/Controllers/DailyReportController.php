<?php

namespace App\Http\Controllers;

use App\Models\DailyReport;
use Illuminate\Http\Request;
use App\Models\CreateBill;
use Auth;

class DailyReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dailyreports = DailyReport::orderBy('id', 'DESC')->get();

        $payments['cheque'] = CreateBill::where('paid_status','paid')->whereDate('paid_date', now())->where('pay_by','cheque')->sum('paid_amount');
        $payments['cash'] = CreateBill::where('paid_status','paid')->whereDate('paid_date', now())->where('pay_by','cash')->sum('paid_amount');
        $payments['credit'] = CreateBill::where('paid_status','paid')->whereDate('paid_date', now())->where('pay_by','credit')->sum('paid_amount');
        $payments['online'] = CreateBill::where('paid_status','paid')->whereDate('paid_date', now())->where('pay_by','online')->sum('paid_amount');
        $payments['settlement'] = CreateBill::where('paid_status','paid')->whereDate('paid_date', now())->where('pay_by','settlement')->sum('paid_amount');
        $payments['today'] = DailyReport::whereDate('created_at', now())->first('id');

        return view('expencesaccount.dailyreport',['payments'=>$payments,'dailyreports'=>$dailyreports]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $payments['cheque'] = CreateBill::where('paid_status','paid')->whereDate('paid_date', now())->where('pay_by','cheque')->sum('paid_amount');
        $payments['cash'] = CreateBill::where('paid_status','paid')->whereDate('paid_date', now())->where('pay_by','cash')->sum('paid_amount');
        $payments['credit'] = CreateBill::where('paid_status','paid')->whereDate('paid_date', now())->where('pay_by','credit')->sum('paid_amount');
        $payments['online'] = CreateBill::where('paid_status','paid')->whereDate('paid_date', now())->where('pay_by','online')->sum('paid_amount');
        $payments['settlement'] = CreateBill::where('paid_status','paid')->whereDate('paid_date', now())->where('pay_by','settlement')->sum('paid_amount');
      $dailyReport =   new DailyReport;
      $dailyReport->bank = $payments['cheque'];
      $dailyReport->cash = $payments['cash'];
      $dailyReport->online = $payments['online'];
      $dailyReport->credit = $payments['credit'];
      $dailyReport->settlement = $payments['settlement'];
      $dailyReport->amount = array_sum($payments);
      $dailyReport->user_id = Auth::user()->id;
      $dailyReport->verified = date('Y-m-d H:i:s');
      $dailyReport->save();
      return redirect()->back()->with('status', 'Data save succesfully..');


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DailyReport  $dailyReport
     * @return \Illuminate\Http\Response
     */
    public function show(DailyReport $dailyReport)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\DailyReport  $dailyReport
     * @return \Illuminate\Http\Response
     */
    public function edit(DailyReport $dailyReport)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DailyReport  $dailyReport
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DailyReport $dailyReport)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DailyReport  $dailyReport
     * @return \Illuminate\Http\Response
     */
    public function destroy(DailyReport $dailyReport)
    {
        //
    }
}
