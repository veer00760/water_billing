<?php

namespace App\Http\Controllers;
use App\Mail\SendBill;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Models\Otp;
use App\Mail\VerifyMail;
use App\Models\Customer;
use Illuminate\Support\Facades\Hash;
use App\Models\Connection;
use App\Models\CreateBill;
use App\Mail\PaySuccess;
use App\Mail\ApplyNoc;
use App\Models\Party;

class EmailController extends Controller
{
    public function sendmail(){

    $mobile = '8955465824';

   $res = $this->paysuccess($mobile);

    dd($res);
        $mobile = '06351118584';
        $otp = '1234';
       $res = $this->sendsms($mobile,$otp);           
dd($res);
// $data = CreateBill::find(2); 
// $mydata=[];
// $mydata['total'] =$data->bill_to_pay;
// $mydata['month'] =$data->billing_month;
// $mydata['conn'] ='12345';
// $mydata['name'] ='Virendra Singh';
// $mydata['unit'] =$data->consume_unit;
// $mydata['status'] ='DUE';
// $mydata['url'] ='www.viaindustrialwaterservices.in';

// $mydata['total'] =$data->bill_to_pay;
// $mydata['paydate'] =date('d-F-Y',strtotime($data->created_at."+ 15 days"));

//      Mail::to('its.virendra.singh.ecb@gmail.com')->send(new SendBill($mydata));
$mydata=[];
$mydata['status'] ='success';
$mydata['payid'] ='123456';
$mydata['amt'] ='588';
Mail::to('its.virendra.singh.ecb@gmail.com')->send(new PaySuccess($mydata));

// $mydata=[];
// $mydata['message'] ='You have applied for Noc your reg No is :12345';
// Mail::to('its.virendra.singh.ecb@gmail.com')->send(new ApplyNoc($mydata));

    }

    public function verifyemail(){
        $otp['otp'] = rand(1000,9999);
        $regdata = session('regdata');
        $email = $regdata['email'];
        if(isset($email)){
        Otp::create([
        'email'=>$email,
        'otp'=>$otp['otp'],
        ]);
        Mail::to($email)->send(new VerifyMail($otp));

        return view('customer.verifyemail',['otp'=>$otp['otp']]);
        }else{
            return redirect()->route('login');

        }
        }

        public function confirmemail(Request $request){
            $regdata = session('regdata');
            $email = $regdata['email'];
           $otp =  Otp::where('email',$email)->orderBy('id','DESC')->first();
        //   dd($otp);
           if($request->otp == $otp->otp){
               
            return redirect()->route('verifymobile');

           } else{
            return redirect()->route('verifyemail');
   
           }
        }

        public function verifymobile(){
            $otp = rand(1000,9999);
            $regdata = session('regdata');
            $mobile = $regdata['mobile'];
            if(isset($mobile)){

            Otp::create([
            'mobile'=>$mobile,
            'otp'=>$otp,
            ]);
            $res = $this->sendsms($mobile,$otp);           
 
            return view('customer.verifymobile',['otp'=>$otp]);
            }else{
                return redirect()->route('verifyemail');
    
            }
            }


            public function confirmmobile(Request $request){
              $regdata = session('regdata');
              $mobile = $regdata['mobile'];
              $email = $regdata['email'];

             $otp =  Otp::where('mobile',$mobile)->orderBy('id','DESC')->first();
            // dd($regdata);
             if($request->otp == $otp->otp){
               if(isset( $regdata['party'])){
                   $party =  $regdata['party'];
                //    $regdata['mobile'];
                   Connection::where('parties_id',$party )->update(['email' => $email,'phone'=>$mobile]);
                   Party::where('id',$party )->update(['email' =>$email]);

               }  else{
                   $party=null;
               }
               if(isset( $regdata['connection_id'])){
                $conn_id =  $regdata['connection_id'];
                Connection::where('id',$conn_id )->update(['email' => $email,'phone'=>$mobile]);

            }  else{
                $conn_id=null;
            }
            $customer = Customer::create([
                        'mobile' => $regdata['mobile'],
                        'email' => $regdata['email'],
                        'password' => Hash::make($regdata['password']),
                        'party_id' => $party,
                        'connection_id' => $conn_id,

                    ]);

                    session()->forget('regdata');

                    // return redirect('/login');
                    return redirect()->route('login')->with('status', 'Registration has been succesfully done ..');

               } else{
                return redirect()->route('verifyemail');
       
               }
            }



public function paysuccess($mobile){
    $apiKey = urlencode('OGJiNWQ2ZDhhOTI3OTRjNTllZDUwNGQ5YjUwYWU4MTA=');
    $numbers = $mobile;
    $sender = urlencode('VIAWSe');
    $amount = '2260';
    $paydate='22-06';
    $consume_unit = '065';
    $month='May-2021';
    $unit='222';
    $msg ="V.no: $unit, Bill month: $month, Bill amount: $amount, unit consumed: $consume_unit, payby: $paydate, Regards, VIA";
     $message = rawurlencode($msg);
    $data = array('apikey' => $apiKey, 'numbers' => $numbers, "sender" => $sender, "message" => $message);
 	// Send the POST request with cURL
	$ch = curl_init('https://api.textlocal.in/send/');
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$response = curl_exec($ch);
	curl_close($ch);
    return $response;
    
   }

            public function sendsms($mobile,$otp){
	// Account details
	$apiKey = urlencode('OGJiNWQ2ZDhhOTI3OTRjNTllZDUwNGQ5YjUwYWU4MTA=');
	
	// Message details
	// $numbers = array(918123456789, 918987654321);
    $numbers = $mobile;
    // $month='Jan';
    // $connection='12345';
    // $party='Virendra';
    // $unit='123';
    // $amount='122';
    // $status='unpaid';
    // $site='admin.valsadindustrialwaterservices.in';
     $sender = urlencode('VIAWSe');
	 $message = rawurlencode("Dear Customer, your OTP for verification is $otp, From VIA.");
 	//  $message = rawurlencode("Bill for the month : $month,%nConnection no.: $connection,%nCustomer Name :$party,%nConsumed Unit : $unit,%nTotal Bill to pay : $amount,%nPayment Status : $status,%nFor online payment visit : $site%nFrom VIA water services.");

 
	// Prepare data for POST request
	$data = array('apikey' => $apiKey, 'numbers' => $numbers, "sender" => $sender, "message" => $message);
 
	// Send the POST request with cURL
	$ch = curl_init('https://api.textlocal.in/send/');
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$response = curl_exec($ch);
	curl_close($ch);
    return $response;
            }

    public function resend(Request $request){
       if($request->type=='email'){
         $this->verifyemail();  
       }

       if($request->type=='mobile'){
        $this->verifymobile();  
      }
      
    }

}
