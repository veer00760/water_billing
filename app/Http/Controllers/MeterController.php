<?php

namespace App\Http\Controllers;

use App\Models\Meter;
use App\Models\Connection;
use Illuminate\Http\Request;
use DB;
class MeterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $meters = Meter::orderBy('id', 'DESC')->get();
        return view('meter.list',['meters'=>$meters]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
      
        $connections = Connection::orderBy('id', 'DESC')->get();
        return view('meter.add',['connections'=>$connections]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'conn_no' => 'required',
            'meter_no' => 'required',
            'innital_meter_reading' => 'required',
            'installation_date' => 'required',
            ]);
      $meterdata = Connection::where('id',$request->conn_no)->first();
      if(empty( $meterdata)) {
        return back()->with('error', 'error');

      }    
      $meter_test_copy='';
      $application_copy='';
      if($request->meter_test_copy)
      {
          $images = $request->file('meter_test_copy');
          $destinationPath = public_path('/meter_test_copy'); 

           // $imageed = $request->file('pdffile')[$key];
              $meter_test_copy = time().'.'.$images->getClientOriginalExtension(); 
              $image->move($destinationPath, $meter_test_copy); 

      }
      if($request->application_copy)
      {
          $image = $request->file('application_copy');
          $destinationPath = public_path('/application_copy'); 

           // $imageed = $request->file('pdffile')[$key];
              $application_copy = time().'.'.$image->getClientOriginalExtension(); 
              $image->move($destinationPath, $application_copy); 

      }
  
      $meterid = Meter::create([
                'connections_id' => $request->conn_no,
                'meter_no' => $request->meter_no,
                'installation_date' => date('y-m-d h:i:s',strtotime($request->installation_date)),
                'party_types_id' => $request->party_types_id,
                'status' => $request->status,
                'innital_meter_reading' => $request->innital_meter_reading,
                'current_unit' => $request->innital_meter_reading,
                'meter_test_copy' => $meter_test_copy,
                'appliaction_copy' => $application_copy,
            ]);

            return redirect()->route('meter')->with('status', 'Meter added  succesfully');
            // return back()->with('status', 'success');
    
            // return redirect()->route('meter');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Meter  $meter
     * @return \Illuminate\Http\Response
     */
    public function show( $meter)
    {
        // $unit = $_GET['closing_unit'];
        $unit = Meter::find($meter);
        Meter::where('id', $meter)
        ->update(['status' => 'inactive','closing_unit'=>$unit->current_unit,'deactivate_date'=>date('Y-m-d H:i:s')]);
        return redirect()->route('meter');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Meter  $meter
     * @return \Illuminate\Http\Response
     */
    public function edit(Meter $meter)
    {
        $connections = Connection::all();
        return view('meter.edit',['meter'=>$meter,'connections'=>$connections]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Meter  $meter
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Meter $meter)
    {
        $request->validate([
            'conn_no' => 'required',
            'meter_no' => 'required',
            'innital_meter_reading' => 'required',
            'installation_date' => 'required',
            ]);

            // $meter->connections_id = $request->conn_no;
            $meter->meter_no = $request->meter_no;
            $meter->installation_date =date('y-m-d h:i:s',strtotime($request->installation_date));
            $meter->status =$request->status;
            $meter->meter_test_copy =$request->meter_test_copy;
            $meter->appliaction_copy =$request->application_copy;
            $meter->innital_meter_reading =$request->innital_meter_reading;
            $meter->save();
            return redirect()->route('meter')->with('status', 'Meter  updated succesfully');

               
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Meter  $meter
     * @return \Illuminate\Http\Response
     */
    public function destroy(Meter $meter)
    {
        //
    }
}
