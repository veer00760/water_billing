<?php

namespace App\Http\Controllers;

use App\Models\ExpencesBill;
use App\Models\ExpencesAccount;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\CreateBill;
use App\Models\CustomBill;
use App\Models\Connection;
use App\Models\ConnectionCharge;

class ExpencesBillController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'bill_no' => 'required',
            'amount' => 'required',
            'paid_by' => 'required',
            'description' => 'required',
            ])->validate();

            $accountid = ExpencesBill::create([
                'bill_no' => $request->bill_no,
                'amount' => $request->amount,
                'paid_by' => $request->paid_by,
                'description' => $request->description,
                'check_no' => $request->check_no,
                'expances_account_id'=> $request->expances_account_id,
                'user_id'=>Auth::user()->id,
            ]);

            return redirect()->route('expensesaccount')->with('status', 'Bill added  succesfully');
       
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ExpencesBill  $expencesBill
     * @return \Illuminate\Http\Response
     */
    public function show( $expencesBill)
    {
        //expances_account_id
        return view('expencesaccount.addbill',['expances_account_id'=>$expencesBill]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ExpencesBill  $expencesBill
     * @return \Illuminate\Http\Response
     */
    public function edit($expencesBill)
    {
      $expencesBill =   ExpencesBill::find($expencesBill);
        return view('expencesaccount.billedit',['expencesBill'=>$expencesBill]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ExpencesBill  $expencesBill
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $expencesBill)
    {
        Validator::make($request->all(), [
            'bill_no' => 'required',
            'amount' => 'required',
            'paid_by' => 'required',
            'description' => 'required',
            ])->validate();

        $expencesBill =   ExpencesBill::find($expencesBill);
        $expencesBill->bill_no = $request->bill_no;
        $expencesBill->amount = $request->amount;
        $expencesBill->paid_by =$request->paid_by;
        $expencesBill->description =$request->description;
        $expencesBill->save();
        return redirect()->route('expencesbilllist',$expencesBill->id)->with('status', 'Bill updated  succesfully');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ExpencesBill  $expencesBill
     * @return \Illuminate\Http\Response
     */
    public function destroy(ExpencesBill $expencesBill)
    {
        //
    }

        /**
     * Display the specified resource.
     *
     * @param  \App\Models\ExpencesBill  $expencesBill
     * @return \Illuminate\Http\Response
     */
    public function list( $expencesBill)
    {
        $billdatas = ExpencesBill::where('expances_account_id',$expencesBill)->get();
        $account = ExpencesAccount::find($expencesBill);
        $today = ExpencesBill::where('expances_account_id',$expencesBill)->whereDate('created_at', Carbon::today())->sum('amount');
        $month = ExpencesBill::where('expances_account_id',$expencesBill)->whereMonth('created_at', date('m'))->whereYear('created_at', date('Y'))->sum('amount');
        $year = ExpencesBill::where('expances_account_id',$expencesBill)->whereYear('created_at', date('Y'))->sum('amount');
        return view('expencesaccount.billlist',['year'=>$year,'month'=>$month,'today'=>$today,'account'=>$account,'billdatas'=>$billdatas]);

    }

   public function reports(){
    return view('expencesaccount.reports');

   }

   public function viewreport(Request $request){
    // $datas = CreateBill::whereMonth('created_at', date('m'))->whereYear('created_at', date('Y'))
    $datas = new CreateBill;
    $conn_charge2 = new ConnectionCharge;
    $conn_charge1 = new ConnectionCharge;
    if ($request->month != null) {
      $datas =  $datas->whereMonth('created_at', $request->month);
      $conn_charge2= $conn_charge2->whereMonth('created_at', $request->month);
      $conn_charge1=$conn_charge1->whereMonth('created_at', $request->month);
    }
     $datas = $datas->whereYear('created_at', $request->year);
     $newconn_charge = $conn_charge1->whereYear('created_at', $request->year);
     $renewconn_charge = $conn_charge2->whereYear('created_at', $request->year);

     $newconn =  $newconn_charge->where('type','new')->sum(\DB::raw('connection_charge + road_crossing + minimum_charge'));
    $renew =  $renewconn_charge->where('type','renew')->sum(\DB::raw('connection_charge + road_crossing + minimum_charge'));
 
    $datas = $datas->orderBy('id','DESC')
     ->get();
    $myarray = [];
    $unpaid =0;
    $paid=0;
    $billtotal = 0;
    $cash = 0;
    $bank = 0;
    $online = 0;
    $bill_credit=0;
    $myexp = [];
    $monthwise = [];
    $html = '';
    $sandeb=0;
    $sancre=0;
    $rec_payment=0;
  $monthwiseliquid=[];
  $liquiddatabymonth=[];
  $ext_payment=0;
  $bill_for_month=0;
  $operational_charges=0;
  $unpaid_bill_to_pay=0;
  $cus_month=[];
  $credit=0;
    // dd($monthwise['04']);
    if($request->month == null){
      if($request->year == date('Y')){
        $sandeb =Connection::where('debit','>',0)->whereYear('created_at', $request->year)
        ->sum('debit');
        $sancre =Connection::where('credit','>',0)->whereYear('created_at', $request->year)
        ->sum('credit');
       
      } else{
        $sandeb = new CreateBill;
        $sandeb = $sandeb->whereYear('created_at', date("Y",strtotime("-1 year")));
        $sandeb = $sandeb->whereMonth('created_at', '12');
        $sandeb = $sandeb->sum('bill_debit');

        $sancre = new CreateBill;
        $sancre = $sancre->whereYear('created_at', date("Y",strtotime("-1 year")));
        $sancre = $sancre->whereMonth('created_at', '12');
        $sancre = $sancre->sum('bill_credit');
    
       }

    } else{
      $sandeb =CreateBill::where('bill_debit','>',0)
      ->where('paid_status','paid')->whereMonth('created_at', $request->month)
   ->sum('bill_debit');
   $sancre =CreateBill::where('bill_credit','>',0)
   ->where('paid_status','paid')->whereMonth('created_at', $request->month)
   ->sum('bill_credit'); 
    }
  //  dd($datas);
    foreach($datas as $data){
      
        if($data->paid_amount==null){
           if(!in_array($data->connections_id,$myarray)){
               $unpaid += $data->total;
               $myarray[] = $data->connections_id;
               $unpaid_bill_to_pay += $data->bill_to_pay;

           }
        } else{
          if(date('m',strtotime($data->created_at)) < date('m',strtotime($data->paid_date)))
          {
            $unpaid_bill_to_pay += $data->bill_to_pay;

          }
            // $paid += $data->total;
             $paid += $data->total;
             $bill_credit += $data->bill_credit;
             $rec_payment += $data->total-$data->pre_bill_due;
            // $ext_payment+=$data->bill_credit;
        }
        $bill_for_month += $data->bill_total;
        if($data->plenty!=null){
          $plenty = $data->plenty;
        }else{
          $plenty =0 ;
        }
        if($data->meter_damage_plenty!=null){
          $meter_damage_plenty = $data->meter_damage_plenty;
        }else{
          $meter_damage_plenty =0 ;
        }
        
        $operational_charges += $data->intrest +$data->late_payment+ $plenty+ $meter_damage_plenty;

        // if($data->pay_by=='cash'){
        //   if($data->paid_amount){
        //    $cash +=$data->paid_amount;
        //   }else{
        //     $cash+=0;
        //   }
        // }
        // if($data->pay_by=='cheque'){
        //   if($data->paid_amount){
        //     $bank +=$data->paid_amount;
        //    }else{
        //     $bank+=0;
        //    }
          
        //  }
        //  if($data->pay_by=='credit'){
        //   if($data->paid_amount){
        //     $credit +=$data->paid_amount;
        //    }else{
        //     $credit+=0;
        //    }
          
        //  }

        //  if($data->pay_by=='online'){
        //   if($data->paid_amount){
        //     $online +=$data->paid_amount;
        //    }else{
        //     $online+=0;
        //    }
           
        //  }

    }
  $ddd = $bill_for_month +$operational_charges;
 
    // $exps = ExpencesBill::whereMonth('created_at', date('m'))->whereYear('created_at', date('Y'))
  //  ->get();
  $exps = new ExpencesBill;
   if ($request->month != null) {
    $exps =  $exps->whereMonth('created_at', $request->month);
   }
   $exps = $exps->whereYear('created_at', $request->year);

   $exps = $exps->orderBy('id','DESC')
   ->get();
 

  //  $custom = CustomBill::whereMonth('created_at', date('m'))->whereYear('created_at', date('Y'))
  //  ->sum('total');
  $custom = new CustomBill;
   if ($request->month != null) {
    $custom =  $custom->whereMonth('created_at', $request->month);
   
  }
   $custom = $custom->whereYear('created_at', $request->year);

   $custom = $custom->sum('total');


   if ($request->month != null) {

    $bill_by_month = CreateBill::select(
     DB::raw('sum(bill_total) as sums'), 
      DB::raw('sum(bill_to_pay) as total'), 
     DB::raw("DATE_FORMAT(created_at,'%m') as month")
 )
 ->whereMonth('created_at', $request->month)
 ->groupBy('month')
 ->orderBy('created_at', 'ASC')
 ->get();
 

 $cash = CreateBill::whereMonth('paid_date', $request->month)->whereYear('paid_date', $request->year)
 ->where('paid_amount','!=',null)->where('pay_by','cash')
 ->sum('paid_amount');

 $bank = CreateBill::whereMonth('paid_date', $request->month)->whereYear('paid_date', $request->year)
 ->where('paid_amount','!=',null)->where('pay_by','cheque')
 ->sum('paid_amount');

 $online = CreateBill::whereMonth('paid_date', $request->month)->whereYear('paid_date', $request->year)
 ->where('paid_amount','!=',null)->where('pay_by','online')
 ->sum('paid_amount');

 $credit = CreateBill::whereMonth('paid_date', $request->month)->whereYear('paid_date', $request->year)
 ->where('paid_amount','!=',null)->where('pay_by','credit')
 ->sum('paid_amount');

    }
   if ($request->month == null) {

    $cash = CreateBill::whereYear('paid_date', $request->year)
    ->where('paid_amount','!=',null)->where('pay_by','cash')
    ->sum('paid_amount');
   
    $bank = CreateBill::whereYear('paid_date', $request->year)
    ->where('paid_amount','!=',null)->where('pay_by','cheque')
    ->sum('paid_amount');
   
    $online = CreateBill::whereYear('paid_date', $request->year)
    ->where('paid_amount','!=',null)->where('pay_by','online')
    ->sum('paid_amount');
   
    $credit = CreateBill::whereYear('paid_date', $request->year)
    ->where('paid_amount','!=',null)->where('pay_by','credit')
    ->sum('paid_amount');

    $bill_by_month = CreateBill::select(
     DB::raw('sum(bill_total) as sums'), 
    //  DB::raw('sum(bill_to_pay) as sums'), 
     DB::raw("DATE_FORMAT(created_at,'%m') as month")
 )
 ->whereYear('created_at', $request->year)
 ->groupBy('month')
 ->orderBy('created_at', 'ASC')
 ->get();


 $cus_by_months = CustomBill::select(
  DB::raw('sum(pay_amount) as sums'), 
  // DB::raw('sum(pre_bill_due) as predue'), 
  DB::raw("DATE_FORMAT(created_at,'%m') as month")
)
->whereYear('created_at', $request->year)
->groupBy('month')
->orderBy('created_at', 'ASC')
->get();

foreach($cus_by_months as $cus_by_month){
  $cus_month[$cus_by_month->month] = $cus_by_month->sums;
}

    }

  

    foreach($bill_by_month as $mymonth){
      $monthwise[$mymonth->month] = $mymonth->sums;
      $billtotal = $mymonth->total;

    }

// dd($bill_by_month);
   if ($request->month == null) {
//    $custommonth = CustomBill::
//    select(
//     DB::raw('sum(total) as sums'), 
//     DB::raw("DATE_FORMAT(created_at,'%m') as month")
// )->
//    whereYear('created_at', $request->year)
//    ->groupBy('month')
//    ->orderBy('created_at', 'ASC')
//    ->get();
//    foreach($custommonth as $mymonths){
//      if(in_array($mymonths->month,$bill_by_month)){
//        $monthwise[$mymonths->month]=$monthwise[$mymonths->month]+$mymonths->month;
//      }else{
//       $monthwise[$mymonths->month]=$mymonth->month;
 
//      }
//    }

   $liquidbymonth = ExpencesBill::
   select(
    DB::raw('sum(amount) as sums'), 
    DB::raw("DATE_FORMAT(created_at,'%m') as month")
)->
   whereYear('created_at', $request->year)
   ->groupBy('month')
   ->orderBy('created_at', 'ASC')
   ->get();

   foreach($liquidbymonth as $monthsliq){
    $monthwiseliquid[$monthsliq->month] = $monthsliq->sums;
  }

  $liquiddata = CreateBill::
  select(
   DB::raw('sum(paid_amount) as sums'), 
   DB::raw("DATE_FORMAT(created_at,'%m') as month")
)->where('paid_status','paid')
->whereYear('created_at', $request->year)
  ->groupBy('month')
  ->orderBy('created_at', 'ASC')
  ->get();

  foreach($liquiddata as $mydatas){
   $liquiddatabymonth[$mydatas->month] = $mydatas->sums;
 }



  }
    // dd($monthwiseliquid);

      foreach($exps as $exp){
        $total = ExpencesBill::where('expances_account_id',$exp->expances_account_id)
            ->sum('amount');
      $myexp[$exp->acc->name] = $total;

      }
    //  $billtotal = $unpaid+$paid;
   
    if(isset($request->month)){
    //   $month = date('m', mktime(0, 0, 0, $request->month, 10)); // March
    //   // dd($month);
    // $bill_for_month = $monthwise[$month];
    
    $sandeb =$unpaid_bill_to_pay+$sandeb;
    } else{
      $billtotal = array_sum($monthwise);
  
    }
  
if($request->month != null){
  $oftheyear= $cash+$bank+$online;
  $twopercentage = (2 / 100) * $online;
  $eighteen = (18 / 100) * $twopercentage;
  $ttlper = $twopercentage+$eighteen;
  $total_liquid = $cash+$bank+$online+$credit;

  $html = '
  <div class="row mb-3"><div class="col-md-3"><button class="btn btn-primary" id="printme">Print Me</button></div>
          <div class="col-md-5 text-center "></div>
          <div class="col-md-4 text-right"></div>
          </div>'; 
  $html .= '<div class="border p-3" id="printdiv ">
    <div class="row mb-3"><div class="col-md-4"><img style="max-width: 80%;" src="'.asset('/img/logo_via.svg').'" class="img img-responsive"/></div>
            <div class="col-md-4 text-center "><b>Rcl-7/70 G.I.D.C. Colony,Gundlav-396035,Valsad,Gujarat,India</b></div>
            <div class="col-md-4 text-right"><sapn><b>Date:</b></span>'.date("d-m-Y h:i:s").'</div></div>';
            $html .='<div class="row mb-2"><div class="col-md-3"></div>
            <div class="col-md-6 text-center"><h4 style="border-bottom: 1px solid black;">Billings & P/L Statement Report</h4></div>
            <div class="col-md-2"></div>
            </div>';
            $html .='<div class="row mb-3"><div class="col-md-5"></div>
    <div class="col-md-3 text-left"><h3>'.date("F", mktime(0, 0, 0, $request->month, 10)).''. $request->year.'</h3></div>
    <div class="col-md-2"></div>
    </div>';
            $html .= '<div class="row ">
                <div class="col-md-6 border-right">
                <div class="col-md-12 mb-3 text-center "><b><h4>Billings</h4></b></div>
                <div class="row">
                <div class="col-md-6">
                <b>Total Billing</b> : 
                </div>
                <div class="col-md-6 text-right">'.$billtotal.'</div>
                </div>
                <div class="row">
                <div class="col-md-6">
                <b>Additional Billing</b> : 
                </div>
                <div class="col-md-6 text-right">'.$custom.'
                  </div>
                </div>';
               $ftotal= $custom+$billtotal;
        $html .='
                </div>
                <div class="col-md-6">
                <div class="col-md-12 mb-3 text-center "><b><h4>Profit/Loss Statement</h4></b></div>
               
               
               
                <div class="row">
                <div class="col-md-8">
                <b>Total Liquid For The Month</b> : 
                </div>
                <div class="col-md-4 text-right">'.$total_liquid.'
                  </div>
                </div>
                <div class="row">
                <div class="col-md-6">
                <b>Total Expances</b> :
                </div>
                <div class="col-md-6 text-right">
                (-'.array_sum($myexp).')</div>
                </div>
                
                <div class="row">
                <div class="col-md-6">
                <b>Sundry Creditors</b> : 
                </div>
                <div class="col-md-6 text-right">(-'.$bill_credit.')
                  </div>
                </div>

                <div class="row">
                <div class="col-md-6">
                <b><h4>Online Charges:</h4><small>2@+(18% of 2%)GST</small></b> 
                </div>
                <div class="col-md-6 text-right">(-'.round($ttlper,2).')
                  </div>
                </div>

               
               
               
                </div></div>';
                $html.='  
                <div class="row  mt-3 border-left-0 border-right-0">
                 <div class=" border col-md-6 border-right">
                 <div class="row">
                 <div class="col-md-8 ">
                 <b>Total Gross Billing  </b> : 
                 </div>
                 <div class="col-md-4 text-right ">'.$ftotal.'
                   </div>
                 </div>
                 </div>                  
                </div>
                
                <div class="row">
                <div class="col-md-6 border-right mt-2">
                <div class="row">
                <div class="col-md-6">
                <b>Cash Payment</b> : 
                </div>
                <div class="col-md-6 text-right">'.$cash.'
                  </div>
                </div>
                <div class="row">
                <div class="col-md-6">
                <b>Cheque Payment</b> : 
                </div>
                <div class="col-md-6 text-right">'.$bank.'</div>
                </div>
                <div class="row">
                <div class="col-md-6">
                <b>Online Payment</b> :
                </div>
                <div class="col-md-6 text-right">
                '.$online.'</div>
                </div>
                <div class="row">
                <div class="col-md-6">
                <b>Credit Payment</b> :
                </div>
                <div class="col-md-6 text-right">
                '.$credit.'</div>
                </div>
                </div>
               
                <div class="col-md-6 mt-2">
                </div>
                
                </div>
                ';

                $html.='  
                <div class="row border mt-3 border-left-0 border-right-0">
                 <div class="col-md-6 border-right">
                 <div class="row">
                 <div class="col-md-8 ">
                 <b>Total Liquid For The Month </b> : 
                 </div>
                 <div class="col-md-4 text-right ">'.$total_liquid.'
                   </div>
                 </div>
                 </div>  
               
                <div class="col-md-6 ">
                <div class="row">
                <div class="col-md-6 ">
                <b>Total Amount in hand </b> : 
                </div>';
              $totalinhand =  $total_liquid - array_sum($myexp)-$bill_credit;
              $gt = round($totalinhand -round($ttlper,2),2);
            $html .='<div class="col-md-6 text-right">'.$gt.'
                </div>
                </div></div>                
                </div>';
                $html .= '<div class="row">
                <div class="col-md-6 border-right">
                <div class="col-md-12 mb-3 text-center "><b><h4>Expances</h4></b></div>
                ';
                $totalexp=0;
                if(!empty($myexp)){ foreach($myexp as $key=>$expp){
                  $totalexp = $expp+$totalexp;
            $html .= ' <div class="row ">
            <div class="col-md-6">
                    <b>'.$key.'</b> : 
                    </div> 
                    <div class="col-md-6 text-right">'.$expp.'</div>
                    </div>';
                }
              }else{
               $html .= '<div class="row">
                <div class="col-md-8"></div>
                <div class="col-md-4"></div>
                </div>';
              }
             

             $html.='</div> <div class="col-md-6">
             <div class="row">
             <div class="col-md-8">Sundry Debtors</div>
             <div class="col-md-4 text-right">'.$sandeb.'</div>
             </div>
             <div class="row">
             <div class="col-md-8">New Connection Charges</div>
             <div class="col-md-4 text-right">'.$renew.'</div>
             </div>
             <div class="row">
             <div class="col-md-8">Re Connection Charges</div>
             <div class="col-md-4 text-right">'.$newconn.'</div>
             </div>
             </div>';
             $html .= '
             </div>';


             $html.='  
             <div class="row border mt-3 border-left-0 border-right-0">
             <div class="col-md-6">
             <div class="row">
             <div class="col-md-8">
              <b>Total Expances For The Month </b> :
              </div> 
              <div class="col-md-4 text-right ">'.$totalexp.'
                </div>             
              </div></div>
              <div class="col-md-6">
              <div class="row">
              <div class="col-md-8">
               <b>Profit/Loss Statement </b> :
               </div> 
               <div class="col-md-4 text-right ">'.(($newconn+$renew)+$sandeb+$gt).'
                 </div>             
               </div>
              </div>';
              
             $html.=' </div> 
             <div class="row border border-top-0 border-left-0 border-right-0">
             <div class="col-md-6">
             <div class="row">
             <div class="col-md-12 text-center"><h5>Net Bill for the month</h5></div>
             <div class="col-md-8">
              <b>Total Bill For The Month </b> :
              </div> 
              <div class="col-md-4 text-right ">'.$bill_for_month.'
                </div>             
              </div>
              <div class="row">
              <div class="col-md-8">
              <b>Operational Charges </b> :
              </div> 
              <div class="col-md-4 text-right ">'.$operational_charges.'
                </div>             
              </div>
              </div>
              <div class="col-md-6">
              <div class="row">
              <div class="col-md-8">
               <b> </b> 
               </div> 
               <div class="col-md-4 text-right ">
                 </div>             
               </div>
               </div> </div>
             <div class="row">
             <div class="col-md-6">
             <div class="row">
             <div class="col-md-8">
               <b>Total </b> :
               </div> 
               <div class="col-md-4 text-right ">'.$ddd.'
                 </div> 
             </div>
             </div>
             <div class="col-md-6"></div>
             </div>

              </div>';
            
            }
             
         //Yearly Bill Data Start from here   
            
            
            
            else{


                $html = '
                <div class="row mb-3"><div class="col-md-3"><button class="btn btn-primary" id="printme">Print Me</button></div>
                        <div class="col-md-5 text-center "></div>
                        <div class="col-md-4 text-right"></div>
                        </div>'; 
                $html .= '<div class="border p-3" id="printdiv ">
                  <div class="row mb-3"><div class="col-md-4"><img style="max-width: 80%;" src="'.asset('/img/logo_via.svg').'" class="img img-responsive"/></div>
                          <div class="col-md-4 text-center "><b>Rcl-7/70 G.I.D.C. Colony,Gundlav-396035,Valsad,Gujarat,India</b></div>
                          <div class="col-md-4 text-right"><sapn><b>Date:</b></span>'.date("d-m-Y h:i:s").'</div></div>';
                          $html .='<div class="row mb-2"><div class="col-md-3"></div>
                          <div class="col-md-6 text-center"><h4 style="border-bottom: 1px solid black;">Billings & P/L Statement Report</h4></div>
                          <div class="col-md-2"></div>
                          </div>';
                          $html .='<div class="row mb-3"><div class="col-md-4"></div>
                  <div class="col-md-5 text-left"><h3> Annual Report'.$request->year.'</h3></div>
                  <div class="col-md-2"></div>
                  </div>';
                $html .='<div class="row">
                <div class="col-md-6 border-right">
                <div class="col-md-12 mb-3 text-center "><b><h4>Records</h4></b></div>
                <div class="row">
                <div class="col-md-6">
                <b>January</b> : 
                </div>';
              //  $jan = (int)(isset($monthwise["01"]) ? $monthwise["01"] :0)+ (int)(isset($cus_month["01"]) ? $cus_month["01"] : 0);
              //  $feb = (int)(isset($monthwise["02"]) ? $monthwise["02"] :0)+ (int)(isset($cus_month["02"]) ? $cus_month["02"] : 0);
               $html .= '<div class="col-md-6 text-right">'.(isset($monthwise["01"]) ? $monthwise["01"] :0).'</div>
                </div>
                <div class="row">
                <div class="col-md-6">
                <b>February</b> : 
                </div>
                <div class="col-md-6 text-right">'.(isset($monthwise["02"]) ? $monthwise["02"] :0).'
                  </div>
                </div>
                <div class="row">
                <div class="col-md-6">
                <b>March</b> : 
                </div>
                <div class="col-md-6 text-right">'.(isset($monthwise["03"]) ? $monthwise["03"] : '0').'
                  </div>
                </div>  <div class="row">
                <div class="col-md-6">
                <b>April</b> : 
                </div>
                <div class="col-md-6 text-right">'.(isset($monthwise["04"]) ? $monthwise["04"] : '0').'
                  </div>
                </div>
                <div class="row">
                <div class="col-md-6">
                <b>May</b> : 
                </div>
                <div class="col-md-6 text-right">'.(isset($monthwise["05"]) ? $monthwise["05"] : '0').'
                  </div>
                </div>
                <div class="row">
                <div class="col-md-6">
                <b>June</b> : 
                </div>
                <div class="col-md-6 text-right">'.(isset($monthwise["06"]) ? $monthwise["06"] : '0').'
                  </div>
                </div>
                <div class="row">
                <div class="col-md-6">
                <b>July</b> : 
                </div>
                <div class="col-md-6 text-right">'.(isset($monthwise["07"]) ? $monthwise["07"] : '0').'
                  </div>
                </div>
                <div class="row">
                <div class="col-md-6">
                <b>August</b> : 
                </div>
                <div class="col-md-6 text-right">'.(isset($monthwise["08"]) ? $monthwise["08"] : '0').'
                  </div>
                </div>
                <div class="row">
                <div class="col-md-6">
                <b>September</b> : 
                </div>
                <div class="col-md-6 text-right">'.(isset($monthwise["09"]) ? $monthwise["09"] : '0').'
                  </div>
                </div>  <div class="row">
                <div class="col-md-6">
                <b>October</b> : 
                </div>
                <div class="col-md-6 text-right">'.(isset($monthwise["10"]) ? $monthwise["10"] : '0').'
                  </div>
                </div>
                <div class="row">
                <div class="col-md-6">
                <b>November</b> : 
                </div>
                <div class="col-md-6 text-right">'.(isset($monthwise["11"]) ? $monthwise["11"] : '0').'
                  </div>
                </div>
                <div class="row">
                <div class="col-md-6">
                <b>December</b> : 
                </div>
                <div class="col-md-6 text-right">'.(isset($monthwise["12"]) ? $monthwise["12"] : '0').'
                  </div>
                </div> 
                <div class="row">
                <div class="col-md-6">
                <b>Operational Charges</b> : 
                </div>
                <div class="col-md-6 text-right">'.$operational_charges.'
                  </div>
                </div>
                
                <div class="row">
                <div class="col-md-6">
                <b>Additional Billing</b> : 
                </div>
                <div class="col-md-6 text-right">'.$custom.'
                  </div>
                </div>
                ';
               $ftotal= $custom+$billtotal+$operational_charges;
             $total_liquid_for_the_year = array_sum($cus_month) + array_sum($liquiddatabymonth);
               $html .='
                </div>
                <div class="col-md-6">
                <div class="col-md-12 mb-3 text-center "><b><h4>Profit/Loss Statement</h4></b></div>
                <div class="row">
                <div class="col-md-8">
                <b>Total Liquid For The Year</b> : 
                </div>
                <div class="col-md-4 text-right">'.$total_liquid_for_the_year.'
                  </div>
                </div>
                 <div class="row">
                <div class="col-md-6">
                <b>Total Expances </b> : 
                </div>
                <div class="col-md-6 text-right">'.array_sum($monthwiseliquid).'
                  </div>
                </div>';
                $oftheyear= $total_liquid_for_the_year - array_sum($monthwiseliquid) - $sancre;
                $twopercentage = (2 / 100) * $online;
                $eighteen = (18 / 100) * $twopercentage;
                $ttlper = $twopercentage+$eighteen;

                $oftheyear = $oftheyear- $ttlper;
              
                $html .='<div class="row">
                <div class="col-md-6">
                <b>Sundry Creditors</b> : 
                </div>
                <div class="col-md-6 text-right">-'.($sancre).'</div>
                </div>
                <div class="row">
                <div class="col-md-6">
                <b>Online Charges</b><br/> <small>2@+(18% of 2%)GST</small> :
                </div>
                <div class="col-md-6 text-right">
                -'.$ttlper.'</div>
                </div>

                
                </div></div>';
                
                $html.='  
                <div class="row border mt-3 border-left-0 border-right-0">
                 <div class="col-md-6 border-right">
                 <div class="row">
                 <div class="col-md-6 ">
                 <b>Total </b> : 
                 </div>
                 <div class="col-md-6 text-right ">'.$ftotal.'
                   </div>
                 </div>
                 </div>
                 <div class="col-md-6 ">
                 <div class="row">
                 <div class="col-md-6 ">
                 <b>Total Amount In Hand </b> : 
                 </div>
                 <div class="col-md-6 text-right">'.$oftheyear.'
                 </div>
                 </div></div>                
                 </div> '; 

                 $html .= '<div class="row">
                <div class="col-md-6 border-right">
                <div class="col-md-12 mb-3 text-center "><b><h4>Liquids Recevied</h4></b></div>
                ';
                // dd($cus_month);
                $jan =  isset($liquiddatabymonth["01"]) ? $liquiddatabymonth["01"] : 0; + isset($cus_month["01"]) ? $cus_month["01"] : 0 ;
                $feb =  isset($liquiddatabymonth["02"]) ? $liquiddatabymonth["02"] : 0; + isset($cus_month["02"]) ? $cus_month["02"] : 0 ;
                $mar =  isset($liquiddatabymonth["03"]) ? $liquiddatabymonth["03"] : 0; + isset($cus_month["03"]) ? $cus_month["03"] : 0 ;
                $apr =  isset($liquiddatabymonth["04"]) ? $liquiddatabymonth["04"] : 0; + isset($cus_month["04"]) ? $cus_month["04"] : 0 ;
                $may =  isset($liquiddatabymonth["05"]) ? $liquiddatabymonth["05"] : 0 ; + isset($cus_month["05"]) ? $cus_month["05"] : 0 ;
                $jun =  isset($liquiddatabymonth["06"]) ? $liquiddatabymonth["06"] : 0 ; + isset($cus_month["06"]) ? $cus_month["06"] : 0 ;
                $jul =  isset($liquiddatabymonth["07"]) ? $liquiddatabymonth["07"] : 0; + isset($cus_month["07"]) ? $cus_month["07"] : 0 ;
                $aug =  isset($liquiddatabymonth["08"]) ? $liquiddatabymonth["08"] : 0; + isset($cus_month["08"]) ? $cus_month["08"] : 0 ;
                $sep =  isset($liquiddatabymonth["09"]) ? $liquiddatabymonth["09"] : 0; + isset($cus_month["09"]) ? $cus_month["09"] : 0 ;
                $oct =  isset($liquiddatabymonth["10"]) ? $liquiddatabymonth["10"] : 0; + isset($cus_month["10"]) ? $cus_month["10"] : 0 ;
                $nov =  isset($liquiddatabymonth["11"]) ? $liquiddatabymonth["11"] : 0; + isset($cus_month["11"]) ? $cus_month["11"] : 0 ;
                $dec =  isset($liquiddatabymonth["12"]) ? $liquiddatabymonth["12"] : 0; + isset($cus_month["12"]) ? $cus_month["12"] : 0 ;
                $html .= '<div class="row">
                <div class="col-md-6">
                <b>January</b> : 
                </div>
                <div class="col-md-6 text-right">'.$jan.'</div>
                </div>
                <div class="row">
                <div class="col-md-6">
                <b>February</b> : 
                </div>
                <div class="col-md-6 text-right">'.$feb.'
                  </div>
                </div>
                <div class="row">
                <div class="col-md-6">
                <b>March</b> : 
                </div>
                <div class="col-md-6 text-right">'.$mar.'
                  </div>
                </div>  <div class="row">
                <div class="col-md-6">
                <b>April</b> : 
                </div>
                <div class="col-md-6 text-right">'.$apr.'
                  </div>
                </div>
                <div class="row">
                <div class="col-md-6">
                <b>May</b> : 
                </div>
                <div class="col-md-6 text-right">'.$may.'
                  </div>
                </div>
                <div class="row">
                <div class="col-md-6">
                <b>June</b> : 
                </div>
                <div class="col-md-6 text-right">'.$jun.'
                  </div>
                </div>
                <div class="row">
                <div class="col-md-6">
                <b>July</b> : 
                </div>
                <div class="col-md-6 text-right">'.$jul.'
                  </div>
                </div>
                <div class="row">
                <div class="col-md-6">
                <b>August</b> : 
                </div>
                <div class="col-md-6 text-right">'.$aug.'
                  </div>
                </div>
                <div class="row">
                <div class="col-md-6">
                <b>September</b> : 
                </div>
                <div class="col-md-6 text-right">'.$sep.'
                  </div>
                </div>  <div class="row">
                <div class="col-md-6">
                <b>October</b> : 
                </div>
                <div class="col-md-6 text-right">'.$oct.'
                  </div>
                </div>
                <div class="row">
                <div class="col-md-6">
                <b>November</b> : 
                </div>
                <div class="col-md-6 text-right">'.$nov.'
                  </div>
                </div>
                <div class="row">
                <div class="col-md-6">
                <b>December</b> : 
                </div>
                <div class="col-md-6 text-right">'.$dec.'
                  </div>
                </div>';

        $html .='
                </div>
                <div class="col-md-6">
                <div class="col-md-12 mb-3 text-center "><b></b></div>
                <div class="row">
                <div class="col-md-6">
                <b><h4>Sundry Debtors:</h4></b>
                </div>
                <div class="col-md-6 text-right">'.$sandeb.'
                  </div>
                </div>
             

                <div class="row">
                <div class="col-md-8">
                <b>New connection charges:</b>  
                </div>
                <div class="col-md-4 text-right">'.$newconn.'</div>
                </div>
                <div class="row">
                <div class="col-md-8">
                <b>Re connection charges:</b>  
                </div>
                <div class="col-md-4 text-right">'.$renew.'</div>
                </div>
                </div>
                </div>';
                $allmonthexp = array_sum($monthwiseliquid);
                
                $total_liquid = round(($allmonthexp+$sancre)+round($ttlper,2),2);
                $m_t = round($newconn + $renew + $sandeb + $oftheyear,2); 
                $html.='  
                <div class="row border mt-3 border-left-0 border-right-0">
                 <div class="col-md-6 border-right">
                 <div class="row">
                 <div class="col-md-6 ">
                 <b>Total </b> : 
                 </div>
                 <div class="col-md-6 text-right ">'.$total_liquid_for_the_year.'
                   </div>
                 </div>
                 </div>
                 <div class="col-md-6 ">
                 <div class="row">
                 <div class="col-md-6 ">
                 <b>Liquid Cash In Hand For Year </b> : 
                 </div>
                 <div class="col-md-6 text-right">'.$m_t.'
                 </div>
                 </div></div>                
                 </div> '; 

                 $html .= '<div class="row">
                <div class="col-md-6 border-right">
                <div class="col-md-12 mb-3 text-center "><b><h4>Expances</h4></b></div>
                <div class="row">
                <div class="col-md-6">
                <b>January</b> : 
                </div>
                <div class="col-md-6 text-right">'.(isset($monthwiseliquid["01"]) ? $monthwiseliquid["01"] : '0').'</div>
                </div>
                <div class="row">
                <div class="col-md-6">
                <b>February</b> : 
                </div>
                <div class="col-md-6 text-right">'.(isset($monthwiseliquid["02"]) ? $monthwiseliquid["02"] : '0').'
                  </div>
                </div>
                <div class="row">
                <div class="col-md-6">
                <b>March</b> : 
                </div>
                <div class="col-md-6 text-right">'.(isset($monthwiseliquid["03"]) ? $monthwiseliquid["03"] : '0').'
                  </div>
                </div>  <div class="row">
                <div class="col-md-6">
                <b>April</b> : 
                </div>
                <div class="col-md-6 text-right">'.(isset($monthwiseliquid["04"]) ? $monthwiseliquid["04"] : '0').'
                  </div>
                </div>
                <div class="row">
                <div class="col-md-6">
                <b>May</b> : 
                </div>
                <div class="col-md-6 text-right">'.(isset($monthwiseliquid["05"]) ? $monthwiseliquid["05"] : '0').'
                  </div>
                </div>
                <div class="row">
                <div class="col-md-6">
                <b>June</b> : 
                </div>
                <div class="col-md-6 text-right">'.(isset($monthwiseliquid["06"]) ? $monthwiseliquid["06"] : '0').'
                  </div>
                </div>
                <div class="row">
                <div class="col-md-6">
                <b>July</b> : 
                </div>
                <div class="col-md-6 text-right">'.(isset($monthwiseliquid["07"]) ? $monthwiseliquid["07"] : '0').'
                  </div>
                </div>
                <div class="row">
                <div class="col-md-6">
                <b>August</b> : 
                </div>
                <div class="col-md-6 text-right">'.(isset($monthwiseliquid["08"]) ? $monthwiseliquid["08"] : '0').'
                  </div>
                </div>
                <div class="row">
                <div class="col-md-6">
                <b>September</b> : 
                </div>
                <div class="col-md-6 text-right">'.(isset($monthwiseliquid["09"]) ? $monthwiseliquid["09"] : '0').'
                  </div>
                </div>  <div class="row">
                <div class="col-md-6">
                <b>October</b> : 
                </div>
                <div class="col-md-6 text-right">'.(isset($monthwiseliquid["10"]) ? $monthwiseliquid["10"] : '0').'
                  </div>
                </div>
                <div class="row">
                <div class="col-md-6">
                <b>November</b> : 
                </div>
                <div class="col-md-6 text-right">'.(isset($monthwiseliquid["11"]) ? $monthwiseliquid["11"] : '0').'
                  </div>
                </div>
                <div class="row">
                <div class="col-md-6">
                <b>December</b> : 
                </div>
                <div class="col-md-6 text-right">'.(isset($monthwiseliquid["12"]) ? $monthwiseliquid["12"] : '0').'
                  </div>
                </div>';
               $ftotal= $custom+$billtotal;
        $html .='
                </div>
                <div class="col-md-6"></div></div>';
                $html.='  
                <div class="row border mt-3 border-left-0 border-right-0">
                 <div class="col-md-6 border-right">
                 <div class="row">
                 <div class="col-md-6 ">
                 <b>Total </b> : 
                 </div>
                 <div class="col-md-6 text-right ">'.array_sum($monthwiseliquid).'
                   </div>
                 </div>
                 </div>               
                 </div> ';
    
              }
             

             echo $html ; exit;
     
}



}
