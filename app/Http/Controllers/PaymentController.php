<?php

namespace App\Http\Controllers;

use App\Models\Payment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\CreateBill;
use App\Models\Connection;
use App\Models\Notification;
use App\Mail\PaySuccess;
use Illuminate\Support\Facades\Mail;
use App\Models\Sno;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function show(Payment $payment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function edit(Payment $payment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Payment $payment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Payment $payment)
    {
        //
    }

    public function paynnow($id){

        //https://sandboxsecure.payu.in/_payment 
        //https://secure.payu.in/_payment 
        $bill = CreateBill::where('connections_id',$id)->orderBy('id','DESC')->first();
        // dd($bill);
$fields_string='';
// $hashSequence = 'TycXMAR7|admin123|100|bill payment|first-name|email@gmail.com|bill_id|conn_id||||||TobZxCFt1J';
$hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
$server_key     = 'u82JKeCBoPJ8XVWyWjEabqqHE35abmFrJkoAtPRBoHg=';
$headers = array(
    'Content-Type:application/json',
    'Authorization:key=' . $server_key,
);

$txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);

$posted = array(
	'key' => 'TycXMAR7',
	'txnid' => $txnid,
	'amount' => $bill->bill_to_pay,
	'email' => $bill->conn->email,
    'firstname'=>$bill->conn->name,
    'productinfo'=>'bill payment',
    'udf1'=>$bill->id,
    'udf2'=>$bill->conn->id,
);
$SALT = "TobZxCFt1J";
$hashVarsSeq = explode('|', $hashSequence);
$hash_string = '';	
foreach($hashVarsSeq as $hash_var) {
  $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
  $hash_string .= '|';
}

$hash_string .= $SALT;


$hash = strtolower(hash('sha512', $hash_string));



// $hash = hash("sha512", $hashSequence);
        $url = 'https://sandboxsecure.payu.in/_payment';
    $fields = array(
	'key' => 'TycXMAR7',
	'hash_string' => 'sha512',
	'hash' => $hash,
	'txnid' => $txnid,
	'amount' => $bill->bill_to_pay,
	'email' => $bill->conn->email,
	'phone' => '8965896854',
    'firstname'=>$bill->conn->name,
    'productinfo'=>'bill payment',
    // 'surl'=>'pay-success',
    // 'furl'=>'pay-failed',
    'service_provider'=>'payu_paisa',
    'udf1'=>$bill->id,
    'udf2'=>$bill->conn->id,
);


//url-ify the data for the POST
// foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
// rtrim($fields_string, '&');

//open connection
// $ch = curl_init();

// //set the url, number of POST vars, POST data
// curl_setopt($ch,CURLOPT_URL, $url);
// curl_setopt($ch, CURLOPT_POST, true);
//  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
// curl_setopt($ch,CURLOPT_POST, count($fields));
// curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
// curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

// //execute post
// $result = curl_exec($ch);

// //close connection
// curl_close($ch);
// var_dump($result);

     return view('payment.paynow',['url'=>$url,'field'=>$fields]);
    }

    public function SubscribeResponse(Request $request){
// dd($request);
        $payid = Payment::create([
            'connection_id' => $request->udf2,
            'bill_id' => $request->udf1,
            'status' => $request->status,
            'amount' => $request->amount,
            'txnid' => $request->txnid,
            'hash' => $request->hash,
            'mobile' => $request->mobile,
            'email' => $request->email,
            'payu_money_id' => $request->payuMoneyId,
            'mode' => $request->mode,
            'payid' => $request->mihpayid,
            'paystatus' => $request->unmappedstatus,

        ]);
        if($request->status=='success'){
            
            $bill_data = CreateBill::find($request->udf1);
        $bill_data->pay_by = 'online';
        $bill_data->paid_amount = $request->amount;
        //    $bill_data->debit = $request->debit;
        //    $bill_data->credit = $request->credit;
        $bill_data->status = 'paid by online payment';
        $bill_data->paid_date = date('Y-m-d H:i:s');
        $bill_data->paid_status = 'paid';
        $bill_data->remaning_amount = 0;
        //  $bill_data->pre_bill_due=$rem;
        $bill_data->save();

        $sno = new Sno;
        $sno->bill_id = $request->udf1;
        $sno->save();

        CreateBill::where('connections_id', $bill_data->connections_id)
            ->where('id', '!=', $bill_data->id)
            ->where('paid_status','unpaid')
            ->update(['paid_status' => 'paid','pay_by'=>'online', 'paid_date' => date('Y-m-d H:i:s') ]);

        $connection = Connection::find($bill_data->connections_id);
        
        $not = Notification::create([
            'type' => 'online_payment',
            'message' => 'Online payment recive from connection no'.$connection->conn_no.'',
        ]);
        $connection->debit = 0;
        // $connection->credit = $request->amount -$connection->credit;
        $connection->capital = 0;
        $connection->save();
        if(isset($connection->phone)){
          
            $data['amount']=$bill_data->bill_to_pay;
            $data['paydate']=date('d-F',strtotime($bill_data->paid_date));
            $data['vno']= $bill_data->id;
            $data['month']=$bill_data->billing_month;
           
       $stat= $this->paysms($connection->phone,$data);
    
        }
        // return redirect()
        //     ->route('createbill')
        //     ->with('status', 'Bill paid succesfully..');

        }
        $myconn = Connection::find($request->udf2);
if(!empty($myconn)){
    if(isset($myconn->email)){
        $mydata=[];
        $mydata['status'] =$request->status;
        $mydata['payid'] =$request->mihpayid;
        $mydata['amt'] = $request->amount;
        Mail::to($myconn->email)->send(new PaySuccess($mydata));
    }
}
         return redirect()->route('paysuccess')->with('status', $request->status);
 
        // return view('payment.success',['response'=>$request]);

    }

    public function paysuccess(){
        $payment = Payment::latest()->first();
        return view('payment.success',['payment'=>$payment]);

    }

    public function SubscribeCancel(){
        return view('payment.failed');

    }

    public function paysms($mobile,$data){
        $apiKey = urlencode('OGJiNWQ2ZDhhOTI3OTRjNTllZDUwNGQ5YjUwYWU4MTA=');
        $numbers = $mobile;
        $sender = urlencode('VIAWSe');
        $amount = $data['amount'];
        $paydate= $data['paydate'];
        $vno = $data['vno'];
        $month= $data['month'];
        $msg ="Receipt for payment : Amount received :$amount: pay date :$paydate,voucher no.$vno,bill month: $month, thanks for payment, Regards VIA water services.";
        
        $message = rawurlencode($msg);
        //  $message = rawurlencode("Receipt for payment : Amount received :$amount: pay date :$paydate,voucher no.$vno,bill month: $month, thanks for payment, Regards VIA water services.");
        
         $data = array('apikey' => $apiKey, 'numbers' => $numbers, "sender" => $sender, "message" => $message);
         // Send the POST request with cURL
        $ch = curl_init('https://api.textlocal.in/send/');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
        
       }
    


}
