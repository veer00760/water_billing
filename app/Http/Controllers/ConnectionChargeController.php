<?php

namespace App\Http\Controllers;

use App\Models\ConnectionCharge;
use App\Models\Connection;
use Illuminate\Http\Request;
use App\Models\Party;
use App\Models\Meter;
class ConnectionChargeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $connection = Connection::find($id);
//         $requestdata = session('data');
//  dd($requestdata);
        return view('connection.connection_charge');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //  dd($request);
        $request->validate([
           
            'connection_charge' => 'required|integer',
            'deposit' => 'required|integer',
            'road_crossing' => 'required|integer',
            'minimum_charge' => 'required|integer',
            'paid_through' => 'required',
            ]);
            //  dd($request);
            if(!isset($request->status)){
           $requestdata = session('data');
        //  dd($requestdata);
        if(isset($requestdata['party'])){
            $partyid =  Party::find($requestdata['party']);

        }else{
     $partyid = Party::create([
                'name' => $requestdata['name'],
                'email' => $requestdata['email'],
                // 'party_types_id' => $requestdata['party_types_id'],
                'address' => $requestdata['address'],
            ]);
        }
      

        $meterid = Meter::create([
            'meter_no' => $requestdata['meter_no'],
            'installation_date' => date('y-m-d h:i:s',strtotime($requestdata['installation_date'])),
            // 'party_types_id' => $requestdata['party_types_id'],
            'status' => $requestdata['status'],
            'innital_meter_reading' => $requestdata['innital_meter_reading'],
            'current_unit' => $requestdata['innital_meter_reading'],
            'meter_test_copy' => $requestdata['meter_test_copy'],
            'appliaction_copy' => $requestdata['application_copy'],
        ]);

        $connid = Connection::create([
                      // 'conn_no' => rand(999,99999),
            'conn_no' => $requestdata['conn_no'],
            'name' => $requestdata['onname'],
            'phone' => $requestdata['onphone'],
            'connection_types_id' => $requestdata['connection_types_id'],
            'parties_id' => $partyid->id,
            'meters_id' => $meterid->id,
            'email' => $requestdata['onemail'],
            'address' => $requestdata['onaddress'],
            'party_types_id' =>$requestdata['party_types_id'],
        ]);

            Meter::where('id', $meterid->id)
        ->update(['connections_id' => $connid->id]);

            $meterid = ConnectionCharge::create([
                'connection_charge' => $request->connection_charge,
                'deposit' => $request->deposit,
                'connections_id' => $connid->id,
                'road_crossing' => $request->road_crossing,
                'minimum_charge' => $request->minimum_charge,
                'paid_through' => $request->paid_through,
                'check_no' => $request->check_no,
                'note' => $request->note,
                'type' => 'new',

            ]);
            session()->forget('data');
            return redirect()->route('conn')->with('status', 'Connection created succesfully');

            }else{
                $meterid = ConnectionCharge::create([
                    'connection_charge' => $request->connection_charge,
                    'deposit' => $request->deposit,
                    'connections_id' => $request->connections_id,
                    'road_crossing' => $request->road_crossing,
                    'minimum_charge' => $request->minimum_charge,
                    'paid_through' => $request->paid_through,
                    'check_no' => $request->check_no,
                    'note' => $request->note,
                    'type' => 'renew',
    
                ]);
                if($request->status=='active'){
                    Connection::where('id', $request->connections_id)
                    ->update(['status' => 'active']);
                }
            }
             
                return redirect()->route('conn')->with('status', 'Connection status updated succesfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ConnectionCharge  $connectionCharge
     * @return \Illuminate\Http\Response
     */
    public function show(ConnectionCharge $connectionCharge)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ConnectionCharge  $connectionCharge
     * @return \Illuminate\Http\Response
     */
    public function edit(ConnectionCharge $connectionCharge)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ConnectionCharge  $connectionCharge
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ConnectionCharge $connectionCharge)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ConnectionCharge  $connectionCharge
     * @return \Illuminate\Http\Response
     */
    public function destroy(ConnectionCharge $connectionCharge)
    {
        //
    }
}
