<?php

namespace App\Http\Controllers;

use App\Models\CustomBill;
use Illuminate\Http\Request;
use DB;
use App\Models\Connection;
use App\Models\Meter;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
class CustomBillController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bills = CustomBill::orderBy('id', 'DESC')->get();
        return view('custombill.list',['bills'=>$bills,]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $connections = Connection::orderBy('id', 'DESC')->get();
        return view('custombill.add',['connections'=>$connections]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        Validator::make($request->all(), [
            'data' => 'required',
            'amount' => 'required',
            'connection_id' => 'required',

            ])->validate();
           $meter =  Meter::where('connections_id',$request->connection_id)->orderBy('id', 'DESC')->first();
      $lastest = CustomBill::latest('id')->first();
    //   dd($lastest);
    if(empty($lastest)){
       $vno =  0;   
    }else{
        $vno =  $lastest->id; 
    }
    if(!empty($lastest)){
    if($lastest->paid_status=='unpaid'){
        $predue = $lastest->total;
    }else{
        $predue= 0;
    }
}else{
    $predue= 0;

}
           $billid = CustomBill::create([
            'data' => json_encode($request->data),
            'amount' =>json_encode($request->amount),
            'total' => array_sum($request->amount)+$predue,
            'previousdue' => $predue,
            'connection_id'=>$request->connection_id,
            'meter_id'=>$meter->id,
            'meter_status'=>$meter->status,
            'title'=>$request->title,
            'voucher_no'=>'cus'.++$vno,
            'paid_status'=>'unpaid',
            'note'=>$request->note,

        ]);
        // $connection = Connection::find($request->connection_id);
        // $bill_data =  CustomBill::find($billid->id);
        // $total = array_sum($request->amount);
        // if($connection->credit>=$total){
        //     $bill_data->pay_by = 'credit';
        //     $bill_data->paid_date = date('Y-m-d H:i:s');
        //     $bill_data->paid_status='paid';	
        //     $bill_data->credit=$total;	
        //     $bill_data->debit=0;
        //     $bill_data->save();
        //     $connection->credit = abs($connection->credit - array_sum($request->amount));
        //     $connection->debit =0;
        // }else{
        //     $bill_data->debit=$total;	
        //     $bill_data->credit=0;
        //     $bill_data->save();

        // $connection->debit = $connection->debit + array_sum($request->amount);
        // $connection->credit =0;
        //  } 
        //  $connection->save();
        return redirect()->route('custombill')->with('status', 'Bill added  succesfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CustomBill  $customBill
     * @return \Illuminate\Http\Response
     */
    public function show( $customBill)
    {
       $customBill = CustomBill::find($customBill);
      $lastunpaid =  CustomBill::where('id','<',$customBill->id)->where('paid_status','unpaid')->first();
       return view('custombill.view',['bill'=>$customBill,'lastunpaid'=>$lastunpaid]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CustomBill  $customBill
     * @return \Illuminate\Http\Response
     */
    public function edit(CustomBill $customBill)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CustomBill  $customBill
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CustomBill $customBill)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CustomBill  $customBill
     * @return \Illuminate\Http\Response
     */
    public function destroy(CustomBill $customBill)
    {
        //
    }
}
