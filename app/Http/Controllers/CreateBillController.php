<?php
namespace App\Http\Controllers;

use App\Models\CreateBill;
use Illuminate\Http\Request;
use App\Models\Connection;
use App\Models\ConnectionCharge;
use App\Models\Meter;
use Carbon\Carbon;
use App\Models\CustomBill;
use App\Models\Party;
use App\Mail\SendBill;
use Illuminate\Support\Facades\Mail;
use Auth;
use App\Models\Sno;
class CreateBillController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bills = CreateBill::orderBy('id', 'DESC')->get();
        return view('bills.list', ['bills' => $bills]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $connections = Connection::all();
        $parties = Party::all();

        $todayrecord = CreateBill::whereDate('created_at', Carbon::today())->count();
        if ($todayrecord > 600)
        {
            return redirect()->route('createbill')
                ->with('status', 'Today limit reached');

        }
        return view('bills.add', ['parties'=>$parties,'connections' => $connections]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //   dd($request);
        $monthbill = CreateBill::where('connections_id', $request->conn_no)
            ->whereMonth('created_at', date('m'))
            ->whereYear('created_at', date('Y'))
            ->count();
        // dd($monthbill);
        //     if($monthbill>=1){
        // // return redirect('createbill')
        // // ->withErrors('Bill already created..');
        //  return redirect()->route('createbill')->with('status', 'Bill already created for this month');
        //      }
        if ($request->meter_status == 'active')
        {
            $request->validate([

            'conn_no' => 'required', 'unit' => 'required|integer', 'billing_month' => 'required', 'consume' => 'required', 'total' => 'required', 'last_unit' => 'required', ]);
        }
        else
        {
            $request->validate([

            'conn_no' => 'required', 'billing_month' => 'required',

            ]);
        }
    //  $lunpaid =  CreateBill::where('paid_status','unpaid')->where('connections_id',$request->conn_no)->latest()->first();
        $lastunpaidbill = CreateBill::where('connections_id', $request->conn_no)
            ->latest()
            ->first();

            // if(!empty($lunpaid)){
            //     $lunpaid->paid_status = 'shifted';
            //     $lunpaid->save();
            // }

        if (!empty($lastunpaidbill))
        {
            if ($lastunpaidbill->remaning_amount > 0)
            {
                $lastdue = $lastunpaidbill->remaning_amount;
                // $pre_bill_due = $lastunpaidbill->pre_bill_due;
            }
            else
            {
                $lastdue = 0;
                // $pre_bill_due=0;
            }
        }
        else
        {
            $lastdue = 0;
            // $pre_bill_due=0;
        }
        $debit = Connection::where('id', $request->conn_no)
            ->first();
// dd($request);
        $billid = CreateBill::create(['connections_id' => $request->conn_no, 
        'unit' => $request->unit, 'billing_month' => $request->billing_month,
         'consume_unit' => $request->consume, 'total' => $request->total, 
         'due' => $request->due, 'bill_total' => $request->bill_total, 
         'intrest' => $request->intrest, 'last_unit' => $request->last_unit, 
         'meter_id' => $request->meter_id, 
         'late_payment' => $request->late_payment, 
         'adjustment_amt' => $request->adjustment_amt, 
         'adjustment_reason' => $request->adjustment_reason, 
         'meter_damage_plenty' => $request->meter_damage_plenty, 
         'plenty' => $request->saveplenty, 'credit' => $debit->credit, 
         'debit' => $debit->debit, 'lastdue' => $lastdue, 
         'meter_status' => $request->meter_status, 
         'unpaid_bill_after_meter_deactivate' => $request->unpaid_bill_after_meter_deactivate,
          'per_unit_charge' => $debit->conn_type->charge,'minimum_charge'=>$request->mincharge, ]);
        Meter::where('connections_id', $request->conn_no)
            ->where('status', 'active')
            ->update(['current_unit' => $request->unit]);
        // return redirect()->route('createbill');
        // dd($billid);
        $connection = Connection::where('id', $billid->connections_id)
            ->first();
            $bill_data = CreateBill::find($billid->id);

        if ($connection->credit >= $request->total)
        {
            $bill_data->pay_by = 'credit';
            $bill_data->paid_amount = $request->total;
            $bill_data->status = 'Pay by credit amount';
            $bill_data->paid_date = date('Y-m-d H:i:s');
            $bill_data->paid_status = 'paid';
            $bill_data->credit = $connection->credit;
            $bill_data->debit = 0;
            $bill_data->bill_to_pay =0;
            $bill_data->pre_bill_due = 0;
            $bill_data->save();
            $connection->debit = 0;
            $connection->capital = 0;
            $connection->credit = $connection->credit - $request->total;
            //  $connection->credit = $connection->credit ;
            $connection->save();

            $sno = new Sno;
            $sno->bill_id = $billid->id;
            $sno->save();
        }
        // if ($connection->credit > 0){
        //     $connection->debit = $request->total-$connection->credit;
        //     $connection->credit = 0;
        //     $connection->save();
        // }
        else
        {  
            // dd($connection);
            $rem = $request->total - $connection->credit;
            $bill_data->credit = $connection->credit;
            $bill_data->bill_to_pay = $rem;
            $bill_data->pre_bill_due = $connection->debit;
            $bill_data->save();
            if ($connection->debit > 0){
                if($request->bill_total> $request->mincharge){
                   $capital =  $request->bill_total;
                }else{
                    $capital =  $request->mincharge;

                }
                $connection->capital = $connection->capital + $capital;

            } else{
                $connection->capital = $request->bill_total - $connection->credit ;

            }
             $connection->debit = $rem;
             // dd($connection->debit);
             $connection->credit = 0;
            $connection->save();

        }
        
        if(isset($connection->email)){
            $data = CreateBill::find($billid->id); 
            $mydata=[];
            $mydata['total'] =$data->bill_to_pay;
            $mydata['month'] =$data->billing_month;
            $mydata['conn'] =$debit->conn_no;
            $mydata['name'] =$debit->name;
            $mydata['unit'] =$data->consume_unit;
            $mydata['status'] ='DUE';
            $mydata['url'] ='www.viaindustrialwaterservices.in';
            $mydata['paydate'] =date('d-F-Y',strtotime($data->created_at."+ 15 days"));
            Mail::to($connection->email)->send(new SendBill($mydata));
            $status[] = 'Email send succesfully <br/>';

        }

        if(isset($connection->phone)){   
            $datass = CreateBill::find($billid->id); 
            $smsdata=[];
            $smsdata['amount'] =$datass->bill_to_pay;
            $smsdata['month'] =$datass->billing_month;
            $smsdata['conn'] =$debit->conn_no;
            $smsdata['unit'] =$datass->id;
            $smsdata['consume_unit'] =$datass->consume_unit;
            $smsdata['paydate'] =date('d-M',strtotime($datass->created_at."+ 15 days"));
   
           $res = $this->sendsms($smsdata,$connection->phone);
           $myres = json_decode($res);
           if($myres->status=='failure'){
            $status[] = 'Message is not send <br/>';
           }else{
            $status[] = 'Message is send succesfully <br/>';  
           }
        }
        // dd($smsdata,$mydata);
        $status[] = 'Bill created succesfully <br/>';  
        $mystatus = json_encode($status);

        return redirect()
            ->route('createbill')
            ->with('status', $mystatus);

    }

    public function sendsms($smsdata,$mobile){
        // Account details
        $apiKey = urlencode('OGJiNWQ2ZDhhOTI3OTRjNTllZDUwNGQ5YjUwYWU4MTA=');
        $numbers = $mobile;
        $sender = urlencode('VIAWSe');
        $amount = $smsdata['amount'];
        $paydate=$smsdata['paydate'];
        $consume_unit = $smsdata['consume_unit'];
        $month=$smsdata['month'];
        $unit= $smsdata['unit'];
        $msg ="V.no: $unit, Bill month: $month, Bill amount: $amount, unit consumed: $consume_unit, payby: $paydate, Regards, VIA";
        $message = rawurlencode($msg);
             $sender = urlencode('VIAWSe');
             $data = array('apikey' => $apiKey, 'numbers' => $numbers, "sender" => $sender, "message" => $message);

        // Send the POST request with cURL
        $ch = curl_init('https://api.textlocal.in/send/');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
                }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CreateBill  $createBill
     * @return \Illuminate\Http\Response
     */
    public function show($createBill)
    {
        $createBill = CreateBill::find($createBill);
        return view('bills.show', ['createBill' => $createBill]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CreateBill  $createBill
     * @return \Illuminate\Http\Response
     */
    public function edit($createBill)
    {
        if(Auth::user()->id !='1'){
            // dd(Auth::user()->id );
            return redirect()->back()->with('status', 'You have not rights to change the bill');   
           
        }
        $bill = CreateBill::find($createBill);
        $status = '';
        if ($bill
            ->meter->status == 'inactive')
        {
            $deactive_on = Carbon::parse($bill
                ->meter
                ->deactivate_date);
            $bill_created_on = Carbon::parse($bill->created_at);
            $result = $deactive_on->lt($bill_created_on); //meter deactive is less then bill created date i.e bill generated after meetr deactive
            // dd($result);
            if ($result)
            {
                $status = 'inactive';
                // return redirect()->route('createbill')->with('status', 'Meter is deactive bill not be editable..');
                
            }

        }
        $connection = Connection::where('id', $bill->connections_id)
            ->first();
        $connectioncal = Connection::where('id', $bill->connections_id)
            ->orderBy('id', 'DESC')
            ->first()
            ->load('meter');

        $bills = CreateBill::where('id', '<', $createBill)->where('connections_id', $bill->connections_id)
            ->where('paid_status', 'unpaid')
            ->orderBy('id', 'DESC')
            ->first();

        if (empty($bills))
        {
            $avgtotal = 0;
        }
        else
        {

            $avgtotal = $bills->total;
        }
        // dd($avgtotal)
        $allbills = CreateBill::where('connections_id', $bill->connections_id)->get();

        return view('bills.edit', ['allbills'=>$allbills->count(),'status' => $status, 'avgtotal' => $avgtotal, 'bill' => $bill, 'connection' => $connection]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CreateBill  $createBill
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $createBill)
    {
        // dd($request);
        if ($request->meter_status == 'active')
        {
            $request->validate([

            'conn_no' => 'required', 'unit' => 'required|integer', 'billing_month' => 'required', 'consume' => 'required', 'total' => 'required',

            ]);
        }
        else
        {
            $request->validate([

            'conn_no' => 'required', 'billing_month' => 'required',

            ]);
        }

        $bill = CreateBill::find($createBill);
        if($bill->pre_bill_due != $request->predue){
            $bill->total = $request->total;
            $bill->bill_total = $request->bill_total;
            $bill->intrest = $request->dueamt;
            $bill->late_payment = $request->latecharge;
            $bill->bill_to_pay = $request->bill_to_pay;
           $bill->pre_bill_due = $request->predue;
           $bill->save();


        } else{

        if ($bill->unit != $request->unit)
        {
            $ttl = 0;
            $intrest = 0;
            $late_payment = 0;
            $unit = 0;
            $total = 0;
            $lastunit = $request->unit;
            // $crdeitdiff = $bill->to$request->total; 
            $prevbills = CreateBill::where('id', '>', $createBill)->where('connections_id',$bill->connections_id)->where('paid_status', 'unpaid')
                ->orderBy('id', 'ASC')
                ->get();
            $ttl = $request->total;

            
				if($bill->minimum_charge > $request->bill_total){
				$prevCapital = $bill->minimum_charge;
				}else{
					$prevCapital = $request->bill_total;
					}
			
			

            //  dd($prevCapital);
            // $pre_bill_due = $request->pre_bill_due;
         
        
        if(isset($request->credit_rem)){
            $remcredit = $request->credit_rem;
        }else{
            $remcredit=0;
        }
        $bill->connections_id = $request->conn_no;
        $bill->unit = $request->unit;
        $bill->billing_month = $request->billing_month;
        $bill->consume_unit = $request->consume;
        $bill->total = $request->total;
        $bill->due = $request->due;
        $bill->bill_total = $request->bill_total;
        $bill->intrest = $request->intrest;
        $bill->last_unit = $request->last_unit;
        $bill->late_payment = $request->late_payment;
        $bill->adjustment_amt = $request->adjustment_amt;
        $bill->adjustment_reason = $request->adjustment_reason;
        $bill->meter_damage_plenty = $request->meter_damage_plenty;
        // $bill->credit = $remcredit;
        $bill->bill_to_pay = $request->bill_to_pay;

         $connection = Connection::where('id', $bill->connections_id)
        ->first();


       
        $bill_data = CreateBill::find($createBill);
        $bill->save();

        if ($bill_data->credit >= $request->total)
        {
            $bill_data->pay_by = 'credit';
            $bill_data->paid_amount = $request->total;
            $bill_data->status = 'Pay by credit amount';
            $bill_data->paid_date = date('Y-m-d H:i:s');
            $bill_data->paid_status = 'paid';
            $bill_data->credit = $bill_data->credit -$request->total;
             $bill_data->debit = 0;
            $bill_data->bill_to_pay =0;
            $bill_data->pre_bill_due = 0;
             $connection->debit = 0;
             $connection->credit = $bill_data->credit -$request->total;
            //  $connection->credit = $connection->credit ;
            $bill_data->save();
            $connection->save();
            $sno = new Sno;
            $sno->bill_id = $bill_data->id;
            $sno->save();
        }

        else
        {  
            // dd($connection);
            $rem = $request->total - $bill_data->credit;
            
            // $bill_data->credit = $connection->credit;
            $bill_data->bill_to_pay = $rem;
            // $bill_data->pre_bill_due = $connection->debit;

            $bill_data->save();

             $connection->debit = $rem;
            // dd($connection->debit);
              $connection->credit = 0;
            $connection->save();

        }

        $bill = CreateBill::find($createBill);


        if ($prevbills->count()>0)
        { 
            foreach ($prevbills as $prebill)
            {
                $changebill = CreateBill::find($prebill->id);
                $intrest = round(($prevCapital / 100) * 17, 2);
                $late_payment = round(($intrest / 100) * 3, 2);
                $total = $prebill->bill_total + $intrest + $late_payment + $ttl;
                $unit = $lastunit + $prebill->consume_unit;

                $changebill->unit = $unit;
                $changebill->last_unit = $lastunit;
                $changebill->intrest = $intrest;
                $changebill->late_payment = $late_payment;
                $changebill->total = $total;
                $changebill->pre_bill_due= $ttl;
                $changebill->bill_to_pay= $total-$changebill->credit;
                
                if($changebill->minimum_charge > $changebill->bill_total){
                    $prevCapital = $changebill->minimum_charge;
                    }else{
                        $prevCapital = $changebill->bill_total;
                        }

                $changebill->save();
                $lastunit = $unit;
                $ttl = $total;
                $pre_bill_due= $total;
               

            }
            Meter::where('id', $bill->meter_id)
                ->where('status', 'active')
                ->update(['current_unit' => $lastunit]);
            Connection::where('id', $bill->connections_id)
            ->update(['debit' => $ttl]);
        }
    
    else
    {
        Meter::where('id', $bill->meter_id)
            ->where('status', 'active')
            ->update(['current_unit' => $request->unit]);
    }
  }
    }    // return redirect()->route('createbill');
        return redirect()
            ->route('createbill')
            ->with('status', 'Bill updated succesfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CreateBill  $createBill
     * @return \Illuminate\Http\Response
     */
    public function destroy(CreateBill $createBill)
    {
        //
        
    }

    public function viewbill($createBill)
    {
        $bill = CreateBill::find($createBill)->load('meter')
            ->load('conn');
        $unpaid_bills = '';
        $predue = CreateBill::where('id', '<', $bill->id)
        ->where('paid_status', 'unpaid')
            ->orderBy('id', 'DESC')
            ->first();
        if ($bill->unpaid_bill_after_meter_deactivate <= 2)
        {
            $unpaid_bills = CreateBill::where('id', '<', $bill->id)
                ->orderBy('id', 'DESC')
                ->take(3)
                ->get();

        }
        $prev_consumption = CreateBill::select('consume_unit','billing_month')
        ->where('id', '<', $bill->id)->where('connections_id',$bill->conn->id)
        ->orderBy('id', 'DESC')
        ->take(3)
        ->get();
        return view('bills.view', ['prev_consumption'=>$prev_consumption,'unpaid_bills' => $unpaid_bills, 'predue' => $predue, 'bill' => $bill]);

    }

    public function paybill()
    {
        // $bill = CreateBill::find($createBill)
        //  ->load('meter')
        //  ->load('conn');
        //  $predue = CreateBill::where('id','<',$bill->id)->orderBy('id', 'DESC')->first();
        return view('bills.payment');

    }

    public function getbilldetail(Request $request)
    {
        //    dd($request);
        if ($request->type == 'manual')
        {         

            if ($request->id != '')
            {
                $bill = CreateBill::find($request->id);
                if (empty($bill))
                {
                    $data = '<h2>No record found..</h2>';
                    echo json_encode($data);
                    exit;
                }

                // if($bill->paid_status=='paid'){
                //     $data ='<h2>Bill paid already..</h2>';
                //     echo  json_encode($data);
                //  exit;
                // }
                $newbilldata = CreateBill::where('connections_id', $bill->connections_id)
                    ->orderBy('id', 'DESC')
                    ->first();
                if ($newbilldata->id > $bill->id)
                {
                    //  dd($newbilldata);
                    $data = '<h2>New bill is generated..</h2>';
                    echo json_encode($data);
                    exit;
                }
                $bill = CreateBill::find($request->id)
                    ->load('meter')
                    ->load('conn');
            }
            else
            {
                $connections = Connection::where('conn_no', $request->conn_id)
                    ->first();
                if (empty($connections))
                {
                    $data = '<h2>No record found..</h2>';
                    echo json_encode($data);
                    exit;
                }
                $bill = CreateBill::where('connections_id', $connections->id)
                    ->orderBy('id', 'DESC')
                    ->first();

                if (empty($bill))
                {
                    $data = '<h2>No record found..</h2>';
                    echo json_encode($data);
                    exit;
                }

                if ($bill->paid_status == 'paid')
                {
                    $data = '<h2>Bill paid already..</h2>';
                    echo json_encode($data);
                    exit;
                }
                $bill = CreateBill::where('connections_id', $connections->id)
                    ->orderBy('id', 'DESC')
                    ->first()
                    ->load('meter')
                    ->load('conn');
            }
        }
        else
        {  
            if ($request->id)
            {
                $bill = CustomBill::where('voucher_no', $request->id)
                    ->first();
                if (empty($bill))
                {
                    $data = '<h2>No record found..</h2>';
                    echo json_encode($data);
                    exit;
                }

                // if($bill->paid_status=='paid'){
                //     $data ='<h2>Bill paid already..</h2>';
                //     echo  json_encode($data);
                //  exit;
                // }
                $newbilldata = CustomBill::where('connection_id', $bill->connection_id)
                ->where('paid_status','unpaid')
                    ->orderBy('id', 'DESC')
                    ->first();
                // if ($newbilldata->id > $bill->id)
                // {
                //     //  dd($newbilldata);
                //     $data = '<h2>New bill is generated..</h2>';
                //     echo json_encode($data);
                //     exit;
                // }
                $bill = CustomBill::where('voucher_no', $request->id)
                    ->first()
                    ->load('conn');
            }
            else
            {
                $connections = Connection::where('conn_no', $request->conn_id)
                    ->first();
                if (empty($connections))
                {
                    $data = '<h2>No record found..</h2>';
                    echo json_encode($data);
                    exit;
                }
                $bill = CustomBill::where('connection_id', $connections->id)
                    ->orderBy('id', 'DESC')
                    ->first();

                if (empty($bill))
                {
                    $data = '<h2>No record found..</h2>';
                    echo json_encode($data);
                    exit;
                }

                if ($bill->paid_status == 'paid')
                {
                    $data = '<h2>Bill paid already..</h2>';
                    echo json_encode($data);
                    exit;
                }
                $bill = CustomBill::where('connection_id', $connections->id)
                    ->orderBy('id', 'DESC')
                    ->first()
                    ->load('conn');
            }

        }
        if ($request->type == 'manual')
        {

            $data = ' <fieldset class="scheduler-border">
        <legend class="scheduler-border">Pay Bill:</legend> ';
            if ($bill->paid_status == 'paid')
            {
                $data .= '<div class="text-success col-md-12"><h3>Bill payed already</h3></div>
            ';
            }
            $data .= '<div class="row"><div class="col-md-4"> <b>Voucher No: ' . $bill->id . '</b></div><div class="col-md-4">
     <b>Billing month: </b>' . $bill->billing_month . '</div>
        <div class="col-md-4 text-right" ><b>Bill date:</b>' . date("d-m-Y", strtotime($bill->created_at)) . '</div>
     </div>
     <div class="row">
     <div class="col-md-12"><b>Pay by date: </b>' . date('d-m-Y', strtotime($bill->created_at . " + 15days")) . '</div>
     </div>
     <div class="row">
     <div class="col-md-3"><b>Connection No:</b>' . $bill
                ->conn->conn_no . '</div>
     <div class="col-md-3"><b>Party:</b> ' . $bill
                ->conn
                ->party->name . '</div>
     <div class="col-md-3"><b>Billing Month:</b> ' . $bill->billing_month . '</div>
     <div class="col-md-3"><b>Meter Status:</b> ' . $bill
                ->meter->status . '</div>
     </div>
     <div class="row">
     <div class="col-md-12"><b>Address:</b> ' . $bill
                ->conn->address . '</div>
     </div>
     <div class="row">
     <div class="col-md-4"><b>Last Unit:</b> ' . $bill->last_unit . '</div>
     <div class="col-md-4"><b>New Unit:</b> ' . $bill->unit . '</div>
     <div class="col-md-4"><b>Connection Pipe:</b> ' . $bill
                ->conn
                ->conn_type->type . '</div>
     </div>
     <div class="row">
     <div class="col-md-4"><b>Consume Unit:</b> ' . $bill->consume_unit . '</div>
     <div class="col-md-4"><b>Minumum charge:</b> ' . $bill->minimum_charge . '</div>
     <div class="col-md-4"><b>Connection Type:</b> ' . $bill
                ->conn
                ->party
                ->partyType($bill
                ->conn
                ->party_types_id)->type . '</div>
     </div>
     <div class="row mt-5">
     <div class="col-md-4"><b>Amount to be paid:</b> </div>
     <div class="col-md-4"><b> ' . $bill->consume_unit . '*'.$bill->per_unit_charge.' </b> </div>
     <div class="col-md-4"><b> ' . $bill->bill_total . ' </b> </div>
     </div>     
     <div class="row">
     <div class="col-md-4"><b>Previous Dues:</b> </div>
     <div class="col-md-4"> </div>';

            // $predue = CreateBill::where('id', '<', $bill->id)
            //     ->orderBy('id', 'DESC')
            //     ->first();
            // if (!empty($predue))
            // {
                if ($bill->pre_bill_due >0)
                {
                    $data .= '<div class="col-md-4"><b>' . $bill->pre_bill_due . '</b> </div>
     </div>

     <div class="row">
     <div class="col-md-4"><b>Intrest due to late payment:</b> </div>
     <div class="col-md-4">17% on due </div>
     <div class="col-md-4"><b>' . $bill->intrest . '</b> </div>
     </div>

     <div class="row">
     <div class="col-md-4"><b> Plenty on late payment:</b> </div>
     <div class="col-md-4">3% on 17% </div>
     <div class="col-md-4"><b>' . $bill->late_payment . '</b> </div>
     </div>';
                }
            
            
            $data .= '</div><div class="row">
     <div class="col-md-4"><b> Plenty on meter damage:</b> </div>
     <div class="col-md-4"> </div>
     <div class="col-md-4"><b>' . $bill->meter_damage_plenty . '</b> </div>
     </div>

     <div class="row">
     <div class="col-md-4"><b>' . $bill->adjustment_reason . '</b> </div>
     <div class="col-md-4"> </div>
     <div class="col-md-4"><b>' . $bill->adjustment_amt . '</b> </div>
     </div>    ';

     if($bill->plenty>0){
        $data .= '</div><div class="row">';
        if($bill->unpaid_bill_after_meter_deactivate==2){
        $data .= ' <div class="col-md-4"><b> 50% plenty due to meter inactive:</b> </div>';
         }
         if($bill->unpaid_bill_after_meter_deactivate==3){
            $data .= ' <div class="col-md-4"><b> 70% plenty due to meter inactive:</b> </div>';
             }
             if($bill->unpaid_bill_after_meter_deactivate==4){
                $data .= ' <div class="col-md-4"><b> 100% plenty due to meter inactive:</b> </div>';
                 }
                 if($bill->unpaid_bill_after_meter_deactivate>=5){
                    $data .= ' <div class="col-md-4"><b> Fix plenty of 5000 due to unpaid bill of more than 5 month:</b> </div>';
                     }

            $data .= '<div class="col-md-4"> </div>';
            $data .= '<div class="col-md-4"><b>'.$bill->plenty.' INR</b> </div>
            </div>';
           
    }

    $data .= ' <div class="row mt-2">
    <div class="col-md-4"><b> Credit amount:</b> </div>
    <div class="col-md-4"> </div>
    <div class="col-md-4"><b>(-'.$bill->credit.') INR</b> </div>
    </div>';
            // if($bill->conn->debit>0){
            //     $bill->total = $bill->total+$bill->conn->debit;
            //     $data.='<div class="row">
            //     <div class="col-md-4"><b> Connection debit amount:</b> </div>
            //     <div class="col-md-4"> </div>
            //     <div class="col-md-4"><b>(+'.$bill->conn->debit.')</b> </div>
            //     </div>';
            // }
            // if($bill->conn->credit>0){
            // $bill->total = $bill->total-$bill->conn->credit;
            // $data.='<div class="row">
            // <div class="col-md-4"><b> Connection credit amount:</b> </div>
            // <div class="col-md-4"> </div>
            // <div class="col-md-4"><b>(-'.$bill->conn->credit.')</b> </div>
            // </div>';
            // }
            $data .= ' <div class="row mt-5">
     <div class="col-md-4"> </div>
     <div class="col-md-4"> </div>
     <div class="col-md-4"><b><hr/></b> </div>
     </div>
     <div class="row mt-2">
     <div class="col-md-4"> </div>
     <div class="col-md-4"><b>Total </b></div>
     <div class="col-md-4"><b><span id="">' . $bill->total . '</span></b> </div>
     </div> 
     <div class="row mt-2">
     <div class="col-md-4"> </div>
     <div class="col-md-4"><b>Amount to be paid </b></div>
     <div class="col-md-4"><b><span id="total">' . $bill->bill_to_pay . '</span></b> </div>
     </div>';

            if ($bill->paid_status == 'paid')
            {
                $data .= '   
        <div class="form-row">
        <input type="hidden" name="bill_id" value="' . $bill->id . '"/>
        <div class=" col-md-6">
        <label for="pay_by">Pay Throught</label>
        <select id="pay_by" class="form-control" name="pay_by" required>
           <option value="' . $bill->pay_by . '">' . $bill->pay_by . '</option> 
        </select> 
        </div>
   
        <div  class="col-md-6">
        <label for="check_no">Cheque No</label> 
        <input readonly class="form-control" value="' . $bill->check_no . '" id="check_no" type="text" disabled name="check_no" required />
        </div>
   
        <div class="col-md-6">
        <label for="amount">Enter Amount</label> 
        <input readonly class="form-control" id="amount" value="' . $bill->paid_amount . '" type="number" name="amount" required />
        </div>
   
        <div class=" col-md-6">
        <label for="debit">Debit Amount</label> 
        <input readonly class="form-control" id="debit" value="' . $bill->debit . '" type="number" readonly name="debit"  />
        </div>
        
        <div class=" col-md-6">
        <label for="credit">Credit Amount</label> 
        <input readonly class="form-control" id="credit" value="' . $bill->credit . '" type="number" readonly name="credit" />
        </div>
   
        <div class=" col-md-6">
        <label for="staus">Payment Status</label>
        <select id="status" class="form-control" name="status" >
           <option value="' . $bill->status . '">' . $bill->status . '</option>       
        </select> 
        </div>
 
        </div>
     
     </fieldset>';
            }
            else
            {
                $data .= '<form method="POST" action="' . route("adminbillpay") . '">
     <input type="hidden" name="_token" value="' . csrf_token() . ' " />

     <div class="form-row">
     <input type="hidden" name="bill_id" value="' . $bill->id . '"/>
     <div class=" col-md-6">
     <label for="pay_by">Pay Throught</label>
     <select id="pay_by" class="form-control" name="pay_by" required>
        <option value="">Choose...</option>       
        <option value="cheque">Cheque</option>
        <option value="cash">Cash</option>
     </select> 
     </div>

     <div  class="col-md-6">
     <label for="check_no">Cheque No</label> 
     <input class="form-control" id="check_no" type="text" disabled name="check_no" required />
     </div>

     <div class="col-md-6">
     <label for="amount">Enter Amount</label> 
     <input class="form-control" id="amount" type="number" name="amount" required />
     </div>

     <div class=" col-md-6">
     <label for="debit">Debit Amount</label> 
     <input class="form-control" id="debit" type="number" readonly name="debit"  />
     </div>
     
     <div class=" col-md-6">
     <label for="credit">Credit Amount</label> 
     <input class="form-control" id="credit" type="number" readonly name="credit" />
     </div>

     <div class=" col-md-6">
     <label for="staus">Bank Name</label>
     <input type="text" name="bank" id="bank" class="form-control"/>
     </div>
<div class="col-md-12 mt-4">
     <input type="submit" class="btn btn-success" value="Submit"/>
    </div>
     </div>
     </form>
  </fieldset>';
            }
        }
        else
        {

            $data = ' <fieldset class="scheduler-border">
    <legend class="scheduler-border">Pay Bill:</legend> ';
            if ($bill->paid_status == 'paid')
            {
                $data .= '<div class="text-success col-md-12"><h3>Bill payed already</h3></div>
        ';
            }
            $data .= '<div class="row"><div class="col-md-4"> <b>Voucher No: ' . $bill->voucher_no . '</b></div><div class="col-md-4">
 <b>Billing month: </b>' . date('M-Y', strtotime($bill->created_at)) . '</div>
    <div class="col-md-4 text-right" ><b>Bill date:</b>' . date("d-m-Y", strtotime($bill->created_at)) . '</div>
 </div>
 <div class="row">
     <div class="col-md-12"><b>Pay by date: </b>' . date('d-m-Y', strtotime($bill->created_at . " + 15days")) . '</div>
     </div>
     <div class="row">
     <div class="col-md-3"><b>Connection No:</b>' . $bill
                ->conn->conn_no . '</div>
     <div class="col-md-3"><b>Party:</b> ' . $bill
                ->conn
                ->party->name . '</div>
     <div class="col-md-3"><b>Billing Month:</b> ' . date('M-Y', strtotime($bill->created_at)) . '</div>
     <div class="col-md-3"><b>Meter Status:</b> ' . $bill
                ->meter_status . '</div>
     </div>
     <div class="row">
     <div class="col-md-12"><b>Address:</b> ' . $bill
                ->conn->address . '</div>
     </div>
     <div class="row mt-5">
     <div class="col-md-12"><b>Title : </b> ' . $bill->title . '</div>
     </div>';
            $datas = json_decode($bill->data, true);
            $amounts = json_decode($bill->amount, true);
            foreach ($datas as $key => $mydata)
            {
                $data .=   '<div class="row mt-3">
     <div class="col-md-4"><b>Reason:</b> ' . $mydata . '</div>
     <div class="col-md-4"></div>
     <div class="col-md-4"><b></b> ' . $amounts[$key] . ' INR</div>
     </div>';
            }
            $data .= ' <div class="row mt-5">
       <div class="col-md-4"> </div>
       <div class="col-md-4"> </div>
       <div class="col-md-4"><b><hr/></b> </div>
       </div>
       <div class="row mt-2">
       <div class="col-md-4"> </div>
       <div class="col-md-4"><b>Total </b></div>
       <div class="col-md-4"><b><span id="total">' . $bill->total . '</span></b> </div>
       </div>';

            if ($bill->paid_status == 'paid')
            {
                $data .= '   
        <div class="form-row">
        <input type="hidden" name="bill_id" value="' . $bill->id . '"/>
        <div class=" col-md-6">
        <label for="pay_by">Pay Throught</label>
        <select id="pay_by" class="form-control" name="pay_by" required>
           <option value="' . $bill->pay_by . '">' . $bill->pay_by . '</option> 
        </select> 
        </div>
   
        <div  class="col-md-6">
        <label for="check_no">Cheque No</label> 
        <input readonly class="form-control" value="' . $bill->check_no . '" id="check_no" type="text" disabled name="check_no" required />
        </div>
   
        <div class="col-md-6">
        <label for="amount">Enter Amount</label> 
        <input readonly class="form-control" id="amount" value="' . $bill->pay_amount . '" type="number" name="amount" required />
        </div>
   
        <div class=" col-md-6">
        <label for="debit">Debit Amount</label> 
        <input readonly class="form-control" id="debit" value="' . $bill->debit . '" type="number" readonly name="debit"  />
        </div>
        
        <div class=" col-md-6">
        <label for="credit">Credit Amount</label> 
        <input readonly class="form-control" id="credit" value="' . $bill->credit . '" type="number" readonly name="credit" />
        </div>
   
        <div class=" col-md-6">
        <label for="staus">Payment Status</label>
        <select id="status" class="form-control" name="status" >
           <option value="' . $bill->status . '">' . $bill->status . '</option>       
        </select> 
        </div>
 
        </div>
     
     </fieldset>';
            }
            else
            {
                $data .= '<form method="POST" action="' . route("custombillpay") . '">
     <input type="hidden" name="_token" value="' . csrf_token() . ' " />

     <div class="form-row">
     <input type="hidden" name="bill_id" value="' . $bill->id . '"/>
     <div class=" col-md-6">
     <label for="pay_by">Pay Throught</label>
     <select id="pay_by" class="form-control" name="pay_by" required>
        <option value="">Choose...</option>       
        <option value="cheque">Cheque</option>
        <option value="cash">Cash</option>
     </select> 
     </div>

     <div  class="col-md-6">
     <label for="check_no">Cheque No</label> 
     <input class="form-control" id="check_no" type="text" disabled name="check_no" required />
     </div>

     <div class="col-md-6">
     <label for="amount">Enter Amount</label> 
     <input class="form-control" min="'.$bill->total.'" max="'.$bill->total.'" id="amount" type="number" name="amount" required />
     </div>

     <div class=" col-md-6">
     <label for="debit">Debit Amount</label> 
     <input class="form-control" id="debit" type="number" readonly name="debit"  />
     </div>
     
     <div class=" col-md-6">
     <label for="credit">Credit Amount</label> 
     <input class="form-control" id="credit" type="number" readonly name="credit" />
     </div>

     <div class=" col-md-6">
     <label for="staus">Payment Status</label>
     <select id="status" class="form-control" name="status" >
        <option value="">Choose...</option>       
        <option value="Payment clear and send out notification">Payment clear and send out notification</option>
        <option value="Payment due and send notification">Payment due and send notification</option>
     </select> 
     </div>
<div class="col-md-12 mt-4">
     <input type="submit" class="btn btn-success" value="Submit"/>
    </div>
     </div>
     </form>
  </fieldset>';
            }
        }
        echo json_encode($data);

    }

    public function adminbillpay(Request $request)
    {
        // dd($request);
        if ($request->debit > $request->credit)
        {
            $rem = $request->debit - $request->credit;
        }
        else
        {
            $rem = 0;
        }
        $bill_data = CreateBill::find($request->bill_id);
        $bill_data->pay_by = $request->pay_by;
        $bill_data->paid_amount = $request->amount;
        //    $bill_data->debit = $request->debit;
        //    $bill_data->credit = $request->credit;
        $bill_data->status = $request->status;
        $bill_data->check_no = $request->check_no;
        $bill_data->paid_date = date('Y-m-d H:i:s');
        if($request->pay_by=='cheque'){
            $bill_data->paid_status = 'pending';    
        }else{
            $bill_data->paid_status = 'paid';
        }
        $bill_data->remaning_amount = $rem;
        // $bill_data->pre_bill_due=$rem;
        $bill_data->bill_credit = $request->credit;
         $bill_data->bill_debit = $request->debit;
         $bill_data->bank = $request->bank;

        $bill_data->save();
        if($request->pay_by=='cheque'){
            CreateBill::where('connections_id', $bill_data->connections_id)
            ->where('id', '!=', $bill_data->id)
            ->where('paid_status','unpaid')
            ->update(['paid_status' => 'pending','pay_by'=>$request->pay_by, ]);

        }else{

        CreateBill::where('connections_id', $bill_data->connections_id)
            ->where('id', '!=', $bill_data->id)
            ->where('paid_status','unpaid')
            ->update(['paid_status' => 'paid','pay_by'=>$request->pay_by, 'paid_date' => date('Y-m-d H:i:s') ]);

        $connection = Connection::find($bill_data->connections_id);
        $connection->debit = $request->debit;
        $connection->credit = $request->credit;
        $connection->capital = $rem;
        $connection->save();
       
        $sno = new Sno;
        $sno->bill_id = $request->bill_id;
        $sno->save();

        if(isset($connection->phone)){
            $data['amount']=$request->amount;
        $data['paydate']=date('d-F',strtotime($bill_data->paid_date));
        $data['consume_unit']= $bill_data->consume_unit;
        $data['month']=$bill_data->billing_month;
        $data['unit']=$bill_data->id;
        // $this->paysuccess($connection->phone,$data);
        }
    }
        return redirect()
            ->route('createbill')
            ->with('status', 'Bill paid succesfully..');

    }

    public function custombillpay(Request $request)
    {
        //  dd($request);
        if ($request->debit > $request->credit)
        {
            $rem = $request->debit - $request->credit;
        }
        else
        {
            $rem = 0;
        }
        $bill_data = CustomBill::find($request->bill_id);
        // dd($bill_data);
        $bill_data->pay_by = $request->pay_by;
        $bill_data->pay_amount = $request->amount;
        $bill_data->debit = $request->debit;
        $bill_data->credit = $request->credit;
        $bill_data->status = $request->status;
        $bill_data->check_no = $request->check_no;
        $bill_data->paid_date = date('Y-m-d H:i:s');
        $bill_data->paid_status = 'paid';
        $bill_data->rem_amount = $rem;
        $bill_data->save();

        return redirect()
            ->route('custombill')
            ->with('status', 'Bill paid succesfully..');

    }

    public function billbyconn($conn_id)
    {
        $unpaid = 0;
        $paid =0;
        $bills = CreateBill::where('connections_id', $conn_id)->get();
        $custom_bills = CustomBill::where('connection_id', $conn_id)->orderBy('id', 'DESC')->get();
        $connection = Connection::find($conn_id);
        $cusunpaid = CustomBill::where('connection_id', $conn_id)->where('paid_status','unpaid')->orderBy('id','DESC')->first();
        $cuspaid = CustomBill::where('connection_id', $conn_id)->where('paid_status','paid')->sum('total');
        $manunpaid = CreateBill::where('connections_id', $conn_id)->where('paid_status','unpaid')->orderBy('id','DESC')->first();
        $manpaid = CreateBill::where('connections_id', $conn_id)->where('paid_status','paid')->sum('total');
 if(isset($cusunpaid->total)){
$cusunpaid = $cusunpaid->total;
 } else{
    $cusunpaid =0;
 }
 if(isset($manunpaid->total)){
$manunpaid = $manunpaid->total;
} else{
 $manunpaid=0;   
} 
 $unpaid =$cusunpaid+$manunpaid;
 
$paid = $manpaid + $cuspaid;

        return view('bills.allbills', ['paid'=>$paid,'unpaid'=>$unpaid,'custom_bills' => $custom_bills, 'bills' => $bills, 'connection' => $connection]);

    }


    public function chequeedit(Request $request){
        $bill = CreateBill::find($request->bill_id);
if($request->status=='unpaid'){

    CreateBill::where('connections_id', $bill->connections_id)
    ->where('id', '!=', $bill->id)
    ->where('paid_status','pending')
    ->update(['paid_status' => 'unpaid','pay_by'=>null, 'paid_date' => null,'bill_credit'=>0, 'bill_debit'=>0, ]);

    $bill->paid_status = 'unpaid';
    $bill->bill_debit = 0;
    $bill->bill_credit = 0;
    $bill->pay_by = null;
    $bill->paid_date = null;
    $bill->bank = null;
    $bill->save();
}else{
    if ($bill->debit > $bill->credit)
    {
        $rem = $bill->debit - $bill->credit;
    }
    else
    {
        $rem = 0;
    }
        CreateBill::where('connections_id', $bill->connections_id)
        ->where('id', '!=', $bill->id)
        ->where('paid_status','pending')
        ->update(['paid_status' => 'paid','pay_by'=>'cheque', 'paid_date' => date('Y-m-d H:i:s') ]);
        
    $connection = Connection::find($bill->connections_id);
    $connection->debit = $bill->bill_debit;
    $connection->credit = $bill->bill_credit;
    $connection->capital = $rem;
    $connection->save();

  

    $bill->paid_status = 'paid';
    $bill->save();
    
    $sno = new Sno;
    $sno->bill_id = $request->bill_id;
    $sno->save();

    $data['amount']=$bill->paid_amount;
    $data['paydate']=date('d-F',strtotime($bill->paid_date));
    $data['consume_unit']= $bill->consume_unit;
    $data['month']=$bill->billing_month;
    $data['unit']=$bill->id;
    
    if(isset($connection->phone)){
        // dd($data);
        $this->paysuccess($connection->phone,$data);
        }
  

    }
    return redirect()
        ->route('createbill')
        ->with('status', 'Bill chnage succesfully..');

    }


    public function paysuccess($mobile,$data){
        $apiKey = urlencode('OGJiNWQ2ZDhhOTI3OTRjNTllZDUwNGQ5YjUwYWU4MTA=');
        $numbers = $mobile;
        $sender = urlencode('VIAWSe');
        $amount = $data['amount'];
        $paydate=$data['paydate'];
        $consume_unit = $data['consume_unit'];
        $month=$data['month'];
        $unit= $data['unit'];
        $msg ="Receipt for payment : Amount received :$amount: pay date :$paydate,voucher no.$unit,bill month: $month, thanks for payment, Regards VIA water services.";
         $message = rawurlencode($msg);
        $data = array('apikey' => $apiKey, 'numbers' => $numbers, "sender" => $sender, "message" => $message);
         // Send the POST request with cURL
        $ch = curl_init('https://api.textlocal.in/send/');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
        
       }
    

       public function viewrecipt($createBill)
       {

           $bill = CreateBill::find($createBill)->load('meter')
               ->load('conn');
               if($bill->paid_status=='unpaid'){
                return redirect()->back();

               }
           $unpaid_bills = '';
           $predue = CreateBill::where('id', '<', $bill->id)
           ->where('paid_status', 'unpaid')
               ->orderBy('id', 'DESC')
               ->first();
           if ($bill->unpaid_bill_after_meter_deactivate <= 2)
           {
               $unpaid_bills = CreateBill::where('id', '<', $bill->id)
                   ->orderBy('id', 'DESC')
                   ->take(3)
                   ->get();
   
           }
           $prev_consumption = CreateBill::select('consume_unit','billing_month')->where('id', '<', $bill->id)
           ->orderBy('id', 'DESC')
           ->take(3)
           ->get();
           return view('bills.recipt', ['prev_consumption'=>$prev_consumption,'unpaid_bills' => $unpaid_bills, 'predue' => $predue, 'bill' => $bill]);
   
       }



}

