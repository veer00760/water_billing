<?php

namespace App\Http\Controllers;

use App\Models\ExpencesAccount;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Validator;
use App\Models\ExpencesBill;
use Carbon\Carbon;
class ExpencesAccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $accounts = ExpencesAccount::orderBy('id', 'DESC')->get();
        $today = ExpencesBill::whereDate('created_at', Carbon::today())->sum('amount');
        $month = ExpencesBill::whereMonth('created_at', date('m'))->whereYear('created_at', date('Y'))->sum('amount');
        $year = ExpencesBill::whereYear('created_at', date('Y'))->sum('amount');
        return view('expencesaccount.list',['year'=>$year,'month'=>$month,'today'=>$today,'accounts'=>$accounts]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    { 
        return view('expencesaccount.add');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'name' => 'required|string|max:255|unique:expences_accounts,name',
            ])->validate();

            $accountid = ExpencesAccount::create([
                'name' => $request->name,
            ]);

            return redirect()->route('expensesaccount')->with('status', 'Account added  succesfully');
       

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ExpencesAccount  $expencesAccount
     * @return \Illuminate\Http\Response
     */
    public function show(ExpencesAccount $expencesAccount)
    {
        return view('expencesaccount.show',['expencesAccount'=>$expencesAccount]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ExpencesAccount  $expencesAccount
     * @return \Illuminate\Http\Response
     */
    public function edit(ExpencesAccount $expencesAccount)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ExpencesAccount  $expencesAccount
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ExpencesAccount $expencesAccount)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ExpencesAccount  $expencesAccount
     * @return \Illuminate\Http\Response
     */
    public function destroy(ExpencesAccount $expencesAccount)
    {
        //
    }

    public function seeallexpances()
    { 
        $accounts = ExpencesAccount::all();
        return view('expencesaccount.viewall',['accounts'=>$accounts]);

    }


    public function getallexpances(Request $request)
    {
        
        //   dd($request);
        // DB::enableQueryLog();
        $exp =  ExpencesBill::query();
        if ($request->party != null) {
            $exp =  $exp->where('expances_account_id',$request->party);

        }
        if ($request->month != null) {
            $exp =  $exp->whereMonth('created_at', $request->month);
        }
        if ($request->year != null) {
            $exp = $exp->whereYear('created_at', $request->year);
        }else{
            $exp = $exp->whereYear('created_at', date('Y'));

        }
       
        // if ($request->party_id != null) {
            $data =  $exp->get()->load('acc');
            // $data = $conn->get()->load('custom_bills');

        // }else{
        //     $data = $conn->get();
        // }


// and then you can get query log

        echo  json_encode($data);
         exit;
      
    }
   

}
