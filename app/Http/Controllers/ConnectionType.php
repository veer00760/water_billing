<?php

namespace App\Http\Controllers;
use App\Models\ConnectionType as ConnType;
use Illuminate\Http\Request;
use DB;
use App\Models\ConnectionTypeHistory;
use Auth;
class ConnectionType extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $conn_types = ConnType::orderBy('id', 'DESC')->get();
        // dd($conn_types);
        return view('conn_type.list',['conn_types'=>$conn_types]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('conn_type.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $request->validate([
            'type' => 'required',
            'minimum_charge' => 'required',
            'charge' => 'required',
            'unmeter_charge' => 'required',

            ]);
            $ConnType = ConnType::create([
                'type' => $request->type,
                'minimum_charge' => $request->minimum_charge,
                'unmeter_charge' => $request->unmeter_charge,
                'charge' => $request->charge,
                'connection_type' => $request->connection_type,

                ]);

            return redirect()->route('conn-type')->with('status', 'Connection type added  succesfully');
            
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $connTypes = ConnectionTypeHistory::where('connection_type_id',$id)->get();
        return view('conn_type.show',['connTypes'=>$connTypes]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $connType = ConnType::find($id);
        return view('conn_type.edit',['connType'=>$connType]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    { 
        $request->validate([
            'type' => 'required',
            'minimum_charge' => 'required',
            'charge' => 'required',
            'unmeter_charge' => 'required',

            ]);

       $connType = ConnType::find($id);

        ConnectionTypeHistory::create([
        'connection_type' => $connType->connection_type,
        'type' => $connType->type,
        'minimum_charge' => $connType->minimum_charge,
        'unmeter_charge' => $connType->unmeter_charge,
        'charge' => $connType->charge,
        'connection_type_id' => $connType->id,
        'user_id' => Auth::user()->id,
        'connection_type'=>$connType->connection_type,

        ]);
       
          
        $connType->connection_type = $request->connection_type;
        $connType->type = $request->type;
            $connType->minimum_charge =$request->minimum_charge;
            $connType->charge = $request->charge;
            $connType->unmeter_charge = $request->unmeter_charge;
            $connType->connection_type = $request->connection_type;
            $connType->save();
            return redirect()->route('conn-type')->with('status', 'Connection type updated  succesfully');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


        /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getslectbox(Request $request)
    {
       
       $connType = ConnType::select('id','type')->where('connection_type',$request->conn_id)
       ->pluck("type","id");
            return response()->json($connType);
    }
    
}
