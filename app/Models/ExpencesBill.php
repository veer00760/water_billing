<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ExpencesBill extends Model
{
    use HasFactory;
    protected $fillable = [
        'expances_account_id',
        'bill_no',
        'description',
        'amount',
        'paid_by',
        'check_no',
        'user_id',

    ];
    public function user(){
        return $this->hasOne(User::class,'id','user_id');
    }

    public function acc(){
        return $this->hasOne(ExpencesAccount::class,'id','expances_account_id');
    }
}
