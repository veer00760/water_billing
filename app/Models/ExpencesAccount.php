<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ExpencesAccount extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'credit',
        'debit',
    ];
}
