<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    use HasFactory;
    protected $fillable = [
        'connection_id',
        'bill_id',
        'status',
        'amount',
        'txnid',
        'hash',
        'mobile',
        'email',
        'payu_money_id',
        'mode',
        'payid',
        'paystatus',
    ];

    public function conn(){
        return $this->hasOne(Connection::class,'id','connection_id');
    } 
}
