<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ConnectionCharge extends Model
{
    use HasFactory;
    protected $fillable = [
        'connections_id',
        'connection_charge',
        'deposit',
        'road_crossing',
        'minimum_charge',
        'type',
        'paid_through',
        'check_no',
        'note',
    ];
}
