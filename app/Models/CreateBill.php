<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CreateBill extends Model
{
    use HasFactory;
    protected $fillable = [
        'billing_month',
        'connections_id',
        'unit',
        'consume_unit',
        'due',
        'adjustment_reason',
        'adjustment_amt',
        'total',
        'meter_damage_plenty',
        'total',
        'bill_total',
        'intrest',
        'late_payment',
         'last_unit',
         'meter_id',
         'check_no',
         'status',
         'plenty',
         'pay_by',
         'debit',
         'credit',
         'return_amt',
         'paid_date',
         'paid_amount',
         'lastdue',
         'remaning_amount',
         'per_unit_charge',    
         'meter_status',
         'unpaid_bill_after_meter_deactivate',
         'bill_to_pay',
         'pre_bill_due',
         'minimum_charge',
         'bill_credit', 
         'bill_debit',
         'bank',

    ];
    public function conn(){
        return $this->hasOne(Connection::class,'id','connections_id');
    }
    public function meter(){
        return $this->hasOne(Meter::class,'id','meter_id');
    }

    public function trans_no(){
        return $this->hasOne(Payment::class,'bill_id','id')->orderBy('id', 'DESC');
    }

    public function sno(){
        return $this->hasOne(Sno::class,'bill_id','id')->orderBy('id', 'DESC');
   
    }

}
