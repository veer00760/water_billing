<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Customer extends Authenticatable
{
    use HasFactory;
    use Notifiable;

    protected $fillable = [
        'connection_id',
        'email',
        'password',
        'party_id',
        'mobile',

    ];
    

    protected $hidden = ['password',  'remember_token'];
}
