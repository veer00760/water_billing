<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;
class Party extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'email',
        'party_types_id',
        'address',
    ];

    public function conn(){
        return $this->hasMany(Connection::class,'parties_id','id');
    }
    public function partyType($id){
        return DB::table('party_types')->where('id',$id)->first();
        // return $this->hasOne(Party::class,'id','connection_types_id');
    }
    public function creditbilltotal($id){

        return CustomBill::where('paid_status','unpaid')->where('connection_id',$id)->sum('total');
    }

}
