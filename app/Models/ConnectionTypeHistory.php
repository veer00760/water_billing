<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ConnectionTypeHistory extends Model
{
    use HasFactory;
    protected $fillable = [
        'type',
        'charge',
        'unmeter_charge',
        'minimum_charge',
        'connection_type_id',
        'user_id',
        'connection_type',
    ];

    public function user(){
        return $this->hasOne(User::class,'id','user_id');
    }

}
