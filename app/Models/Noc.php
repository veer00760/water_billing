<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Noc extends Model
{
    use HasFactory;
    protected $fillable = [
        'party_id',
        'connection_id',
        'certificate_no',
        'status',
        'reason',
        'issued_by',
        'user_id',
    ];
    
    public function conn(){
        return $this->hasOne(Connection::class,'id','connection_id');
    }

    
    public function party(){
        return $this->hasOne(Party::class,'id','party_id');
    }
    public function customer(){
        return $this->hasOne(Customer::class,'id','user_id');
    }
}
