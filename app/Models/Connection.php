<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;
class Connection extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'email',
        'conn_no',
        'status',
        'phone',
        'connection_types_id',
        'parties_id',
        'meters_id',
        'address',
        'party_types_id',
    ];

    public function party(){
        return $this->hasOne(Party::class,'id','parties_id');
    }
    public function activity(){
        return $this->hasMany(activity::class,'connections_id','id')->orderBy('id', 'DESC');
    }
    public function meter(){
        return $this->hasOne(Meter::class,'connections_id','id')->orderBy('id', 'DESC');
    }
    public function conn_charge(){
        return $this->hasOne(ConnectionCharge::class,'connections_id','id');
    }
    public function conn_type(){
        return $this->hasOne(ConnectionType::class,'id','connection_types_id');
    }

    public function partyType($id){
        return DB::table('party_types')->where('id',$id)->first();
        // return $this->hasOne(Party::class,'id','connection_types_id');
    }

    public function allcharges(){
        return $this->hasMany(ConnectionCharge::class,'connections_id','id');
    }

    public function total_charges(){
        return $this->hasMany(ConnectionCharge::class,'connections_id','id');
    }

    public function bill(){
        return $this->hasOne(CreateBill::class,'connections_id','id')->orderBy('id', 'DESC');
    }
    public function connType($id){
        return DB::table('connection_types')->where('id',$id)->first();
        // return $this->hasOne(Party::class,'id','connection_types_id');
    }

  

    public function mybill()
    {
        return $this->belongsTo(CreateBill::class,'id','connections_id')->orderBy('id', 'DESC');
    }

    
    public function custom_bills(){
        return $this->hasMany(CustomBill::class,'connection_id','id');
    }
}
