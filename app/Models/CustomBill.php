<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomBill extends Model
{
    use HasFactory;
    protected $fillable = [
        'connection_id',
        'data',
        'amount',
        'total',
        'meter_id',
        'meter_status',
        'paid_status',
        'paid_date',
        'title',
        'debit',
        'credit',
        'voucher_no',
        'pay_amount',
        'rem_amount',
        'pay_by',
        'check_no',
        'previousdue',
        'note',

    ];
    public function conn(){
        return $this->hasOne(Connection::class,'id','connection_id');
    }
}
