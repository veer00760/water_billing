<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ConnectionType extends Model
{
    use HasFactory;
    protected $fillable = [
        'type',
        'charge',
        'unmeter_charge',
        'minimum_charge',
        'user_id',
        'connection_type',
    ];
}
