<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Meter extends Model
{
    use HasFactory;
    protected $fillable = [
        'meter_no',
        'connections_id',
        'installation_date',
        'status',
        'current_unit',
        'innital_meter_reading',
        'closing_unit',
        'deactivate_date',
    ];

    protected $guarded = array('connections_id');

    public function conn(){
        return $this->hasOne(Connection::class,'id','connections_id');
    }

    public function connection()
    {
        return $this->belongsTo(Connection::class,'connections_id','id');
    }

}
