<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Settlement extends Model
{
    use HasFactory;
    protected $fillable = [
        'amount',
        'debit',
        'credit',
        'party_id',
        'paid_by',
        'cheque_no',
    ];

    public function party(){
        return $this->hasOne(Party::class,'id','party_id');
    }
}
