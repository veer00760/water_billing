<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PartyController;
use App\Http\Controllers\MeterController;
use App\Http\Controllers\ConnectionController;
use App\Http\Controllers\ConnectionChargeController;
use App\Http\Controllers\CreateBillController;
use App\Http\Controllers\ExpencesAccountController;
use App\Http\Controllers\ExpencesBillController;
use App\Http\Controllers\CustomBillController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Auth\RegisteredUserController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\ConnectionType;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\NocController;
use App\Http\Controllers\EmailController;
use App\Http\Controllers\DailyReportController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');

})->name('home');;


// Route::get('/dashboard', function () {
//     // return view('dashboard');
// })->middleware(['auth'])->name('dashboard');
//  Route::get('/dashboard', [HomeController::class,'index'])->middleware(['auth','2fa'])->name('dashboard');
 Route::get('/dashboard', [HomeController::class,'index'])->middleware(['auth'])->name('dashboard');

Route::group(['prefix'=>'admin','middleware'=>['auth']],function(){

    Route::resource('/party', PartyController::class);
    Route::resource('/meter', MeterController::class);
    Route::resource('/conn', ConnectionController::class);
    Route::resource('/createbill', CreateBillController::class);
    Route::get('/conndetails/{id}', [ConnectionController::class,'details'])->name('conndetails');
    Route::get('charges',[ConnectionChargeController::class, 'index'])->name('charge');
    Route::post('savecharge',[ConnectionChargeController::class, 'store'])->name('savecharge');
    Route::get('/viewbill/{id}', [CreateBillController::class,'viewbill'])->name('viewbill');
    // Route::get('/viewrecipt/{id}', [CreateBillController::class,'viewrecipt'])->name('viewrecipt');
    Route::post('adminbillpay', [CreateBillController::class,'adminbillpay'])->name('adminbillpay');
    Route::get('/billbyconn/{id}', [CreateBillController::class,'billbyconn'])->name('billbyconn');
    Route::resource('/expensesaccount', ExpencesAccountController::class);
    // Route::get('/expencesbill/{id}', [ExpencesAccountController::class,'expencesbill'])->name('expencesbill');
    Route::resource('/expencesbill', ExpencesBillController::class);
    Route::get('/expencesbilllist/{id}', [ExpencesBillController::class,'list'])->name('expencesbilllist');
    Route::resource('/custombill', CustomBillController::class);
    Route::resource('/conn-type', ConnectionType::class);
    Route::post('custombillpay', [CreateBillController::class,'custombillpay'])->name('custombillpay');
    Route::get('adminprofile', [HomeController::class,'viewprofile'])->name('adminprofile');
    Route::post('settlement', [PartyController::class,'settlement'])->name('settlement');
    Route::get('settlement/{id}', [PartyController::class,'settlementdetail'])->name('settlementdetail');
    Route::post('updateprofile', [HomeController::class,'updateprofile'])->name('updateprofile');
    Route::get('reports', [ExpencesBillController::class,'reports'])->name('reports');
    Route::post('viewreport', [ExpencesBillController::class,'viewreport'])->name('viewreport');
    Route::get('noclist', [NocController::class,'noclist'])->name('noclist');
    Route::get('shownoc/{id}', [NocController::class,'show'])->name('shownoc');
    Route::post('update-noc', [NocController::class,'update_noc'])->name('update-noc');
    Route::get('downloadnoc/{id}', [NocController::class,'createPDF'])->name('downloadnoc');
    Route::get('transactions', [HomeController::class,'transactions'])->name('transactions');
    Route::get('seeall', [NocController::class,'seeall'])->name('seeall');
    Route::get('updatenoti', [NocController::class,'updatenoti'])->name('updatenoti');
    Route::post('getemailbyconn', [ConnectionController::class,'getemailbyconn'])->name('getemailbyconn');
    Route::get('seeallexpances', [ExpencesAccountController::class,'seeallexpances'])->name('seeallexpances');
    Route::post('getallexpances', [ExpencesAccountController::class,'getallexpances'])->name('getallexpances');
    Route::post('getslectbox', [ConnectionType::class,'getslectbox'])->name('getslectbox');
    Route::post('chequeedit', [CreateBillController::class,'chequeedit'])->name('chequeedit');
    Route::resource('counter', DailyReportController::class);
    Route::post('applynoc', [NocController::class,'applynoc'])->name('applynoc');
    Route::get('ledger', [ConnectionController::class,'ledger'])->name('ledger');


});
Route::get('conn-type',[ConnectionType::class, 'index'])->middleware(['auth'])->name('conn-type');
Route::get('party',[PartyController::class, 'index'])->middleware(['auth'])->name('party');
Route::get('meter',[MeterController::class, 'index'])->middleware(['auth'])->name('meter');
Route::get('conn',[ConnectionController::class, 'index'])->middleware(['auth'])->name('conn');
Route::get('createbill',[CreateBillController::class, 'index'])->middleware(['auth'])->name('createbill');
Route::get('paybill',[CreateBillController::class, 'paybill'])->middleware(['auth'])->name('paybill');
Route::post('getconnection',[ConnectionController::class, 'getconnection'])->middleware(['auth'])->name('getconnection');
Route::post('getconnectionbyparty',[ConnectionController::class, 'getconnectionbyparty'])->middleware(['auth'])->name('getconnectionbyparty');
Route::post('getconnectionbyconn',[ConnectionController::class, 'getconnectionbyconn'])->middleware(['auth'])->name('getconnectionbyconn');
Route::get('partybill',[ConnectionController::class, 'partybill'])->middleware(['auth'])->name('partybill');
Route::post('getallconnection',[ConnectionController::class, 'getallconnection'])->middleware(['auth'])->name('getallconnection');
Route::post('getbilldetail', [CreateBillController::class,'getbilldetail'])->name('getbilldetail');
Route::get('/expensesaccount', [ExpencesAccountController::class,'index'])->name('expensesaccount');
Route::get('/expencesbill', [ExpencesBillController::class,'index'])->name('expencesbill');
Route::get('/custombill', [CustomBillController::class,'index'])->name('custombill');
Route::get('/complete-registration', [RegisteredUserController::class,'completeRegistration']);
Route::post('/2fa', function () {
    return redirect(URL()->previous());
})->name('2fa')->middleware('2fa');
Route::get('/login', [CustomerController::class, 'index'])
                ->middleware('guest')
                ->name('login');
Route::post('/customerlogin', [CustomerController::class, 'login'])
                ->middleware('guest')
                ->name('customerlogin');

Route::group(['prefix'=>'customer','middleware'=>['customer']],function(){
    Route::get('/dashboard', [HomeController::class,'customerdashboard']);
    Route::get('bill/{id?}/', [HomeController::class,'customerbill']);
    Route::get('paynow/{id}/', [PaymentController::class,'paynnow']);
    Route::resource('noc', NocController::class);
    Route::get('records/{id?}', [CustomerController::class,'records']);
    Route::get('downloadnoc/{id}', [NocController::class,'downloadnoc'])->name('downloadnoc');
    Route::get('profile', [CustomerController::class,'profile'])->name('profile');
    Route::post('updateprofile', [CustomerController::class,'updateprofile'])->name('updateprofile');
    // Route::get('verifymail/{email}', [CustomerController::class,'verifymail'])->name('verifymail');
    Route::post('updatedata', [CustomerController::class,'updatedata'])->name('updatedata');
    Route::post('resend', [CustomerController::class,'resend'])->name('resend');
    // Route::get('verifymobile/{mobile}', [CustomerController::class,'verifymobile'])->name('verifymobile');
    Route::get('shownocc/{id}', [NocController::class,'customershownoc'])->name('shownocc');
    Route::get('checkmail', [CustomerController::class,'checkmail'])->name('checkmail');
    Route::get('checkmob', [CustomerController::class,'checkmob'])->name('checkmob');
    Route::get('/viewrecipts/{id}', [HomeController::class,'viewrecipt'])->name('viewrecipt');
    Route::get('newconn', [HomeController::class,'newconn'])->name('newconn');


});
Route::get('/customer-reg', [CustomerController::class, 'registration'])
                ->middleware('guest')
                ->name('customer-reg');
Route::get('/cu-reg', [CustomerController::class, 'register'])
->middleware('guest')
->name('cu-reg');

Route::get('/housing-reg', [CustomerController::class, 'housing_register'])
->middleware('guest')
->name('housing-reg');

Route::post('/storecustomer', [CustomerController::class, 'store'])
->middleware('guest')
->name('storecustomer');
Route::get('/viewrecipt/{id}', [CreateBillController::class,'viewrecipt'])->name('viewrecipt');


// Route::post('subscribe-cancel', [
//     'as' => 'subscribe-cancel',
//     'uses' => 'PaymentController@SubscribeCancel'
// ]);

// Route::post('subscribe-response', [
//     'as' => 'subscribe-response',
//     'uses' => 'PaymentController@SubscribeResponse'
// ]);
Route::post('subscribe-response', [PaymentController::class,'SubscribeResponse']);
Route::get('/paysuccess', [PaymentController::class,'paysuccess'])->name('paysuccess');
Route::get('/sendmail', [EmailController::class,'sendmail'])->name('sendmail');
Route::get('/verifyemail', [EmailController::class,'verifyemail'])->name('verifyemail');
Route::post('/confirmemail', [EmailController::class,'confirmemail'])->name('confirmemail');
Route::get('/verifymobile', [EmailController::class,'verifymobile'])->name('verifymobile');
Route::post('/confirmmobile', [EmailController::class,'confirmmobile'])->name('confirmmobile');
Route::post('/resend', [EmailController::class,'resend'])->name('resend');
Route::get('/contact-us', [HomeController::class,'contactus'])->name('contactus');
Route::get('/terms-condition', [HomeController::class,'terms'])->name('terms');

require __DIR__.'/auth.php';


