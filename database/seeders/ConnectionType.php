<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
class ConnectionType extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $types = [
            ['id' => 1, 'type' => '15', 'minimum_charge' => '95' , 'created_at' => '2021-01-07 16:07:42','updated_at' => '2021-01-07 16:07:42',],
            ['id' => 2, 'type' => '20', 'minimum_charge' => '150','created_at' => '2021-01-07 16:07:42','updated_at' => '2021-01-07 16:07:42',],
            ['id' => 3, 'type' => '25', 'minimum_charge' => '200','created_at' => '2021-01-07 16:07:42','updated_at' => '2021-01-07 16:07:42',],
            ['id' => 4, 'type' => 'Boaring', 'minimum_charge' => '250','created_at' => '2021-01-07 16:07:42','updated_at' => '2021-01-07 16:07:42',],
        ];
        DB::table('connection_types')->insert($types);

    }
}
