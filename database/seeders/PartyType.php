<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
class PartyType extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $types = [
            ['id' => 1, 'type' => 'Industry',  'created_at' => '2021-01-07 16:07:42','updated_at' => '2021-01-07 16:07:42',],
            ['id' => 2, 'type' => 'Housing Colony', 'created_at' => '2021-01-07 16:07:42','updated_at' => '2021-01-07 16:07:42',],
         ];
        DB::table('party_types')->insert($types);
    }
}
