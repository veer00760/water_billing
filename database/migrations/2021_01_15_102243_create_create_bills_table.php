<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCreateBillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('create_bills', function (Blueprint $table) {
            $table->id();
            $table->string('billing_month');
            $table->foreignId('connections_id')->constrained();
            $table->float('unit');
            $table->float('consume_unit');
            $table->float('due')->nullable();
            $table->float('adjustment_amt')->nullable();
            $table->string('adjustment_reason')->nullable();
            $table->float('total');
            $table->float('meter_damage_plenty')->nullable();
            $table->enum('paid_status', ['paid', 'unpaid'])->default('unpaid');
            $table->enum('pay_by', ['cheque', 'cash'])->nullable();
            $table->float('return_amt')->default(0);
            $table->float('debit')->default(0);
            $table->float('credit')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('create_bills');
    }
}
