<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMetersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meters', function (Blueprint $table) {
            $table->id();
            $table->string('meter_no')->unique();
            $table->dateTime('installation_date');
            $table->string('meter_test_copy')->nullable();
            $table->string('appliaction_copy')->nullable();
            $table->foreignId('connections_id')->constrained()->nullable();
            $table->enum('status', ['active', 'inactive']);
            $table->integer('innital_meter_reading')->nullable();
            $table->integer('closing_unit')->nullable();
            $table->integer('current_unit')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meters');
    }
}
