<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConnectionChargesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('connection_charges', function (Blueprint $table) {
            $table->id();
            $table->float('connection_charge');
            $table->float('deposit');
            $table->float('road_crossing')->nullable();
            $table->float('minimum_charge');
            $table->enum('type', ['new', 'renew'])->default('new');
            $table->enum('paid_through', ['cash', 'cheque'])->default('cash');
            $table->string('check_no')->nullable();
            $table->string('note')->nullable();
            $table->foreignId('connections_id')->constrained();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('connection_charges');
    }
}
