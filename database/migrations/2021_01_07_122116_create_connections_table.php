<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConnectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('connections', function (Blueprint $table) {
            $table->id();
            $table->string('conn_no');
            $table->string('name');
            $table->string('phone');
            $table->foreignId('connection_types_id');
            $table->foreignId('parties_id')->constrained();
            $table->foreignId('meters_id')->constrained();
            $table->string('email')->unique();
            $table->text('address')->nullable();
            $table->float('debit')->default(0);
            $table->float('credit')->default(0);
            $table->text('deactivate_reason')->nullable();
            $table->enum('status', ['active', 'inactive'])->default('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('connections');
    }
}
