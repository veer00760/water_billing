<!DOCTYPE html>
<html>
<head>
    @include('layouts.customer.head')
    @yield('custom_styles')
    <style>
        #loading {
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  position: fixed;
  display: block;
  opacity: 0.7;
  background-color: #63043d;
  z-index: 99;
  text-align: center;
}

#loading-image {
    position: absolute;
  border: 16px solid #f3f3f3; /* Light grey */
  border-top: 16px solid #3498db; /* Blue */
  border-radius: 50%;
  width: 120px;
  height: 120px;
  animation: spin 2s linear infinite;
  top: 50%;
    left: 50%;
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
        </style>
</head>
<body class="hold-transition skin-blue sidebar-mini body">
<div id="loading" style="display:none">
    <div id="loading-image"></div>
  </div>
@yield('content')

@include('layouts.customer.scripts')

@yield('custom_scripts')

</body>
</html>