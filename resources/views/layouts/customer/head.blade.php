<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>{{ config('app.name') }} | </title>
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="csrf-token" content="{{ csrf_token() }}">

<!-- Bootstrap 3.3.7 -->
<link rel="stylesheet" href="{{ asset('public_folders/panel-resources/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
<!-- Font Awesome -->
<link rel="stylesheet" href="{{ asset('public_folders/panel-resources/bower_components/font-awesome/css/font-awesome.min.css') }}">
<!-- Ionicons -->
<link rel="stylesheet" href="{{ asset('public_folders/panel-resources/bower_components/Ionicons/css/ionicons.min.css') }}">
<!-- Theme style -->
<link rel="stylesheet" href="{{ asset('public_folders/panel-resources/dist/css/BoolFalse.min.css') }}">
<!-- BoolFalse Skins. Choose a skin from the css/skins
     folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="{{ asset('public_folders/panel-resources/dist/css/skins/_all-skins.min.css') }}">
<!-- Morris chart -->
{{--<link rel="stylesheet" href="{{ asset('public_folders/panel-resources/bower_components/morris.js/morris.css') }}">--}}
<!-- jvectormap -->
{{--<link rel="stylesheet" href="{{ asset('public_folders/panel-resources/bower_components/jvectormap/jquery-jvectormap.css') }}">--}}
<!-- Date Picker -->
{{--<link rel="stylesheet" href="{{ asset('public_folders/panel-resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">--}}
<!-- Daterange picker -->
{{--<link rel="stylesheet" href="{{ asset('public_folders/panel-resources/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">--}}
<!-- bootstrap wysihtml5 - text editor -->
<link rel="stylesheet" href="{{ asset('public_folders/panel-resources/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('public_folders/panel-resources/bower_components/select2/dist/css/select2.min.css') }}">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<!-- Google Font -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
<link rel="stylesheet" href="{{ asset('/css/bill.css') }}">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css"> 
<link rel="stylesheet" href="{{ asset('/css/responsive.css') }}">

<style>
     .body{
          min-height: 100%;
    background-color: #2d112d;
    margin-left: 20px;
    margin-right: 20px;
    margin-top: 30px;
     margin-bottom: 30px; 
     }
     .wrapper{
          border-radius:40px;
          padding-bottom: 70px;
     }
     .skin-blue .wrapper, .skin-blue .main-sidebar, .skin-blue .left-side{
          background-color:#fff;
     }
     .mb-3{
          margin-bottom: 15px;
     }
     .mt-3{
          margin-top: 15px;
     }
     .h3-dash{
    border: 1px solid #4b2757e0;
     }
     .bd-btm{
          border-bottom: 2px solid #4b2757e0;
         width: 46%;   
     }
     .btn-success{
          color: #2d112d;
    background-color: #fff;
    border-radius: 8px;  
     }
     .success-btn{
          color:#fff ;
    background-color:#2d112d ;
    border-radius: 8px;  
     }
     .card{
          padding-left: 27px;
    border-radius: 10px;
    padding-right: 27px;
    padding-top:  30px;
    padding-bottom:  15px;
    margin-bottom: 38px;

     }
     .svg-class{
          height:50%;
          width:100%;
     }
     .bg-purples{
          background-color:#2d112d;
          color:#fff;
     }
     .color-purple{
     color:#2d112d;     
     }
  
     .divli{
          font-size: 20px;
    padding-left: 15px;
    margin-top: 10px;
    font-weight: 600;
     }
     ul {
   list-style: none;
}
.btn:hover {
    color: #fff  !important;
}
.active{
     background-color: #2d112d;
    width: 111px;
    border-radius: 16px;
    color: #fff;
    /* padding-left: 31px; */
    font-size: 20px;
     }
     .active a{
          color:#fff;
     }

ul li:before {
   content: "•";
   font-size: 170%; /* or whatever */
   padding-right: 5px;
}
.pd-0{
     padding:0px;
}
.logo{
     
    width: 70%;
    margin: 20px;

}
</style>