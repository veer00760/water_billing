<aside class="main-sidebar">
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">Customer Dashboard</li>

            <li class=""><a href="/customer.dashboard"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
        </ul>
    </section>
</aside>