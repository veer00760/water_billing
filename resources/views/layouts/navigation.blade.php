<nav x-data="{ open: false }" class=" border-b border-gray-100">
    <!-- Primary Navigation Menu -->
    <div class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
        <div class="flex justify-between h-16">
            <div class="flex">
                <!-- Logo -->
                <div class="flex-shrink-0 flex items-center">
                    <a href="{{ route('dashboard') }}">
                        <x-application-logo class="block h-10 w-auto fill-current text-gray-600" />
                    </a>
                </div>

                <!-- Navigation Links -->
                <div class="hidden space-x-8 sm:-my-px sm:ml-10 sm:flex">
                    <x-nav-link :href="route('dashboard')" :active="request()->routeIs('dashboard')">
                        {{ __('Dashboard') }}
                    </x-nav-link>
               
                    <x-dropdown style="padding-top:20px;">
                    <x-slot name="trigger">
                        <button class="flex items-center text-sm font-medium text-gray-500 hover:text-gray-700 hover:border-gray-300 focus:outline-none focus:text-gray-700 focus:border-gray-300 transition duration-150 ease-in-out">
                            <div>Party Wise</div>

                            <div class="ml-1">
                                <svg class="fill-current h-16 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                    <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd" />
                                </svg>
                            </div>
                        </button>
                    </x-slot>

                    <x-slot name="content">
                            <x-dropdown-link  :href="route('partybill')" :active="request()->routeIs('party')">
                            {{ __('Party Wise Bill') }}
                        </x-dropdown-link>

                    <x-dropdown-link :href="route('ledger')" :active="request()->routeIs('createbill')">
                    {{ __('Rough Ledger') }}
                        </x-dropdown-link>

                       
                    </x-slot>
                </x-dropdown>

                 

                    <x-nav-link :href="route('paybill')" :active="request()->routeIs('paybill')">
                        {{ __('Pay Bill') }}
                    </x-nav-link>

                    <x-nav-link :href="route('createbill')" :active="request()->routeIs('custombill')">
                        {{ __('Create Bill') }}
                    </x-nav-link>

                    <x-nav-link :href="route('noclist')" :active="request()->routeIs('noclist')">
                        {{ __('Noc') }}
                    </x-nav-link>

                    <x-dropdown style="padding-top:20px;">
                    <x-slot name="trigger">
                        <button class="flex items-center text-sm font-medium text-gray-500 hover:text-gray-700 hover:border-gray-300 focus:outline-none focus:text-gray-700 focus:border-gray-300 transition duration-150 ease-in-out">
                            <div>Party</div>

                            <div class="ml-1">
                                <svg class="fill-current h-16 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                    <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd" />
                                </svg>
                            </div>
                        </button>
                    </x-slot>

                    <x-slot name="content">
                            <x-dropdown-link  :href="route('party')" :active="request()->routeIs('party')">
                        {{ __('Party') }}
                        </x-dropdown-link>

                    <x-dropdown-link :href="route('custombill')" :active="request()->routeIs('createbill')">
                        {{ __('Custom Bill') }}
                        </x-dropdown-link>

                       
                    </x-slot>
                </x-dropdown>




                    <x-dropdown style="padding-top:20px;">
                    <x-slot name="trigger">
                        <button class="flex items-center text-sm font-medium text-gray-500 hover:text-gray-700 hover:border-gray-300 focus:outline-none focus:text-gray-700 focus:border-gray-300 transition duration-150 ease-in-out">
                            <div>Create</div>

                            <div class="ml-1">
                                <svg class="fill-current h-16 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                    <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd" />
                                </svg>
                            </div>
                        </button>
                    </x-slot>

                    <x-slot name="content">

                    <x-dropdown-link :href="route('meter')" :active="request()->routeIs('meter')">
                        {{ __('Meter') }}
                        </x-dropdown-link>

                    <x-dropdown-link :href="route('conn')" :active="request()->routeIs('conn')">
                        {{ __('Connection') }}
                        </x-dropdown-link>

                        <x-dropdown-link :href="route('expensesaccount')" :active="request()->routeIs('expensesaccount')">
                        {{ __('Expances Account') }}
                        </x-dropdown-link>                      
                       
                    </x-slot>
                </x-dropdown>

                <x-nav-link :href="route('transactions')" :active="request()->routeIs('transactions')">
                        {{ __('Transactions') }}
                    </x-nav-link>
       
                    <x-nav-link :href="route('counter.index')" :active="request()->routeIs('counter')">
                        {{ __('Counter Report') }}
                    </x-nav-link>
                </div>
            </div>


         

            <!-- Settings Dropdown -->
            <div class="hidden sm:flex sm:items-center sm:ml-6">

            <x-dropdown align="right"   width="w-80">
                    <x-slot name="trigger">
                        <button id="noti" class="flex items-center text-sm font-medium text-gray-500 hover:text-gray-700 hover:border-gray-300 focus:outline-none focus:text-gray-700 focus:border-gray-300 transition duration-150 ease-in-out">
                            <div style="margin:20px;"><i class="fa fa-bell bell" aria-hidden="true"></i>
                                                              </div>
@php $allnoti = App\Models\Notification::orderBy('id','DESC')->where('status','0')->take(6)->get(); @endphp
                            <div class="ml-1">
                            <span class=" badge badge-danger my-badge">{{$allnoti->count()}}</span>
                            </div>
                        </button>
                    </x-slot>

                    <x-slot name="content" >
                    <ul class="list-group mylist-group">
                    @foreach($allnoti as $noti)
                     @if($noti->type=='online_payment')
                     @php $myclass ='mysuccess';
                     $link = 'transactions';
                     @endphp
                     @else
                     @php $myclass ='mydanger';
                     $link = 'noclist';
                     @endphp
                     @endif
                    <a href="/admin/{{$link}}"><li class="list-group-item my-item {{$myclass}}">{{$noti->message}}</li></a>
                      
                    @endforeach
                    <span align="right"><a class="btn btn-link" href="/admin/seeall">See all</a></sapn>
                    </ul>

                       
                    </x-slot>
                </x-dropdown>


                <x-dropdown align="right" width="48">
                    <x-slot name="trigger">
                        <button class="flex items-center text-sm font-medium text-gray-500 hover:text-gray-700 hover:border-gray-300 focus:outline-none focus:text-gray-700 focus:border-gray-300 transition duration-150 ease-in-out">
                            <div>{{ Auth::user()->name }}</div>

                            <div class="ml-1">
                                <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                    <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd" />
                                </svg>
                            </div>
                        </button>
                    </x-slot>

                    <x-slot name="content">
                        <!-- Authentication -->
                       <x-dropdown-link :href="route('adminprofile')">
                                {{ __('Profile') }}
                            </x-dropdown-link>
                            <x-dropdown-link :href="route('reports')">
                                {{ __('Reports') }}
                            </x-dropdown-link>

                            <x-dropdown-link :href="route('conn-type')" :active="request()->routeIs('conn-type')">
                        {{ __('Meter Type') }}
                        </x-dropdown-link>

                        <form method="POST" action="{{ route('logout') }}">
                            @csrf

                            <x-dropdown-link :href="route('logout')"
                                    onclick="event.preventDefault();
                                                this.closest('form').submit();">
                                {{ __('Logout') }}
                            </x-dropdown-link>
                        </form>
                   
                    </x-slot>
                   
                    
                </x-dropdown>
            </div>

            <!-- Hamburger -->
            <div class="-mr-2 flex items-center sm:hidden">
                <button @click="open = ! open" class="inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:bg-gray-100 focus:text-gray-500 transition duration-150 ease-in-out">
                    <svg class="h-6 w-6" stroke="currentColor" fill="none" viewBox="0 0 24 24">
                        <path :class="{'hidden': open, 'inline-flex': ! open }" class="inline-flex" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16" />
                        <path :class="{'hidden': ! open, 'inline-flex': open }" class="hidden" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                    </svg>
                </button>
            </div>
        </div>
    </div>

    <!-- Responsive Navigation Menu -->
    <div :class="{'block': open, 'hidden': ! open}" class="hidden sm:hidden">
        <div class="pt-2 pb-3 space-y-1">
            <x-responsive-nav-link :href="route('dashboard')" :active="request()->routeIs('dashboard')">
                {{ __('Dashboard') }}
            </x-responsive-nav-link>
        </div>

        <!-- Responsive Settings Options -->
        <div class="pt-4 pb-1 border-t border-gray-200">
            <div class="flex items-center px-4">
                <div class="flex-shrink-0">
                    <svg class="h-10 w-10 fill-current text-gray-400" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M16 7a4 4 0 11-8 0 4 4 0 018 0zM12 14a7 7 0 00-7 7h14a7 7 0 00-7-7z" />
                    </svg>
                </div>

                <div class="ml-3">
                    <div class="font-medium text-base text-gray-800">{{ Auth::user()->name }}</div>
                    <div class="font-medium text-sm text-gray-500">{{ Auth::user()->email }}</div>
                </div>
            </div>

            <div class="mt-3 space-y-1">
                <!-- Authentication -->
                <form method="POST" action="{{ route('logout') }}">
                    @csrf

                    <x-responsive-nav-link :href="route('logout')"
                            onclick="event.preventDefault();
                                        this.closest('form').submit();">
                        {{ __('Logout') }}
                    </x-responsive-nav-link>
                </form>
            </div>
        </div>
    </div>

    <script>
jQuery( "#noti" ).click(function(e) {
    // e.noConflict();
    jQuery.ajax({
        url: "/admin/updatenoti/",
        dataType:"json",
        type: "get",
        headers: {
                  'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
              },
        success: function(data){   
        }
    });
    // jQuery(".my-badge").text('0');

});

    </script>

</nav>

