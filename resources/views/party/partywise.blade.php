<x-app-layout>
   <x-slot name="header">
      <div class="row">
         <div class="col-md-10">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
               {{ __('Party Wise') }}
            </h2>
         </div>
         <div class="col-md-2">
            <a class="btn btn-primary" href="{{ route('party') }}">
            {{ __('Add List') }}
            </a>
         </div>
      </div>
   </x-slot>
   <div class="py-12">
   <div class="max-w-5xl mx-auto sm:px-6 lg:px-8">
         <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
            <div class="p-6 bg-white border-b border-gray-200">
     
         <div class="form-row">
            <!--Connection  Name -->
            <div class="mt-4 col-m-4">
               <x-label for="billing_month" :value="__('Search Party')" />
               <x-input id="party" class="block mt-1 w-full"
                  type="text"    />
            </div>
            <div class="mt-4 col-m-4">
               <x-label for="connection" :value="__('Search Connection')" />
               <x-input id="connection" class="block mt-1 w-full"
                  type="number"    />
            </div>

            <div class="mt-4 col-m-4">
               <x-label for="billing_month" :value="__('From date')" />
               <x-input id="from_date" class="block mt-1 w-full"
                  type="date" name="from_date"    />
            </div>

            <div class="mt-4 col-m-4">
               <x-label for="conn_type" :value="__('Connection Type')" />
               <select id="conn_type" style="width:150px;" class="block mt-1 w-full form-control"
                  name="conn_type"  >
                  <option value="">Select Type</option>
                  <option value="1">Industry   </option>
                  <option value="2">Housing    </option>
                </select>
            </div>

            </div>
            <div class="form-row">

            <div class="mt-4 col-m-4">
               <x-label for="to_date" :value="__('To date')" />
               <x-input id="to_date" class="block mt-1 w-full"
                  type="date" name="to_date"    />
            </div>

            <div class="mt-4 col-m-4">
               <x-label for="amount" :value="__('Enter amount')" />
               <x-input id="amount" class="block mt-1 w-full"
                  type="number"     />
            </div>

            <div class="mt-4 col-md-3">
               <x-label for="conn_type" :value="__('Paid  Type')" />
               <select id="paid_type" style="width:200px;" class="block mt-1 w-full form-control"
                  name="conn_type"  >
                  <option value="">Select Type</option>
                  <option value="paid">Paid   </option>
                  <option value="unpaid">Unpaid </option>
                </select>
            </div>
            
            <div class="flex items-center justify-end mt-5">
                        <x-button id="search" class="ml-4 btn-primary ">
                           {{ __('Search') }}
                        </x-button>
                     </div>
                     <input type="hidden" value="" id="party_id"/>
                     <input type="hidden" value="" id="conn_id"/>

         </div>
      </div>

   <div class="col-md-12  mt-5">
      <div id="hide" style="display:none;" class="bg-white overflow-hidden shadow-sm sm:rounded-lg table-responsive">
         <table class="table mt-50" id="table" >
            <thead>
               <tr>
                  <th scope="col">#</th>
                  <!-- <th scope="col">Voucher No</th> -->
                  <th scope="col">Party Name</th>
                  <th scope="col">Connection No</th>
                  <th scope="col">Connection on name</th>
                  <th scope="col">Debit amount</th>
                  <th scope="col">Credit amount</th>
                   <!-- <th scope="col">Due amount</th> -->
                  <th scope="col">Action</th>
               </tr>
            </thead>
            <tbody id="data">
            </tbody>
         </table>
      </div>

   </div>
   </div>
   <script>
      // jQuery.noConflict();
      // jQuery(document).ready(function(e) {
      //   e.noConflict();
      //   jQuery('#table').DataTable();
      // } );
      $(document).ready(function() {
     
      } );
      $( "#party" ).autocomplete({
      source: function( request, response ) {
      // Fetch data
      $.ajax({
      url: "/getconnectionbyparty",
         data: {
            value: request.term
          },
        dataType:"json",
        type: "post",
        headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
      success: function( data ) {
      console.log(data);
      //   response( data );
      response($.map(data, function (item) {
            return {
                label: item.name,
                value: item.id
            };
        }));
      }
      });
      },
      select: function (event, ui) {
      // alert(ui.item.value);
      // Set selection
      $('#party').val(ui.item.label); // display the selected text
      $('#party').val(ui.item.label); // save selected id to input
      $('#party_id').val(ui.item.value);
      // getdata(ui.item.value);
      return false;
      }
      });

      $("#search").click(function() {
 var party_id = $("#party_id").val();
//  if(!party_id){
//    alert("please select party");
//    return false;
//  }
var conn_id = $("#conn_id").val();
 var from_date = $("#from_date").val();
 var to_date = $("#to_date").val();
 var amount = $("#amount").val();
//  var amount = $("#conn_type").val();
var conn_type = $( "#conn_type option:selected" ).val();
var paid_type =$( "#paid_type option:selected" ).val();
 getdata(paid_type,conn_type,party_id,conn_id,from_date,to_date,amount);
      } );
      
      function getdata(paid_type,conn_type,party_id,conn_id,from_date,to_date,amount){
      $.ajax({
        url: "/getallconnection",
        data: { "paid_type": paid_type,"conn_type": conn_type,"party_id": party_id,"conn_id":conn_id,"from_date": from_date,"to_date": to_date,"amount": amount },
        dataType:"Json",
        type: "post",
        headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
        success: function(data){
           console.log(data);
           var html = '';
           var i =0;
                  data.map(function(item){
                   i = parseInt(i+1);
                  //  if(item.debit > 0){
                  //    var classname = 'text-danger';
                  //    var amt = item.debit;
                  //  }else{
                  //   var classname = 'text-success';
                  //    var amt = item.credit;
                  //  }
                   if(item.party){
                      var partyname = item.party.name;
                   }else{
                     var partyname = 'N/A';
                   }
                        html=html+`<tr><td>${i}</td> <td> ${partyname}</td><td>${item.conn_no}</td><td>${item.name}</td><td class="text-danger">${item.debit}</td><td class="text-success">${item.credit}</td><td><a href="/admin/billbyconn/${item.id}">View</a></td></tr>`
                        })
                  
         // $("#data").html(html);
          $("#hide").show();
          $("#table").dataTable().fnDestroy()
          $("#data").html(html);

          $('#table').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
      });
          return false;
  
        }
      });
      }

      $("#connection").autocomplete({
	source: function (request, response) {
		// Fetch data
		$.ajax({
			url: "/getconnectionbyconn",
			data: {
				value: request.term
			},
			dataType: "json",
			type: "post",
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			success: function (data) {
				console.log(data);
				//   response( data );
				response($.map(data, function (item) {
					return {
						label: item.conn_no,
						value: item.id
					};
				}));
			}
		});
	},
	select: function (event, ui) {
		//   alert(ui.item.label);
		// Set selection
		$('#connection').val(ui.item.label); // display the selected text
		$('#conn_id').val(ui.item.value);

		return false;
	}
});
   </script>
</x-app-layout>