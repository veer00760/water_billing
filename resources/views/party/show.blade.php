<x-app-layout>
    <x-slot name="header">
    <div class="row">
    <div class="col-md-10">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Connections Show') }}
        </h2>
        </div>
        <div class="col-md-2">
        <a class="btn btn-primary" href="{{ route('conn') }}">
                        {{ __('Connections List') }}
                    </a>
        </div>
        </div>
    </x-slot>
    <div class="py-12">     
        <div class="py-12 bg-white max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg table-responsive">
   <div class="col-md-12">

<table id="table" class="table table-bordered">
    <thead class="thead-light">
    <tr><th class="text-center" colspan=10>Party Name : {{$party->name}}</th></tr>
    <tr><th class="text-center" colspan=10>Connection Details</th></tr>
      <tr>
      <th>#</th>
        <th>Connection No</th>
        <th>Bills</th>
        <th>Name</th>
        <th>Address</th>
        <th>Status</th>
        <th>Email</th>
        <th>Phone NO</th>
        <th>Debit</th>
        <th>Credit</th>
      </tr>
    </thead>
    <tbody>
    @php $i=1 @endphp
    @foreach($party->conn as $connection)
      <tr>
      <td>{{$i++ }}</td>
        <td>{{$connection->conn_no}}</td>
        <td>@if (isset($connection->mybill)) {{$connection->mybill->count()}} @else 0  @endif</td>
        <td><a href="{{route('conndetails',$connection->id)}}">{{$connection->name}}</a></td>
        <td>{{$connection->address}}</td>
        <td>{{$connection->status}}</td>
        <td>{{$connection->email}}</td>
        <td>{{$connection->phone}}</td>
        <td>{{$connection->debit}}</td>
        <td>{{$connection->credit}}</td>
      </tr>
@endforeach
    </tbody>
  </table>


            </div>
        </div>
    </div>
    <script>
 
      //  jQuery.noConflict();
      jQuery(document).ready(function(e) {
      // e.noConflict();
      jQuery('#table').DataTable();

            $('.alert-success').hide('slide', {direction: 'right'}, 4000);
      
} );


    </script>
</x-app-layout>
