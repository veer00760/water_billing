<x-app-layout>
    <x-slot name="header">
    <div class="row">
    <div class="col-md-10">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Party') }}
        </h2>
        </div>
        <div class="col-md-2">
        <a class="btn btn-primary" href="{{ route('party.create') }}">
                        {{ __('Add Party') }}
                    </a>
        </div>
        </div>
    </x-slot>
    <div class="col-md-12">
    <div class="row">
    <div class="col-md-9"></div>
    <div class="col-md-3" style="position:absolute; right:0">
    @if (session('status'))
      <div class="alert alert-success">
          <p class="msg"> <?php echo  session("status"); ?></p>
      </div>
    @endif
     </div>
     </div></div>
    <div class="py-12">

        <div class="py-12 bg-white max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg table-responsive">
            <table class="table" id="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Name</th>
      <th scope="col">Email</th>
      <th scope="col">Address</th>
      <th scope="col">Connection No</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
  @php $i=1; @endphp
  @foreach($parties as $party)
    <tr>
      <th scope="row">{{$i++}}</th>
      <td>{{$party->name}}</td>
      <td>{{$party->email}}</td>
      <td>{{$party->address}}</td>
      <td><a href="{{route('party.show',$party->id)}}">{{count($party->conn)}}</a></td>
      <td>  <a href="{{route('party.edit',$party->id)}}"><i class="fa fa-pencil" aria-hidden="true"></i></a> | <a href="#"  data-toggle="modal" data-target="#{{$i}}"><i class="fa fa-eye" aria-hidden="true"></i></a> | <a href="#"  data-toggle="modal" data-target="#set{{$i}}"><i class="fa fa-handshake-o" aria-hidden="true"></i></a> | <a href="{{route('settlementdetail',$party->id)}}"><i class="fa fa-info" aria-hidden="true"></i></a>
      </td>
    </tr>

    <!-- Modal -->
<div class="modal fade" id="{{$i}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">{{$party->name}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

      <div class="form-row">
      <div class="col-md-6"><span><b>Party Name:</b>  {{$party->name}}</span></div>
      <div class="col-md-6"><span><b>Email:</b>  {{$party->email}}</span></div>
      <div class="col-md-6"><span><b>Address:</b>  {{$party->address}}</span></div>
      @php $debit =0; $credit=0  @endphp
@php $customBill =0; @endphp
@foreach($party->conn as $conn)
@php $debit += $conn->debit  @endphp
@php $credit += $conn->credit @endphp
@php 
 $customBill += $party->creditbilltotal($conn->id) @endphp

@endforeach
<div class="col-md-6"><span class="text-danger"><b>Debit Amount :</b> {{$debit}}</span></div>
      <div class="col-md-6"><span class="text-success"><b>Credit Amount :</b> {{$credit}}</span></div>
      <div class="col-md-6"><span class="text-primary"><b>Settlement Amount :</b> {{($debit+$customBill)-$credit}}</span>
      @if(($credit-$debit)+$customBill > 0)
      <span class="text-success">Amount to be collect</span>
      @endif
      @if(($credit-$debit)+$customBill < 0)
      <span class="text-danger">Amount to be paid</span>
      @endif
      </div>

       </div>

      </div>
      <div class="modal-footer">
      <!-- <a href="{{route('settlement',$party->id)}}" class="btn btn-primary" >Set Settlement</a> -->
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>


</div>



<div class="modal fade" id="set{{$i}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Settlement</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

      <div class="form-row">
      <form method="POST" action="{{route('settlement')}}">
      @csrf
                                 <div class="form-row">
                                   <input type="hidden" name="id" value="{{$party->id}}"/>
                                    <div class="mt-4 col-md-4">
                                       <x-label for="debit" :value="__(' Debit amount ')" />
                                       <input  class="block mt-1 w-full form-control"
                                          type="number" readonly name="debit" value="{{$debit}}" required   />
                                    </div>

                                    <div class="mt-4 col-md-4">
                                       <x-label for="debit" :value="__(' Credit amount ')" />
                                       <input  class="block mt-1 w-full form-control"
                                          type="number"  readonly name="credit" value="{{$credit}}" required   />
                                    </div>

                                    <div class="mt-4 col-md-4">
                                       <x-label for="custombill" :value="__(' Custom bill amount ')" />
                                       <input  class="block mt-1 w-full form-control"
                                          type="number"  readonly name="custombill" value="{{$customBill}}" required   />
                                    </div>

                                    <div class="mt-4 col-md-4">
                                       <x-label for="amount" :value="__(' Settlement amount ')" />
                                       <input  class="block mt-1 w-full form-control"
                                          type="number" readonly name="amount" value="{{($customBill+$debit)-$credit}}" required   />
                                    </div>
                                    
                                    <hr style="border: 1px solid black;"/>
                           
                           <div class="mt-4 col-md-5">
                                 <x-label for="paid_through"  :value="__('Paid Through')" />
                                 <select  paytype="{{$i}}"  class="paid_through form-control" name="paid_by" required>
                                 <option value="">Choose...</option>
                                 <option value="cash">Cash</option>
                                 <option value="cheque">Cheque</option>
                              </select>
                              </div>

                              <div class="mt-4 col-md-5">
                                 <x-label for="check_no" :value="__('Cheque No')" />
                                 <x-input   class="check_no{{$i}} block mt-1 w-full form-control"
                                    type="number"   name="cheque_no" :value="old('check_no')"    />
                              </div>
                                 </div>
                                 <div class="modal-footer">
                                    <div class="flex items-center justify-end mt-4">
                                       <x-button class="ml-4">
                                          {{ __('Save') }}
                                       </x-button>
                                    </div>
                                 </div>
                              </form>
       </div>

      </div>
      <div class="modal-footer">
      <!-- <a href="{{route('settlement',$party->id)}}" class="btn btn-primary" >Set Settlement</a> -->
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>


</div>

    @endforeach
  </tbody>
</table>
             
            </div>
        </div>
    </div>

    <script>
      // jQuery.noConflict();
    jQuery(document).ready(function(e) {
      e.noConflict();
      jQuery('#table').DataTable();
      jQuery('.alert-success').hide('slide', {direction: 'right'}, 10000);

} );
jQuery('.paid_through').change(function(){
var unit =jQuery(this).val();
var paytype =jQuery(this).attr('paytype');
// var className = ".check_no " + type;
// alert(type);
if(unit=='cheque'){
     jQuery(".check_no"+paytype).removeAttr("disabled"); 
  // jQuery(this).next().find('.check_no').removeAttr("disabled"); 

}else{
  // alert("helllo");
  jQuery(".check_no"+paytype).attr('disabled','disabled');

}
});

    </script>
</x-app-layout>
