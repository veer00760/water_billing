<x-app-layout>
   <x-slot name="header">
      <div class="row">
         <div class="col-md-10">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
               {{ __('Add Party') }}
            </h2>
         </div>
         <div class="col-md-2">
            <a class="btn btn-primary" href="{{ route('party') }}">
            {{ __('Party List') }}
            </a>
         </div>
      </div>
   </x-slot>
   <div class="py-12">
      <div class="max-w-5xl mx-auto sm:px-6 lg:px-8">
         <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
            <div class="p-6 bg-white border-b border-gray-200">
            @if (session('status') == 'success')
            <div class="mb-4 font-medium text-sm text-green-600">
               <h5> {{ __('.Data Save Succesfully') }}</h5>
            </div>
        @endif
               <x-auth-validation-errors class="mb-4" :errors="$errors" />
               <fieldset class="scheduler-border">
                  <legend class="scheduler-border">Party Name:</legend>
                  <form method="POST" id="saveparty" action="{{ route('party.store') }}">
                     @csrf
                     <div class="form-row">
                        <!-- Name -->
                        <div class="mt-4 col">
                           <x-label for="name" :value="__('Name')" />
                           <x-input id="name" class="block mt-1 w-full" type="text" name="name" :value="old('name')" required autofocus />
                        </div>
                        <!-- Party type id -->
                      
                     </div>
                     <div class="form-row">
                        <!-- Email -->
                        <div class="mt-4 col">
                           <x-label for="email" :value="__('Email')" />
                           <x-input id="email" class="block mt-1 w-full"
                              type="email" :value="old('email')" name="email"   />
                        </div>
                        <!-- Address -->
                        <div class="mt-4 col">
                           <x-label for="address" :value="__('Address')" />
                           <textarea id="address"  class="block mt-1 w-full form-control"
                              type="textarea"
                              name="address" required >{{old('address')}}</textarea>
                        </div>
                     </div>
                     <div class="form-row mt-4">
                        <h3><b>Add Connection:          </b></h3>
                        <span>(At Least one connection requires)</span>
                     </div>
                     <fieldset class="scheduler-border">
                        <legend class="scheduler-border">Add Connection</legend>
                        <div class="form-row">

                        <div class="mt-4 col">
                           <x-label for="party_types_id" :value="__('Connection Type')" />
                           <select id="party_types_id" class="form-control" name="party_types_id" required>
                              <option value="">Choose...</option>
                              @foreach($partyTypes as $partyType)
                              <option value="{{$partyType->id}}">{{$partyType->type}}</option>
                              @endforeach
                           </select>
                           <!-- <x-input id="party_types_id" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required /> -->
                        </div>
                       
                           <!-- Date -->
                           <div class="mt-4 col">
                              <x-label for="date" :value="__('Date')" />
                              <x-input id="date" class="block mt-1 w-full" type="text" name="date" value="{{date('d-m-Y')}}"  />
                           </div>
                        </div>

                        <div class="form-row">
                           <!--Connection  Name -->
                           <div class="mt-4 col">
                              <x-label for="conn_no" :value="__('Connection No')" />
                              <x-input id="conn_no" class="block mt-1 w-full"
                                 type="number" name="conn_no" :value="old('conn_no')" required   />
                           </div>

                            <!-- Name -->
                            <div class="mt-4 col">
                              <x-label for="ctype" :value="__('Meter Type')" />
                              <select id="ctype" class="form-control" name="connection_types_id" required>
                                 <option value="">Choose...</option>
                                 @foreach($connectionTypes as $connectionType)
                                 <option  charge="{{$connectionType->minimum_charge}}" value="{{$connectionType->id}}">{{$connectionType->type}}</option>
                                 @endforeach
                              </select>
                              <span id="min-charge"></sapn>
                           </div>

                         </div>

                        <div class="form-row">
                           <!--Connection  Name -->
                           <div class="mt-4 col">
                              <x-label for="onname" :value="__('Connection on Name')" />
                              <x-input id="onname" class="block mt-1 w-full"
                                 type="text" name="onname" :value="old('onname')" required   />
                           </div>
                           <!-- <x-input id="date" class="block mt-1 w-full" type="text" name="date" :value="old('date')" required /> -->
                           <!-- Date -->
                           <div class="mt-4 col">
                              <x-label for="onphone" :value="__('Connection On Phone No')" />
                              <x-input id="onphone" minlength="10" maxlength="10" class="block mt-1 w-full" type="text" name="onphone" :value="old('onphone')"  />
                           </div>
                        </div>
                        <div class="form-row">
                           <!--Connection  Name -->
                           <div class="mt-4 col">
                              <x-label for="onemail" :value="__(' Email')" />
                              <x-input id="onemail" class="block mt-1 w-full"
                                 type="email" name="onemail" :value="old('onemail')"    />
                           </div>
                           <!-- <x-input id="date" class="block mt-1 w-full" type="text" name="date" :value="old('date')" required /> -->
                           <!-- Date -->
                           <div class="mt-4 col">
                              <x-label for="onaddress" :value="__('Address')" />
                              <textarea id="onaddress" class="block mt-1 w-full form-control"  name="onaddress">{{old('onaddress')}}</textarea>
                           </div>
                        </div>
                        <fieldset class="scheduler-border">
                           <legend class="scheduler-border">Add Meter</legend>
                           <div class="form-row">
                              <!--Connection  Name -->
                              <div class="mt-4 col">
                                 <x-label for="meter_no" :value="__(' Add Meter No')" />
                                 <x-input id="meter_no" class="block mt-1 w-full"
                                    type="text" name="meter_no" value="{{random_int(100000, 999999)}}"    />
                              </div>
                              <!-- <x-input id="date" class="block mt-1 w-full" type="text" name="date" :value="old('date')" required /> -->
                              <!-- Date -->
                              <div class="mt-4 col">
                                 <x-label for="installation_date" :value="__('Date of Installation')" />
                                 <x-input id="installation_date" class="block mt-1 w-full"
                                    type="date" max="{{date('Y-m-d')}}" format="dd-mm-yyyy" name="installation_date" :value="old('installation_date')" required   />
                              </div>
                           </div>
                           <div class="form-row">
                              <!--Connection  Name -->
                              <div class="mt-4 col">
                                 <x-label for="meter_test_copy" :value="__(' Copy of Meter Test')" />
                                 <x-input id="meter_test_copy" class="block mt-1 w-full"
                                    type="file" name="meter_test_copy" :value="old('meter_test_copy')"    />
                              </div>
                              <!-- <x-input id="date" class="block mt-1 w-full" type="text" name="date" :value="old('date')" required /> -->
                              <!-- Date -->
                              <div class="mt-4 col">
                                 <x-label for="application_copy" :value="__('Copy of Application')" />
                                 <x-input id="application_copy" class="block mt-1 w-full"
                                    type="file" name="application_copy" :value="old('application_copy')"    />
                              </div>
                           </div>
                           <div class="form-row">
                              <!--Connection  Name -->
                              <div class="mt-4 col">
                                 <x-label for="meter_test_copy" :value="__(' Meter Status')" />
                                 <div class="form-check">
                                    <input class="form-check-input" type="radio" name="status" id="active" value="active" checked>
                                    <label class="form-check-label" for="gridRadios1">
                                    Active
                                    </label>
                                 </div>
                                
                              </div>
                              <!-- <x-input id="date" class="block mt-1 w-full" type="text" name="date" :value="old('date')" required /> -->
                              <!-- Date -->
                              <div class="mt-4 col">
                                 <x-label for="innital_meter_reading" :value="__('Initial Mete Reading')" />
                                 <x-input id="innital_meter_reading" class="block mt-1 w-full"
                                    type="number"  min="0" name="innital_meter_reading" :value="old('innital_meter_reading',0)" required   />
                              </div>
                           </div>
                        </fieldset>
                     </fieldset>
                     <div class="flex items-center justify-end mt-4">
                        <x-button class="ml-4">
                           {{ __('Save') }}
                        </x-button>
                     </div>
                  </form>
               </fieldset>
            </div>
         </div>
      </div>
   </div>

   <script>

$('#saveparty').submit(function () {
	var c = confirm("Click OK to continue?");
	return c; //you can just return c because it will be true or false
});
      $("#party_types_id").change(function() {
    
    var conn_id = $(this).val();
    $.ajax({
     url: "/admin/getslectbox",
     data: {
      conn_id: conn_id,
			},
     type: "post",
     dataType: "json",
	 headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
     success: function(res){

      //   $("#ctype").html('<option ></option>');
        if(res){
        $("#ctype").empty();
        $("#ctype").append('<option>Select</option>');
        $.each(res,function(key,value){
          $("#ctype").append('<option value="'+key+'">'+value+'</option>');
        });
      
      }else{
        $("#ctype").empty();
      }

           }
  });
    });
   </script>
</x-app-layout>