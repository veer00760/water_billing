<x-app-layout>
    <x-slot name="header">
    <div class="row">
    <div class="col-md-10">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Settlement') }}
        </h2>
        </div>
        <div class="col-md-2">
        <a class="btn btn-primary" href="{{ route('party') }}">
                        {{ __('Party') }}
                    </a>
        </div>
        </div>
    </x-slot>
    <div class="py-12">
        <div class="py-12 bg-white max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg table-responsive">
            <table class="table" id="table">
  <thead>
  <tr><th colspan="4">{{$party->name}}</th></tr>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Amount</th>
      <th scope="col">Paid By</th>
      <th scope="col">Paid on</th>

    </tr>
  </thead>
  <tbody>
  @php $i=1; @endphp
  @foreach($settlements as $settlement)
    <tr>
      <th scope="row">{{$i++}}</th>
      <th scope="row">{{$settlement->amount}}
      @if($settlement->amount > 0)
      <span class="text-success">(Amount  collected)</span>
      @endif
      @if($settlement->amount < 0)
      <span class="text-danger">(Amount  paid)</span>
      @endif
      </th>
     <td>{{$settlement->paid_by}}</td>
      <td>{{$settlement->created_at}}</td>
    </tr>
        @endforeach
  </tbody>
</table>             
            </div>
        </div>
    </div>
    <input type="hidden" id="name" value="{{$party->name}}"/>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.5/css/buttons.dataTables.min.css">

    <script type="text/javascript"  src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script> 
      
      <script type="text/javascript"  src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script> 
      <script type="text/javascript"  src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script> 
      <script type="text/javascript"  src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script> 
      <script type="text/javascript"  src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script> 
      <script type="text/javascript"  src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js"></script> 
      <script type="text/javascript"  src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js"></script> 

    <script>
      // jQuery.noConflict();
    jQuery(document).ready(function(e) {
      e.noConflict();
      
      
      jQuery('#table').DataTable({
        dom: 'Bfrtip',
        buttons: [
          {
                extend: 'excelHtml5',
                title:  jQuery("#name").val(),
            },
            {
                extend: 'csvHtml5',
                title: jQuery("#name").val(),
            },
            {
                extend: 'copyHtml5',
                title: jQuery("#name").val(),
            },
            {
                extend: 'pdfHtml5',
                title: jQuery("#name").val(),
            },
         'print', 
        ]
      });
      
      
} );
    </script>
</x-app-layout>
