<x-app-layout>
   <x-slot name="header">
      <div class="row">
         <div class="col-md-10">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
               {{ __('Add Custom Bill') }}
            </h2>
         </div>
         <div class="col-md-2">
            <a class="btn btn-primary" href="{{ route('custombill') }}">
            {{ __('Custom Bill List') }}
            </a>
         </div>
      </div>
   </x-slot>
   <div class="py-12">
      <div class="max-w-5xl mx-auto sm:px-6 lg:px-8">
         <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
            <div class="p-6 bg-white border-b border-gray-200">
               <x-auth-validation-errors class="mb-4" :errors="$errors" />
               <fieldset class="scheduler-border">
                  <legend class="scheduler-border">Add Custom Bill:</legend>
                  <form method="POST" action="{{ route('custombill.store') }}">
                     @csrf
                     <div class="form-row">
                      <div class="mt-4 col-md-5">
                                 <x-label for="conn_no" :value="__(' Connection No')" />
                                 <!-- <x-input id="conn_no" class="block mt-1 w-full"
                                    type="text" name="conn_no" :value="old('conn_no')" required   /> -->
                                    <select id="conn_no" class="form-control" name="connection_id" required>
                              <option value="">Choose...</option>
                              @foreach($connections as $connection)
                              <option value="{{$connection->id}}">{{$connection->conn_no}}</option>
                            
                              @endforeach
                           </select>
                        </div>
                        <div class="mt-4 col-md-5">
                                 <label for="data">Title</label>
                                 <x-input  class="form-control data"
                                    type="text" name="title"  required   />
                              </div>
                              </div>
             <div id="add">
                      <div class="form-row" >              
                              <!--Connection  Name -->
                              <div class="mt-4 col-md-5">
                                 <label for="data">Reason</label>
                                 <x-input  class="form-control data"
                                    type="text" name="data[]"  required   />
                              </div>
                              <div class="mt-4 col-md-5">
                                 <label for="amount">Amount</label>
                                 <x-input  class="form-control amount"
                                    type="number" name="amount[]"  required   />
                              </div>
                              <div class="mt-4 pt-2 col-md-2">               <button id="plus" type="button" class="btn btn-primary mt-4"><i class="fa fa-plus" aria-hidden="true"></i></button>
</div>
                        </div>
                        </div>
                        <div class="mt-4  col-md-5">
                                 <label for="data">Note</label>
                                 <textarea rows="5" class="form-control data"
                                     name="note"    ></textarea>
                              </div>
                        </fieldset>
                     </fieldset>
                     <div class="flex items-center justify-end mt-4">
                        <x-button class="ml-4">
                           {{ __('Save') }}
                        </x-button>
                     </div>
                  </form>
               </fieldset>
            </div>
         </div>
      </div>
   </div>
   <script>
     $('#plus').on('click', function () { 
//   alert("hello");
  // Adding a row inside the tbody. 
  $('#add').append(`<div class="form-row"> 
       <div class="col-md-5"><label for="data">Reason</label>
         <input  class="form-control data" type="text" name="data[]"  required   />
      </div>
      <div class="col-md-5"><label for="amount">Amount</label>
         <input  class="form-control amount" type="number" name="amount[]"  required   />
      </div>
      <div class="col-md-2">
          <button class="btn btn-danger mt-30 remove"
            type="button"><i class="fa fa-times" aria-hidden="true"></i>
       </button> 
          </div> 
        </div>`); 
});

$(document).on('click','.remove', function () { 
$(this).closest('.form-row').remove()
});
   </script>
</x-app-layout>