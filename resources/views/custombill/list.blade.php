<x-app-layout>
   <x-slot name="header">
      <div class="row">
         <div class="col-md-10">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
               {{ __('Custom Bill') }}
            </h2>
         </div>
         <div class="col-md-2">
            <a class="btn btn-primary" href="{{ route('custombill.create') }}">
            {{ __('Add Custom Bill') }}
            </a>
         </div>
      </div>
   </x-slot>
   <div class="col-md-12">
    <div class="row">
    <div class="col-md-9"></div>
    <div class="col-md-3" style="position:absolute; right:0">
    @if (session('status'))
      <div class="alert alert-success">
          <p class="msg"> <?php echo  session("status"); ?></p>
      </div>
    @endif
     </div>
     </div></div>
   <div class="py-12">
      <div class="py-12 bg-white max-w-7xl mx-auto sm:px-6 lg:px-8">
         <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg table-responsive">
            <table class="table" id="table">
               <thead>
                  <tr>
                     <th scope="col">#</th>
                     <th scope="col">Conn No</th>
                     <th scope="col">Voucher No</th>
                      <th scope="col">Title</th>
                     <th scope="col">Total</th>
                     <th scope="col">Paid Status</th>
                     <th scope="col">Action</th>
                  </tr>
               </thead>
               <tbody>
                  @php $i=1; @endphp
                  @foreach($bills as $bill)
                  <tr>
                     <th scope="row">{{$i++}}</th>
                     <th scope="row">{{$bill->conn->conn_no}}</th>
                     <td>{{$bill->voucher_no}}</td>
                      <td>{{$bill->title}}</td>
                     <td>{{$bill->total}}</td>
                     <td>{{$bill->paid_status}}</td>
                     <td><a href="{{route('custombill.show',$bill->id)}}" ><i class="fa fa-eye" aria-hidden="true"></i></a></td>
                  </tr>
              
                  @endforeach
               </tbody>
            </table>
         </div>
      </div>
   </div>
   <script>
      // jQuery.noConflict();
      jQuery(document).ready(function(e) {
      e.noConflict();
      jQuery('#table').DataTable();
      jQuery('.alert-success').hide('slide', {direction: 'right'}, 10000);

      } );
   </script>
</x-app-layout>