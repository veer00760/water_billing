<x-app-layout>
   <x-slot name="header">
      <h2 class="font-semibold text-xl text-gray-800 leading-tight">
         {{ __('Dashboard') }}
      </h2>
   </x-slot>
   <div class="py-12">
   <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
      <div class="row">
 <!-- /.col -->
 <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
               <span class="info-box-icon bg-red">
               <img class="h-90" src="{{ asset('img/connection.png') }}"/>            </span>
               <div class="info-box-content">
                  <span class="info-box-text">Connection</span>
                  <span class="info-box-number">{{$conn}}</span>
               </div>
               <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
         </div>
         <!-- /.col -->

               <!-- /.col -->
               <div class="col-md-3 col-sm-6 col-xs-12 ">
            <div class="info-box ">
               <span class="info-box-icon bg-yellow">
               <img class="h-90" src="{{ asset('img/water-control.png') }}"/>            </span>
               <div class="info-box-content">
                  <span class="info-box-text">Active Connection</span>
                  <span class="info-box-number">{{$active}}</span>
               </div>
               <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
         </div>

         <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
               <span class="info-box-icon bg-aqua">
               <i class="fa fa-tachometer" aria-hidden="true"></i>
               </span>
               <div class="info-box-content">
                  <span class="info-box-text">METER</span>
                  <span class="info-box-number">
                     {{$meter}}
                     <!-- <small>%</small> -->
                  </span>
               </div>
               <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
         </div>
        
         <!-- fix for small devices only -->
         <div class="clearfix visible-sm-block"></div>
         <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
               <span class="info-box-icon bg-green">
               <i class="fa fa-tachometer" aria-hidden="true"></i>
               </span>
               <div class="info-box-content">
                  <span class="info-box-text">Active Meter</span>
                  <span class="info-box-number">{{$active_meter}}</span>
               </div>
               <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
         </div>

          <!-- fix for small devices only -->
          <div class="clearfix visible-sm-block"></div>
         <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
               <span class="info-box-icon bg-green">
               <i class="fa fa-users"></i>
               </span>
               <div class="info-box-content">
                  <span class="info-box-text">Parties</span>
                  <span class="info-box-number">{{$party}}</span>
               </div>
               <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
         </div>

          <!-- fix for small devices only -->
          <div class="clearfix visible-sm-block"></div>
         <div class="col-md-3 col-sm-6 col-xs-12 ">
            <div class="info-box">
               <div class="p-3">
                  <span class="info-box-text text-center " style="font-weight:700; font-size: 20px;">Bill Created {{date('M')}}</span>
                  <span class="info-box-number"><b><h4 class="text-center" style="font-weight: 700;font-size: 100px;color: #796d6d;">{{$current_month}}</h4></b></span>
               <span class="info-box-text">
               <b><h3 class="text-center">Bills</h3></b>
               </span>
               </div>
               <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
         </div>

            <!-- fix for small devices only -->
            <div class="clearfix visible-sm-block"></div>
         <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
               <div class="p-3">
                  <span class="info-box-text text-center"style="font-weight:700; font-size: 20px;"> Bill Created  For {{date("M",strtotime("first day of previous month"))}}</span>
                  <span class="info-box-number"><b><h4 class="text-center" style="font-weight: 700;font-size: 100px;color: #796d6d;">{{$last_month}}</h4></b></span>
               <span class="info-box-text">
               <b><h3 class="text-center">Bills</h3></b>
               </span>
               </div>
               <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
         </div>

         <!-- /.col -->
      </div>
      <!-- <div class="row">
         <div class="col-lg-3 col-md-6 col-sm-6 ">
             <div class="card card-stats bg-primary">
             <div class="card-body ">
                 <div class="row">
                 <div class="col-5 col-md-4">
                     <div class="icon-big text-center icon-warning">
                     <i class="fa fa-users" aria-hidden="true"></i>
                                                    </div>
                 </div>
                 <div class="col-7 col-md-8">
                     <div class="numbers">
                     <p class="card-category">Party</p>
                     <p class="card-title">15</p><p>
                     </p></div>
                 </div>
                 </div>
             </div>
             
             </div>
         </div>
         <div class="col-lg-3 col-md-6 col-sm-6">
             <div class="card card-stats bg-success">
             <div class="card-body ">
                 <div class="row">
                 <div class="col-5 col-md-4">
                     <div class="icon-big text-center icon-warning">
                     <i class="fa fa-tachometer" aria-hidden="true"></i>
                     </div>
                 </div>
                 <div class="col-7 col-md-8">
                     <div class="numbers">
                     <p class="card-category">Meter</p>
                     <p class="card-title">1,345</p><p>
                     </p></div>
                 </div>
                 </div>
             </div>
             
             </div>
         </div>
         <div class="col-lg-3 col-md-6 col-sm-6">
             <div class="card card-stats bg-warning">
             <div class="card-body ">
                 <div class="row">
                 <div class="col-5 col-md-4">
                     <div class="icon-big text-center icon-warning">
                     <i class="fa fa-tachometer" aria-hidden="true"></i>
                                                                                                              </div>
                 </div>
                 <div class="col-7 col-md-8">
                     <div class="numbers">
                     <p class="card-category">Connections</p>
                     <p class="card-title">23</p><p>
                     </p></div>
                 </div>
                 </div>
             </div>
         
             </div>
         </div>
         <div class="col-lg-3 col-md-6 col-sm-6">
             <div class="card card-stats bg-info">
             <div class="card-body ">
                 <div class="row">
                 <div class="col-5 col-md-4">
                     <div class="icon-big text-center icon-warning">
                     <i class="fa fa-tachometer" aria-hidden="true"></i>
                     </div>
                 </div>
                 <div class="col-7 col-md-8">
                     <div class="numbers">
                     <p class="card-category">Active Connection</p>
                     <p class="card-title">45</p><p>
                     </p></div>
                 </div>
                 </div>
             </div>
         
             </div>
         </div>
         </div> -->
      <div class="row">
         <div class="col-md-6">
         <div class="box box-default">
               <div class="box-header with-border">
                  <h3 class="box-title">Income & Expances</h3>
                  <div class="box-tools pull-right">
                     <!-- <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div> -->
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                     <div class="row">
                        <div class="col-md-12">
                           <div class="chart-responsive">
                              <canvas id="myChart" ></canvas>
                           </div>
                           <!-- ./chart-responsive -->
                        </div>
                        <!-- /.col -->
                        <!-- /.col -->
                     </div>
                     <!-- /.row -->
                  </div>
                  <!-- /.box-body -->
                  <!-- /.footer -->
               </div>
            </div>
         </div>
         <div class="col-md-6">
            <div class="box box-default">
               <div class="box-header with-border">
                  <h3 class="box-title">Payments</h3>
                  <div class="box-tools pull-right">
                     <!-- <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div> -->
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                     <div class="row">
                        <div class="col-md-12">
                           <div class="chart-responsive">
                              <canvas id="donutChart" ></canvas>
                           </div>
                           <!-- ./chart-responsive -->
                       
                        <!-- /.col -->
                        <!-- /.col -->
                     </div>
                     <!-- /.row -->
                  </div>
                  <!-- /.box-body -->
                  <!-- /.footer -->
               </div>
            </div>
         </div>
      </div>
   </div>

   <div class="row">
            <div class="col-md-12">
               <div class="box box-default">
                     <div class="box-header with-border">
                        <h3 class="box-title">Monthly Payments</h3>
                        <div class="box-tools pull-right">
                           <!-- <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                              </button>
                              <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                              </div> -->
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                           <div class="row">
                              <div class="col-md-12">
                                 <div class="chart-responsive">
                                    <canvas id="group-chart" ></canvas>
                                 </div>
                                 <!-- ./chart-responsive -->
                              </div>
                              <!-- /.col -->
                              <!-- /.col -->
                           </div>
                           <!-- /.row -->
                        </div>
                        <!-- /.box-body -->
                        <!-- /.footer -->
                     </div>
                  </div>
               </div>
            </div>
         </div>

   <script>

      data = {
      datasets: [{
          data: {!! json_encode($payment) !!},
       
          backgroundColor: [
                  'rgba(255, 99, 132, 0.2)',
                  'rgba(54, 162, 235, 0.2)',
                  'rgba(255, 206, 86, 0.2)',
                  'rgba(75, 192, 192, 0.2)',
                  'rgba(153, 102, 255, 1)',

              ],
              borderColor: [
                  'rgba(255, 99, 132, 1)',
                  'rgba(54, 162, 235, 1)',
                  'rgba(255, 206, 86, 1)',
                  'rgba(75, 192, 192, 0.2)',
                  'rgba(153, 102, 255, 0.2)',


              ],
      }],
      
      // These labels appear in the legend and in the tooltips when hovering different arcs
      labels: [
          'Cheque',
          'Cash',
          'Credit',
          'Online',
          'Settlement',
      ]
      };
      var myDoughnutChart = new Chart(donutChart, {
      type: 'doughnut',
      data: data,
 
      })
      
      var ctx = document.getElementById('myChart').getContext('2d');
      var myChart = new Chart(ctx, {
      type: 'line',
      data: {
          labels: ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
          datasets: [{
              label: '# Income',
              data:{!! json_encode($income) !!},
              backgroundColor: [
                  'rgba(255, 99, 132, 0.2)',
                  'rgba(54, 162, 235, 0.2)',
                  'rgba(255, 206, 86, 0.2)',
                  'rgba(75, 192, 192, 0.2)',
                  'rgba(153, 102, 255, 0.2)',
                  'rgba(255, 159, 64, 0.2)'
              ],
              borderColor: [
                  'rgba(255, 99, 132, 1)',
                  'rgba(54, 162, 235, 1)',
                  'rgba(255, 206, 86, 1)',
                  'rgba(75, 192, 192, 1)',
                  'rgba(153, 102, 255, 1)',
                  'rgba(255, 159, 64, 1)'
              ],
              borderWidth: 1
          },
          {
              label: '# Expances',
              data: {!! json_encode($exp) !!},
        
              borderWidth: 1
          }]
      },
      options: {
          scales: {
              yAxes: [{
                  ticks: {
                      beginAtZero: true
                  }
              }]
          }
      }
      });


      // Group chart

      new Chart(document.getElementById("group-chart"), {
    type: 'bar',
    data: {
      labels: {!! json_encode($months) !!},
      datasets: [
        {
          label: "Online",
          backgroundColor: "#3e95cd",
          data: {!! json_encode($online) !!}
        }, {
          label: "Bank",
          backgroundColor: "#8e5ea2",
          data: {!! json_encode($bank) !!}
        }
        , {
          label: "Cash",
          backgroundColor: "#6eeb34",
          data: {!! json_encode($cash) !!}
        }
      ]
    },
    options: {
      title: {
        display: true,
        text: 'Amount (Thousand)'
      }
    }
});
   </script>
</x-app-layout>