<html >
<head>
 <title>Title</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/> 
  <!-- <link rel="stylesheet"  href="{{ public_path('/css/app.css') }}" crossorigin="anonymous"> -->
  <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> -->
  <link rel="stylesheet" href="{{ public_path('css/pdf.css') }} ">
</head>
<body class="ft-fm" >

            <div id="DivIdToPrint" class="p-6 p-3 bill-background ft-fm">
                  <div class="row">
                     <div class="col-xs-3">
                     <img src="{{ public_path('img/LOGO_FINAL_VIA.png') }}" alt="logo">
                      </div>
                     <div class="col-xs-1 text-center"></div>
                     <div class="col-xs-4 text-center">This is the address of office. Here people can come and visit here people can pay bill inclding pin 333045</div>
                     <div class="col-xs-4 text-right "><b>Date: </b>{{date('d-m-y')}}</div>
                  </div>
                  <div class="row">
                     <div class="col-xs-3"></div>
                     <div class="col-xs-7">
                        <div class="heading">
                           <h3 class="bill-heading-text text-center m-3" style="  color:  #707070;  font-weight: 700;">WATER SYSTEM GUNDLAV</h3>
                           <h5 class="bill-heading-text text-center m-3" style="  color:  #707070; ">GUNDLAV    Email:watersyatemvia@gmail.com</h5>
                        </div>
                     </div>
                     <div class="col-xs-2">
                     </div>
                  </div>
                  <!-- <div class="row"><div class="col-xs-6"></div>
                     <div class="col-xs-6 text-right" ></div>
                     </div> -->
                  <div class="row mt-3">
                     <div class="col-xs-4"><b>No VIA/WAT/VLS/{{$noc->id}}</b></div>
                     <div class="col-xs-4 "></div>
                     <div class="col-xs-4 "></div>
                  </div>
                  <div class="row mt-3">
                     <div class="col-xs-12">
                        <h3 class="text-center font-weight-bold">No DUES CERTIFICATE</h3>
                     </div>
                  </div>
                  <div class="row mt-3">
                     <div class="col-xs-12">
                        This is to certify that M/S  <b>@if(isset($noc->party)){{$noc->party->name}}@else {{$noc->conn->name}}@endif</b>
                     </div>
                     <div class="col-xs-4 mt-3 ">
                        Plot/Shed/Qtr No <span class="pl-2">@if(isset($noc->party)){{$noc->party->address}}@else {{$noc->conn->address}}@endif </span> 
                     </div>
                     <div class="col-xs-4">
                        <span class="pl-5">Conn.NO @if(isset($noc->party)) {{$noc->party->name}} @else {{$noc->conn->name}}@endif</span>
                     </div>
                     <div class="col-xs-4 ">
                        <span class="pl-5"> GIDC GUNDLAV</span>
                     </div>
                     <br/>
                     <div class="col-xs-12 mt-3">
                        <span class=" mt-3 text-left"> has paid the outstanding dues of water charges. There is no due upto  <span class="pl-3">January 2020</span> </span>
                     </div>
                  </div>
                  <div class="row mt-3">
                     <div class="col-xs-5 mt-3" style="max-height:300px">
                        To, </br>
                        M/S <b>M/S ARMC.</b> </br>
                        Plot/Shed/Qtr No <span class="pl-2">GDN 19 </span> </br>
                        GIDC GUNDLAV </br>
                        <b>COPY OF REGIONAL MANAGER GIDC VAPI </b>
                     </div>
                     <div class="col-xs-5 mt-3 pl-5 m-5 ">
                        Stamp here                     
                     </div>
                     <div class="col-xs-2 mt-3  ">
                        <b>CHAIRMAN </b></br>
                        WATER SYSTEM  </br>
                        GUNDLAV
                     </div>
                  </div>
            </div>

   
         </body>
</html>
