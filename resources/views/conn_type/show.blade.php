<x-app-layout>
   <x-slot name="header">
      <div class="row">
         <div class="col-md-10">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
               {{ __('Meter Types History') }}
            </h2>
         </div>
         <div class="col-md-2">
            <a class="btn btn-primary" href="{{ route('conn-type.create') }}">
            {{ __('Add Meter Type') }}
            </a>
         </div>
      </div>
   </x-slot>
   <div class="col-md-12">
    <div class="row">
    <div class="col-md-9"></div>
    <div class="col-md-3" style="position:absolute; right:0">
    @if (session('status'))
      <div class="alert alert-success">
          <p class="msg"> <?php echo  session("status"); ?></p>
      </div>
    @endif
     </div>
     </div></div>
   <div class="py-12">
      <div class="py-12 bg-white max-w-7xl mx-auto sm:px-6 lg:px-8">
         <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg table-responsive">
            <table class="table" id="table">
               <thead>
                  <tr>
                     <th scope="col">#</th>
                     <th scope="col">Type</th>
                     <th scope="col">Minumum Charge</th>
                     <th scope="col">Charge per unit</th>
                     <th scope="col">Unmeter Charge</th>
                     <th scope="col">Edit By</th>

                  </tr>
               </thead>
               <tbody>
                  @php $i=1; @endphp
                  @foreach($connTypes as $conn_type)
                  <tr>
                     <th scope="row">{{$i++}}</th>
                     <td>{{$conn_type->type}}</td>
                     <td>{{$conn_type->minimum_charge}}</td>
                     <td>{{$conn_type->charge}}</td>
                     <td>{{$conn_type->unmeter_charge}}</td>
                     <td>{{$conn_type->user->name}}</td>
                  </tr>

                  @endforeach
               </tbody>
            </table>
         </div>
      </div>
   </div>
   <script>
      // jQuery.noConflict();
      jQuery(document).ready(function(e) {
      e.noConflict();
      jQuery('#table').DataTable();
      jQuery('.alert-success').hide('slide', {direction: 'right'}, 10000);

      } );
   </script>
</x-app-layout>