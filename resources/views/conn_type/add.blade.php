<x-app-layout>
   <x-slot name="header">
      <div class="row">
         <div class="col-md-10">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
               {{ __('Add Meter Type') }}
            </h2>
         </div>
         <div class="col-md-2">
            <a class="btn btn-primary" href="{{ route('conn-type') }}">
            {{ __('Meter Type List') }}
            </a>
         </div>
      </div>
   </x-slot>
   <div class="py-12">
      <div class="max-w-5xl mx-auto sm:px-6 lg:px-8">
         <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
            <div class="p-6 bg-white border-b border-gray-200">
               <x-auth-validation-errors class="mb-4" :errors="$errors" />
               <fieldset class="scheduler-border">
                  <legend class="scheduler-border">Add Meter Type:</legend>
                  <form method="POST" action="{{ route('conn-type.store') }}">
                     @csrf
                           <div class="form-row">

                           <div class="mt-4 col">
                                 <x-label for="Type" :value="__('Connection Type')" />
                           <select name="connection_type" id="connection_type" class="form-control" required>
                           <option value="">Select Conn Type</option>
                           <option value="1">Industries</option>
                           <option value="2">Housing</option>
                           
                           </select>
                           </div>

                              <!--Connection  Name -->
                              <div class="mt-4 col">
                                 <x-label for="Type" :value="__('Meter Type')" />
                                 <x-input id="type" class="block mt-1 w-full"
                                    type="text" name="type" :value="old('type')"  required  />
                              </div>
                              <!-- Date -->
                              <div class="mt-4 col">
                                 <x-label for="minimum_charge" :value="__('Minumum Charge')" />
                                 <x-input id="minimum_charge" class="block mt-1 w-full"
                                    type="number" name="minimum_charge" :value="old('minimum_charge')" required   />
                              </div>

                            
                           </div>
                           <div class="form-row">

                           <div class="mt-4 col">
                                 <x-label for="charge" :value="__('Charge per unit')" />
                                 <x-input id="charge" class="block mt-1 w-full"
                                    type="number" name="charge" :value="old('charge') "  required  />
                              </div>
                              <div class="mt-4 col">
                                 <x-label for="unmeter_charge" :value="__('Un Meter Charge')" />
                                 <x-input id="unmeter_charge" class="block mt-1 w-full"
                                    type="number" name="unmeter_charge" :value="old('unmeter_charge') "  required  />
                              </div>
                              <div>
                        </fieldset>
                     </fieldset>
                     <div class="flex items-center justify-end mt-4">
                        <x-button class="ml-4">
                           {{ __('Save') }}
                        </x-button>
                     </div>
                  </form>
               </fieldset>
            </div>
         </div>
      </div>
   </div>
</x-app-layout>