<x-guest-layout>
    <x-auth-card>
        <x-slot name="logo">
            <a href="/">
                <x-application-logo class="w-20 h-20 fill-current text-gray-500" />
            </a>
        </x-slot>

        <!-- Session Status -->
        <x-auth-session-status class="mb-4" :status="session('status')" />
        @if ($error = $errors->first('password'))
            <!-- <div class="alert alert-danger">
           
            </div> -->
            @endif
        <!-- Validation Errors -->
        <x-auth-validation-errors class="mb-4" :errors="$errors" />
        <div class="col-md-12 mr-3">
        <div class="row">
        <div class="col-md-2"></div>
            <div class="col-md-8">
            <img class="img img-responsive" src="{{ asset('img/LOGO_FINAL_VIA.png') }}" />
            </div>
            <div class="col-md-2"></div>
        </div>
        <div class="row">
        
            <div class="col-md-12 text-center ">
                <h4 class="login-heading">WATER BILLING SERVICE</h4>
                <hr class="new4"/>
<span>'Save Water, Save Life !!'</span>
            </div>
        </div>

        <div class="row">
        
        <div class="col-md-12 text-center ">
            <h4 class="login-heading">Log In</h4>
        </div>
    </div>

        <form method="POST" action="{{ route('customerlogin') }}">
            @csrf

            <!-- Email Address -->
            <div>
                <!-- <x-label for="email" :value="__('Email')" /> -->

                <x-input id="email" placeholder="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required autofocus />
            </div>

            <!-- Password -->
            <div class="mt-4">
                <!-- <x-label for="password" :value="__('Password')" /> -->

                <x-input id="password" placeholder="Password" class="block mt-1 w-full"
                                type="password"
                                name="password"
                                required autocomplete="current-password" />
            </div>

        

            <div class="flex items-center justify-end mt-4">
                @if (Route::has('password.request'))
                    <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('password.request') }}">
                        {{ __('Forgot your password?') }}
                    </a>
                @endif
                </div>
                <div class="row">
                    <div class="col-md-3"></div>
                <button class="btn btn-lg mt-3 login-btn col-md-6">
                    {{ __('Login') }}
                </button>
                <div class="col-md-3"></div>
             </div>

             <div class="row mt-3">
             <div class="col-md-3 "></div>
             <a href="/cu-reg" class="btn btn-sm mt-3 login-btn col-md-6">
                    {{ __('Register ') }}
                </a>
             <div class="col-md-6">
           </div>
             </div>

             <div class="row mt-3">
             <div class="col-md-3 "></div>
             
             <div class="col-md-5">
             <img class="img img-responsive ved-logo logo" src="{{ asset('img/black.png') }}" />
           </div>
             </div>

        </form>
</div>
<script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
<script>
  $(".btn-sm").click(function() {
      $('#loading').show();
  });
</script>
    </x-auth-card>
</x-guest-layout>
