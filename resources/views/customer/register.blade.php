<x-guest-layout>
    <x-auth-card>
        <x-slot name="logo">
            <a href="/">
                <x-application-logo class="w-20 h-20 fill-current text-gray-500" />
            </a>
        </x-slot>

        <!-- Session Status -->
        <x-auth-session-status class="mb-4" :status="session('status')" />
        @if ($error = $errors->first('password'))
            <!-- <div class="alert alert-danger">
           
            </div> -->
            @endif
        <!-- Validation Errors -->
        <x-auth-validation-errors class="mb-4" :errors="$errors" />
        <div class="col-md-12 mr-3">
        <div class="row">
        <div class="col-md-2"></div>
            <div class="col-md-8">
            <img class="img img-responsive" src="{{ asset('img/LOGO_FINAL_VIA.png') }}" />
            </div>
            <div class="col-md-2"></div>
        </div>
        <div class="row">
        
            <div class="col-md-12 text-center ">
                <h4 class="login-heading">WATER BILLING SERVICE</h4>
                <hr class="new4"/>
<span>'Save Water, Save Life !!'</span>
            </div>
        </div>

        <div class="row">
        
        <div class="col-md-12 text-center ">
            <h4 class="login-heading">REGISTER AS</h4>
        </div>
    </div>
    <div class="row mt-4">
    <div class="col-md-6">
    <a class="bg-purples btn btn-primary btn-lg" style="white-space:inherit !important; background: #4b2757" href="/customer-reg/"><h4>Register as industry</h4><span class="text-sm"> Recommended for Industries</sapn></a>
    </div>
    <div class="col-md-6">
    <a class="bg-purples btn btn-primary btn-lg" style="white-space:inherit !important; background: #4b2757" href="/housing-reg/"><h4>Register as individuals</h4><sapn class="text-sm">Recommended for housing colony</sapn> </a>
    </div>
    </div>

</div>
<script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
<script>
  $(".btn-lg").click(function() {
      $('#loading').show();
  });
</script>
    </x-auth-card>
</x-guest-layout>
