@extends('layouts.customer.app')
@section('content')
<style>
  @media print {
  
    h3{
   text-align:center;
 
      }
}

</style>
<div class="row">
   <div class="col-md-10"></div>
   <div class="col-md-2 ">
      <a class="btn btn-small btn-success mb-3" href="/customer/dashboard">
      Dashboard
      </a>
   </div>
</div>
<div class="wrapper">
   <div class="row">
        <div class="col-md-3">
            <img class="img img-responsive logo" src="{{ asset('img/LOGO_FINAL_VIA.png') }}" />
        </div>
        <div class="col-md-1"></div>
        <div class="col-md-4">
            <div class=" text-center ">
                <h2 class="login-heading">WATER BILLING SERVICE</h2>
                <hr class="h3-dash"/>
                <span>'Save Water, Save Life !!'</span>
            </div>
            <div class="col-md-12 text-center ">
                <h3 class="login-heading">Bill Records</h3>
            </div>
        </div>
        <div class="col-md-2"></div>
        <div class="col-md-2">
            <!-- <a class="btn btn-small success-btn mt-3 mb-3" href="#">
            Download
            </a> -->
        </div>
   </div>
<div class="row">
<div class="col-md-12 ">
<div class="col-md-12 table-responsive">
<div class="row">
@php $i=1; @endphp
<div class="col-md-4 mb-3">
<select id="party" style="border-radius:10px;"  class="block mt-1 w-full form-control"  name="party"  required autofocus >
<option value="">Select Connection</option> 
               @foreach($conns as $conn)
               @if($conn->id==$i) @php $active = 'active' @endphp @else @php $active = '' @endphp  @endif
               <!-- <div class="col-md-1" id="{{$conn->id}}" class="divli {{$active}}"><a class="btn btn-small success-btn mt-3 mb-3 " href="/customer/records/{{$conn->id}}">{{$conn->conn_no}}</a>
               </div> -->
               <!-- <li  class="divli"><a class="menu1 color-purple" href="#">{{$conn->conn_no}}</a></li> -->
            <option value="{{$conn->id}}">{{$conn->conn_no}}</option> 
              @php $i++; @endphp
               @endforeach()  
               </select>
</div>
               </div>
   <table class="table" id="table">
               <thead>
                  <tr>
                     <th scope="col">#</th>
                     <th scope="col">Pay Date</th>
                       <th scope="col">Conn No</th>
                     <th scope="col">Month</th>
                     <th scope="col">Total</th>
                     <th scope="col">Pay By</th>
                     <th scope="col" class="sum">Pay Amount</th>
                  </tr>
               </thead>
               <tbody>
                  @php $i=1; $total=0; @endphp
                  @foreach($bills as $bill)
                  <tr>
                     <th scope="row">{{$i++}}</th>
                     <td>@if(isset($bill->paid_amount))
                         {{date('d-m-Y h:i',strtotime($bill->paid_date))}}@else ---@endif</td>
                     <td>{{$bill->conn->conn_no}}</td>
                     <td>{{$bill->billing_month}}</td>
                     <td>{{$bill->bill_to_pay}}</td>
                     <td>@if(isset($bill->paid_amount))
                         {{$bill->pay_by}}@else ---@endif</td>

                     <td>@if(isset($bill->paid_amount)){{$bill->paid_amount}} @else Shifted @endif</td>
                  </tr>
                 @php $total = $total+$bill->paid_amount @endphp
                  @endforeach
               </tbody>
               <tfoot>
               <tr>
               <td colspan="6" class="text-center">Total</td>
               <td></td>
               </tr>
               </tfoot>
            </table>
        </div>
        </div>
        </div>
</div>
<div class="row">
    <div class="col-md-5"></div>
    <div class="col-md-2 mt-3 text-center">
    <span class="text-sm " style="color:#fff">Powered By</span>
    <img class="img img-responsive ved-logo " style=" width: 70%;  margin-left: 30px;"  src="{{ asset('img/white_logo.png') }}" />

<!-- <a class="btn  success-btn" href="#">&nbsp;&nbsp;&nbsp;BACK&nbsp;&nbsp;&nbsp;</a> -->
    </div>
    <div class="col-md-5"></div>
</div>
</div>

@endsection
@section('custom_scripts')
      
      <script type="text/javascript"  src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script> 
      <script type="text/javascript"  src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script> 
      <script type="text/javascript"  src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script> 
      <script type="text/javascript"  src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script> 
      <script type="text/javascript"  src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js"></script> 
      <script type="text/javascript"  src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js"></script> 

<script>
$('select').on('change', function() {
  var myval = this.value;
  $('#loading').show();
  window.location.href = "/customer/records/"+myval;

});
//   $(".divli").click(function() {
//       $('#loading').show();
//   });

   $(document).ready(function(){
    $('#loading').hide();
    $('#table').append("<caption class='title' style='text-align:center; caption-side: top'>Valsad Water Services. Payment Reports {{$profile->name}} </caption>");
    $('#table').DataTable({
      dom: 'Bfrtip',
        buttons: [
         {
                extend: 'print',
                title: jQuery("#name").val(),
                footer: true,
                customize: function ( win ) {
                    $(win.document.body)
                        .css( 'font-size', '20pt' )
                        .prepend(
                            '<img src=".../img/logo.jpeg" style="position:absolute; top:0; left:0;" />'
                        );
 
                    $(win.document.body).find( 'table' )
                        .addClass( 'compact' )
                        .css( 'text-align', 'center' );
                }
            },
        ],
        
        "footerCallback": function (settings, json) {
        this.api().columns('.sum').every(function () {
            var column = this;

            var sum = column
               .data()
               .reduce(function (a, b) { 
                   a = parseInt(a, 10);
                   if(isNaN(a)){ a = 0; }
                   
                   b = parseInt(b, 10);
                   if(isNaN(b)){ b = 0; }
                   
                   return a + b;
               });

            // $(column.footer()).html('Sum: ' + sum);
            $(column.footer()).html( sum);

        });
    }
  
    });

   });
</script>
@endsection