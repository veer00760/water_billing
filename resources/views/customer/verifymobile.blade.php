<x-guest-layout>
    <x-auth-card>
        <x-slot name="logo">
            <a href="/">
                <x-application-logo class="w-20 h-20 fill-current text-gray-500" />
            </a>
        </x-slot>

        <!-- Session Status -->
        <x-auth-session-status class="mb-4" :status="session('status')" />
        @if ($error = $errors->first('password'))
            <!-- <div class="alert alert-danger">
           
            </div> -->
            @endif
        <!-- Validation Errors -->
        <x-auth-validation-errors class="mb-4" :errors="$errors" />
        <div class="col-md-12 mr-3">
        <div class="row">
        <div class="col-md-2"></div>
            <div class="col-md-8">
            <img class="img img-responsive" src="{{ asset('img/LOGO_FINAL_VIA.png') }}" />
            </div>
            <div class="col-md-2"></div>
        </div>
        <div class="row">
        
            <div class="col-md-12 text-center ">
                <h4 class="login-heading">WATER BILLING SERVICE</h4>
                <hr class="new4"/>
<span>'Save Water, Save Life !!'</span>
            </div>
        </div>

        <div class="row">
        
        <div class="col-md-12 text-center ">
            <h4 class="login-heading">Mobile Verification</h4>
        </div>
    </div>

        <form method="POST" action="{{ route('confirmmobile') }}">
            @csrf

            <!-- Email Address -->
            <div>
                <x-label for="otp" :value="__('Enter OTP')" />

                <x-input id="otp" placeholder="Enter OTP" class="block mt-1 w-full" type="number" name="otp"  required autofocus />
                <sapn class="countdown text-center"></span>

            </div>



        


                <div class="row">
                    <div class="col-md-3"></div>
                <button class="btn btn-lg mt-3 login-btn col-md-6">
                    {{ __('Verify') }}
                </button>
                <div class="col-md-3"></div>
             </div>

             <div class="row mt-3">
             <div class="col-md-3 "></div>
             <button type="button" id="resend" disabled  class="btn btn-sm mt-3 login-btn col-md-6">
                    {{ __('Resend ') }}
                </button>
             <div class="col-md-6">
             <img class="img img-responsive ved-logo logo" src="{{ asset('img/black.png') }}" />
           </div>
             </div>

        </form>
</div>
    </x-auth-card>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script>
     $("form").submit(function(e){
         var otp = {{$otp}}
         if($('#otp').val()==otp){
        //  console.log('success');
         }else{
             alert('please enter a valid OTP')
                e.preventDefault(e);
         }
            });

            $( document ).ready(function() {
    timer();
});

$("#resend").click(function() {
    $('#resend').prop('disabled', true);
    $.ajax({
     url: "/resend",
     data: {
				type: 'mobile',
			},
     type: "post",
	 headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
     success: function(result){
        timer();

           }
  });
    });

function timer(){

 var timer2 = "3:1";

var interval = setInterval(function() {

var timer = timer2.split(':');
//by parsing integer, I avoid all extra string processing
var minutes = parseInt(timer[0], 10);
var seconds = parseInt(timer[1], 10);
--seconds;
minutes = (seconds < 0) ? --minutes : minutes;
seconds = (seconds < 0) ? 59 : seconds;
seconds = (seconds < 10) ? '0' + seconds : seconds;
//minutes = (minutes < 10) ?  minutes : minutes;
$('.countdown').html(minutes + ':' + seconds);
if (minutes < 0) clearInterval(interval);
//check if both minutes and seconds are 0
if ((seconds <= 0) && (minutes <= 0)) clearInterval(interval);
if((seconds == 0) && (minutes == 0)){
    $('#resend').prop('disabled', false);

}
timer2 = minutes + ':' + seconds;
}, 1000);
 }

    </script>
</x-guest-layout>
