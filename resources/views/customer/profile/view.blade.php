@extends('layouts.customer.app')
@section('content')
<div class="row">
   <div class="col-md-10"></div>
   <div class="col-md-2 ">
      <a class="btn btn-small btn-success mb-3" href="/customer/dashboard">
      Dashboard
      </a>
   </div>
</div>
<div class="wrapper">
   <div class="row">
      <div class="col-md-3">
         <img class="img img-responsive logo" src="{{ asset('img/LOGO_FINAL_VIA.png') }}" />
      </div>
      <div class="col-md-1"></div>
      <div class="col-md-4">
         <div class=" text-center ">
            <h2 class="login-heading">WATER BILLING SERVICE</h2>
            <hr class="h3-dash"/>
            <span>'Save Water, Save Life !!'</span>
         </div>
         <div class="col-md-12 text-center ">
            <h3 class="login-heading">Profile</h3>
         </div>
      </div>
      <div class="col-md-2"></div>
      <div class="col-md-2">
         <!-- <a class="btn btn-small success-btn mt-3 mb-3" href="#">
            Download
            </a> -->
      </div>
   </div>
<div class="col-md-12 text-center">

</div>
   <div class="row">
    <div class="col-md-9">
  
    </div>
    <div class="col-md-3 text-center" style="height:100px; position:absolute; right:0">
    @if (session('status'))
      <div class="alert alert-primary" >
          <p class="msg text-success"> <?php echo  session("status"); ?></p>
      </div>
    @endif
    @if($errors->any())
    <div class="text-danger">{{ implode('', $errors->all(':message')) }}
</div>
@endif
     </div>
     </div>
@if(isset($profile->connection_id))
         <div class="row" style="margin-bottom:10px ;">
         <div class="col-md-3"></div>
         <div class="col-md-6">
         <div class="row mysec">
            <h3 class=" text-center" style="margin-bottom: 56px;"><span class="p-3 profile-heading">REGISTRATION INDIVIDUAL</span></h3>
            <div style="margin-bottom:20px; line-height: 35px">
            <div class="col-md-6"><span><b>Connection No: {{$myconn->conn_no}}</b></span></div>
            <div class="col-md-6"><span><b>Party Name: {{$myconn->party->name}}</b></span></div>
            <div class="col-md-6"><span><b>Meter No: {{$myconn->meter->meter_no}}</b></span></div>
            <div class="col-md-6"><span><b>Connection On Name: {{$myconn->name}}</b></span></div>
            <div class="col-md-6"><span><b>Meter Status: <span class="text-warning">{{$myconn->meter->status}}</span></b></span></div>
         <div class="col-md-6"><span><b>Connection Type: <span class="text-primary">{{$myconn->conn_type->connection_type}}</span></b></span></div>
       </div>   
      </div>
         </div>
         </div>
@endif
@if(isset($profile->party_id))

         <div class="row" style="margin-bottom:10px ;">
         <div class="col-md-3"></div>
         <div class="col-md-6">
         <div class="row mysec">
            <h3 class=" text-center" style="margin-bottom: 56px;"><span class="p-3 profile-heading">REGISTRATION INDUSTRY</span></h3>
           @foreach($myconn as $conn)
            <div class="col-md-6 " style="margin-bottom:20px; line-height: 35px">
            <div class="col-md-12"><span><b>Connection No: {{$conn->conn_no}}</b></span></div>
            <div class="col-md-12"><span><b>Meter No: {{$conn->meter->meter_no}}</b></span></div>
            <div class="col-md-12"><span><b>Meter Status: <span class="text-warning">{{$conn->meter->status}}</span></b></span></div>
            <div class="col-md-12"><span><b>Connection Type:<span class="text-primary">{{$conn->conn_type->connection_type}}</span></b></span></div>
            </div>
          @endforeach
            <!-- <div class="col-md-6">
            <div class="col-md-12"><span><b>Connection No: 1234</b></span></div>
            <div class="col-md-12"><span><b>Meter No: 1234</b></span></div>
            <div class="col-md-12"><span><b>Meter Status: 1234</b></span></div>
            <div class="col-md-12"><span><b>Connection Type: 1234</b></span></div>
            </div> -->
         </div>
         </div>
         </div>
@endif
   <div class="row">
      <div class="col-md-3 "></div>
      <div class="col-md-6 ">
         <div class="col-md-12">
            <form method="POST" action="{{ route('updateprofile') }}">
               @csrf
               <div class="form-group">
                  <label for="exampleInputEmail1">Email address</label>
                  <div class="row">
                  <div class="col-md-9">
                  <input type="email" readonly name="email" value="{{old('email',$profile->email) }}"  class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                  </div>
                  <div class="col-md-3 mt-3"><a href="#" class=" btn btn-success btn-link" data-toggle="modal" data-target="#emailModel">Change</a></div></div>
                  <small id="emailHelp" class="form-text text-muted text-danger">You have to verify email before change.</small>
               </div>
               <div class="form-group">
                  <label for="mobile">Mobile</label>
                  <div class="row">
                  <div class="col-md-9">
                  <input type="number" name="mobile" value="{{old('email',$profile->mobile) }}" readonly class="form-control" id="mobile"  placeholder="Enter mobile">
                  </div>
                  <div class="col-md-3 mt-3"><a href="#" class=" btn btn-success btn-link" data-toggle="modal" data-target="#mobileModel">Change</a></div></div>
                  <small id="emailHelp" class="form-text text-muted text-danger">You have to verify mobile before change.</small>

               </div>

               <div class="form-group">
                  <label for="exampleInputPassword">Current Password</label>
                  <input type="password" name="cur_password" class="form-control" id="exampleInputPassword" placeholder="Current Password">
               </div>

               <div class="form-group">
                  <label for="exampleInputPassword1">New Password</label>
                  <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="New Password">
               </div>

               <div class="form-group">
                  <label for="exampleInputPassword2">Confirm Password</label>
                  <input type="password" name="c_password" class="form-control" id="exampleInputPassword2" placeholder="Confirm Password">
               </div>

               <button type="submit" class="btn login-btn">Submit</button>
            </form>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-md-5"></div>
      <div class="col-md-2 mt-3 text-center">
         <span class="text-sm " style="color:#000">Powered By</span>
         <img class="img img-responsive logo-120" style=" width: 70%;  margin-left: 30px;"  src="{{ asset('img/black.png') }}" />
         <!-- <a class="btn  success-btn" href="#">&nbsp;&nbsp;&nbsp;BACK&nbsp;&nbsp;&nbsp;</a> -->
      </div>
      <div class="col-md-5"></div>
   </div>
</div>

<!-- Modal -->
<div class="modal fade" id="emailModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Change Email</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form method="GET" action="{{ route('checkmail') }}">
            
            <div class="form-group">
               <label for="mobile">Email</label>
               <input type="email" name="email" required value="" class="form-control" id="email"  placeholder="Enter email">
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Verify</button>
      </div>
      </form>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="mobileModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Change Mobile</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form method="GET" action="{{ route('checkmob') }}">
            
            <div class="form-group">
               <label for="mobile">Mobile</label>
               <input type="number" name="mobile" value="" class="form-control" id="mobile" required  placeholder="Enter mobile">
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Verify</button>
      </div>
      </form>
    </div>
  </div>
</div>

@endsection
@section('custom_scripts')
<script>
   $(document).ready(function(){
    jQuery('.alert-success').hide('slide', {direction: 'right'}, 1000000);

   });
</script>
@endsection