@extends('layouts.customer.app')
@section('content')
<div class="row">
   <div class="col-md-10"></div>
   <div class="col-md-2 ">
      <a class="btn btn-small btn-success mb-3" href="/customer/dashboard">
      Dashboard
      </a>
   </div>
</div>
<div class="wrapper">
   <div class="row">
      <div class="col-md-3">
         <img class="img img-responsive logo" src="{{ asset('img/LOGO_FINAL_VIA.png') }}" />
      </div>
      <div class="col-md-1"></div>
      <div class="col-md-4">
         <div class=" text-center ">
            <h2 class="login-heading">WATER BILLING SERVICE</h2>
            <hr class="h3-dash"/>
            <span>'Save Water, Save Life !!'</span>
         </div>
         <div class="col-md-12 text-center ">
            <h3 class="login-heading">Profile</h3>
         </div>
      </div>
      <div class="col-md-2"></div>
      <div class="col-md-2">
         <!-- <a class="btn btn-small success-btn mt-3 mb-3" href="#">
            Download
            </a> -->
      </div>
   </div>

   <div class="row">
    <div class="col-md-9"></div>
    <div class="col-md-3" style="position:absolute; right:0">
    @if (session('status'))
      <div class="alert alert-success">
          <p class="msg"> <?php echo  session("status"); ?></p>
      </div>
    @endif
     </div>
     </div>

   <div class="row">
      <div class="col-md-3 "></div>
      <div class="col-md-6 ">
      <!-- <form method="POST" action="{{ route('updateprofile') }}"> -->
            <div class="form-group">
               <label for="otp">Enter OTP</label>
               <input type="number" name="otp"  class="form-control" id="otp" aria-describedby="emailHelp" placeholder="Enter OTP">
               <small id="emailHelp" class="form-text text-muted">Please check your email for OTP.</small>
               <sapn class="countdown text-center">
               </span>
            </div>
         

            <button type="button" id="submit" class="btn login-btn">Submit</button>
            <button type="button" id="resend" disabled  class="btn  login-btn">
                    {{ __('Resend ') }}
                </button>
         <!-- </form> -->
      </div>
   </div>
   </div>
   <div class="row">
      <div class="col-md-5"></div>
      <div class="col-md-2 mt-3 text-center">
         <span class="text-sm " style="color:#fff">Powered By</span>
         <img class="img img-responsive " style=" width: 70%;  margin-left: 30px;"  src="{{ asset('img/white_logo.png') }}" />
         <!-- <a class="btn  success-btn" href="#">&nbsp;&nbsp;&nbsp;BACK&nbsp;&nbsp;&nbsp;</a> -->
      </div>
      <div class="col-md-5"></div>
   </div>

@endsection
@section('custom_scripts')
<script>
   $(document).ready(function(){
    jQuery('.alert-success').hide('slide', {direction: 'right'}, 10000);
    timer();
   });
   var otp = "{{$otp}}";
   var mobile = "{{$mobile}}";
   $("#submit").click(function(e){
  
          var textval = $('#otp').val();
          if(textval==otp){
      $.ajax({
      url: "/customer/updatedata",
      data: {
    mobile: mobile,
      },
      type: "post",
      headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      success: function(result){
         window.location.href = '/customer/profile';
      
            }
      });
          }else{
              alert('please enter a valid OTP')
                 e.preventDefault(e);
          }
             });
      
   
      
      $("#resend").click(function() {
      $('#resend').prop('disabled', true);
      $.ajax({
      url: "/resend",
      data: {
    mobile: mobile,
      },
      type: "post",
      dataType:'json',
      headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      success: function(result){
         otp = result.otp;
         timer();
      
            }
      });
      });
      
      function timer(){
      
      var timer2 = "3:1";
      
      var interval = setInterval(function() {
      
      var timer = timer2.split(':');
      //by parsing integer, I avoid all extra string processing
      var minutes = parseInt(timer[0], 10);
      var seconds = parseInt(timer[1], 10);
      --seconds;
      minutes = (seconds < 0) ? --minutes : minutes;
      seconds = (seconds < 0) ? 59 : seconds;
      seconds = (seconds < 10) ? '0' + seconds : seconds;
      //minutes = (minutes < 10) ?  minutes : minutes;
      $('.countdown').html(minutes + ':' + seconds);
      if (minutes < 0) clearInterval(interval);
      //check if both minutes and seconds are 0
      if ((seconds <= 0) && (minutes <= 0)) clearInterval(interval);
      if((seconds == 0) && (minutes == 0)){
      $('#resend').prop('disabled', false);
      
      }
      timer2 = minutes + ':' + seconds;
      }, 1000);
      }
</script>
@endsection