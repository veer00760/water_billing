@extends('layouts.customer.app')

@section('content')
<div class="row">
   <div class="col-md-10"></div>
   <div class="col-md-2 ">
   <div class="col-md-2 ">
      <a class="btn btn-small btn-success mb-3" href="/customer/dashboard">
      Back
      </a>
   </div>
                            </div>

</div>
<div class="wrapper">

<div class="row">
      <div class="col-md-3">
         <img class="img img-responsive logo" src="{{ asset('img/LOGO_FINAL_VIA.png') }}" />
      </div>
      <div class="col-md-1"></div>
      <div class="col-md-4">
         <div class=" text-center ">
            <h2 class="login-heading">WATER BILLING SERVICE</h2>
            <hr class="h3-dash"/>
            <span>'Save Water, Save Life !!'</span>
         </div>
         <div class="col-md-12 text-center mb-3">
            <h3 class="login-heading ">New Connection Documents</h3>
            <hr class="h3-dash"/>
         </div>
      </div>
      <div class="col-md-1"></div>
      <div class="col-md-3 text-center">   
       
      </div>
   </div>
   <div class="row">
   <div class="col-md-2"></div>
   <div class="col-md-10">
       <div class="row">
       <div class="col-md-4"></div>
    <div class="col-md-2">
   <div class="card-columns">
  <div class="card bg-purples">
    <div class="card-body text-center">
     <a class="bg-purples" href="/customer/bill/">
     <a  class="bg-purples" href="../img/sample.pdf" download>

      <h3 class="card-text">Download Pdf</h3></a>
    </div>
  </div>
</div>
</div>







<div class="col-md-2"></div>
   </div>

<div class="row">
    <div class="col-md-4"></div>
    <div class="col-md-2 mt-3 ml-100">
      <span class="text-center ft-16">Powered By</span>
    <img class="img img-responsive  ved-logo" src="{{ asset('img/black.png') }}" />

<!-- <a class="btn  success-btn" href="#">&nbsp;&nbsp;&nbsp;BACK&nbsp;&nbsp;&nbsp;</a> -->
    </div>
    <div class="col-md-5">
    </div>
</div>
</div>
@endsection
@section('custom_scripts')
<script>

</script>
@endsection