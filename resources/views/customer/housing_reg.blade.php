<x-guest-layout>
    <x-auth-card>
        <x-slot name="logo">
            <a href="/">
                <x-application-logo class="w-20 h-20 fill-current text-gray-500" />
            </a>

            <style>
            .selectize-input{
                border-radius: 11px !important;
                padding: 10px 12px !important;
            }
            </style>
        </x-slot>

        <!-- Session Status -->
        <x-auth-session-status class="mb-4" :status="session('status')" />
        @if ($error = $errors->first('password'))
            <!-- <div class="alert alert-danger">
           
            </div> -->
            @endif
        <!-- Validation Errors -->
        <x-auth-validation-errors class="mb-4" :errors="$errors" />
        <div class="col-md-12 mr-3">
        <div class="row">
        <div class="col-md-2"></div>
            <div class="col-md-8">
            <img class="img img-responsive" src="{{ asset('img/LOGO_FINAL_VIA.png') }}" />
            </div>
            <div class="col-md-2"></div>
        </div>
        <div class="row">
        
            <div class="col-md-12 text-center ">
                <h4 class="login-heading">WATER BILLING SERVICE</h4>
                <hr class="new4"/>
<span>'Save Water, Save Life !!'</span>
            </div>
        </div>

        <div class="row">
        
        <div class="col-md-12 text-center ">
            <h4 class="login-heading">REGISTER</h4>
        </div>
    </div>

        <form method="POST" action="{{ route('storecustomer') }}">
            @csrf

            <div>
                <!-- <x-label for="email" :value="__('Email')" /> -->

                <select id="connection_id" style="border-radius:10px;"  class="block mt-1 w-full form-control"  name="connection_id"  required autofocus >
            <option value="">Select Connection</option> 
            @foreach($conns as $con)
            <option value="{{$con->id}}">{{$con->conn_no .'-----'. $con->party->name}} </option> 
            @endforeach
            </select>
            </div>

            <!-- Email Address -->
            <div class="mt-4">
                <!-- <x-label for="email" :value="__('Email')" /> -->

                <x-input id="email" placeholder="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required  />
            </div>

            <div class="mt-4">
                <!-- <x-label for="email" :value="__('Email')" /> -->

                <x-input id="mobile" placeholder="Mobile" class="block mt-1 w-full" type="number" name="mobile" :value="old('mobile')" required  />
            </div>

            <!-- Password -->
            <div class="mt-4">
                <!-- <x-label for="password" :value="__('Password')" /> -->

                <x-input id="password" placeholder="Password" class="block mt-1 w-full"
                                type="password"
                                name="password"
                                required autocomplete="current-password" />
            </div>

        
            <div class="mt-4">
                <!-- <x-label for="password_confirmation" :value="__('Confirm Password')" /> -->

                <x-input id="password_confirmation" placeholder="Confirm password" class="block mt-1 w-full"
                                type="password"
                                name="password_confirmation" required />
            </div>

                <div class="row">
                    <div class="col-md-3"></div>
                <button class="btn btn-lg mt-3 login-btn col-md-6">
                    {{ __('Submit') }}
                </button>
                <div class="col-md-3"></div>
             </div>

             <div class="row">
                    <div class="col-md-3"></div>
                <a href="/login" class="btn btn-sm mt-3 login-btn col-md-6">
                    {{ __('Already a Member ') }}
                </a>
                <div class="col-md-3"></div>
             </div>

             <div class="row mt-3">
             <div class="col-md-3 "></div>
             <div class="col-md-6">
             <img class="img img-responsive logo ved-logo" src="{{ asset('img/black.png') }}" />
           </div>
             </div>
             
        </form>
</div>
<script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js" integrity="sha256-+C0A5Ilqmu4QcSPxrlGpaZxJ04VjsRjKu+G82kl5UJk=" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap3.min.css" integrity="sha256-ze/OEYGcFbPRmvCnrSeKbRTtjG4vGLHXgOqsyLFTRjg=" crossorigin="anonymous" />

<script>
$(document).ready(function(){
  $('#connection_id').selectize();
});
  $(".btn-sm").click(function() {
      $('#loading').show();
  });
</script>
    </x-auth-card>
</x-guest-layout>
