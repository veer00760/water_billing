<x-app-layout>
   <x-slot name="header">
      <div class="row">
         <div class="col-md-10">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
               {{ __('Edit Connection') }}
            </h2>
         </div>
         <div class="col-md-2">
            <a class="btn btn-primary" href="{{ route('conn') }}">
            {{ __('Connection List') }}
            </a>
         </div>
      </div>
   </x-slot>
   <div class="py-12">
      <div class="max-w-5xl mx-auto sm:px-6 lg:px-8">
         <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
            <div class="p-6 bg-white border-b border-gray-200">
            @if (session('status') == 'success')
            <div class="mb-4 font-medium text-sm text-green-600">
               <h5> {{ __('.Data Save Succesfully') }}</h5>
            </div>
        @endif
               <x-auth-validation-errors class="mb-4" :errors="$errors" />
               <fieldset class="scheduler-border">
                  <legend class="scheduler-border">Edit Connection:</legend>
                  <form method="POST" action="{{ route('conn.update',$connection->id) }}">
                  @method('PUT')
                     @csrf
                     <div class="form-row">
         
         <!-- Party type id -->
         <div class="mt-4 col">
            <x-label for="party" :value="__('Select Party')" />
            <select id="party" class="form-control" name="party" required>
               <option value="">Choose...</option>
               @foreach($parties as $party)
               @if($party->id==$connection->parties_id)
                          @php $selected = "selected='selcted'"; @endphp
                          @else
                          @php $selected=""; @endphp
                          @endif
               <option {{$selected}} value="{{$party->id}}">{{$party->name}}</option>
               @endforeach
            </select>
            <!-- <x-input id="party_types_id" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required /> -->
         </div>
      </div>
    
      <div class="form-row mt-4">
         
      </div>
      <fieldset class="scheduler-border">
         <legend class="scheduler-border">Edit Connection</legend>
         <div class="form-row">
            <!-- Name -->
            <div class="mt-4 col">
               <x-label for="ctype" :value="__('Connection Type')" />
              
               <!-- <x-input id="onname" class="block mt-1 w-full"
                  type="text" name="connection_types_id" value="{{$connection->connType($connection->connection_types_id)->type}}" readonly   /> -->
            <select id="ctype" class="form-control" name="connection_types_id" required>
                  <option value="">Choose...</option>
                  @foreach($connectionTypes as $connectionType)
                  @if($connectionType->id==$connection->connection_types_id)
                          @php $selected = "selected='selcted'"; @endphp
                          @else
                          @php $selected=""; @endphp
                          @endif
                  <option {{$selected}}  charge="{{$connectionType->minimum_charge}}" value="{{$connectionType->id}}">{{$connectionType->type}}</option>
                  @endforeach
               </select>
               <span id="min-charge"></sapn>
            </div>
            <!-- Date -->
            <div class="mt-4 col">
               <x-label for="date" :value="__('Date')" />
               <x-input id="date" readonly class="block mt-1 w-full" type="text" name="date" value="{{date('d-m-Y')}}"  />
            </div>
         </div>
         <div class="form-row">
                           <!--Connection  Name -->
                           <div class="mt-4 col">
                              <x-label for="conn_no" :value="__('Connection No')" />
                              <x-input id="conn_no" class="block mt-1 w-full"
                                 type="number" name="conn_no" :value="old('conn_no',$connection->conn_no)" required   />
                           </div>
            <div class="mt-4 col">
      <x-label for="party_types_id" :value="__('Party Type')" />
      <select id="party_types_id" class="form-control" name="party_types_id" required>
         <option value="">Choose...</option>
         @foreach($partyTypes as $partyType)
         @if($partyType->id==$connection->party_types_id)
      @php $selected = "selected='selcted'"; @endphp
      @else
      @php $selected=""; @endphp
      @endif
         <option {{$selected}} value="{{$partyType->id}}">{{$partyType->type}}</option>
         @endforeach
      </select> 
    </div>
    </div>
         <div class="form-row">
            <!--Connection  Name -->
            <div class="mt-4 col">
               <x-label for="onname" :value="__('Connection on Name')" />
               <x-input id="onname" class="block mt-1 w-full"
                  type="text" name="onname" :value="old('onname',$connection->name)" required   />
            </div>
            <!-- <x-input id="date" class="block mt-1 w-full" type="text" name="date" :value="old('date')" required /> -->
            <!-- Date -->
            <div class="mt-4 col">
               <x-label for="onphone" :value="__('Connection On Phone No')" />
               <x-input id="onphone" minlength="10" maxlength="10" class="block mt-1 w-full" type="text" name="onphone" :value="old('onphone',$connection->phone)"  />
            </div>
         </div>
         <div class="form-row">
            <!--Connection  Name -->
            <div class="mt-4 col">
               <x-label for="onemail" :value="__(' Email')" />
               <x-input id="onemail" class="block mt-1 w-full"
                  type="email" name="onemail" :value="old('onemail',$connection->email)"    />
            </div>
            <!-- <x-input id="date" class="block mt-1 w-full" type="text" name="date" :value="old('date')" required /> -->
            <!-- Date -->
            <div class="mt-4 col">
               <x-label for="onaddress" :value="__('Address')" />
               <textarea id="onaddress" class="block mt-1 w-full form-control"  name="onaddress">{{old('onaddress',$connection->address)}}</textarea>
            </div>
         </div>
     
                     <div class="flex items-center justify-end mt-4">
                        <x-button class="ml-4">
                           {{ __('Save') }}
                        </x-button>
                     </div>
                  </form>
               </fieldset>
            </div>
         </div>
      </div>
   </div>
</x-app-layout>