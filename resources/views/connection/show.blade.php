<x-app-layout>
    <x-slot name="header">
    <div class="row">
    <div class="col-md-10">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Connections Show') }}
        </h2>
        </div>
        <div class="col-md-2">
        <a class="btn btn-primary" href="{{ route('conn') }}">
                        {{ __('Connections List') }}
                    </a>
        </div>
        </div>
    </x-slot>
    <div class="col-md-12">
    <div class="row">
    <div class="col-md-9"></div>
    <div class="col-md-3" style="position:absolute; right:0">
    @if (session('status'))
      <div class="alert alert-success">
          <p class="msg"> <?php echo  session("status"); ?></p>
      </div>
  @endif
     </div>
     </div></div>
    <div class="py-12">     
        <div class="py-12 bg-white max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg table-responsive">
   <div class="col-md-12">

<table class="table table-bordered">
    <thead class="thead-light">
    <tr><th class="text-center" colspan=7>Connection Details</th></tr>
      <tr>
        <th>Connection No</th>
        <th>Party Name</th>
        <th>Current</th>
        <th>Address</th>
        <th>Status</th>
        <th>Email</th>
        <th>Phone NO</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>{{$connection->conn_no}}</td>
        <td>{{$connection->party->name}}</td>
        <td>{{$connection->name}}</td>
        <td>{{$connection->address}}</td>
        <td>{{$connection->status}}</td>
        <td>{{$connection->email}}</td>
        <td>{{$connection->phone}}</td>
      </tr>

    </tbody>
  </table>


  <table class="table table-bordered">
    <thead class="thead-light">
    <tr><th class="text-center" colspan=6>New & Renew Details</th></tr>
      <tr>
        <th>Connection Type</th>
        <th>Connection Charge</th>
        <th>Deposit Charge</th>
        <th>Road Crossing Charge</th>
        <th>Minimum Charge</th>
        <th>Note</th>
      </tr>
    </thead>
    <tbody>
    @php $total = 0; $renewtotal=0; $newtotal=0; @endphp
   @foreach($connection->allcharges as $charges)
   @if($charges->type=='new')
   @php $type = 'New' @endphp
   @else
   @php $type = 'Renew' @endphp
   @endif
      <tr>
        <td><b>{{$type.' ' }}</b>{{ date('d-m-Y',strtotime($charges->created_at))}}</td>
        <td>{{$charges->connection_charge}}</td>
        <td>{{$charges->deposit}}</td>
        <td>{{$charges->road_crossing}}</td>
        <td>{{$charges->minimum_charge}}</td>
        <td>{{$charges->note}}</td>
      </tr>
      @endforeach

    </tbody>
  </table>

  <table class="table table-bordered">
    <thead class="thead-light">
    <tr><th class="text-center" colspan=2>Deactivate Details</th></tr>
      <tr>
        <th>Deactivate On</th>
        <th>Reason</th>
      </tr>
    </thead>
    <tbody>
    @foreach($connection->activity as $act)
      <tr>
        <td>{{$act->created_at}}</td>
        <td>{{$act->reason}}</td>
      </tr>

      @endforeach
    </tbody>
  </table>

            </div>
        </div>
    </div>
    <script>
 
      //  jQuery.noConflict();
      jQuery(document).ready(function(e) {
      // e.noConflict();
      jQuery('#table').DataTable();

            $('.alert-success').hide('slide', {direction: 'right'}, 4000);
      
} );


    </script>
</x-app-layout>
