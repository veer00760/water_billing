<x-app-layout>
    <x-slot name="header">
    <div class="row">
    <div class="col-md-10">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Connections') }}
        </h2>
        </div>
        <div class="col-md-2">
        <a class="btn btn-primary" href="{{ route('conn.create') }}">
                        {{ __('Add Connections') }}
                    </a>
        </div>
        </div>
    </x-slot>
    <div class="col-md-12">
    <div class="row">
    <div class="col-md-9"></div>
    <div class="col-md-3" style="position:absolute; right:0">
    @if (session('status'))
      <div class="alert alert-success">
          <p class="msg"> <?php echo  session("status"); ?></p>
      </div>
    @endif
     </div>
     </div></div>
    <div class="py-12">

<div class="col-md-12">
<div class="row">
<div class="col-md-6">
<button class="btn btn-lg btn-success mb-5">All Connections : {{$connections->count()}}</button>
</div>
<div class="col-md-6">
<button style="float:right" class="btn btn-lg  mb-5 btn-primary">Active Connections : {{$active}}</button>
</div>
</div>
</div>
        <div class="py-12 bg-white max-w-7xl mx-auto sm:px-6 lg:px-8">

            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg table-responsive">
            
            <table class="table" id="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Connection No</th>
      <th scope="col">Party Name</th>
      <th scope="col">Current</th>
      <th scope="col">Conn Type</th>
      <th scope="col">Status</th>
      <th scope="col">Email</th>
      <th scope="col">Phone No</th>
      <th scope="col">Connection</th>
      <th scope="col">Meter</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
  @php $i=1; @endphp
  @foreach($connections as $connection)
  
    <tr>
      <th scope="row">{{$i++}}</th>
      <th>{{$connection->conn_no}}</th>
      <td>{{$connection->party->name}}</td>
      <td>{{$connection->name}}</td>
      <td>@if($connection->party_types_id=='1')Industry @else Housing @endif</td>
      <td>{{$connection->status}}</td>
      <td>{{$connection->email}}</td>
      <td>{{$connection->phone}}</td>
      <td>{{$connection->connType($connection->connection_types_id)->type}}@if($connection->connType($connection->connection_types_id)->type=='Boring')@else @endif</td>
      <td>{{$connection->meter->status}}</td>
      <td> @if($connection->status=='active') 
      <a href="#"  data-toggle="modal" data-target="#edit{{$i}}"><i class="fa fa-trash"></i></a>
       @else <a href="#"  data-toggle="modal" data-target="#charge{{$i}}"><i class="fa fa-trash"></i></a> @endif |<a href="{{route('conn.edit',$connection->id)}}"><i class="fa fa-pencil" aria-hidden="true"></i></a> | <a href="#"  data-toggle="modal" data-target="#{{$i}}"><i class="fa fa-eye" aria-hidden="true"></i></a> | <a href="{{route('conndetails',$connection->id)}}"><i class="fa fa-info-circle" aria-hidden="true"></i></a> 
      </td>
    </tr>

           <!-- Modal -->
<div class="modal fade" id="{{$i}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">{{$connection->party->name}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

      <div class="form-row">
      <div class="col-md-6"><span><b>Connection No :</b>{{$connection->conn_no}}</span></div>
      <div class="col-md-6"><span><b>Party Name :</b> {{$connection->party->name}}</span></div>
      <div class="col-md-6"><span><b>Current :</b>  {{$connection->name}}</span></div>
      <div class="col-md-6"><span><b>Address :</b>  {{$connection->address}}</span></div>
      <div class="col-md-6"><span><b>Status :</b> {{$connection->status}}</span></div>
      <div class="col-md-6"><span><b>Email :</b> {{$connection->email}}</span></div>
      <div class="col-md-6"><span><b>Phone No :</b> {{$connection->phone}}</span></div>
      <div class="col-md-6"><span><b>Capital Amount :</b> {{$connection->capital}}</span></div>
 @php $total =0; @endphp
    @if(empty($connection->total_charges['0']))
    <div class="col-md-6"><span><b>Total Security Deposit :</b> 0</span></div>
@else
     @foreach($connection->total_charges as $totalcharge)

     @php $total += $totalcharge->deposit @endphp
@endforeach
<div class="col-md-6"><span><b>Total Security Deposit :</b> {{$total}}</span></div>

@endif
<div class="col-md-6"><span class="text-danger"><b>Debit Amount :</b> {{$connection->debit}}</span></div>
      <div class="col-md-6"><span class="text-success"><b>Credit Amount :</b> {{$connection->credit}}</span></div>

       </div>

      </div>
     
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="edit{{$i}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                     <div class="modal-dialog" role="document">
                        <div class="modal-content">
                           <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLongTitle">{{$connection->conn_no}}</h5>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                              </button>
                           </div>
                           <div class="modal-body">
                              <form method="GET" action="{{route('conn.show',$connection->id)}}">
                                 <div class="form-row">
                                    <div class="mt-4 col">
                                       <x-label for="deactivate_reason" :value="__(' Deactivate Reason ')" />
                                       <input  class="block mt-1 w-full form-control"
                                          type="text" name="deactivate_reason" :value="old('deactivate_reason')" required   />
                                    </div>
                                 </div>
                                 <div class="modal-footer">
                                    <div class="flex items-center justify-end mt-4">
                                       <x-button class="ml-4">
                                          {{ __('Save') }}
                                       </x-button>
                                    </div>
                                 </div>
                              </form>
                           </div>
                        </div>
                     </div>
                  </div>












<div class="modal fade" id="charge{{$i}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Renew Connection</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

      <form method="POST" action="{{ route('savecharge') }}">
                     @csrf
                     <input type="hidden" name="connections_id" value="{{$connection->id}}"/>
                     <input type="hidden" name="status" value="active"/>
                     <input type="hidden" name="type" value="renew"/>

                      <div class="form-row">
                      <div class="mt-4 col">
                                 <x-label for="connection_charge" :value="__(' Connection Charge')" />
                                 <x-input  class="block mt-1 w-full"
                                    type="number" name="connection_charge" :value="old('connection_charge')" required   />
                            
                              </div>
                              <!--Connection  Name -->
                              <div class="mt-4 col">
                                 <x-label for="deposit" :value="__(' Deposit')" />
                                 <x-input   class="block mt-1 w-full"
                                    type="number" name="deposit" :value="old('deposit')" required   />
                              </div>
                              <!-- <x-input id="date" class="block mt-1 w-full" type="text" name="date" :value="old('date')" required /> -->
                           </div>
                           <div class="form-row">

                                                         <!-- Date -->
                                                         <div class="mt-4 col">
                                 <x-label for="road_crossing" :value="__('Road Crossing Charges')" />
                                 <x-input   class="block mt-1 w-full"
                                    type="number"   name="road_crossing" :value="old('road_crossing')" required   />
                              </div>
                              <div class="mt-4 col">
                                 <x-label for="minimum_charge" :value="__('Minnumum Charge')" />
                                 <x-input   class="block mt-1 w-full"
                                    type="number"   name="minimum_charge" :value="old('minimum_charge')" required   />
                              </div>
                              </div>
                              <hr style="border: 1px solid black;"/>
                           Total Charges For New Charges: <span id="total"></sapn>INR
                           <div class="form-row">
                           <div class="mt-4 col">
                                 <x-label for="paid_through"  :value="__('Paid Through')" />
                                 <select  paytype="{{$i}}"  class="paid_through form-control" name="paid_through" required>
                                 <option value="">Choose...</option>
                                 <option value="cash">Cash</option>
                                 <option value="cheque">Cheque</option>
                              </select>
                              </div>

                              <div class="mt-4 col">
                                 <x-label for="check_no" :value="__('Cheque No')" />
                                 <x-input   class="check_no{{$i}} block mt-1 w-full form-control"
                                    type="number"   name="check_no" :value="old('check_no')"    />
                              </div>
                              <div class="mt-4 col">
                                 <x-label for="note" :value="__('Note')" />
                                 <x-input   class="block mt-1 w-full"
                                    type="text"   name="note" :value="old('note')"    />
                              </div>

                           </div>
                    
                  
             
      <div class="modal-footer">
      <div class="flex items-center justify-end mt-4">
                        <x-button class="ml-4">
                           {{ __('Save') }}
                        </x-button>
                     </div>
                     </form>
      </div>
    </div>
  </div>
</div>
    @endforeach
  </tbody>
</table>
             
            </div>
        </div>
    </div>
   <style>
   .dt-button{
      
    background-color: #007bff !important;
    color: #fff !important;
    padding-left: 17px !important;
    padding-right: 17px !important;
    padding-top: 10px !important;
    padding-bottom: 10px !important;

   }
   </style>
    <script type="text/javascript"  src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script> 
      <script type="text/javascript"  src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script> 
      <script type="text/javascript"  src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script> 
      <script type="text/javascript"  src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script> 
      <script type="text/javascript"  src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js"></script> 
      <script type="text/javascript"  src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js"></script> 

    <script>
 
      //  jQuery.noConflict();
      jQuery(document).ready(function(e) {
      // e.noConflict();
      jQuery('#table').DataTable({
         dom: 'Bfrtip',
        buttons: [
         {
                extend: 'pdfHtml5',
                title: 'All Connection Records VIA',
                footer: true,
                orientation: 'portrait',
                pageSize: 'LEGAL',
                text: 'Print Me'

            },
        ],
      });

            $('.alert-success').hide('slide', {direction: 'right'}, 10000);
      
} );

jQuery('.paid_through').change(function(){
var unit =jQuery(this).val();
var paytype =jQuery(this).attr('paytype');
// var className = ".check_no " + type;
// alert(type);
if(unit=='cheque'){
     jQuery(".check_no"+paytype).removeAttr("disabled"); 
  // jQuery(this).next().find('.check_no').removeAttr("disabled"); 

}else{
  // alert("helllo");
  jQuery(".check_no"+paytype).attr('disabled','disabled');

}
});
    </script>
</x-app-layout>
