<x-app-layout>
   <x-slot name="header">
      <div class="row">
         <div class="col-md-10">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
               {{ __('New Connection Charges') }}
            </h2>
         </div>
         <!-- <div class="col-md-2">
            <a class="btn btn-primary" href="{{ route('conn') }}">
            {{ __('Connection List') }}
            </a>
            </div> -->
      </div>
   </x-slot>
   <div class="py-12">
      <div class="max-w-5xl mx-auto sm:px-6 lg:px-8">
         <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
            <div class="p-6 bg-white border-b border-gray-200">
               <x-auth-validation-errors class="mb-4" :errors="$errors" />
               <form method="POST" action="{{ route('savecharge') }}">
                  @csrf
                  <div class="form-row">
                     <div class="mt-4 col">
                        <x-label for="connection_charge" :value="__(' Connection Charge')" />
                        <x-input id="connection_charge" class="block mt-1 w-full"
                           type="number" name="connection_charge" :value="old('connection_charge')" required   />
                     </div>
                     <!--Connection  Name -->
                     <div class="mt-4 col">
                        <x-label for="deposit" :value="__(' Deposit')" />
                        <x-input id="deposit" class="block mt-1 w-full"
                           type="number" name="deposit" :value="old('deposit')" required   />
                     </div>
                     <!-- <x-input id="date" class="block mt-1 w-full" type="text" name="date" :value="old('date')" required /> -->
                  </div>
                  <div class="form-row">
                     <!-- Date -->
                     <div class="mt-4 col">
                        <x-label for="road_crossing" :value="__('Road Crossing Charges')" />
                        <x-input id="road_crossing" class="block mt-1 w-full"
                           type="number"   name="road_crossing" :value="old('road_crossing')" required   />
                     </div>
                     <div class="mt-4 col">
                        <x-label for="minimum_charge" :value="__('Other Charge')" />
                        <x-input id="minimum_charge" class="block mt-1 w-full"
                           type="number"   name="minimum_charge" :value="old('minimum_charge')" required   />
                     </div>
                  </div>
                  <hr style="border: 1px solid black;"/>
                  Total Charges For New Connection: <b><span id="total"> </span></b>   INR
                  <div class="form-row">
                     <div class="mt-4 col">
                        <x-label for="paid_through" :value="__('Paid Through')" />
                        <select id="paid_through" class="form-control" name="paid_through" required>
                           <option value="">Choose...</option>
                           <option value="cash">Cash</option>
                           <option value="cheque">Cheque</option>
                        </select>
                     </div>
                     <div class="mt-4 col">
                        <x-label for="check_no" :value="__('Cheque No')" />
                        <x-input id="check_no" class="block mt-1 w-full"
                           type="number" disabled   name="check_no" :value="old('check_no')"    />
                     </div>
                     <div class="mt-4 col">
                        <x-label for="note" :value="__('Note')" />
                        <x-input id="note" class="block mt-1 w-full"
                           type="text"   name="note" :value="old('note')"    />
                     </div>
                  </div>
                  <div class="flex items-center justify-end mt-4">
                     <x-button class="ml-4">
                        {{ __('Save') }}
                     </x-button>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
   <script>
      $('#minimum_charge').change(function(){
      var min =parseInt($(this).val());
      var cs = parseInt($("#road_crossing").val()*1);
      var dp = parseInt($("#deposit").val()*1);
      var conn = parseInt($("#connection_charge").val()*1);
      var ttl = min+cs+dp+conn;
      $("#total").text(ttl);
      });

      $('#paid_through').change(function(){
var unit =$(this).val();
if(unit=='cheque'){
   $("#check_no").removeAttr("disabled"); 

}else{
   $("#check_no").attr('disabled','disabled');

}
});
   </script>
</x-app-layout>