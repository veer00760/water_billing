<x-app-layout>
    <x-slot name="header">
    <div class="row">
    <div class="col-md-10">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Ledger') }}
        </h2>
        </div>
        <div class="col-md-2">
        <!-- <a class="btn btn-primary" href="{{ route('conn.create') }}">
                        {{ __('Add Connections') }}
                    </a> -->
                    <!-- <input type="button" class="btn btn-primary" value="Print" onclick="pdf()" /> -->

        </div>
        </div>
    </x-slot>
    <div class="col-md-12">
    <div class="row">
    <div class="col-md-9"></div>
    <div class="col-md-3" style="position:absolute; right:0">
    @if (session('status'))
      <div class="alert alert-success">
          <p class="msg"> <?php echo  session("status"); ?></p>
      </div>
    @endif
     </div>
     </div></div>
    <div class="py-12">
        <div class="py-12 bg-white max-w-7xl mx-auto sm:px-6 lg:px-8">

            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg table-responsive">
            <div id="print-window">
         
            <table class="table" id="table">
  <thead>
   
    <tr>
      <th>S.No</th>
      <th scope="col">Connection No</th>
      <th scope="col">Conn Type</th>
      <th scope="col">Size MM</th>
      <th scope="col">Prev Reading</th>
       <th scope="col">Current Reading</th>
      <th scope="col">Remark</th>
    </tr>
  </thead>
  <tbody>
 
  @php $i=1; @endphp
  @foreach($connections as $connection)
  
    <tr>
    <th scope="row">{{$i++}}</th>
   <th>{{$connection->conn_no}}</th>
      <td>@if($connection->party_types_id=='1')Industry @else Housing @endif</td>
      <td>{{$connection->connType($connection->connection_types_id)->type}}@if($connection->connType($connection->connection_types_id)->type=='Boring')@else @endif</td>
      <td>{{$connection->meter->current_unit}}</td>
      <td></td>
      <td></td>
    </tr>
    @endforeach
  </tbody>
</table>
</div>    
            </div>
        </div>
    </div>
   <style>
   .dt-button{
      
    background-color: #007bff !important;
    color: #fff !important;
    padding-left: 17px !important;
    padding-right: 17px !important;
    padding-top: 10px !important;
    padding-bottom: 10px !important;

   }
   </style>
    <script type="text/javascript"  src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script> 
      <script type="text/javascript"  src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script> 
      <script type="text/javascript"  src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script> 
      <script type="text/javascript"  src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script> 
      <script type="text/javascript"  src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js"></script> 
      <script type="text/javascript"  src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js"></script> 

    <script>
 
      //  jQuery.noConflict();
      jQuery(document).ready(function(e) {
      // e.noConflict();
      jQuery('#table').DataTable({
         dom: 'Bfrtip',
        buttons: [
         {
               extend: 'print',
                title: "Valsad Water Services   &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;             {{date('d-M-Y')}} <br/>All Connection Rough Ledger of   {{date('F - Y', strtotime('last month'))}} ",
                footer: true,
                orientation: 'portrait',
                pageSize: 'LEGAL',
                text: 'Print Me'

            },
        ],
      });

            $('.alert-success').hide('slide', {direction: 'right'}, 10000);
      
} );

jQuery('.paid_through').change(function(){
var unit =jQuery(this).val();
var paytype =jQuery(this).attr('paytype');
// var className = ".check_no " + type;
// alert(type);
if(unit=='cheque'){
     jQuery(".check_no"+paytype).removeAttr("disabled"); 
  // jQuery(this).next().find('.check_no').removeAttr("disabled"); 

}else{
  // alert("helllo");
  jQuery(".check_no"+paytype).attr('disabled','disabled');

}
});

function pdf() {
    let t = document.getElementById('print-window').innerHTML;

    let style = "<style>";
    style = style + "table {width: 100%; font-size: 17px;}";
    style = style + "table, th, td {border: solid 1px #DDD; border-collapse: collapse;";
    style = style + "padding: 2px 3px;text-align: center;}";
    style = style + "#table_info{display:none;} #table_paginate{display:none; } #table_filter{display:none; } .dt-buttons{display:none;} #table_length{display:none;} </style>";
    let win = window.open('', '', 'height=600,width=600');

    win.document.write('<html><head>');
    win.document.write('<title>Profile</title>');
    win.document.write(style);
    win.document.write('</head>');
    win.document.write('<body>');
    win.document.write(t);
    win.document.write('</body></html>');

    win.document.close();
    win.print();
}
    </script>
</x-app-layout>
