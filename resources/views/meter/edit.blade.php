<x-app-layout>
   <x-slot name="header">
      <div class="row">
         <div class="col-md-10">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
               {{ __('Edit Meter') }}
            </h2>
         </div>
         <div class="col-md-2">
            <a class="btn btn-primary" href="{{ route('meter') }}">
            {{ __('Meter List') }}
            </a>
         </div>
      </div>
   </x-slot>
   <div class="py-12">
      <div class="max-w-5xl mx-auto sm:px-6 lg:px-8">
         <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
            <div class="p-6 bg-white border-b border-gray-200">
            @if (session('status') == 'success')
            <div class="mb-4 font-medium text-sm text-green-600">
               <h5> {{ __('.Data Save Succesfully') }}</h5>
            </div>
        @endif
               <x-auth-validation-errors class="mb-4" :errors="$errors" />
               <fieldset class="scheduler-border">
                  <legend class="scheduler-border">Edit Meter:</legend>
                  <form method="POST" action="{{ route('meter.update',$meter->id) }}">
                  @method('PUT')
                     @csrf
                     <div class="mt-4 col">
                                 <x-label for="conn_no" :value="__(' Connection No')" />
                                 <x-input id="conn_no" class="block mt-1 w-full"
                                    type="text" name="conn_no" value="{{$meter->conn->conn_no}}" readonly    />
                                    <!-- <select id="conn_no" class="form-control" name="conn_no" required>
                              <option value="">Choose...</option>
                              @foreach($connections as $connection)
                              @if($connection->id==$meter->connections_id)
                          @php $selected = "selected='selcted'"; @endphp
                          @else
                          @php $selected=""; @endphp
                          @endif
                              <option {{$selected}} value="{{$connection->id}}">{{$connection->conn_no}}</option>
                              @endforeach
                           </select> -->
                   </div>
                   <div class="form-row">
                              <div class="mt-4 col">
                                 <x-label for="meter_no" :value="__(' Add Meter No')" />
                                 <x-input id="meter_no" class="block mt-1 w-full"
                                    type="text" name="meter_no" :value="old('meter_no',$meter->meter_no)" required   />
                              </div>
                              <!-- <x-input id="date" class="block mt-1 w-full" type="text" name="date" :value="old('date')" required /> -->
                              <!-- Date -->
                              <div class="mt-4 col">
                                 <x-label for="installation_date" :value="__('Date of Installation')" />
                                 <x-input id="installation_date" class="block mt-1 w-full"
                                    type="date" max="{{date('Y-m-d')}}" format="dd-mm-yyyy" name="installation_date" :value="old('installation_date',date('Y-m-d',strtotime($meter->installation_date)))" required   />
                              </div>
                           </div>
                           <div class="form-row">
                              <!--Connection  Name -->
                              <div class="mt-4 col">
                                 <x-label for="meter_test_copy" :value="__(' Copy of Meter Test')" />
                                 <x-input id="meter_test_copy" class="block mt-1 w-full"
                                    type="file" name="meter_test_copy" :value="old('meter_test_copy')"    />
                              </div>
                              <!-- <x-input id="date" class="block mt-1 w-full" type="text" name="date" :value="old('date')" required /> -->
                              <!-- Date -->
                              <div class="mt-4 col">
                                 <x-label for="application_copy" :value="__('Copy of Application')" />
                                 <x-input id="application_copy" class="block mt-1 w-full"
                                    type="file" name="application_copy" :value="old('application_copy')"    />
                              </div>
                           </div>
                           <div class="form-row">
                              <!--Connection  Name -->
                              <div class="mt-4 col">
                                 <x-label for="meter_test_copy" :value="__(' Meter Status')" />
                                 <div class="form-check">
                                 @if($meter->status=='active')
                                 @php $active='checked'; @endphp
                                 @else
                                 @php $active='' @endphp
                                 @endif

                                 @if($meter->status=='inactive')
                                 @php $inactive='checked'; @endphp
                                 @else
                                 @php $inactive='' @endphp
                                 @endif
                                    <input class="form-check-input" type="radio" name="status" id="active" value="active" {{$active}}>
                                    <label class="form-check-label" for="gridRadios1">
                                    Active
                                    </label>
                                 </div>
                                 <div class="form-check">
                                    <input class="form-check-input" type="radio" name="status" id="inactive" value="inactive" {{$inactive}}>
                                    <label class="form-check-label" for="gridRadios1">
                                    Inactive
                                    </label>
                                 </div>
                              </div>
                              <!-- <x-input id="date" class="block mt-1 w-full" type="text" name="date" :value="old('date')" required /> -->
                              <!-- Date -->
                              <div class="mt-4 col">
                                 <x-label for="innital_meter_reading" :value="__('Initial Mete Reading')" />
                                 <x-input id="innital_meter_reading" class="block mt-1 w-full"
                                    type="text" name="innital_meter_reading" :value="old('innital_meter_reading',$meter->innital_meter_reading)" required   />
                              </div>
                           </div>
                     <div class="flex items-center justify-end mt-4">
                        <x-button class="ml-4">
                           {{ __('Save') }}
                        </x-button>
                     </div>
                  </form>
               </fieldset>
            </div>
         </div>
      </div>
   </div>
</x-app-layout>