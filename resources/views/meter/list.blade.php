<x-app-layout>
   <x-slot name="header">
      <div class="row">
         <div class="col-md-10">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
               {{ __('Meter') }}
            </h2>
         </div>
         <div class="col-md-2">
            <a class="btn btn-primary" href="{{ route('meter.create') }}">
            {{ __('Add Meter') }}
            </a>
         </div>
      </div>
   </x-slot>
   <div class="col-md-12">
    <div class="row">
    <div class="col-md-9"></div>
    <div class="col-md-3" style="position:absolute; right:0">
    @if (session('status'))
      <div class="alert alert-success">
          <p class="msg"> <?php echo  session("status"); ?></p>
      </div>
    @endif
     </div>
     </div></div>
   <div class="py-12">
      <div class="py-12 bg-white max-w-7xl mx-auto sm:px-6 lg:px-8">
         <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg table-responsive">
            <table class="table" id="table">
               <thead>
                  <tr>
                     <th scope="col">#</th>
                     <th scope="col">Status</th>
                     <th scope="col">Connection No</th>
                     <th scope="col">Meter No</th>
                     <th scope="col">Current Reading</th>
                     <th scope="col">Date</th>
                     <th scope="col">Action</th>
                  </tr>
               </thead>
               <tbody>
                  @php $i=1; @endphp
                  @foreach($meters as $meter)
                  <tr>
                     <th scope="row">{{$i++}}</th>
                     <td>{{$meter->status}}</td>
                     <td>@if(isset($meter->conn->conn_no)){{$meter->conn->conn_no}}@else ------ @endif</td>
                     <td>{{$meter->meter_no}}</td>
                     <!-- <td>@if(isset($meter->connection->mybill)){{$meter->connection->mybill->unit}}@endif</td> -->
                        <td>{{$meter->current_unit}}</td>
                     <td>{{date('d-m-y',strtotime($meter->installation_date))}}</td>
                     <td>
                     <a href="{{ route('meter.edit',$meter->id) }}"><i class="fa fa-pencil"></i></a>
                        @if($meter->status=='active') 
                        <!-- <a href="#"  data-toggle="modal" data-target="#edit{{$i}}"><i class="fa fa-trash"></i></a> -->
                        <a onclick="return confirm('Are you sure want to change status?')" href="{{route('meter.show',$meter->id)}}" type="button btn-sm"><i class="fa fa-trash"></i></a>
                        @else not editable @endif|
                        <a href="#"  data-toggle="modal" data-target="#{{$i}}"><i class="fa fa-eye" aria-hidden="true"></i></a>
                     </td>
                  </tr>
                  <div class="modal fade" id="edit{{$i}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                     <div class="modal-dialog" role="document">
                        <div class="modal-content">
                           <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLongTitle">@if(isset($meter->meter_no)){{$meter->meter_no}}@else ------ @endif</h5>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                              </button>
                           </div>
                           <div class="modal-body">
                              <form method="GET" action="{{route('meter.show',$meter->id)}}">
                                 <div class="form-row">
                                    <div class="mt-4 col">
                                       <x-label for="connection_charge" :value="__(' Closing Unit ')" />
                                       <input  class="block mt-1 w-full form-control"
                                          type="number" name="closing_unit" :value="old('connection_charge')" required   />
                                    </div>
                                 </div>
                                 <div class="modal-footer">
                                    <div class="flex items-center justify-end mt-4">
                                       <x-button class="ml-4">
                                          {{ __('Save') }}
                                       </x-button>
                                    </div>
                                 </div>
                              </form>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- Modal -->
                  <div class="modal fade" id="{{$i}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                     <div class="modal-dialog" role="document">
                        <div class="modal-content">
                           <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLongTitle">@if(isset($meter->conn->conn_no)){{$meter->conn->conn_no}}@else ------ @endif</h5>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                              </button>
                           </div>
                           <div class="modal-body">
                              <div class="form-row">
                                 <div class="col-md-6"><span><b>Meter Status:</b>  {{$meter->status}}</span></div>
                                 <div class="col-md-6"><span><b>Connection No:</b> @if(isset($meter->conn->conn_no)){{$meter->conn->conn_no}}@else ------ @endif</span></div>
                                 <div class="col-md-6"><span><b>Meter No:</b>  {{$meter->meter_no}}</span></div>
                                 <div class="col-md-6"><span><b>Innital Reading:</b> {{$meter->innital_meter_reading}}</span></div>
                                 <div class="col-md-6"><span><b>Closing Reading:</b> {{$meter->closing_unit}}</span></div>
                                 <div class="col-md-6"><span><b>Date:</b> {{date('d-m-y',strtotime($meter->installation_date))}}</span></div>
                              </div>
                           </div>
                           <div class="modal-footer">
                              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                           </div>
                        </div>
                     </div>
                  </div>
                  @endforeach
               </tbody>
            </table>
         </div>
      </div>
   </div>
   <script>
      // jQuery.noConflict();
      jQuery(document).ready(function(e) {
      e.noConflict();
      jQuery('#table').DataTable();
      jQuery('.alert-success').hide('slide', {direction: 'right'}, 10000);

      } );
   </script>
</x-app-layout>