@extends('layouts.customer.app')
@section('custom_styles')
<style>
#loading {
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  position: fixed;
  display: block;
  opacity: 0.7;
  background-color: #fff;
  z-index: 99;
  text-align: center;
}

#loading-image {
  position: absolute;
  top: 100px;
  left: 240px;
  z-index: 100;
}
</style>
@endsection

@section('content')
<div class="row">
<div id="loading">
  <img id="loading-image" src="{{ asset('img/loader.GIF') }}" alt="Loading..." />
</div>

<form action="<?php echo $url; ?>" method="post" name="payuForm"><br />
            <input type="hidden" name="key" value="{{$field['key']}}" /><br />
            <input type="hidden" name="hash" value="{{$field['hash']}}"/><br />
            <input type="hidden" name="txnid" value="{{$field['txnid']}}" /><br />
            <input type="hidden" name="amount" value="{{$field['amount']}}" /><br />
            <input type="hidden" name="firstname" id="firstname" value="{{$field['firstname']}}" /><br />
            <input type="hidden" name="email" id="email" value="{{$field['email']}}" /><br />
            <input type="hidden" name="productinfo" value="{{$field['productinfo']}}"><br />
            <input type="hidden" name="surl" value="https://admin.valsadindustrialwaterservices.in/subscribe-response" /><br />
            <input type="hidden" name="furl" value="https://admin.valsadindustrialwaterservices.in/subscribe-response" /><br />
            <input type="hidden" name="service_provider" value="payu_paisa"  /><br />
            <input type="hidden" name="udf1" value="{{$field['udf1']}}"  /><br />
            <input type="hidden" name="udf2" value="{{$field['udf2']}}"  /><br />
            
          
        </form>
</div>
@endsection
@section('custom_scripts')
<script>
   $(document).ready(function(){
      var payuForm = document.forms.payuForm;
           payuForm.submit();
      $('#loading').hide();

   });
</script>
@endsection