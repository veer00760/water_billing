@props(['status'])

@if ($status)
    <div {{ $attributes->merge(['class' => 'font-medium succ-msg text-sm text-green-600']) }}>
        {{ $status }}
    </div>
@endif
