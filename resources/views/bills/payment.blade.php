<x-app-layout>
   <x-slot name="header">
      <div class="row">
         <div class="col-md-10">
            <!-- <h2 class="font-semibold text-xl text-gray-800 leading-tight">
               {{ __('Add Meter') }} (Only if last meter is inactive)
            </h2> -->
         </div>
         <div class="col-md-2">
            <a class="btn btn-primary" href="{{ route('createbill') }}">
            {{ __('Bill List') }}
            </a>
         </div>
      </div>
   </x-slot>
   <div class="py-12">
      <div class="max-w-5xl mx-auto sm:px-6 lg:px-8">
         <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
            <div class="p-6 bg-white border-b border-gray-200">
               <x-auth-validation-errors class="mb-4" :errors="$errors" />
               <div class="form-row">
                           <!--Connection  Name -->
                           <div class="mt-3 col">
                              <x-label for="onname" :value="__('Enter Voucher No')" />
                              <x-input id="id" class="block mt-1 w-full"
                                 type="text" name="id" :value="old('id')"    />
                           </div>
                           <div class="mt-3 col">
                              <x-label for="onname" :value="__('Enter Connection no')" />
                              <x-input id="conn_no" class="block mt-1 w-full"
                                 type="number" name="conn_no" :value="old('conn_no')"    />
                           </div>
                           <div class="mt-3 col">
                              <x-label for="onname" :value="__('Bill type')" />
                              <select id="type" class="form-control">
                              <option value="manual">Manual</option>
                              <option value="custom">Custom</option>
                             </select>
                           </div>
                           <div class="mt-5 col">
                      <button class="btn btn-sm btn-primary" id="search">Search</button>
                           </div>
                </div>
              
               <div id="data" style="line-height:35px;"></div>

                </div>
            </div>
         </div>
      </div>
   </div>
   <script>

   // $('#id').change(function(){
   //    var id =parseInt($(this).val());
   //  $.ajax({
   //      url: "/getbilldetail",
   //      data: { "id": id },
   //      dataType:"json",
   //      type: "post",
   //      headers: {
   //                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
   //            },
   //      success: function(data){
   //        $("#data").html(data);
   //      }
   //  });
   // });

   $('#search').click(function(){
       var id =$("#id").val();
       var conn_no =$("#conn_no").val();
       var type =$("#type").val();

    $.ajax({
        url: "/getbilldetail",
        data: { "conn_id": conn_no,"id":id, "type":type },
        dataType:"json",
        type: "post",
        headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
        success: function(data){
          $("#data").html(data);
        }
    });
   });

   $(document).on('change', '#pay_by', function() {
      var unit =$(this).val();
if(unit=='cheque'){
   $("#check_no").removeAttr("disabled"); 
   $("#bank").attr("required", true);
   $("#bank").removeAttr("disabled");

}else{
   $("#check_no").attr('disabled','disabled');
   $("#bank").attr("required", false);
   $("#check_no").val(" "); 
   $("#bank").attr('disabled','disabled');



}
    });

    $(document).on('change', '#amount', function() {
      var total =parseInt($("#total").text());
      var amount =parseInt($(this).val());
      if(amount > total){
         $("#credit").val(parseInt(amount-total));
      }else{
         $("#credit").val(0);  
      }
      if(amount < total){
         $("#debit").val(parseInt(total-amount)); 
      }else{
         $("#debit").val(0);  
      }
      if(amount==total){
         $("#debit").val(0);  
         $("#credit").val(0);  

      }

    });

    $("#conn_no").autocomplete({
	source: function (request, response) {
		// Fetch data
		$.ajax({
			url: "/getconnectionbyconn",
			data: {
				value: request.term
			},
			dataType: "json",
			type: "post",
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			success: function (data) {
				console.log(data);
				//   response( data );
				response($.map(data, function (item) {
					return {
						label: item.conn_no,
						value: item.id
					};
				}));
			}
		});
	},
	select: function (event, ui) {
		//   alert(ui.item.label);
		// Set selection
		// $('#conn_no').val(ui.item.label); // display the selected text
		// $('#conn_no').val(ui.item.label); // save selected id to input
		$('#conn_no').val(ui.item.label);
      // getbilldata(ui.item.label);
		return false;
	}
});

   </script>
</x-app-layout>