<x-app-layout>
   <x-slot name="header">
      <div class="row">
         <div class="col-md-10">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
               {{ __('Add Bill') }} 
            </h2>
         </div>
         <div class="col-md-2">
            <a class="btn btn-primary" href="{{ route('createbill') }}">
            {{ __('Bill List') }}
            </a>
         </div>
      </div>
	  <script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js" integrity="sha256-+C0A5Ilqmu4QcSPxrlGpaZxJ04VjsRjKu+G82kl5UJk=" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap3.min.css" integrity="sha256-ze/OEYGcFbPRmvCnrSeKbRTtjG4vGLHXgOqsyLFTRjg=" crossorigin="anonymous" />

   </x-slot>
   <div class="py-12">
      <div class="max-w-5xl mx-auto sm:px-6 lg:px-8">
         <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
            <div class="p-6 bg-white border-b border-gray-200">
            @if (session('status') == 'success')
            <div class="mb-4 font-medium text-sm text-green-600">
               <h5> {{ __('.Data Save Succesfully') }}</h5>
            </div>
            @endif

            @if (session('error')=='error')
            <div class="mb-4 font-medium text-sm text-green-600">
               <h5> {{ __('.Connection Not Found') }}</h5>
            </div>
            @endif
               <x-auth-validation-errors class="mb-4" :errors="$errors" />
               <fieldset class="scheduler-border">
                  <legend class="scheduler-border">Add Bill:</legend>
                  <form id="createbill" method="POST" action="{{ route('createbill.store') }}">
                     @csrf
					 <div class="form-row">
                              <div class="mt-4 col">
                                 <x-label for="party" :value="__('Select Party')" />
                               <input type="text" class="form-control" id="myparty" readonly/>
						</div>
						</div>
                      <div class="form-row">
                      <div class="mt-4 col">
                                 <x-label for="conn_no" :value="__(' Connection No')" />
                                 <input id="conn_no" 
                                    type="hidden" name="conn_no" value=""    />
                                    <x-input id="conn_id" class="block mt-1 w-full"
                                    type="number" name="conn_id" :value="old('conn_id')"  required  />
                                    <!-- <select id="conn_no" class="form-control" name="conn_no" required>
                              <option value="">Choose...</option>
                              @foreach($connections as $connection)
                              <option value="{{$connection->id}}">{{$connection->conn_no}}</option>
                              @endforeach
                           </select> -->
                              </div>
                              <!--Connection  Name -->
                              <div class="mt-4 col">
                                 <x-label for="party_name" :value="__(' Party Name')" />
                                 <x-input id="party_name" class="block mt-1 w-full"
                                    type="text" name="party_name"  required   />
                              </div>
                              <!-- <x-input id="date" class="block mt-1 w-full" type="text" name="date" :value="old('date')" required /> -->
                              <!-- Date -->
                              <div class="mt-4 col">
                                 <x-label for="meter_status" :value="__('Meter Status')" />
                                 <x-input id="meter_status" class="block mt-1 w-full"
                                    type="text" name="meter_status" readonly required   />
                              
                              
                              <!-- <select id="meter_status" class="form-control" name="meter_status" required>
                              <option value="">Choose...</option>
                              <option value="active">Active</option>
                              <option value="inactive">In Active</option>
                              </select> -->
                             </div>
                           </div>
                           <div class="form-row">
                              <!--Connection  Name -->
                              <div class="mt-4 col">
                                 <x-label for="billing_month" :value="__('Billing Month')" />
                                  <x-input id="billing_month" class="block mt-1 w-full"
                                    type="text" value="{{date('M-Y', strtotime('-1 month'))}}" name="billing_month"  readonly    />
                              
                              <!--   <select id="billing_month" class="form-control" name="billing_month" required>-->
                              <!--<option value="">Choose...</option>-->
                              <!--<option value="{{date('M-Y')}}">{{date('M-Y')}}</option>-->
                              <!--<option value="{{date('M-Y', strtotime('-1 month'))}}">{{date('M-Y', strtotime('-1 month'))}}</option>-->
                              <!--<option value="{{date('M-Y', strtotime('-2 month'))}}">{{date('M-Y', strtotime('-2 month'))}}</option>-->
                              <!--<option value="{{date('M-Y', strtotime('-3 month'))}}">{{date('M-Y', strtotime('-3 month'))}}</option>-->
                              <!--<option value="{{date('M-Y', strtotime('-4 month'))}}">{{date('M-Y', strtotime('-4 month'))}}</option>-->
                              <!--<option value="{{date('M-Y', strtotime('-5 month'))}}">{{date('M-Y', strtotime('-5 month'))}}</option>-->

                               <!-- 
                              <option value="mar">Mar</option>
                              <option value="apr">Apr</option>
                              <option value="may">May</option>
                              <option value="jun">Jun</option>
                              <option value="jul">Jul</option>
                              <option value="aug">Aug</option>
                              <option value="sep">Sep</option>
                              <option value="oct">Oct</option>
                              <option value="nov">Nov</option>
                              <option value="dec">Dec</option> -->
                           <!--</select>-->
                               </div>
                          
                           </div>
                           <div class="form-row">
                              <!--Connection  Name -->
                              <div class="mt-4 col">
                                 <x-label for="Lat Unit" :value="__(' Last Unit')" />
                                 <input id="last_unit" class=" form-control block mt-1 w-full"
                                    type="text" name="last_unit"  readonly    />
                             </div>
                             <div class="mt-4 col">
                                 <x-label for="connection_type" :value="__(' Meter Type')" />
                                 <x-input id="connection_type" class="block mt-1 w-full"
                                    type="text" name="connection_type" readonly required   />
                             </div>
                             <div class="mt-4 col">
                                 <x-label for="unit" :value="__(' New Unit')" />
                                 <input id="unit" class="form-control block mt-1 w-full"
                                    type="number" name="unit"  required   />
                            
                                 </div>
                              </div>

                              <div class="form-row">
                              <div class="mt-4 col">
                                 <x-label for="address" :value="__('Address')" />
                                 <x-input id="address" class="block mt-1 w-full"
                                    type="text" name="address" readonly required   />
                             </div>
                             <div class="mt-4 col">
                                 <x-label for="consume" :value="__('Unit Consume')" />
                                 <input id="consume" class="form-control block mt-1 w-full"
                                    type="text" name="consume"  readonly required   />
                             </div>
                             <div class="mt-4 col">
                                 <x-label for="mincharge" :value="__('Minnumum Charge')" />
                                 <x-input id="mincharge" class="block mt-1 w-full"
                                    type="text" name="mincharge" value="200" readonly required   />
                             </div>
                         
                           </div>

                <div class="form-row">
      <div class="mt-4 col"><b>Bill total:</b></div><div class="mt-4 col"><b><sapn id="afterthree"><span id="myunit">0</span>*<span id="charge"></span></span></b></div> <div class="mt-4 col"> <sapn id="ttlunit"></sapn> INR</div></div>
	  <div class="form-row"><div class="mt-4 col"><b id="mytext"></b></div><div id="record"></div><div class="mt-4 col"><span id="plenty" ></span></div> <div class="mt-4 col"><span id="plentyttl"> </span> </div></div>
	  <div class="form-row"><div class="mt-4 col"><b>Credit Amount:</b></div><div class="mt-4 col"><span id=""></span></div> <div class="mt-4 col"><span id="creditamount"> </span> INR</div></div>
	  <div class="form-row"><div class="mt-4 col"><b>Previous Dues:</b></div><div class="mt-4 col"><span id=""></span></div> <div class="mt-4 col"><span id="predue"> </span> INR</div></div>
	  <div class="form-row"><div class="mt-4 col"><b>Late payment:</b></div><div class="mt-4 col"><span >17% of due</span></div> <div class="mt-4 col"><span id="dueamt"> </span> INR</div></div>
      <div class="form-row"><div class="mt-4 col"><b>Intrerst Due to late Payment  :</b></div><div class="mt-4 col"><span >3% on 17%</span></div> <div class="mt-4 col"><span id="latecharge"></span> INR</div></div>
      <div class="form-row"><div class="mt-4 col"> <x-input id="adjustment_reason" class="block mt-1 w-full"
                                    type="text" name="adjustment_reason" placeholder="Adjustment Reason if any"    />
                             </div><div class="mt-4 col"></div> <div class="mt-4 col"><x-input id="adjustment_amt" class="block mt-1 w-full"
                                    type="number" name="adjustment_amt" placeholder="Adjustment Amount"    /></div></div>

                                    <div class="form-row"><div class="mt-4 col"></div><div class="mt-4 col"><span ><b>Total :</b></span></div> <div class="mt-4 col"><span id="total"></span> INR</div></div>
									<div style="display:none" class="row rembal"><div class="mt-4 col"></div><div class="mt-4 col"><span ><b>Amount to be paid :</b></span></div> <div class="mt-4 col"><span id="tobepaid"></span> </div></div>
                                	<div style="display:none" class="row rembal"><div class="mt-4 col"></div><div class="mt-4 col"><span ><b>Remaning amount :</b></span></div><div class="mt-4 col"><span id="remcredit"></span> </div></div>
                        </fieldset>
                     </fieldset>
                     <input type="hidden" id="ttl" name="total"/>
                     <input type="hidden" id="consume_unit" name="consume_unit"/>
                     <input type="hidden" id="ttlbill"/>
                     <input type="hidden" id="avgunit"/>
                     <input type="hidden" id="bill_total" name="bill_total"/>
                     <input type="hidden" id="intrest" name="intrest"/>
                     <input type="hidden" id="late_payment" name="late_payment"/>
                     <input type="hidden" id="meter_id" name="meter_id"/>
                     <input type="hidden" id="saveplenty" name="saveplenty"/>
                     <input type="hidden" id="unpaid_bill_after_meter_deactivate" name="unpaid_bill_after_meter_deactivate"/>
                     <input type="hidden" id="debit" name="debit"/>
                     <input type="hidden" id="credit" name="credit"/>

                     <div class="flex items-center justify-end mt-4">
                        <x-button class="ml-4">
                           {{ __('Save') }}
                        </x-button>
                     </div>
                  </form>
               </fieldset>
            </div>
         </div>
      </div>
   </div>
   <script>
    var prevttl = parseInt($('#total').text()*1);
// var prevttl=0;
$('#createbill').submit(function () {
	var c = confirm("Click OK to continue?");
	return c; //you can just return c because it will be true or false
});

function getdata(id) {
	$.ajax({
		url: "/getconnection",
		data: {
			"value": id
		},
		dataType: "json",
		type: "post",
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		success: function (data) {
			if (data.status == 'inactive') {
				$('#conn_id').val('');

				alert('Sorry.. This connection is inactive')
				return;
			}
			if (data == 'error') {
				$('#conn_id').val('');
				alert('Sorry.. Bill already created')
				return;
			}
			//   console.log(data.conn_no);
			// alert(data.bill.adjustment_amt);
			// alert(data.totalbill);
			$("#charge").text(data.conn_type.charge)
			$('#conn_id').val(data.conn_no);
			$('#party_name').val(data.name);
			$('select[name^="party"] option[data-value="12"]').attr("selected","selected");
			 $('#myparty').val(data.party.name);
			$('#meter_status').val(data.meter.status);
			$('#ttlbill').val(data.totalbill);
			$('#avgunit').val(data.avgunit);
			$('#address').val(data.party.address);
			$('#meter_id').val(data.meter.id);
			$('#unpaid_bill_after_meter_deactivate').val(0);

			if (data.conn_type == null) {
				$('#mincharge').val(0);
			} else {
				$('#mincharge').val(data.conn_type.minimum_charge);
			}

		// 	if(data.bill==null){
		// 		var prevCapital=0;
		// 	} else{
		// 	if(data.bill.paid_status=='unpaid'){
		// 		if(data.bill.minimum_charge >data.bill.bill_total){
		// 		var prevCapital = data.prevCapital;
		// 		// var prevCapital = data.bill.minimum_charge;
		// 		}else{
		// 			// var prevCapital = data.bill.bill_total;
		// 			var prevCapital = data.prevCapital;

		// 			}
		// 	}else{
		// 		var prevCapital=0;
		// 	}
		// }
			var prevCapital = data.prevCapital;
			if(data.latepayment){
				var prevCapital = data.bill.bill_total;
				}
			if (data.meter.status == 'inactive') {
				// alert(data.avgtotal);
				// alert(data.ttm);
				// alert(data.ttlunit);

				$("#last_unit").attr('readonly','readonly');
	
					var remunit = data.avgunit*data.conn_type.charge;
					// if(data.ttm){
					// 	var plenty = data.ttm;
					// }else{
					// 	var plenty=0;	
					// }
				 	var plenty=0;	

				 $("#myunit").text(parseInt(data.avgunit));
				 $('#consume').val(data.avgunit);
				if(data.bill!=null){
					if(data.totalbill >=3){
					// $("#unit").val(parseInt(data.bill.unit+data.bill.consume_unit));
					// $('#consume').val(data.bill.consume_unit);
					$("#unit").val(parseInt(data.bill.unit+data.avgunit));
					$('#consume').val(data.avgunit);
					$("#myunit").text(parseInt(data.avgunit));

				}else{
					$("#unit").val(parseInt(0));
					$('#consume').val(0);	

				}
				}else{
					$("#unit").val(0);
				    $('#consume').val(0);

				}
				$('#consume_unit').val(0);
				// var ttlunit = remunit * 9;
				if(remunit){
					$('#ttlunit').text(remunit);

				}else{
					$('#ttlunit').text(0);
					remunit=0;
				}
				if(data.latepayment){
					var predue = parseInt(data.latepayment);

				}else{
					var predue = data.debit;

				}
                 //var predue = data.debit;
				$("#debit").val(data.debit);
				$("#credit").val(data.credit);
				$("#creditamount").text(data.credit);
                  
				// if(data.latepayment){
				// 	var predue = parseInt(data.latepayment);
				// }else{
				// var predue = parseInt(data.subtotal);
				// }
				// if(!data.subtotal){
				// 	predue=0;	
				// }

				//alert(predue);
				// var dueamt = parseInt(predue % 17);
				var dueamt = parseInt(Math.round(((prevCapital / 100) * 17)));
				var dueamt = parseInt(Math.round(dueamt / 12));

				$('#dueamt').text(dueamt);
				$('#intrest').val(dueamt);

				// var latecharge = parseInt(dueamt % 3);
				var latecharge = Math.round(((dueamt / 100) * 3));
				$('#late_payment').val(latecharge);
				$('#latecharge').text(latecharge);

				if(data.totalbill<=1){
					alert("Total bill is 1 so average unit not be calaulate . Please enter consume unit.");
					// $("#unit").focus();
					$("#myunit").text(parseInt(0));
					$('#ttlunit').text(0);
				   $("#consume").removeAttr('readonly'); 


				}
				if(data.totalbill==2){
					alert("Total bill is 2 so average unit not be calaulate . Please enter consume unit.");
					// $("#unit").focus();
					$("#myunit").text(parseInt(0));
					$('#ttlunit').text(0);
				   $("#consume").removeAttr('readonly'); 


				}
				if (data.totalbill >= 3 && data.unpaidbill < 2 ) {
					$('#plenty').html("<sapn>Last three month unit</span>"+data.mbills);
					$('#mytext').text("Avrage Consumption for  three months");
					$('#unpaid_bill_after_meter_deactivate').val(data.unpaidbill);

				}
				 else if (data.unpaidbill == 2) {
					$('#mytext').text("50% plenty due to unpaid bill of two months");
				 plenty =  Math.round(((remunit / 100) * 50));
				  $('#plentyttl').text(plenty);
				  $('#saveplenty').val(plenty);
				  $('#unpaid_bill_after_meter_deactivate').val(data.unpaidbill);

                //    $("#afterthree").text("Last three bill amount +data.aftertext");
				} else if (data.unpaidbill == 3 ) {
					$('#mytext').text("70% plenty due to unpaid bill of last three or four months");
					plenty =  Math.round(((remunit / 100) * 70));
					$('#plentyttl').text(plenty);
					$('#saveplenty').val(plenty);
					$('#unpaid_bill_after_meter_deactivate').val(data.unpaidbill);

				} else if (data.unpaidbill == 4) {
					$('#mytext').text("100% plenty due to unpaid bill of last five months");
					// $("#afterthree").html("</sapn>Last three month bill</sapn>"+data.aftertext);
					
					plenty =  Math.round(((remunit / 100) * 100));
                  $('#plentyttl').text(plenty);
					$('#saveplenty').val(plenty);
					$('#unpaid_bill_after_meter_deactivate').val(data.unpaidbill);

				} else if (data.unpaidbill >= 5) {
					$('#mytext').text("Fix planty of 5000 plenty due to unpaid bill of couple of month");
					plenty =5000;
					$('#plentyttl').text(plenty);
					$('#saveplenty').val(plenty);
					$('#unpaid_bill_after_meter_deactivate').val(data.unpaidbill);

				}

				var total = parseInt(data.debit + remunit +plenty  + dueamt + latecharge);
				// $('#total').text(total);

				$('#bill_total').val(remunit);

				var mincharge = parseInt($('#mincharge').val() * 1);
				if (mincharge < total) {
					if(data.credit>total){
					var remcredit = parseInt(data.credit-total);
					$("#credit").val(remcredit);
				$("#creditamount").text(data.credit);
					}else{
					// var total = parseInt(total-data.credit);
					}
					// $('#total').text(total-data.credit);
					$('#total').text(total);
					$('#ttl').val(total);

				} else {
					// $('#total').text(mincharge);
					// $('#ttl').val(mincharge);
					$('#total').text(0);
					$('#ttl').val(0);

				}
				 $("#unit").attr('readonly','readonly');

				if (data.unpaidbill <=2) {
					//   $("#unit").removeAttr('readonly'); 
				} else {
					 $("#unit").attr('readonly','readonly');
				}
				if(data.totalbill >= 3) {
					$("#unit").attr('readonly','readonly');

				}
				var prevttl = parseInt($('#total').text()*1);

			
			} 
			else {
			    
				$('#record').html('');
				$('#mytext').text("Amount to be paid");
				// var mincharge = parseInt($('#mincharge').val() * 1);
				// $('#total').text(mincharge);
				// 		$('#ttl').val(mincharge);
				var remunit = data.avgtotal;
				if(data.latepayment){
					var predue = parseInt(data.latepayment);

					var dueamt = parseInt(Math.round(((prevCapital / 100) * 17)));
					var dueamt = parseInt(Math.round(dueamt / 12));

					$('#dueamt').text(dueamt);
				$('#intrest').val(dueamt);

				// var latecharge = parseInt(dueamt % 3);
				var latecharge = Math.round(((dueamt / 100) * 3));
				$('#late_payment').val(latecharge);
				$('#latecharge').text(latecharge);

				}else{
					var predue = data.debit;

				}
			

				$("#debit").val(data.debit);
				$("#credit").val(data.credit);
				$("#creditamount").text(data.credit);

				// var percentage = Math.round(((predue / 17) * 100)) +"%";
				 var dueamt = Math.round(((prevCapital / 100) * 17));
				 var dueamt = parseInt(Math.round(dueamt / 12));

				 // var dueamt = parseInt(predue % 17);
				$('#dueamt').text(dueamt);
				$('#intrest').val(dueamt);

				var latecharge = Math.round(((dueamt / 100) * 3));
				// var latecharge = parseInt(dueamt % 3);
				$('#latecharge').text(latecharge);
				$('#late_payment').val(latecharge);

				var total = parseInt(data.debit  + dueamt + latecharge);
				// $('#total').text(total);

				var total = parseInt(total-data.credit);

				var mincharge = parseInt($('#mincharge').val() * 1);
				if (mincharge < total) {
					if(data.credit>total){
					var remcredit = parseInt(data.credit-total);
					// $("#credit").val(remcredit);
				$("#creditamount").text(data.credit);
					}else{
					// var total = parseInt(total-data.credit);
					}
					// $('#total').text(total-data.credit);
					 $('#total').text(total);
                      	$('#ttl').val(total);
				} else {
					// $('#total').text(mincharge);
					// $('#ttl').val(mincharge);
					$('#total').text(0);
					$('#ttl').val(0);
				}
				var prevttl = parseInt($('#total').text()*1);

			}
			$('#connection_type').val(data.type.type);
			if (data.bill == null) {
			    
				if(data.meter.current_unit){
				$('#last_unit').val(data.meter.current_unit);
				$('#unit').attr('min', data.meter.current_unit);
				$('#predue').text(data.debit);
				}else{
					$('#last_unit').val(0);
				$('#unit').attr('min', 0);
				$('#predue').text(0);	
				}
			} else {
				$('#last_unit').val(data.meter.current_unit);
            	// $('#last_unit').val(data.bill.unit);
				$('#unit').attr('min', data.meter.current_unit);
				 $('#predue').text(data.debit);
				// $('#predue').text(data.subtotal);
				// if (data.bill.adjustment_amt == null) {
				// 	$('#predue').text(0);

				// } else {
				// 	$('#predue').text(data.bill.adjustment_amt);
				// }
			}


		}
	});
}
$('#unit').focusout(function () {
	var unit = parseInt($(this).val());
	var lastunit = parseInt($("#last_unit").val() * 1);
	if (unit < lastunit) {
		alert("new unit can not be less than past unit")
		$("#unit").focus();

	}
});

$('#consume').change(function () {
	// alert("hello");
	var consumeunit = parseInt($(this).val());
	var lastunit = parseInt($("#last_unit").val() * 1);
	// if (consumeunit < lastunit) {
	// 	return;

	// }
	var remunit = parseInt(consumeunit);
	$('#myunit').text(remunit);
	$('#consume_unit').val(remunit);
	$('#consume').val(remunit);
	var charge = parseInt($('#charge').text() * 1);

	var ttlunit = remunit * charge;
	$('#ttlunit').text(remunit * charge);
	$('#bill_total').val(ttlunit);
	// alert(ttlunit);
	 var predue = parseInt($('#predue').text() * 1);
	 var dueamt = parseInt($('#dueamt').text() * 1);
	// var dueamt = parseInt(predue % 17);
	//  var dueamt = Math.round(((predue / 100) * 17));
	// $('#dueamt').text(dueamt);
	// $('#intrest').val(dueamt);
	// var latecharge = Math.round(((dueamt / 100) * 3));
	// var latecharge = parseInt(dueamt % 3);
	var latecharge = parseInt($('#latecharge').text() * 1);
	// $('#latecharge').text(latecharge);
	// $('#late_payment').val(latecharge);
	// var prevttl = parseInt($('#total').text()*1);
 	// var total = parseInt(predue + ttlunit + dueamt + latecharge);
// var total = parseInt(prevttl + ttlunit );
 	 var total = parseInt(ttlunit);

   var credit = parseInt($('#credit').val());
	var adjustment_amt = parseInt($('#adjustment_amt').val() * 1);
	var mincharge = parseInt($('#mincharge').val() * 1);
	var total = parseInt(total - adjustment_amt);

	if (mincharge < total) {
		var total = parseInt(predue + ttlunit + dueamt + latecharge);
		var total = parseInt(total - adjustment_amt);

		if(credit>total){
					var remcredit = parseInt(credit-total);
					// $("#credit").val(remcredit);
					 $(".rembal").show();
					$("#tobepaid").text(' 0 ');
				    $("#remcredit").text(''+remcredit);
					}else{
						var remcredit = parseInt(total-credit);
					// var total = parseInt(total-credit);
					$(".rembal").show();
					$("#tobepaid").text(''+remcredit);
				    $("#remcredit").text(' 0');
				
					}
		$('#total').text(parseInt(total));
		$('#ttl').val(total);
	} else {
		var total = parseInt(predue + mincharge + dueamt + latecharge);
		 var total = parseInt(total - adjustment_amt);
		 		if(credit>total){
					var remcredit = parseInt(credit-total);
					// $("#credit").val(remcredit);
					$(".rembal").show();
					$("#tobepaid").text(' 0 ');
				    $("#remcredit").text(''+remcredit);
					}else{
						var remcredit = parseInt(total-credit);
					// var total = parseInt(total-credit);
					$("#tobepaid").text('Amount to be paid :  '+remcredit);
				    $("#remcredit").text('Remaning credit amount : 0');
				
					}
		// $('#total').text(mincharge);
		// $('#ttl').val(mincharge);
		$('#total').text(total);
		$('#ttl').val(total);
	}

});

$('#unit').change(function () {
	var unit = parseInt($(this).val());
	var lastunit = parseInt($("#last_unit").val() * 1);
	if (unit < lastunit) {
		return;

	}
	var remunit = parseInt(unit - lastunit);
	$('#myunit').text(remunit);
	$('#consume_unit').val(remunit);
	$('#consume').val(remunit);
	var charge = parseInt($('#charge').text() * 1);

	var ttlunit = remunit * charge;
	$('#ttlunit').text(remunit * charge);
	$('#bill_total').val(ttlunit);
	// alert(ttlunit);
	 var predue = parseInt($('#predue').text() * 1);
	 var dueamt = parseInt($('#dueamt').text() * 1);
	// var dueamt = parseInt(predue % 17);
	//  var dueamt = Math.round(((predue / 100) * 17));
	// $('#dueamt').text(dueamt);
	// $('#intrest').val(dueamt);
	// var latecharge = Math.round(((dueamt / 100) * 3));
	// var latecharge = parseInt(dueamt % 3);
	var latecharge = parseInt($('#latecharge').text() * 1);
	// $('#latecharge').text(latecharge);
	// $('#late_payment').val(latecharge);
	// var prevttl = parseInt($('#total').text()*1);
 	// var total = parseInt(predue + ttlunit + dueamt + latecharge);
// var total = parseInt(prevttl + ttlunit );
 	 var total = parseInt(ttlunit);

   var credit = parseInt($('#credit').val());
	var adjustment_amt = parseInt($('#adjustment_amt').val() * 1);
	var mincharge = parseInt($('#mincharge').val() * 1);

	if (mincharge < total) {
 	 var total = parseInt(predue + ttlunit + dueamt + latecharge);
     var total = parseInt(total - adjustment_amt);

		if(credit>total){
					var remcredit = parseInt(credit-total);
					// $("#credit").val(remcredit);
					 $(".rembal").show();
					$("#tobepaid").text(' 0 ');
				    $("#remcredit").text(''+remcredit);
					}else{
						var remcredit = parseInt(total-credit);
					// var total = parseInt(total-credit);
					$(".rembal").show();
					$("#tobepaid").text(''+remcredit);
				    $("#remcredit").text(' 0');
				
					}
		$('#total').text(parseInt(total));
		$('#ttl').val(total);
	} else {
		// var total = parseInt(mincharge - adjustment_amt);
		var total = parseInt(predue + mincharge + dueamt + latecharge);
		 var total = parseInt(total - adjustment_amt);

		if(credit>total){
					var remcredit = parseInt(credit-total);
					// $("#credit").val(remcredit);
					$(".rembal").show();
					$("#tobepaid").text(' 0 ');
				    $("#remcredit").text(''+remcredit);
					}else{
						var remcredit = parseInt(total-credit);
						$(".rembal").show();
						// var total = parseInt(total-credit);
					$("#tobepaid").text(remcredit);
				    $("#remcredit").text('0');
				
					}
		// $('#total').text(mincharge);
		// $('#ttl').val(mincharge);
		$('#total').text(total);
		$('#ttl').val(total);
	}

});

$('#adjustment_amt').change(function () {

	var predue = parseInt($('#predue').text() * 1);
	var ttlunit = parseInt($('#ttlunit').text() * 1);
	var dueamt = parseInt($('#dueamt').text() * 1);
	var latecharge = parseInt($('#latecharge').text() * 1);
	var adjustment_amt = parseInt($('#adjustment_amt').val() * 1);
	// var total = parseInt(predue + ttlunit + dueamt + latecharge);
	var total = parseInt($('#total').text());
	// $('#ttl').val(total);
	var mincharge = parseInt($('#mincharge').val() * 1);
	if (mincharge < total) {
		var total = parseInt(total - adjustment_amt);
		$('#total').text(total);
		$('#ttl').val(total);
	} else {
		var total = parseInt(mincharge - adjustment_amt);
		$('#total').text(total);
		$('#ttl').val(total);

	}


});


$("#party_name").autocomplete({
	source: function (request, response) {
		// Fetch data
		$.ajax({
			url: "/getconnectionbyparty",
			data: {
				value: request.term
			},
			dataType: "json",
			type: "post",
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			success: function (data) {
				console.log(data);
				//   response( data );
				response($.map(data, function (item) {
					return {
						label: item.name,
						value: item.id
					};
				}));
			}
		});
	},
	select: function (event, ui) {
		// alert(ui.item.value);
		// Set selection
		$('#party_name').val(ui.item.label); // display the selected text
		$('#party_name').val(ui.item.label); // save selected id to input
		$('#conn_no').val(ui.item.value);
		$('#unit').val('');
		$('#consume').val('');
		getdata(ui.item.value);
		return false;
	}
});


$("#conn_id").autocomplete({
	source: function (request, response) {
		// Fetch data
		$.ajax({
			url: "/getconnectionbyconn",
			data: {
				value: request.term
			},
			dataType: "json",
			type: "post",
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			success: function (data) {
				console.log(data);
				//   response( data );
				response($.map(data, function (item) {
					return {
						label: item.conn_no,
						value: item.id
					};
				}));
			}
		});
	},
	select: function (event, ui) {
		//   alert(ui.item.label);
		// Set selection
		$('#conn_id').val(ui.item.label); // display the selected text
		$('#conn_id').val(ui.item.label); // save selected id to input
		$('#conn_no').val(ui.item.value);
		$('#unit').val('');
		$('#consume').val('');
        getdata(ui.item.value);

		return false;
	}
});

      $('select').selectize({
          sortField: 'text'
      });
  
   </script>
</x-app-layout>