<x-app-layout>
   <x-slot name="header">
      <div class="row">
         <div class="col-md-10">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
               {{ __('Edit Bill') }}
            </h2>
         </div>
         <div class="col-md-2">
            <a class="btn btn-primary" href="{{ route('createbill') }}">
            {{ __('Bill List') }}
            </a>
         </div>
      </div>
   </x-slot>
   <div class="py-12">
      <div class="max-w-5xl mx-auto sm:px-6 lg:px-8">
         <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
            <div class="p-6 bg-white border-b border-gray-200">
            @if (session('status') == 'success')
            <div class="mb-4 font-medium text-sm text-green-600">
               <h5> {{ __('.Data Save Succesfully') }}</h5>
            </div>
        @endif
               <x-auth-validation-errors class="mb-4" :errors="$errors" />
               <fieldset class="scheduler-border">
                  <legend class="scheduler-border">Edit Bill:</legend>
                  <form id="updatebill" method="POST" action="{{ route('createbill.update',$bill->id) }}">
                  @method('PUT')
                     @csrf
                     <div class="form-row">
                      <div class="mt-4 col">
                                 <x-label for="conn_no" :value="__(' Connection No')" />
                                 <input id="conn_no" 
                                    type="hidden" name="conn_no" value="{{old('conn_no',$bill->conn->id)}}"   />
                                    <x-input id="conn_id" class="block mt-1 w-full"
                                    type="number" name="conn_id" :value="old('conn_id',$bill->conn->conn_no)"  required readonly />
                       
                              </div>
                              <!--Connection  Name -->
                              <div class="mt-4 col">
                                 <x-label for="party_name" :value="__(' Party Name')" />
                                 <x-input id="party_name" class="block mt-1 w-full"
                                    type="text" name="party_name" :value="old('party_name',$connection->party->name)"   required readonly  />
                              </div>
                              <!-- <x-input id="date" class="block mt-1 w-full" type="text" name="date" :value="old('date')" required /> -->
                              <!-- Date -->
                              <div class="mt-4 col">
                                 <x-label for="meter_status" :value="__('Meter Status')" />
                          
                              <select id="meter_status" class="form-control" name="meter_status" required>
                              <option  value="">Choose...</option>
                              @php $meterstatus = '' @endphp
                              @if($connection->meter->status=='active')
                              @php $meterstatus ='selected' @endphp
                              @endif
                             
                              <option {{$meterstatus}} value="active">Active</option>
                              @php $inactive = '' @endphp
                              @if($connection->meter->status=='inactive')
                              @php $inactive ='selected' @endphp
                              @endif
                              <option {{$inactive}} value="inactive">In Active</option>
                              </select>
                             </div>
                           </div>
                           <div class="form-row">
                              <!--Connection  Name -->
                              <div class="mt-4 col">
                                 <x-label for="billing_month" :value="__('Billing Month')" />
                                 <x-input id="billing_month" class="block mt-1 w-full"
                                    type="text" :value="old('billing_month',$bill->billing_month)" name="billing_month"  readonly    />
                             
                               </div>
                          
                           </div>
                           <div class="form-row">
                              <!--Connection  Name -->

                              <div class="mt-4 col">
                                 <x-label for="Lat Unit" :value="__(' Last Unit')" />
                                 <x-input id="last_unit" class="block mt-1 w-full"
                                    type="text" name="last_unit" :value="old('last_unit',$bill->last_unit)" readonly    />
                             </div>

                             <div class="mt-4 col">
                                 <x-label for="connection_type" :value="__(' Connection Type')" />
                                 <x-input id="connection_type" class="block mt-1 w-full"
                                    type="text" :value="old('connection_type',$connection->connType($connection->connection_types_id)->type)" name="connection_type" readonly required   />
                             </div>
                             <div class="mt-4 col">
                                 <x-label for="unit" :value="__(' New Unit')" />
								 @if($bill->meter_status=='inactive')@php $read = 'readonly'; @endphp @else @php $read = '' @endphp  @endif
                                 <input id="unit" class="block mt-1 w-full form-control"
								  type="number" min="{{$bill->last_unit}}" value="{{old('unit',$bill->unit)}}" name="unit"  required  {{$read}}   />
                            
                                 </div>
                              </div>

                              <div class="form-row">
                              <div class="mt-4 col">
                                 <x-label for="address" :value="__('Address')" />
                                 <x-input id="address" class="block mt-1 w-full"
                                    type="text" name="address" :value="old('address',$connection->address)" readonly required   />
                             </div>
							 @if($bill->meter_status=='inactive' || $allbills<=2)@php $myread = ''; @endphp @else @php $myread = 'readonly' @endphp  @endif

                             <div class="mt-4 col">
                                 <x-label for="consume" :value="__('Unit Consume')" />
                                 <input id="consume" class="block mt-1 w-full form-control"
                                    type="text" name="consume"  value="{{old('consume',$bill->consume_unit)}}"   required {{$myread}}  />
                             </div>
                             <div class="mt-4 col">
                                 <x-label for="mincharge" :value="__('Minnumum Charge')" />
                                 <x-input id="mincharge" class="block mt-1 w-full"
                                    type="text" name="mincharge" :value="old('mincharge',$bill->minimum_charge)"  readonly required   />
                             </div>
                         
                           </div>

                           <div class="form-row">
      <div class="mt-4 col"><b id="mytext">Amount to be Paid:</b></div><div class="mt-4 col"><span id="myunit">{{$bill->consume_unit}}</span>*<span id="charge">{{$bill->per_unit_charge}}</span></div> <div class="mt-4 col"> <sapn id="ttlunit">{{$bill->bill_total}}</sapn> INR</div></div>
      <div class="form-row">
		  <div class="mt-4 col"><b>Previous Dues:</b></div><div class="mt-4 col"><span></span></div> <div class="mt-4 col">
			  <!-- +<span id="predue">{{$bill->pre_bill_due}} </span> -->
			  <input type="number" class="form-control editcal" value="{{$bill->pre_bill_due}}" name="predue" id="predue" /> 
 
			  INR</div></div>
	  @if($bill->plenty>0)
                <div class="row mt-4">
                @if($bill->unpaid_bill_after_meter_deactivate==2)
                <div class="col-md-4"><b> 50% plenty due to meter inactive:</b> </div>
               @endif
               @if($bill->unpaid_bill_after_meter_deactivate==3)
                <div class="col-md-4"><b> 70% plenty due to meter inactive:</b> </div>
               @endif
               @if($bill->unpaid_bill_after_meter_deactivate==4)
                <div class="col-md-4"><b> 100% plenty due to meter inactive:</b> </div>
               @endif
               @if($bill->unpaid_bill_after_meter_deactivate>=5)
                <div class="col-md-4"><b> Fix plenty of 5000 due to unpaid bill of more than 5 month:</b> </div>
               @endif
                <div class="col-md-4"> </div>
                <div class="col-md-4">+{{$bill->plenty}} INR </div>
                </div>
               @endif
	  <div class="form-row"><div class="mt-4 col"><b>Intrerst Due to late Payment :</b></div><div class="mt-4 col"><span >17% of due</span></div> <div class="mt-4 col">
		  <!-- <span id="dueamt">{{$bill->intrest}} </span> -->
		  <input type="number" class="form-control editcal" id="dueamt" name="dueamt" value="{{$bill->intrest}}"/>
  
		  INR</div></div>
      <div class="form-row"><div class="mt-4 col"><b>Late payment :</b></div><div class="mt-4 col"><span >3% on 17%</span></div> <div class="mt-4 col">
		  <!-- <span id="latecharge">{{$bill->late_payment}}</span> -->
		  
		   <input type="number" class="form-control editcal" value="{{$bill->late_payment}}" name="latecharge" id="latecharge"/>
 INR</div></div>
	  <div class="form-row"><div class="mt-4 col"><b>Credit amount :</b></div><div class="mt-4 col"><span ></span></div> <div class="mt-4 col"><span id="">(-{{$bill->credit}})</span> INR</div></div>
 <div class="form-row"><div class="mt-4 col"> <x-input id="adjustment_reason" class="block mt-1 w-full"
                                    type="text" name="adjustment_reason" :value="old('adjustment_reason',$bill->adjustment_reason)" placeholder="Adjustment Reason if any"    />
                             </div><div class="mt-4 col"></div> <div class="mt-4 col"><x-input id="adjustment_amt" class="block mt-1 w-full"
                                    type="number" name="adjustment_amt" placeholder="Adjustment Amount" :value="old('adjustment_amt',$bill->adjustment_amt)"    /></div></div>
					
                                    <div class="form-row"><div class="mt-4 col"></div><div class="mt-4 col"><span ><b>Total :</b></span></div> <div class="mt-4 col"><span id="total">{{$bill->total}}</span> INR</div></div>
									<div  class="row rembal"><div class="mt-4 col"></div><div class="mt-4 col"><span ><b>Amount to be paid :</b></span></div> <div class="mt-4 col"><span id="tobepaid">{{$bill->bill_to_pay}}</span> </div></div>
                                	<div  class="row rembal"><div class="mt-4 col"></div><div class="mt-4 col"><span ><b>Remaning amount :</b></span></div><div class="mt-4 col"><span id="remcredit">@if($bill->total>$bill->credit) {{0}}@else{{$bill->total-$bill->credit}}@endif</span> </div></div>

                        </fieldset>
                     </fieldset>
					 <input type="hidden" id="ttl" value="{{$bill->total}}" name="total"/>
                     <input type="hidden" id="consume_unit" value="{{$bill->consume_unit}}"  name="consume_unit"/>
                     <input type="hidden" value=""  id="ttlbill"/>
                     <input type="hidden"  id="avgunit"/>
                     <input type="hidden" value="{{$bill->bill_total}}" id="bill_total" name="bill_total"/>
                     <input type="hidden" value="{{$bill->intrest}}" id="intrest" name="intrest"/>
                     <input type="hidden" value="{{$bill->late_payment}}" id="late_payment" name="late_payment"/>
                     <input type="hidden" value="{{$bill->meter_id}}" id="meter_id" name="meter_id"/>
                     <input type="hidden" value="{{$bill->plenty}}" id="saveplenty" name="saveplenty"/>
                     <input type="hidden" value="{{$bill->unpaid_bill_after_meter_deactivate}}" id="unpaid_bill_after_meter_deactivate" name="unpaid_bill_after_meter_deactivate"/>
                     <input type="hidden" value="{{$bill->debit}}" id="debit" name="debit"/>
					 <input type="hidden" value="{{$bill->credit}}" id="credit" name="credit"/>
					 <input type="hidden"  id="bill_to_pay" value="{{$bill->bill_to_pay}}" name="bill_to_pay"/>
                     <input type="hidden"  id="credit_rem" name="credit_rem"/>
					 <input type="hidden"  id="pre_bill_due" value="{{$bill->pre_bill_due}}" name="pre_bill_due"/>

					 
         <div class="flex items-center justify-end mt-4">
                        <x-button class="ml-4">
                           {{ __('Save') }}
                        </x-button>
                     </div>
                  </form>
               </fieldset>
            </div>
         </div>
      </div>
   </div>
   <script>
$('#updatebill').submit(function () {
	var c = confirm("Click OK to continue?");
	return c; //you can just return c because it will be true or false
});
$( document ).ready(function() {
    // getdata({{$bill->connections_id}});
});

function getdata(id) {
	$.ajax({
		url: "/getconnection",
		data: {
			"value": id
		},
		dataType: "json",
		type: "post",
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		success: function (data) {
			if (data.status == 'inactive') {
				$('#conn_id').val('');

				alert('Sorry.. This connection is inactive')
				return;
			}
			if (data == 'error') {
				$('#conn_id').val('');
				alert('Sorry.. Bill already created')
				return;
			}
			//   console.log(data.conn_no);
			// alert(data.bill.adjustment_amt);
			// alert(data.totalbill);
			$("#charge").text(data.conn_type.charge)
			$('#conn_id').val(data.conn_no);
			$('#party_name').val(data.name);
			$('select[name^="party"] option[data-value="12"]').attr("selected","selected");
			 $('#myparty').val(data.party.name);
			$('#meter_status').val(data.meter.status);
			$('#ttlbill').val(data.totalbill);
			$('#avgunit').val(data.avgunit);
			$('#address').val(data.party.address);
			$('#meter_id').val(data.meter.id);
			$('#unpaid_bill_after_meter_deactivate').val(0);

			if (data.conn_charge == null) {
				$('#mincharge').val(0);
			} else {
				$('#mincharge').val(data.conn_charge.minimum_charge);
			}
			if (data.meter.status == 'inactive') {
				// alert(data.avgtotal);
				// alert(data.ttm);
				// alert(data.ttlunit);

				$("#last_unit").attr('readonly','readonly');
	
					var remunit = data.avgunit*data.conn_type.charge;
					// if(data.ttm){
					// 	var plenty = data.ttm;
					// }else{
					// 	var plenty=0;	
					// }
				 	var plenty=0;	

				 $("#myunit").text(parseInt(data.avgunit));
				 $('#consume').val(data.avgunit);
				if(data.bill!=null){
					if(data.totalbill >=3){
					// $("#unit").val(parseInt(data.bill.unit+data.bill.consume_unit));
					// $('#consume').val(data.bill.consume_unit);
					$("#unit").val(parseInt(data.bill.unit+data.avgunit));
					$('#consume').val(data.avgunit);
					$("#myunit").text(parseInt(data.avgunit));

				}else{
					$("#unit").val(parseInt(0));
					$('#consume').val(0);	

				}
				}else{
					$("#unit").val(0);
				    $('#consume').val(0);

				}
				$('#consume_unit').val(0);
				// var ttlunit = remunit * 9;
				if(remunit){
					$('#ttlunit').text(remunit);

				}else{
					$('#ttlunit').text(0);
					remunit=0;
				}
				if(data.latepayment){
					var predue = parseInt(data.latepayment);

				}else{
					var predue = data.debit;

				}
                // var predue = data.debit;
				$("#debit").val(data.debit);
				$("#credit").val(data.credit);
				$("#creditamount").text(data.credit);
                  
				// if(data.latepayment){
				// 	var predue = parseInt(data.latepayment);
				// }else{
				// var predue = parseInt(data.subtotal);
				// }
				// if(!data.subtotal){
				// 	predue=0;	
				// }

				// alert(predue);
				// var dueamt = parseInt(predue % 17);
				var dueamt = parseInt(Math.round(((predue / 100) * 17)));
				$('#dueamt').text(dueamt);
				$('#intrest').val(dueamt);

				// var latecharge = parseInt(dueamt % 3);
				var latecharge = Math.round(((dueamt / 100) * 3));
				$('#late_payment').val(latecharge);
				$('#latecharge').text(latecharge);

				if(data.totalbill==1){
					alert("Total bill is 1 so average unit not be calaulate . Please enter consume unit.");
					// $("#unit").focus();
					$("#myunit").text(parseInt(0));
					$('#ttlunit').text(0);
				   $("#consume").removeAttr('readonly'); 


				}
				if(data.totalbill==2){
					alert("Total bill is 2 so average unit not be calaulate . Please enter consume unit.");
					// $("#unit").focus();
					$("#myunit").text(parseInt(0));
					$('#ttlunit').text(0);
				   $("#consume").removeAttr('readonly'); 


				}
				if (data.totalbill >= 3 && data.unpaidbill < 2 ) {
					$('#plenty').html("<sapn>Last three month unit</span>"+data.mbills);
					$('#mytext').text("Avrage Consumption for  three months");

				}
				 else if (data.unpaidbill == 2) {
					$('#mytext').text("50% plenty due to unpaid bill of two months");
				 plenty =  Math.round(((remunit / 100) * 50));
				  $('#plentyttl').text(plenty);
				  $('#saveplenty').val(plenty);
				  $('#unpaid_bill_after_meter_deactivate').val(data.unpaidbill);

                //    $("#afterthree").text("Last three bill amount +data.aftertext");
				} else if (data.unpaidbill == 3 ) {
					$('#mytext').text("70% plenty due to unpaid bill of last three or four months");
					plenty =  Math.round(((remunit / 100) * 70));
					$('#plentyttl').text(plenty);
					$('#saveplenty').val(plenty);
					$('#unpaid_bill_after_meter_deactivate').val(data.unpaidbill);

				} else if (data.unpaidbill == 4) {
					$('#mytext').text("100% plenty due to unpaid bill of last five months");
					// $("#afterthree").html("</sapn>Last three month bill</sapn>"+data.aftertext);
					
					plenty =  Math.round(((remunit / 100) * 100));
                  $('#plentyttl').text(plenty);
					$('#saveplenty').val(plenty);
					$('#unpaid_bill_after_meter_deactivate').val(data.unpaidbill);

				} else if (data.unpaidbill >= 5) {
					$('#mytext').text("Fix planty of 5000 plenty due to unpaid bill of couple of month");
					plenty =5000;
					$('#plentyttl').text(plenty);
					$('#saveplenty').val(plenty);
					$('#unpaid_bill_after_meter_deactivate').val(data.unpaidbill);

				}

				var total = parseInt(data.debit + remunit +plenty  + dueamt + latecharge);
				// $('#total').text(total);

				$('#bill_total').val(remunit);

				var mincharge = parseInt($('#mincharge').val() * 1);
				if (mincharge < total) {
					if(data.credit>total){
					var remcredit = parseInt(data.credit-total);
					$("#credit").val(remcredit);
				$("#creditamount").text(data.credit);
					}else{
					// var total = parseInt(total-data.credit);
					}
					// $('#total').text(total-data.credit);
					$('#total').text(total);
					$('#ttl').val(total);

				} else {
					// $('#total').text(mincharge);
					// $('#ttl').val(mincharge);
					$('#total').text(0);
					$('#ttl').val(0);

				}
				 $("#unit").attr('readonly','readonly');

				if (data.unpaidbill <=2) {
					//   $("#unit").removeAttr('readonly'); 
				} else {
					 $("#unit").attr('readonly','readonly');
				}
				if(data.totalbill >= 3) {
					$("#unit").attr('readonly','readonly');

				}
				var prevttl = parseInt($('#total').text()*1);

			
			} 
			else {
				$('#record').html('');
				$('#mytext').text("Amount to be paid");
				// var mincharge = parseInt($('#mincharge').val() * 1);
				// $('#total').text(mincharge);
				// 		$('#ttl').val(mincharge);
				var remunit = data.avgtotal;
				if(data.latepayment){
					var predue = parseInt(data.latepayment);

					var dueamt = parseInt(Math.round(((predue / 100) * 17)));
				$('#dueamt').text(dueamt);
				$('#intrest').val(dueamt);

				// var latecharge = parseInt(dueamt % 3);
				var latecharge = Math.round(((dueamt / 100) * 3));
				$('#late_payment').val(latecharge);
				$('#latecharge').text(latecharge);

				}else{
					var predue = data.debit;

				}
			

				$("#debit").val(data.debit);
				$("#credit").val(data.credit);
				$("#creditamount").text(data.credit);

				// var percentage = Math.round(((predue / 17) * 100)) +"%";
				 var dueamt = Math.round(((predue / 100) * 17));
				// var dueamt = parseInt(predue % 17);
				$('#dueamt').text(dueamt);
				$('#intrest').val(dueamt);

				var latecharge = Math.round(((dueamt / 100) * 3));
				// var latecharge = parseInt(dueamt % 3);
				$('#latecharge').text(latecharge);
				$('#late_payment').val(latecharge);

				var total = parseInt(data.debit  + dueamt + latecharge);
				// $('#total').text(total);

				var total = parseInt(total-data.credit);

				var mincharge = parseInt($('#mincharge').val() * 1);
				if (mincharge < total) {
					if(data.credit>total){
					var remcredit = parseInt(data.credit-total);
					// $("#credit").val(remcredit);
				$("#creditamount").text(data.credit);
					}else{
					// var total = parseInt(total-data.credit);
					}
					// $('#total').text(total-data.credit);
					 $('#total').text(total);
                      	$('#ttl').val(total);
				} else {
					// $('#total').text(mincharge);
					// $('#ttl').val(mincharge);
					$('#total').text(0);
					$('#ttl').val(0);
				}
				var prevttl = parseInt($('#total').text()*1);

			}
			$('#connection_type').val(data.type.type);
			if (data.bill == null) {
				if(data.meter.current_unit){
				$('#last_unit').val(data.meter.current_unit);
				$('#unit').attr('min', data.meter.current_unit);
				$('#predue').text(0);
				}else{
					$('#last_unit').val(0);
				$('#unit').attr('min', 0);
				$('#predue').text(0);	
				}
			} else {
				$('#last_unit').val(data.meter.current_unit);
            	// $('#last_unit').val(data.bill.unit);
				$('#unit').attr('min', data.meter.current_unit);
				 $('#predue').text(data.debit);
				// $('#predue').text(data.subtotal);
				// if (data.bill.adjustment_amt == null) {
				// 	$('#predue').text(0);

				// } else {
				// 	$('#predue').text(data.bill.adjustment_amt);
				// }
			}


		}
	});
}
$('#unit').focusout(function () {
	var unit = parseInt($(this).val());
	var lastunit = parseInt($("#last_unit").val() * 1);
	if (unit < lastunit) {
		alert("new unit can not be less than past unit")
		$("#unit").focus();

	}
});

$('#meter_status').change(function () {
	var unit = $(this).val();
	var totalbill = parseInt($('#ttlbill').val() * 1);
	var avgunit = parseInt($('#avgunit').val() * 1);

	if (unit == 'inactive') {
		$("#last_unit").attr('disabled', 'disabled');
		if (totalbill <= 2) {
			$("#unit").removeAttr("disabled");
		} else {
			$("#unit").attr('disabled', 'disabled');
		}
		if (avgunit > 0) {
			var remunit = avgunit;
			$('#mytext').text("Avrage Consumption for last three months");
			$('#myunit').text(remunit);
			$('#consume_unit').val(remunit);
			$('#consume').val(remunit);
			var ttlunit = remunit * 9;
			$('#ttlunit').text(remunit * 9);
			var predue = parseInt($('#predue').text() * 1);
			// var dueamt = parseInt(predue % 17);
			var dueamt = Math.round(((predue / 100) * 17));
			$('#dueamt').text(dueamt);
			// var latecharge = parseInt(dueamt % 3);
			var latecharge = Math.round(((dueamt / 100) * 3));

			$('#latecharge').text(latecharge);
			var total = parseInt(predue + ttlunit + dueamt + latecharge);
			// $('#total').text(total);
			var mincharge = parseInt($('#mincharge').val() * 1);
			if (mincharge < total) {
				$('#total').text(total);
				$('#ttl').val(total);
			} else {
				$('#total').text(mincharge);
				$('#ttl').val(mincharge);

			}
		}
	}

	if (unit == 'active') {
		$("#last_unit").removeAttr("disabled");
		$("#unit").removeAttr("disabled");
	}
	// if(unit=='active'){
	//    $("#last_unit").removeAttr("disabled");
	//    $("#unit").removeAttr("disabled"); 
	// }else{
	//    $("#last_unit").attr('disabled','disabled');
	//    $("#unit").attr('disabled','disabled');

	// }
});
// $('#unit').change(function () {
// 	var unit = parseInt($(this).val());
// 	var lastunit = parseInt($("#last_unit").val() * 1);
// 	if (unit < lastunit) {
// 		return;

// 	}
// 	var remunit = parseInt(unit - lastunit);
// 	$('#myunit').text(remunit);
// 	$('#consume_unit').val(remunit);
// 	$('#consume').val(remunit);
// 	var ttlunit = remunit * 9;
// 	$('#ttlunit').text(remunit * 9);
// 	$('#bill_total').val(ttlunit);
// 	var predue = parseInt($('#predue').text() * 1);
// 	// var dueamt = parseInt(predue % 17);
// 	var dueamt = Math.round(((predue / 100) * 17));
// 	$('#dueamt').text(dueamt);
// 	$('#intrest').val(dueamt);
// 	var latecharge = Math.round(((dueamt / 100) * 3));
// 	// var latecharge = parseInt(dueamt % 3);
// 	$('#latecharge').text(latecharge);
// 	$('#late_payment').val(latecharge);
// 	var total = parseInt(predue + ttlunit + dueamt + latecharge);
// 	// $('#total').text(total);
// 	var adjustment_amt = parseInt($('#adjustment_amt').val() * 1);
// 	var mincharge = parseInt($('#mincharge').val() * 1);
// 	if (mincharge < total) {
// 		$('#total').text(total);
// 		$('#ttl').val(total);
// 	} else {
// 		var total = parseInt(mincharge - adjustment_amt);
// 		$('#total').text(mincharge);
// 		$('#ttl').val(mincharge);

// 	}

// });

$('.editcal').change(function () {
	var ttlunit = parseInt($('#bill_total').val() * 1);
	var predue = parseInt($('#predue').val() * 1);
	 var dueamt = parseInt($('#dueamt').val() * 1);
	 var latecharge = parseInt($('#latecharge').val() * 1);
	 var credit = parseInt($('#credit').val());
	 var mincharge = parseInt($('#mincharge').val() * 1);
	 var total = parseInt( ttlunit);

	 if (mincharge < total) {
	 var total = parseInt(predue + ttlunit + dueamt + latecharge);
		if(credit>total){
					var remcredit = parseInt(credit-total);
					// $("#credit").val(remcredit);
					 $(".rembal").show();
					$("#tobepaid").text(' 0 ');
				    $("#remcredit").text(''+remcredit);
					$("#credit_rem").val(remcredit);
					$("#bill_to_pay").val(0);

					}else{
						var remcredit = parseInt(total-credit);
					// var total = parseInt(total-credit);
					$(".rembal").show();
					$("#tobepaid").text(''+remcredit);
				    $("#remcredit").text(' 0');
					$("#credit_rem").val(0);
					$("#bill_to_pay").val(remcredit);


					}
					// alert(total);
		$('#total').text(parseInt(total));
		$('#ttl').val(total);
	} else {
		var total = parseInt(predue + mincharge + dueamt + latecharge);

		if(credit>total){
					var remcredit = parseInt(credit-total);
					// $("#credit").val(remcredit);
					$(".rembal").show();
					$("#tobepaid").text(' 0 ');
				    $("#remcredit").text(''+remcredit);
					$("#credit_rem").val(remcredit);
					$("#bill_to_pay").val(0);

					}else{
						// alert(total);
						var remcredit = parseInt(total-credit);
					// var total = parseInt(total-credit);
					$("#tobepaid").text('Amount to be paid :  '+remcredit);
				    $("#remcredit").text('Remaning credit amount : 0');
					$("#credit_rem").val(0);
					$("#bill_to_pay").val(remcredit);

					}
		// $('#total').text(mincharge);
		// $('#ttl').val(mincharge);
		$('#total').text(total);
		$('#ttl').val(total);
	}


});
$('#unit').change(function () {
	var unit = parseInt($(this).val());
	var lastunit = parseInt($("#last_unit").val() * 1);
	if (unit < lastunit) {
		return;

	}
	var remunit = parseInt(unit - lastunit);
	$('#myunit').text(remunit);
	$('#consume_unit').val(remunit);
	$('#consume').val(remunit);
	var charge = parseInt($('#charge').text() * 1);

	var ttlunit = parseInt(remunit * charge);
	$('#ttlunit').text(remunit * charge);
	$('#bill_total').val(ttlunit);
	var predue = parseInt($('#predue').val() * 1);
	 var dueamt = parseInt($('#dueamt').val() * 1);
	// alert(ttlunit);
	// var predue = parseInt($('#predue').text() * 1);
	// var dueamt = parseInt(predue % 17);
	// var dueamt = Math.round(((predue / 100) * 17));
	// $('#dueamt').text(dueamt);
	// $('#intrest').val(dueamt);
	// var latecharge = Math.round(((dueamt / 100) * 3));
	// var latecharge = parseInt(dueamt % 3);
	// $('#latecharge').text(latecharge);
	// $('#late_payment').val(latecharge);
	var latecharge = parseInt($('#latecharge').val() * 1);
	// var prevttl = parseInt($('#total').text()*1);
	// alert(prevttl);
    // var total = parseInt(prevttl + ttlunit );
	// var total = parseInt(predue + ttlunit + dueamt + latecharge);
	 var total = parseInt( ttlunit);

   var credit = parseInt($('#credit').val());
	var adjustment_amt = parseInt($('#adjustment_amt').val() * 1);
	var mincharge = parseInt($('#mincharge').val() * 1);
	if (mincharge < total) {
	 var total = parseInt(predue + ttlunit + dueamt + latecharge);
		if(credit>total){
					var remcredit = parseInt(credit-total);
					// $("#credit").val(remcredit);
					 $(".rembal").show();
					$("#tobepaid").text(' 0 ');
				    $("#remcredit").text(''+remcredit);
					$("#credit_rem").val(remcredit);
					$("#bill_to_pay").val(0);

					}else{
						var remcredit = parseInt(total-credit);
					// var total = parseInt(total-credit);
					$(".rembal").show();
					$("#tobepaid").text(''+remcredit);
				    $("#remcredit").text(' 0');
					$("#credit_rem").val(0);
					$("#bill_to_pay").val(remcredit);


					}
					// alert(total);
		$('#total').text(parseInt(total));
		$('#ttl').val(total);
	} else {
		var total = parseInt(predue + mincharge + dueamt + latecharge);

		if(credit>total){
					var remcredit = parseInt(credit-total);
					// $("#credit").val(remcredit);
					$(".rembal").show();
					$("#tobepaid").text(' 0 ');
				    $("#remcredit").text(''+remcredit);
					$("#credit_rem").val(remcredit);
					$("#bill_to_pay").val(0);

					}else{
						// alert(total);
						var remcredit = parseInt(total-credit);
					// var total = parseInt(total-credit);
					$("#tobepaid").text('Amount to be paid :  '+remcredit);
				    $("#remcredit").text('Remaning credit amount : 0');
					$("#credit_rem").val(0);
					$("#bill_to_pay").val(remcredit);

					}
		// $('#total').text(mincharge);
		// $('#ttl').val(mincharge);
		$('#total').text(total);
		$('#ttl').val(total);
	}

});
$('#adjustment_amt').change(function () {

	var predue = parseInt($('#predue').text() * 1);
	var ttlunit = parseInt($('#ttlunit').text() * 1);
	var dueamt = parseInt($('#dueamt').text() * 1);
	var latecharge = parseInt($('#latecharge').text() * 1);
	var adjustment_amt = parseInt($('#adjustment_amt').val() * 1);
	var total = parseInt(predue + ttlunit + dueamt + latecharge);
	var total =  parseInt($('#total').text());
	// $('#ttl').val(total);
	var mincharge = parseInt($('#mincharge').val() * 1);
	if (mincharge < total) {
		var total = parseInt(total - adjustment_amt);
		$('#total').text(total);
		$('#ttl').val(total);
	} else {
		var total = parseInt(mincharge - adjustment_amt);
		$('#total').text(total);
		$('#ttl').val(total);

	}


});


$("#party_name").autocomplete({
	source: function (request, response) {
		// Fetch data
		$.ajax({
			url: "/getconnectionbyparty",
			data: {
				value: request.term
			},
			dataType: "json",
			type: "post",
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			success: function (data) {
				console.log(data);
				//   response( data );
				response($.map(data, function (item) {
					return {
						label: item.name,
						value: item.id
					};
				}));
			}
		});
	},
	select: function (event, ui) {
		// alert(ui.item.value);
		// Set selection
		$('#party_name').val(ui.item.label); // display the selected text
		$('#party_name').val(ui.item.label); // save selected id to input
		$('#conn_no').val(ui.item.value);
		$('#unit').val('');
		$('#consume').val('');
		getdata(ui.item.value);
		return false;
	}
});


$("#conn_id").autocomplete({
	source: function (request, response) {
		// Fetch data
		$.ajax({
			url: "/getconnectionbyconn",
			data: {
				value: request.term
			},
			dataType: "json",
			type: "post",
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			success: function (data) {
				console.log(data);
				//   response( data );
				response($.map(data, function (item) {
					return {
						label: item.conn_no,
						value: item.id
					};
				}));
			}
		});
	},
	select: function (event, ui) {
		//   alert(ui.item.label);
		// Set selection
		$('#conn_id').val(ui.item.label); // display the selected text
		$('#conn_id').val(ui.item.label); // save selected id to input
		$('#conn_no').val(ui.item.value);
		$('#unit').val('');
		$('#consume').val('');
		getdata(ui.item.value);

		return false;
	}
});

$('#consume').change(function () {
	// alert("hello");
	var consumeunit = parseInt($(this).val());
	var lastunit = parseInt($("#last_unit").val() * 1);
	// if (consumeunit < lastunit) {
	// 	return;

	// }
	var remunit = parseInt(consumeunit);
	$('#myunit').text(remunit);
	$('#consume_unit').val(remunit);
	$('#consume').val(remunit);
	var charge = parseInt($('#charge').text() * 1);

	var ttlunit = remunit * charge;
	$('#ttlunit').text(remunit * charge);
	$('#bill_total').val(ttlunit);
	// alert(ttlunit);
	 var predue = parseInt($('#predue').text() * 1);
	 var dueamt = parseInt($('#dueamt').text() * 1);
	// var dueamt = parseInt(predue % 17);
	//  var dueamt = Math.round(((predue / 100) * 17));
	// $('#dueamt').text(dueamt);
	// $('#intrest').val(dueamt);
	// var latecharge = Math.round(((dueamt / 100) * 3));
	// var latecharge = parseInt(dueamt % 3);
	var latecharge = parseInt($('#latecharge').text() * 1);
	// $('#latecharge').text(latecharge);
	// $('#late_payment').val(latecharge);
	// var prevttl = parseInt($('#total').text()*1);
 	// var total = parseInt(predue + ttlunit + dueamt + latecharge);
// var total = parseInt(prevttl + ttlunit );
 	 var total = parseInt(ttlunit);

   var credit = parseInt($('#credit').val());
	var adjustment_amt = parseInt($('#adjustment_amt').val() * 1);
	var mincharge = parseInt($('#mincharge').val() * 1);
	var total = parseInt(total - adjustment_amt);

	if (mincharge < total) {
		var total = parseInt(predue + ttlunit + dueamt + latecharge);
		var total = parseInt(total - adjustment_amt);

		if(credit>total){
					var remcredit = parseInt(credit-total);
					// $("#credit").val(remcredit);
					 $(".rembal").show();
					$("#tobepaid").text(' 0 ');
				    $("#remcredit").text(''+remcredit);
					}else{
						var remcredit = parseInt(total-credit);
					// var total = parseInt(total-credit);
					$(".rembal").show();
					$("#tobepaid").text(''+remcredit);
				    $("#remcredit").text(' 0');
				
					}
		$('#total').text(parseInt(total));
		$('#ttl').val(total);
	} else {
		var total = parseInt(predue + mincharge + dueamt + latecharge);
		 var total = parseInt(total - adjustment_amt);
		 		if(credit>total){
					var remcredit = parseInt(credit-total);
					// $("#credit").val(remcredit);
					$(".rembal").show();
					$("#tobepaid").text(' 0 ');
				    $("#remcredit").text(''+remcredit);
					}else{
						var remcredit = parseInt(total-credit);
					// var total = parseInt(total-credit);
					$("#tobepaid").text('Amount to be paid :  '+remcredit);
				    $("#remcredit").text('Remaning credit amount : 0');
				
					}
		// $('#total').text(mincharge);
		// $('#ttl').val(mincharge);
		$('#total').text(total);
		$('#ttl').val(total);
	}

});


   </script>
</x-app-layout>