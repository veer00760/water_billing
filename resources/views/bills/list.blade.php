<x-app-layout>
    <x-slot name="header">
    <div class="row">
    <div class="col-md-10">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Bill') }}
        </h2>
        </div>
        <div class="col-md-2">
        <a class="btn btn-primary" href="{{ route('createbill.create') }}">
                        {{ __('Add Bill') }}
                    </a>
        </div>
        </div>
    </x-slot>
    <div class="col-md-12">
    <div class="row">
    <div class="col-md-9"></div>
    <div class="col-md-3" style="position:absolute; right:0">
    @if (session('status'))
      <div class="alert alert-success">
          <p class="msg"> <?php echo  session("status"); ?></p>
      </div>
    @endif
     </div>
     </div></div>
    <div class="py-12">
        <div class="py-12 bg-white max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg table-responsive">
            <table class="table" id="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Voucher No</th>
      <th scope="col">Connection No</th>
      <th scope="col">Billing Month</th>
      <th scope="col">Consume Unit</th>
      <th scope="col">Total Amount</th>
      <th scope="col">Payment Method</th>
      <th scope="col">Payment</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
  @php $i=1; @endphp
  @php $connid=[] @endphp
  @foreach($bills as $bill)
  
    <tr>
      <th scope="row">{{$i++}}</th>
      <th scope="row">{{$bill->id}}</th>
      <td>@if(isset($bill->conn->conn_no)){{$bill->conn->conn_no}}@else xxxxxx @endif</td>
     <td>{{$bill->billing_month}}</td>
      <td>{{$bill->consume_unit}}</td>
      <td>{{$bill->bill_to_pay}}</td>
   {{--   <td>@if($bill->paid_status=='paid')<i class="fa fa-check-circle fa-lg" aria-hidden="true"></i>
          @else
      @php       $today =date('Y-m-d H:i:s');
                 $chdate =  date('Y-m-d H:i:s', strtotime($bill->created_at. " + 15days"));
               @endphp
      @if($today<=$chdate)<i class="fa fa-exclamation-circle fa-lg"></i>@else<i class="fa fa-times-circle fa-lg" aria-hidden="true"></i>
      @endif @endif
      </td>--}}
      <td>{{$bill->pay_by}}</td>
      <td>@if($bill->paid_status=='paid')<i class="fa fa-check-circle fa-lg" data-toggle="tooltip" data-placement="top" title="Bill is paid" aria-hidden="true"></i>
      @else @if(in_array("$bill->connections_id",$connid)) <i class="fa fa-times-circle fa-lg" data-toggle="tooltip" data-placement="top" title="Bill disclose sechedule to next" aria-hidden="true"></i>
     
      @else
      @if($bill->paid_status=='pending')<i class="fa fa-times-circle fa-lg" data-toggle="tooltip" data-placement="top" title="Bill is Pending" aria-hidden="true"></i>
       @else<i class="fa fa-exclamation-circle fa-lg" data-toggle="tooltip" data-placement="top" title="Bill due"></i> 
       @endif 
      @endif 
      @endif</td>
      @php $connid[]=$bill->connections_id   @endphp
     </td>

       <td>@if($bill->paid_status=='pending') 
       <a href="#"  data-toggle="modal" data-target="#edit{{$i}}"><i class="fa fa-trash"></i></a>
         @endif|
       @if($bill->paid_status=='unpaid')@if(Auth::user()->id=='1')<a href="{{route('createbill.edit',$bill->id)}}"><i class="fa fa-pencil" aria-hidden="true"></i></a>@endif |@endif <a href="{{route('viewbill',$bill->id)}}"><i class="fa fa-eye" aria-hidden="true"></i></a>   
    </tr>

        <!-- Modal -->
<div class="modal fade" id="{{$i}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">{{$bill->total}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

      <div class="form-row">
      <div class="col-md-6"><span><b>Billing Month:</b>  {{$bill->billing_month}}</span></div>
      <div class="col-md-6"><span><b>Bill Generated On:</b>  {{date('d-M-Y',strtotime($bill->created_at))}}</span></div>
      <div class="col-md-6"><span><b>Connection No:</b> @if(isset($bill->conn->conn_no)){{$bill->conn->conn_no}}@else xxxxxx @endif</span></div>
      <div class="col-md-6"><span><b>Total Unit:</b>  {{$bill->unit}}</span></div>
      <div class="col-md-6"><span><b>Consume Unit:</b> {{$bill->consume_unit}}</span></div>
      <div class="col-md-6"><span><b>Total Amount :</b> {{$bill->total}}</span></div>

       </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

 <!-- Cheque Paymnet  model  -->
 <div class="modal fade" id="edit{{$i}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                     <div class="modal-dialog" role="document">
                        <div class="modal-content">
                           <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLongTitle">Edit Cheque Payment</h5>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                              </button>
                           </div>
                           <div class="modal-body">
                              <form method="Post" action="/admin/chequeedit">
                             @csrf
                              <input type="hidden" name="bill_id" value="{{$bill->id}}"/>
                                 <div class="form-row">
                                    <div class="mt-4 col">
                                       <x-label for="deactivate_reason" :value="__(' Change Status ')" />
                                       <select  class="form-control" name="status">
                                       <option value="">Select Status</option>
                                       <option value="paid">Paid</option>
                                       <option value="unpaid">Unpaid</option>
                                       </select>
                                    </div>
                                 </div>
                                 <div class="modal-footer">
                                    <div class="flex items-center justify-end mt-4">
                                       <x-button class="ml-4">
                                          {{ __('Save') }}
                                       </x-button>
                                    </div>
                                 </div>
                              </form>
                           </div>
                        </div>
                     </div>
                  </div>

    @endforeach
  </tbody>
</table>
             
            </div>
        </div>
    </div>

    <script>
      // jQuery.noConflict();
    jQuery(document).ready(function(e) {
      e.noConflict();
      jQuery('#table').DataTable();
      jQuery('.alert-success').hide('slide', {direction: 'right'}, 10000);

} );
    </script>
</x-app-layout>
