<x-app-layout>
    <x-slot name="header">
    <div class="row">
    <div class="col-md-10">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Bill') }}
        </h2>
        </div>
        <div class="col-md-2">
        <a class="btn btn-primary" href="{{ route('createbill.create') }}">
                        {{ __('Add Bill') }}
        </a>
        <input type="button" class="btn btn-primary" value="Print" onclick="pdf()" />

        </div>
        </div>
        <link rel="stylesheet" href="https://cdn.datatables.net/datetime/1.1.0/css/dataTables.dateTime.min.css">

    </x-slot>
    <div class="py-12">

        <div class="py-12 bg-white max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg table-responsive">
            <table border="0" cellspacing="5" cellpadding="5">
        <tbody>
        <tr>
            <td>From date:</td>
            <td><input type="text" id="min" name="min"></td>
            <td>To date:</td>
            <td><input type="text" id="max" name="max"></td>
        </tr>
    </tbody>
    </table>
    <div id="print-window">
            <table class="table" id="table">
  <thead class="thead-light">
  <tr><th class="text-center" colspan=10 scope="col">Party Name :  {{$connection->party->name}} , {{$connection->conn_no}}</th></tr>
    <tr><th class="text-center" colspan=10 scope="col">Connection Details</th></tr>
 
    <tr>
      <th scope="col">Bill Date</th>
      <th scope="col">Bill No</th>
      <!-- <th scope="col">Bill Type</th> -->
      <th scope="col">Bill Month</th>
      <th scope="col">Consume Unit</th>
      <th scope="col">Bill Amt</th>
      <th scope="col">Status</th>
        <th scope="col">Paid Amt</th>
    <th scope="col">Paid Date</th>
      <th scope="col">Txn No</th>
      <th scope="col">Balance</th>
   </tr>

  </thead>
  <tbody>
  @php $i=0; $paidamt =0; $unpaidamt=0; $cusarray = []@endphp
 @php $numItems = count($bills); @endphp
  @foreach($bills as $bill)
  @php ++$i @endphp
    <tr>
      <th scope="row">{{date('Y/m/d',strtotime($bill->created_at))}}</th>
      <th scope="row">{{$bill->id}}</th>
      <!-- <td>Manual</td> -->
     <td>{{$bill->billing_month}}</td>
      <td>{{$bill->consume_unit}}</td>
      <td>{{$bill->bill_to_pay}}</td>
      @if($bill->paid_status=='paid')
      @if($bill->paid_date!=null)
      <td class="text-success"><b>{{ucwords($bill->paid_status)}}</b></td>
          @php $paidamt+=$bill->paid_amount;  @endphp
      @else
        <td class="text-primary"><b>Shifted</b></td>
      @endif

      @else
      
      @if($i === $numItems) 
      <td class="text-danger"><b>{{ucwords($bill->paid_status)}}</b></td>
         
      @php $unpaid=$bill->total;  @endphp
     @else
     <td class="text-primary"><b>Shifted</b></td>
       @endif

      @endif
      @if($bill->paid_amount)
      <td>{{$bill->paid_amount}}</td>
      @else
      <td>N/A</td>
      @endif
      @if($bill->paid_date)
      <td>{{date('d-m-Y',strtotime($bill->paid_date))}}</td>
       @else
       <td>N/A</td>
       @endif
      @if($bill->paid_status=='paid')
      <td>Txn-{{$bill->id}}</td>
      @else

      <td>N/A</td>
      @endif
      @if($bill->paid_status=='paid')
      @if($bill->bill_credit>0)
      @php $amt = $bill->bill_credit .' CR' @endphp
      @else
      @php $amt = $bill->bill_debit .' DR' @endphp
      @endif
      <td><b>{{$amt}}</b></td>
      @else
      <td><b>{{$bill->total}} DR </b></td>
      @endif

    </tr>
        @endforeach
     
       @php $mainarray=[]; @endphp
        @foreach($custom_bills as $custom_bill)
    <tr>
    <th scope="row">{{date('d-m-Y',strtotime($custom_bill->created_at))}}</th>
      <td>{{$custom_bill->voucher_no}}</td>
   <td>@if(isset($custom_bill->conn->conn_no)){{$custom_bill->conn->conn_no}}@else xxxxxx @endif</td>
      <!-- <td>Custom</td> -->
     <td>{{date('M-Y',strtotime($custom_bill->created_at))}}</td>
      <td></td>
      <td>{{$custom_bill->total}}</td>
@if($custom_bill->paid_status=='paid')
      @if(!in_array('paid',$mainarray))
      <td>{{$custom_bill->paid_status}}</td>
          @php $paidamt+=$custom_bill->total; $mainarray[]=$custom_bill->paid_status; @endphp
      @else
        <td>shifted</td>
      @endif

      @else
      @if(!in_array('unpaid',$mainarray))
      <td>{{$custom_bill->paid_status}}</td>
      @else
        <td>shifted</td>
        @endif
      @endif
    </tr>
        @endforeach
        </tbody>
        <tfoot>
    <tr>
    <th></th>
      <th colspan="2" >Total Paid
      {{$paidamt}} INR </th>
      <th></th>
      <th></th>
      <th></th>
      
      <th></th>
      <th colspan="3">@if($connection->debit>0)
      Current Balance Amount :{{$connection->debit}} DR @else Current Balance Amount: {{$connection->credit}} CR @endif</th>
      <th></th> </tr>
  </tfoot>
</table>      
</div>       
            </div>
        </div>
    </div>
    <input type="hidden" id="name" value="{{$connection->name}}"/>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.5/css/buttons.dataTables.min.css">

    <script type="text/javascript"  src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script> 
      
      <script type="text/javascript"  src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script> 
      <script type="text/javascript"  src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script> 
      <script type="text/javascript"  src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script> 
      <script type="text/javascript"  src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script> 
      <script type="text/javascript"  src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js"></script> 
      <script type="text/javascript"  src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js"></script> 
      <script type="text/javascript"  src=" https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
      <script type="text/javascript"  src="https://cdn.datatables.net/datetime/1.1.0/js/dataTables.dateTime.min.js"></script>
    <script>
          var minDate, maxDate;
 
 // Custom filtering function which will search data in column four between two values
 jQuery.fn.dataTable.ext.search.push(
     function( settings, data, dataIndex ) {
         var min = minDate.val();
         var max = maxDate.val();
         var date = new Date( data[0] );
         console.log(data);
  
         if (
             ( min === null && max === null ) ||
             ( min === null && date <= max ) ||
             ( min <= date   && max === null ) ||
             ( min <= date   && date <= max )
         ) {
             return true;
         }
         return false;
     }
 );

 jQuery.fn.dataTable.Api.register( 'sum()', function ( ) {
    return this.flatten().reduce( function ( a, b ) {
        if ( typeof a === 'string' ) {
            a = a.replace(/[^\d.-]/g, '') * 1;
        }
        if ( typeof b === 'string' ) {
            b = b.replace(/[^\d.-]/g, '') * 1;
        }
 
        return a + b;
    }, 0 );
} );
      // jQuery.noConflict();
    jQuery(document).ready(function(e) {
      e.noConflict();
      

  // Create date inputs
  minDate = new DateTime(jQuery('#min'), {
    format: 'Do MMMM YYYY'
    });
    maxDate = new DateTime(jQuery('#max'), {
        format: 'Do MMMM YYYY'
    });
 
    // DataTables initialisation
    // var table = jQuery('#table').DataTable();
 
    // Refilter the table
    jQuery('#min, #max').on('change', function () {
        table.draw();
    });
    var table=  jQuery('#table').DataTable({
  
        "pageLength": 30,
       
      
        drawCallback: function () {
      var api = this.api();
      // jQuery( api.table().footer() ).html(
      //   api.column( 7, {page:'current'} ).data().sum()
      // );
    }
      });
      
      
} );

function pdf() {
    let t = document.getElementById('print-window').innerHTML;

    let style = "<style>";
    style = style + "table {width: 100%; font-size: 17px;}";
    style = style + "table, th, td {border: solid 1px #DDD; border-collapse: collapse;";
    style = style + "padding: 2px 3px;text-align: center;}";
    style = style + "#table_info{display:none;} #table_paginate{display:none; } #table_filter{display:none; } .dt-buttons{display:none;} #table_length{display:none;} </style>";
    let win = window.open('', '', 'height=600,width=600');

    win.document.write('<html><head>');
    win.document.write('<title>Profile</title>');
    win.document.write(style);
    win.document.write('</head>');
    win.document.write('<body>');
    win.document.write(t);
    win.document.write('</body></html>');

    win.document.close();
    win.print();
}
    </script>
</x-app-layout>
