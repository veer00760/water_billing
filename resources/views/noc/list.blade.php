<x-app-layout>
   <x-slot name="header">
      <div class="row">
         <div class="col-md-10">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
               {{ __('Noc List') }}
            </h2>
         </div>
         <div class="col-md-2">
         <!-- Button trigger modal -->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
 Apply Noc
</button>
         </div>
      </div>
   </x-slot>
   <div class="col-md-12">
    <div class="row">
    <div class="col-md-9"></div>
    <div class="col-md-3" style="position:absolute; right:0">
    @if (session('status'))
      <div class="alert alert-success">
          <p class="msg"> <?php echo  session("status"); ?></p>
      </div>
    @endif
     </div>
     </div></div>
   <div class="py-12">
      <div class="py-12 bg-white max-w-7xl mx-auto sm:px-6 lg:px-8">
         <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg table-responsive">
            <table class="table" id="table">
               <thead>
                  <tr>
                     <th scope="col">#</th>
                     <th scope="col">Connection</th>
                     <th scope="col">Party</th>
                     <th scope="col">Reason</th>
                     <th scope="col">Status</th>
                     <th scope="col">Applied On</th>
                     <th scope="col">Download Count</th>
                     <th scope="col">Action</th>

                  </tr>
               </thead>
               <tbody>
                  @php $i=1; @endphp
                  @foreach($nocs as $noc)
                  <tr>
                     <th scope="row">{{$i++}}</th>
                     <td>@if(isset($noc->conn)){{$noc->conn->conn_no}} @endif</td>
                     <td>@if(isset($noc->conn)){{$noc->conn->party->name}} @else {{$noc->party->name}} @endif</td>
                     <td>{{$noc->reason}}</td>
                     <td> {{$noc->status}}</td> 
                     <td> {{date('d-m-y',strtotime($noc->created_at))}}</td> 
                     <td> {{$noc->download}}</td> 
                     <td> <a href="{{ route('shownoc',$noc->id) }}" ><i class="fa fa-eye" aria-hidden="true"></i></a> </td>
                  </tr>

                  @endforeach
               </tbody>
            </table>
         </div>
      </div>
   </div>

   <!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Apply Noc</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form method="POST" action="/admin/applynoc">
                     @csrf
             
                      <div class="form-row">
                      <div class="mt-4 col-md-12">
                                 <x-label for="conn_no" :value="__(' Connection No')" />
                                 <!-- <x-input id="conn_no" class="block mt-1 w-full"
                                    type="text" name="conn_no" :value="old('conn_no')" required   /> -->
                                    <select id="conn_no" class="form-control" name="conn_id" required>
                              <option value="">Choose...</option>
                              @foreach($connections as $connection)
                              <option value="{{$connection->id}}">{{$connection->conn_no}}</option>
                           
                              @endforeach
                           </select>
                              </div>
                              <!--Connection  Name -->
                              <div class="mt-4 col-md-12">
                                 <x-label for="reson" :value="__(' Reason')" />
                                 <textarea id="reson" class="block mt-1 w-full" name="reason"  required ></textarea>
                              </div>
                              <!-- <x-input id="date" class="block mt-1 w-full" type="text" name="date" :value="old('date')" required /> -->
                            
                 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
                
        </form>
      </div>
    </div>
  </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js" integrity="sha256-+C0A5Ilqmu4QcSPxrlGpaZxJ04VjsRjKu+G82kl5UJk=" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap3.min.css" integrity="sha256-ze/OEYGcFbPRmvCnrSeKbRTtjG4vGLHXgOqsyLFTRjg=" crossorigin="anonymous" />

   <script>
      // jQuery.noConflict();
      jQuery(document).ready(function(e) {
      e.noConflict();
      jQuery('#conn_no').selectize();
      jQuery('#table').DataTable();
      jQuery('.alert-success').hide('slide', {direction: 'right'}, 10000);

      } );
   </script>
</x-app-layout>