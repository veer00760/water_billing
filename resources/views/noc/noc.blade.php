@extends('layouts.customer.app')
@section('content')
<div class="row">
   <div class="col-md-10"></div>
   <div class="col-md-2 ">
      <a class="btn btn-small btn-success mb-3" href="/customer/dashboard">
      Dashboard
      </a>
   </div>
</div>
<div class="wrapper">
   <div class="row">
      <div class="col-md-3">
         <img class="img img-responsive logo" src="{{ asset('img/LOGO_FINAL_VIA.png') }}" />
      </div>
      <div class="col-md-1"></div>
      <div class="col-md-4">
         <div class=" text-center ">
            <h2 class="login-heading">WATER BILLING SERVICE</h2>
            <hr class="h3-dash"/>
            <span>'Save Water, Save Life !!'</span>
         </div>
         <div class="col-md-12 text-center ">
            <h3 class="login-heading">No Dues Certificate Application</h3>
         </div>
      </div>
      <div class="col-md-2"></div>

   </div>
   <div class="col-md-12">
   <div class="row">
    <div class="col-md-9"></div>
    <div class="col-md-3" style="position:absolute; right:0">
    @if (session('status'))
      <div class="alert alert-success" style="height: 100px;">
          <p class="msg"> <?php echo  session("status"); ?></p>
      </div>
    @endif
     </div>
     </div>

   @if($debit>0)
   <div class="col-md-12 text-center ">
            <h3 class=" text-danger">Application Cannot be processed !!</h3>
         </div>
         <div class="col-md-12 text-center ">
            <h3 class="login-heading">Found dues to be cleared </br>
             Please contact office for more details !!</h3>
         </div>
   @else
    @if(!empty($noc))
      <div class="row">
         <div class="container padding-bottom-3x mb-1">
         <div class="card mb-3">
            <div class="card-body">
               <div class="steps d-flex flex-wrap flex-sm-nowrap justify-content-between padding-top-2x padding-bottom-1x">
             @if($noc->status=='pending' || $noc->status=='checked')
             @php $first = 'completed' @endphp
             @else
             @php $first = '' @endphp
             @endif
               <div class="step {{$first}}">
                  <div class="step-icon-wrap">
                     <div class="step-icon">
                     <!-- <i class="pe-7s-cart"></i> -->
                     </div>
                  </div>
                  <h4 class="step-title">Subbmited</h4>
               </div>
               <div class="step {{$first}}">
                  <div class="step-icon-wrap">
                     <div class="step-icon">
                     <!-- <i class="pe-7s-config"></i> -->
                     </div>
                  </div>
                  <h4 class="step-title">Under review</h4>
               </div>
               @if($noc->status=='checked' || $noc->status=='approve')
             @php $sec = 'completed' @endphp
             @else
             @php $sec = '' @endphp
             @endif
               <div class="step {{$sec}}">
                  <div class="step-icon-wrap">
                     <div class="step-icon">
                     <!-- <i class="pe-7s-medal"></i> -->
                     </div>
                  </div>
                  <h4 class="step-title">Under signature process</h4>
               </div>
               @if($noc->status=='approve')
             @php $third = 'completed' @endphp
             @else
             @php $third = '' @endphp
             @endif
               <div class="step {{$third}}">
                  <div class="step-icon-wrap">
                     <div class="step-icon">
                     <!-- <i class="pe-7s-car"></i> -->
                     </div>
                  </div>
                  <h4 class="step-title">Done</h4>
               </div>
               </div>
            </div>
         </div>       
         </div>
      </div>
      @else
      <form method="POST" class="mt-3" action="{{ route('noc.store') }}">
      @csrf
      <div class="row">
      <div class="col-md-3"></div>
      <div class="col-md-2"><b>Party Name: {{$record->name}}</b></div>
      <div class="col-md-2"><b>Application No: </b></div>
      <div class="col-md-2"><b>Date: {{date('d-m-Y')}} </b></div>
      <div class="col-md-3"></div>
      </div>

      <div class="row mt-3">
      @if(!empty($myconn))
      <div class="col-md-3"></div>
      <div class="col-md-5">
         <label for="conn_id"> Select Connection</label>
         <select id="conn_id"  class=" form-control"
            name="conn_id" required >
            <option value="">Select Connection</option>
            @foreach($myconn as $conns)
            <option value="{{$conns->id}}">{{$conns->conn_no}}</option>
            @endforeach
            </select>
      </div>
      <div class="col-md-3"></div>
      </div>
      @else
      <input type="hidden" name="conn_no" value="{{$record->id}}"/>
      @endif
      
      <div class="col-md-3"></div>
      <div class="col-md-5">
         <label for="reason">Describe a reason for noc certificate</label>
         <textarea id="reason" rows="6" class=" form-control"
            type="textarea"
            name="reason" required >{{old('reason')}}</textarea>
      </div>
      <div class="col-md-3"></div>
      </div>

      <div class="row mt-3">
      <div class="col-md-5"></div>
      <div class="col-md-2">
      <input type="submit" class="btn btn-small success-btn mt-3 mb-3"/>
      </div>
      <div class="col-md-5"></div>
      </div>
      </form>
      @endif
      @endif

   </div>
   
</div>
</div>
</div>
@endsection
@section('custom_styles')
<style>
body{margin-top:20px;}
@media (min-width: 576px){
.flex-sm-nowrap {
    -ms-flex-wrap: nowrap!important;
    flex-wrap: nowrap!important;
}
}
.card-body {
    -ms-flex: 1 1 auto;
    flex: 1 1 auto;
    min-height: 1px;
    padding: 1.25rem;
}
.steps .step {
    display: block;
    width: 100%;
    margin-bottom: 35px;
    text-align: center
}

.d-flex {
    display: -ms-flexbox!important;
    display: flex!important;
}
.steps .step .step-icon-wrap {
    display: block;
    position: relative;
    width: 100%;
    height: 35px;
    text-align: center
}

.steps .step .step-icon-wrap::before,
.steps .step .step-icon-wrap::after {
    display: block;
    position: absolute;
    top: 60%;
    width: 50%;
    height: 3px;
    margin-top: -1px;
    background-color: #e1e7ec;
    content: '';
    z-index: 1
}

.steps .step .step-icon-wrap::before {
    left: 0
}

.steps .step .step-icon-wrap::after {
    right: 0
}

.steps .step .step-icon {
    display: inline-block;
    position: relative;
    width: 40px;
    height: 40px;
    border: 1px solid #e1e7ec;
    border-radius: 50%;
    background-color: #f5f5f5;
    color: #374250;
    font-size: 38px;
    line-height: 41px;
    z-index: 5
}

.steps .step .step-title {
    margin-top: 16px;
    margin-bottom: 0;
    color: #606975;
    font-size: 14px;
    font-weight: 500
}

.steps .step:first-child .step-icon-wrap::before {
    display: none
}

.steps .step:last-child .step-icon-wrap::after {
    display: none
}

.steps .step.completed .step-icon-wrap::before,
.steps .step.completed .step-icon-wrap::after {
    background-color: #0da9ef
}

.steps .step.completed .step-icon {
    border-color: #0da9ef;
    background-color: #0da9ef;
    color: #fff
}

@media (max-width: 576px) {
    .flex-sm-nowrap .step .step-icon-wrap::before,
    .flex-sm-nowrap .step .step-icon-wrap::after {
        display: none
    }
}

@media (max-width: 768px) {
    .flex-md-nowrap .step .step-icon-wrap::before,
    .flex-md-nowrap .step .step-icon-wrap::after {
        display: none
    }
}

@media (max-width: 991px) {
    .flex-lg-nowrap .step .step-icon-wrap::before,
    .flex-lg-nowrap .step .step-icon-wrap::after {
        display: none
    }
}

@media (max-width: 1200px) {
    .flex-xl-nowrap .step .step-icon-wrap::before,
    .flex-xl-nowrap .step .step-icon-wrap::after {
        display: none
    }
}

.bg-faded, .bg-secondary {
    background-color: #f5f5f5 !important;
}
</style>
@endsection
@section('custom_scripts')
<script>
   $(document).ready(function(e){
    e.noConflict();
      jQuery('#table').DataTable();
      jQuery('.alert-success').hide('slide', {direction: 'right'}, 10000);

   });
</script>
@endsection