<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN">
<HTML>
   <HEAD>
   <meta name="csrf-token" content="{{ csrf_token() }}">
      <TITLE>NOC</TITLE>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
   </HEAD>
   <BODY>
   <div class="container">
     <h2 class="text-center">WATER SYSTEM GUNDLAV<h2>
     <h4 class="text-center">GUNDLAV    Email:watersyatemvia@gmail.com</h4>
        <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-4">
        No VIA/WAT/VLS/2
        </div>
        <div class="col-md-3"></div>
        <div class="col-md-3">
        Date:   {{date('d-m-Y')}}
        </div>
        </div>
        <h3 class="text-center font-weight-bold">No DUES CERTIFICATE</h3>
   
    <div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-10">
    <h5 class="pl-3"> This is to certify that M/S  <b>M/S ARMEC </b> </h5>
    </div>
        <div class="col-md-2"></div>
        <div class="col-md-7">
       Plot/Shed/Qtr No <span class="pl-2">GDN 19 </span> <span class="pl-5">Conn.NO  1245</span>
        </div>  
        <div class="col-md-3">
        <span >GIDC GUNDLAV</span>
        </div>

        <div class="col-md-2"></div>
    <div class="col-md-10">
    <span class="pl-3 mt-3"> has paid the outstanding dues of water charges. There is no due upto  <span class="pl-3">January 2020</span> </span>
    </div>

        </div>
<div class="row mt-4">
<div class="col-md-2"></div>
<div class="col-md-4">To, </br>
M/S <b>M/S ARMC.</b> </br>
Plot/Shed/Qtr No <span class="pl-2">GDN 19 </span> </br>
GIDC GUNDLAV </br>
<b>COPY OF REGIONAL MANAGER GIDC VAPI </b>
</div>
<div class="col-md-3"></div>
<div class="col-md-3">
<b>CHAIRMAN </br>
WATER SYSTEM  </br>
GUNDLAV
</b>
</div>

</div>
        

         </div>
     
   </BODY>
</HTML>