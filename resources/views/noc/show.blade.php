<x-app-layout>
   <x-slot name="header">
      <div class="row">
         <div class="col-md-10">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
               {{ __('Noc Show') }}
            </h2>
         </div>
         <div class="col-md-2">
            <!-- <a class="btn btn-primary"  href="{{route('downloadnoc',$noc->id)}}" > -->
            <a class="btn btn-primary" onclick="saveDiv('DivIdToPrint','Title')" href="#" >
            {{ __('Download') }}
            </a>
         </div>
      </div>
      <style>
      </style>
      <link rel="stylesheet" href="{{ asset('css/bill.css') }} ">
   </x-slot>
   <div class="col-md-12">
      <div class="row">
         <div class="col-md-9"></div>
         <div class="col-md-3" style="position:absolute; right:0">
            @if (session('status'))
            <div class="alert alert-success">
               <p class="msg"> <?php echo  session("status"); ?></p>
            </div>
            @endif
         </div>
      </div>
   </div>
   <div class="py-12 bg-bill-screen">
      <div class="max-w-5xl mx-auto sm:px-6 lg:px-8">
         <div  class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
            <div class="container">
               <div id="DivIdToPrint" style=" " class="p-6 bill-background ft-fm">
                <div style="border:1px solid; padding:20px; margin-top: 10px; margin-bottom: 10px;">
                  <div class="row">
                     <div class="col-md-3">
                        <img class="img img-responsive" src="{{asset('/img/LOGO_FINAL_VIA.png')}}"/>
                     </div>
                     <div class="col-md-1 text-center" ></div>
                     <div class="col-md-4 text-center" ><b>Rcl-7/70 G.I.D.C. Colony,Gundlav-396035,Valsad,Gujarat,India</b></div>
                     <div class="col-md-4 text-right " ><b>Date: </b>{{ date('d-F-Y g:i:A',strtotime($noc->created_at))}}</div>
                  </div>
                  <div class="row mt-2" >
                     <div class="col-md-3 text-center"></div>
                     <div class="col-md-7 text-left">
                        <h6 style="padding-left:10px; padding-right:5px">Office Mobile No :9157236648, Pump Man No 8347084834</h6>
                     </div>
                  </div>
                  <div class="row mt-2">
                     <div class="col-md-12 text-center">
                        <h4>WATER SYSTEM V.I.A GUNDLAV</h4>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-3"></div>
                     <div class="col-md-6">
                        <div class="heading">
                           <h3 class="bill-heading-text text-center " style="  color:  #707070;  font-weight: 700;">No Dues Certificate</h3>
                        </div>
                     </div>
                  </div>
                  <!-- <div class="row"><div class="col-md-6"></div>
                     <div class="col-md-6 text-right" ></div>
                     </div> -->
                  <div class="row mt-3">
                     <div class="col-md-4"><b>@if($noc->status=='approve')No VIA/NOC/ {{$noc->id}} @endif</b></div>
                     <div class="col-md-4 "></div>
                     <div class="col-md-4 "></div>
                  </div>
                  <div class="row mt-3">
                     <!-- <div class="col-md-12">
                        <h3 class="text-center font-weight-bold">No DUES CERTIFICATE</h3>
                        </div> -->
                  </div>
                  <div class="row mt-3">
                     <p style="font-size:22px;padding: 5px; text-align: center;">
                        This is to certify that M/S  <b>@if(isset($noc->party)){{ucwords($noc->party->name)}}@else {{ucwords($noc->conn->name)}}@endif</b>
                        <b> Plot/Shed/Qtr Number <span class="pl-2"> {{$noc->conn->address}}</span></b> 
                        <span >Connection number <b>@if(isset($noc->conn))  {{$noc->conn->conn_no}}@endif,</b></span>
                        <b> <span > GIDC GUNDLAV</span></b>
                        <span class=" mt-3"> has paid the outstanding dues of water charges. There is no due upto  <b><span>{{date('M-Y',strtotime($noc->created_at))}}</span></b> </span>
                     </p>
                  </div>
                  <div class="row mt-3">
                     <div class="col-md-4 mt-3">
                        <p>
                           To, </br>
                           M/S <b>@if(isset($noc->party)){{ucwords($noc->party->name)}}@else {{ucwords($noc->conn->name)}}@endif.</b> </br>
                           Plot/Shed/Qtr No <span class="pl-2">{{$noc->conn->address}} </span> </br>
                           GIDC GUNDLAV </br>
                        </p>
                     </div>
                     <div class="col-md-4 mt-3 pl-5 text-center"style="margin-top: 90px !important;">
                        Stamp here
                     </div>
                     <div class="col-md-4  text-center" style="margin-top: 93px !important;">
                        <b >CHAIRMAN </b></br>
                        <span>WATER SYSTEM GUNDLAV</span>
                     </div>
                     <div class="col-md-6 ">
                     <b>COPY OF REGIONAL MANAGER GIDC VAPI </b>
                     </div>
                  </div>
                  <div class="col-md-12 pl-5 text-center"style="margin-top: 90px !important;">
                     ** Not valid untill signed or stamped **
                  </div>
                <div>
              </div>
            </div>
            @if($noc->status !='approve')
            <div class="row" style="margin-right: 15px;" >
               <div class="col-md-12 m-3" style="background-color: #4b275752;" >
                  <form method="POST" action="{{ route('update-noc') }}">
                     @csrf
                     <input type="hidden" value="{{$noc->id}}" name="nocid"/>
                     <div class="form-row">
                        <div class="mt-4 col">
                           <label for="issued_by" ><b>Signed by Authorised person</b></label>
                           <select id="issued_by" class="form-control" name="issued_by" required>
                              <option value="">Choose...</option>
                              <option value="Ram kumar joshi">Ram Kunar Joshi</option>
                              <option value="Rahul alva">Rahul Alva</option>
                              <option value="Rajesh joshi">Rajesh Joshi</option>
                              <option value="bavasi">Bavisi</option>
                           </select>
                        </div>
                        <div class="mt-4 col ">
                           <div class="form-check" style="margin-top: 38px;">
                              <input class="form-check-input " name="status"  type="checkbox" value="approve" id="flexCheckDefault" required>
                              <label class="form-check-label" for="flexCheckDefault">
                              Set as done and ready to collect
                              </label>
                           </div>
                        </div>
                        <div class="col-md-12 mt-4 mb-3">
                           <div class="row">
                              <div class="col-md-4">
                              </div>
                              <div class="col-md-4">
                                 <x-button class="ml-4 btn-success btn-lg">
                                    {{ __('Update') }}
                                 </x-button>
                              </div>
                              <div class="col-md-4">
                              </div>
                           </div>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
            @endif 
         </div>
      </div>
   </div>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.4/jspdf.min.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js" integrity="sha512-s/XK4vYVXTGeUSv4bRPOuxSDmDlTedEpMEcAQk0t/FMd9V6ft8iXdwSBxV0eD60c6w/tjotSlKu9J2AAW1ckTA==" crossorigin="anonymous"></script>
   <script>
      // jQuery.noConflict();
      jQuery(document).ready(function(e) {
      e.noConflict();
      jQuery('#table').DataTable();
      jQuery('.alert-success').hide('slide', {direction: 'right'}, 10000);
      
      } );
      
      var doc = new jsPDF('p', 'pt', 'letter');
      function saveDiv(divId, title) {  
      // jQuery("#DivIdToPrint").css("border", "2px solid","margin", "10px");
              var nocid = {{$noc->id}};
      jQuery.ajax({
        url: "/admin/downloadnoc/"+nocid,
        dataType:"json",
        type: "get",
        headers: {
                  'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
              },
        success: function(data){
      
        }
      });
      var noc = '<?php if(isset($noc->party)){echo $noc->party->name; } else { echo $noc->conn->name ;} ?>';
      var month = '<?php date('M',strtotime($noc->created_at)); ?>';
      //   console.log(noc+month+'.pdf'); 
      doc.addHTML(jQuery('#DivIdToPrint')[0], function () {
       doc.save(noc+month+'.pdf');
      });
      
      }
   </script>
</x-app-layout>