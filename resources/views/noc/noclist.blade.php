@extends('layouts.customer.app')
@section('content')
<div class="row">
   <div class="col-md-10"></div>
   <div class="col-md-2 ">
      <a class="btn btn-small btn-success mb-3" href="/customer/dashboard">
      Dashboard
      </a>
   </div>
</div>
<div class="wrapper">
   <div class="row">
      <div class="col-md-3">
         <img class="img img-responsive logo" src="{{ asset('img/LOGO_FINAL_VIA.png') }}" />
      </div>
      <div class="col-md-1"></div>
      <div class="col-md-4">
         <div class=" text-center ">
            <h2 class="login-heading">WATER BILLING SERVICE</h2>
            <hr class="h3-dash"/>
            <span>'Save Water, Save Life !!'</span>
         </div>
         <div class="col-md-12 text-center ">
            <h3 class="login-heading">No Due Certificate Application</h3>
         </div>
      </div>
      <div class="col-md-2"></div>

   </div>
   <div class="col-md-12">
      <div class="row">
      <div class="col-md-12">
         <table class="table" id="table">
                  <thead>
                     <tr>
                        <th scope="col">S.No</th>
                        <th scope="col">Certificate No</th>
                        <th scope="col">Date</th>
                        <th scope="col">Reason</th>
                        <th scope="col">Status</th>
                        <th scope="col">Issued By</th>
                        <th scope="col">Note</th>
                        <th scope="col"></th>
                     </tr>
                  </thead>
                  <tbody>
                     @php $i=1; @endphp
                     @foreach($nocs as $noc)
                     @php
                     
         $today = strtotime(date('Y-m-d')) ;
        $sevendays =  strtotime($noc->updated_at. " + 7 days");
        if($today<=$sevendays){
           $check = true;
        }else{
           $check=false;
        }
                      @endphp
                     <tr>
                        <th scope="row">{{$i++}}</th>
                        <td>VIA/NOC/{{$noc->certificate_no}}</td>
                        <td>{{date('d-m-y',strtotime($noc->created_at))}}</td>
                        <td>{{$noc->reason}}</td>
                        <td><b>{{$noc->status}}</b></td>
                        <td>{{$noc->issued_by}}</td>
                        @if($noc->status=='approve')
                          @if($check)
                        <!-- <td> <a href="/customer/noc/create" class="btn btn-sm success-btn" id="download"><i class="fa fa-download" aria-hidden="true"></i></a> 
                        </td> -->
                        <td>
                        |<a href="/customer/shownocc/{{$noc->id}}" class="btn btn-sm success-btn" ><i class="fa fa-eye" aria-hidden="true"></i></a>
                       </td>
                       @endif              
                        <td>
                           <span class="text-success"><b>Please Collect Valid NOC from office</b></span>
                        </td>
                        @endif
                     </tr>
                     @endforeach
                  </tbody>
         </table>
       </div>
      </div>
      <div class="row">
      <div class="col-md-5"></div>
      <div class="col-md-2">
      <a class="btn btn-small btn-lg success-btn mt-3 ml-3 mb-3" href="/customer/noc/create">
         Apply for new
         </a>
      </div>
      <div class="col-md-5"></div>
      </div>
       
      <div class="row">
    <div class="col-md-5"></div>
    <div class="col-md-6 mt-3">

<!-- <a class="btn  success-btn" href="#">&nbsp;&nbsp;&nbsp;BACK&nbsp;&nbsp;&nbsp;</a> -->
    </div>
    <div class="col-md-1">
    <span class="text-sm">Powered By</span>
    <img class="img img-responsive  " src="{{ asset('img/black.png') }}" />

    </div>
</div>
   </div>
  
</div>
</div>
</div>
@endsection
@section('custom_scripts')
<script>
   $(document).ready(function(){
      $('#table').DataTable();

   });
</script>
@endsection