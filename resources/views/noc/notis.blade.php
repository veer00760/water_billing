<x-app-layout>
   <x-slot name="header">
      <div class="row">
         <div class="col-md-10">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
               {{ __('Notification List') }}
            </h2>
         </div>
         <div class="col-md-2">
         
         </div>
      </div>
   </x-slot>
   <div class="col-md-12">
    <div class="row">
    <div class="col-md-9"></div>
    <div class="col-md-3" style="position:absolute; right:0">
    @if (session('status'))
      <div class="alert alert-success">
          <p class="msg"> <?php echo  session("status"); ?></p>
      </div>
    @endif
     </div>
     </div></div>
   <div class="py-12">
      <div class="py-12 bg-white max-w-7xl mx-auto sm:px-6 lg:px-8">
         <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg table-responsive">
            <table class="table" id="table">
               <thead>
                  <tr>
                     <th scope="col">#</th>
                     <th scope="col">Message</th>
                     <th scope="col">Date</th>
                  </tr>
               </thead>
               <tbody>
                  @php $i=1; @endphp
                  @foreach($notis as $noti)
                  @if($noti->type=='online_payment')
                     @php $myclass ='mysuccess';
                     @endphp
                     @else
                     @php $myclass ='mydanger';
                     @endphp
                     @endif
                  <tr class="{{$myclass}}">
                     <th scope="row">{{$i++}}</th>
                     <td >{{$noti->message}}</td>
                     <td> {{date('d-m-y',strtotime($noti->created_at))}}</td> 
                  </tr>

                  @endforeach
               </tbody>
            </table>
         </div>
      </div>
   </div>
   <script>
      // jQuery.noConflict();
      jQuery(document).ready(function(e) {
      e.noConflict();
      jQuery('#table').DataTable();
      jQuery('.alert-success').hide('slide', {direction: 'right'}, 10000);

      } );
   </script>
</x-app-layout>