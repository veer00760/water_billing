<x-app-layout>
    <x-slot name="header">
    <div class="row">
    <div class="col-md-10">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Expences Account') }}
        </h2>
        </div>
        <div class="col-md-2">
        <a class="btn btn-primary" href="{{ route('expensesaccount') }}">
                        {{ __('Back') }}
                    </a>
        </div>
        </div>
    </x-slot>
    <div class="col-md-12">
    <div class="row">
    <div class="col-md-9"></div>
    <div class="col-md-3" style="position:absolute; right:0">
    @if (session('status'))
      <div class="alert alert-success">
          <p class="msg"> <?php echo  session("status"); ?></p>
      </div>
    @endif
     </div>
     </div></div>
    <div class="col-md-12">
 
    <div class="py-12">
    <div class="col-md-12">
    <div class="row">
    <div class="col-md-10">
        
        </div>
        <div class="col-md-2 mb-2">
       
        </div>
        </div>
        </div>
        <div class="py-12 bg-white max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg table-responsive">
            <div class="row"><div class="col-md-3">
           
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            <a class="btn btn-primary" href="{{ route('expencesbill.show',$account->id) }}">
        <i class="fa fa-plus" aria-hidden="true"></i>
                    </a> {{$account->name }}
        </h2>
            </div>
            <div class="col-md-3 text-left">
            <div class="card" style="width: 18rem;">
  <ul class="list-group list-group-flush">
    <li class="list-group-item">Expances till month: &nbsp;<b> {{$month}}</b></li>
  </ul>
</div>
             </div>
             <div class="col-md-3 text-left">
            <div class="card" style="width: 18rem;">
  <ul class="list-group list-group-flush">
    <li class="list-group-item">Expances of today: &nbsp;&nbsp;&nbsp;<b> {{$today}}</b></li>
  </ul>
</div>
             </div>
            <div class="col-md-3 mb-3 text-left">
            <div class="card" style="width: 18rem;">
  <ul class="list-group list-group-flush">
    <li class="list-group-item">Expances till year:&nbsp;&nbsp;&nbsp; <b> {{$year}}</b></li>
  </ul>
</div>
             </div>
            </div>
            <table class="table" id="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Bill No</th>
      <th scope="col">Date</th>
      <th scope="col">Description</th>
      <th scope="col">Amount</th>
      <th scope="col">Pay Through</th>
      <th scope="col">Added By</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
  @php $i=1; @endphp
  @foreach($billdatas as $billdata)
    <tr>
      <th scope="row">{{$i++}}</th>
      <td>{{$billdata->bill_no}}</td>
      <td>{{$billdata->created_at}}</td>
      <td>{{$billdata->description}}</td>
      <td>{{$billdata->amount}}</td>
      <td>{{$billdata->paid_by}}</td>
      <td>{{$billdata->user->name}}</td>
     <td><a href="#"  data-toggle="modal" data-target="#{{$i}}"><i class="fa fa-eye" aria-hidden="true"></i></a> | <a href="{{route('expencesbill.edit',$billdata->id)}}"><i class="fa fa-pencil" aria-hidden="true"></i></a> </td>
    </tr>
    <div class="modal fade" id="{{$i}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Expances Infromation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

      <div class="form-row">
      <div class="col-md-6"><span><b>Bill No:</b>  {{$billdata->bill_no}}</span></div>
      <div class="col-md-6"><span><b>Date:</b>  {{$billdata->created_at}}</span></div>
      <div class="col-md-6"><span><b>Amount:</b>  {{$billdata->amount}}</span></div>
      <div class="col-md-6"><span><b>Billing month:</b>  {{date('M',strtotime($billdata->created_at))}}</span></div>
   
<div class="col-md-6"><span ><b>Payment :</b> {{$billdata->paid_by}}</span></div>
      <div class="col-md-6"><span ><b>Checqe No :</b> {{$billdata->check_no}}</span></div>
       </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
    @endforeach
  </tbody>
</table>
             
            </div>
        </div>
    </div>

    <script>
      // jQuery.noConflict();
    jQuery(document).ready(function(e) {
      e.noConflict();
      jQuery('#table').DataTable();
      jQuery('.alert-success').hide('slide', {direction: 'right'}, 10000);

} );
    </script>
</x-app-layout>
