<x-app-layout>
   <x-slot name="header">
      <div class="row">
         <div class="col-md-10">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
               {{ __('Reports') }}
            </h2>
         </div>
         <div class="col-md-2">
         </div>
      </div>
   </x-slot>
   <div class="py-12">
      <div class="max-w-5xl mx-auto sm:px-6 lg:px-8">
         <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
            <div class="p-6 bg-white border-b border-gray-200">
               <x-auth-validation-errors class="mb-4" :errors="$errors" />
               <fieldset class="scheduler-border">
                  <legend class="scheduler-border">Reports:</legend>
                  <div class="mt-4 row">
                     <!-- <x-label for="paid_by" :value="__('Report Type')" />
                        <select id="type" class="form-control" name="type" required>
                           <option value="">Choose...</option>
                           <option value="monthly">Monthly</option>
                           <option value="yearly">Yearly</option>
                        </select> -->
                     <div class="col-md-6">
                        <button type="button" id="getmonth" reptype="monthly"  class=" btn btn-lg btn btn-outline-secondary rounded-pill">Monthly</button>
                     </div>
                     <div class="col-md-4"></div>
                     <div class="col-md-2">
                        <button type="button" id="getyear" align="right" reptype="yearly"  class=" btn btn-lg btn btn-outline-secondary rounded-pill">Yearly</button>
                     </div>
                  </div>
                  <div class="mt-4 row" style="display:none" id="yeartabs">
                     <div class="col-md-3"></div>
                     <div class="col-md-3">
                        <button type="button" reptype="2021"  class="rep btn-lg btn btn-outline-secondary rounded-pill btn-success" style="background-color:#3cd042ab !important">2021</button>
                     </div>
                     <div class="col-md-3">
                        <button type="button" align="right" reptype="2022"  class="rep btn-lg btn btn-outline-secondary rounded-pill btn-primary" style="background-color:#ef6868ab !important">2022</button>
                     </div>
                     <div class="col-md-3"></div>
                  </div>
                  <div id="monthshow">
                     <div class="mt-4 row" style="display:none" id="monthyear">
                        <div class="col-md-3"></div>
                        <div class="col-md-3">
                           <button type="button" reptype="2021"  class="reps  btn-lg btn btn-outline-secondary rounded-pill btn-success" style="background-color:#3cd042ab !important">(2021)</button>
                        </div>
                        <div class="col-md-3">
                           <button type="button" align="right" reptype="2022"  class="reps btn-lg btn btn-outline-secondary rounded-pill btn-primary" style="background-color:#ef6868ab !important">(2022)</button>
                        </div>
                        <div class="col-md-3"></div>
                     </div>
                     <div class="mt-4 row" style="display:none" id="monthstabs">
                        <div class="col-md-3"></div>
                        <div class="col-md-2 mt-4">
                           <button type="button" reptype="1"  class="rep  btn-lg btn btn-outline-secondary rounded-pill btn-success pd-30"  >Jan</button>
                        </div>
                        <div class="col-md-2 mt-4">
                           <button type="button" align="right" reptype="2"  class="rep btn-lg btn btn-outline-secondary rounded-pill btn-primary pd-30"  >Feb</button>
                        </div>
                        <div class="col-md-2 mt-4">
                           <button type="button" align="right" reptype="3"  class="rep btn-lg btn btn-outline-secondary rounded-pill btn-primary pd-30"  >Mar</button>
                        </div>
                        <div class="col-md-2"></div>
                        <div class="col-md-3"></div>
                        <div class="col-md-2 mt-4">
                           <button type="button" reptype="4"  class="rep  btn-lg btn btn-outline-secondary rounded-pill btn-success pd-30"  >Apr</button>
                        </div>
                        <div class="col-md-2 mt-4">
                           <button type="button" align="right" reptype="5"  class="rep btn-lg btn btn-outline-secondary rounded-pill btn-primary pd-30"  >May</button>
                        </div>
                        <div class="col-md-2 mt-4">
                           <button type="button" align="right" reptype="6"  class="rep btn-lg btn btn-outline-secondary rounded-pill btn-primary pd-30"  >Jun</button>
                        </div>
                        <div class="col-md-2"></div>
                        <div class="col-md-3"></div>
                        <div class="col-md-2 mt-4">
                           <button type="button" reptype="7"  class="rep  btn-lg btn btn-outline-secondary rounded-pill btn-success pd-30"  >Jul</button>
                        </div>
                        <div class="col-md-2 mt-4">
                           <button type="button" align="right" reptype="8"  class="rep btn-lg btn btn-outline-secondary rounded-pill btn-primary pd-30"  >Aug</button>
                        </div>
                        <div class="col-md-2 mt-4">
                           <button type="button" align="right" reptype="9"  class="rep btn-lg btn btn-outline-secondary rounded-pill btn-primary pd-30"  >Sep</button>
                        </div>
                        <div class="col-md-2"></div>
                        <div class="col-md-3"></div>
                        <div class="col-md-2 mt-4">
                           <button type="button" reptype="10"  class="rep  btn-lg btn btn-outline-secondary rounded-pill btn-success pd-30"  >Oct</button>
                        </div>
                        <div class="col-md-2 mt-4">
                           <button type="button" align="right" reptype="11"  class="rep btn-lg btn btn-outline-secondary rounded-pill btn-primary pd-30"  >Nov</button>
                        </div>
                        <div class="col-md-2 mt-4">
                           <button type="button" align="right" reptype="12"  class="rep btn-lg btn btn-outline-secondary rounded-pill btn-primary pd-30"  >Dec</button>
                        </div>
                        <div class="col-md-2"></div>
                     </div>
                </div>
                  <!-- <div class="flex items-center justify-end mt-5">
                     <x-button id="search" class="ml-4 btn-primary ">
                        {{ __('Search') }}
                     </x-button>
                     </div> -->
                  <div class="col-md-12 mt-5" id="result">
                  </div>
               </fieldset>
            </div>
         </div>
      </div>
   </div>
   <input type="hidden" id="selected-year" value=""/>
   <script type="text/JavaScript" src="https://cdnjs.cloudflare.com/ajax/libs/jQuery.print/1.6.0/jQuery.print.js"></script>
   <script>
      $(document).ready(function() {
         $("#getmonth").click(function() {
           $("#monthyear").show();
           $("#yeartabs").hide();
           $("#monthstabs").hide();

         });

         $("#getyear").click(function() {
            $("#selected-year").val('');
           $("#yeartabs").show();
           $("#monthyear").hide();
           $("#monthstabs").hide();

         });

         $(".reps").click(function() {
           $("#monthstabs").show();
           $("#yeartabs").hide();
           $("#monthyear").hide();
           var type =  $(this).attr('reptype');
           $("#selected-year").val(type);
         });

        $(".rep").click(function() {
        var selectedyear =  $("#selected-year").val();
            if(selectedyear==''){
               var year =  $(this).attr('reptype');  
               var month =  '';  
             }else{
               var year = selectedyear;
               var month =  $(this).attr('reptype');  
             }
           alert(year);
            $.ajax({
            url: "/admin/viewreport",
            data: { "year": year,"month": month, },
            dataType:"html",
            type: "post",
            headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
            success: function(data){
                // alert(data);
               console.log(data);
               $("#result").html(data);
      
            }
          });
      
        } );
        } );
        $(document).on('click','#printme',function(){
      $(".bg-bill-screen").css("background-color", ""); 
      $("#printme").hide();
      printDiv();
   //  window.print()
});

function printDiv() 
{

//   var divToPrint=document.getElementById('printdiv');
var printContents = $("#result").html();
     console.log(printContents);
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
      //   $("#printme").show();
      location.reload();


// $("#printdiv").print();

}
   </script>
</x-app-layout>