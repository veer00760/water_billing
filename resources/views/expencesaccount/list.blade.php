<x-app-layout>
    <x-slot name="header">
    <div class="row">
    <div class="col-md-10">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Expences Account') }}
        </h2>
        </div>
        <div class="col-md-2">
        <a class="btn btn-primary" href="{{ route('expensesaccount.create') }}">
                        {{ __('Add Expences Account') }}
                    </a>
        </div>
        </div>
    </x-slot>
    <div class="col-md-12">
    <div class="row">
    <div class="col-md-9"></div>
    <div class="col-md-3" style="position:absolute; right:0">
    @if (session('status'))
      <div class="alert alert-success">
          <p class="msg"> <?php echo  session("status"); ?></p>
      </div>
    @endif
     </div>
     </div></div>
    <div class="py-12">

        <div class="py-12 bg-white max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg table-responsive">
            <div class="row">
            <div class="col-md-3 text-center">
            <div class="card" style="width: 10rem;">
<a href="/admin/seeallexpances" class="btn btn-lg btn-success">Get Records</a>
</div>
           </div>
            <div class="col-md-3 mb-3 text-left"> 
            <div class="card" style="width: 18rem;">
  <ul class="list-group list-group-flush">
    <li class="list-group-item">Expances till month: &nbsp;<b> {{$month}}</b></li>
  </ul>
</div>     
            </div>
            <div class="col-md-3 text-center">
            <div class="card" style="width: 18rem;">
  <ul class="list-group list-group-flush">
    <li class="list-group-item">Expances of today: &nbsp;&nbsp;&nbsp;<b> {{$today}}</b></li>
  </ul>
</div>
           </div>

           
            <div class="col-md-3 mb-3 text-left">
            <div class="card" style="width: 18rem;">
  <ul class="list-group list-group-flush">
    <li class="list-group-item">Expances till year:&nbsp;&nbsp;&nbsp; <b> {{$year}}</b></li>
  </ul>
</div>

            </div>
            </div>
            <table class="table" id="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Name</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
  @php $i=1; @endphp
  @foreach($accounts as $account)
    <tr>
      <th scope="row">{{$i++}}</th>
      <td>{{$account->name}}</td>
     <td><a href="{{route('expencesbill.show',$account->id)}}"><i class="fa fa-plus" aria-hidden="true"></i></a> |  <a href="{{route('expencesbilllist',$account->id)}}"><i class="fa fa-eye" aria-hidden="true"></i></a> </td>
    </tr>

    @endforeach
  </tbody>
</table>
             
            </div>
        </div>
    </div>

    <script>
      // jQuery.noConflict();
    jQuery(document).ready(function(e) {
      e.noConflict();
      jQuery('#table').DataTable();
      $('.alert-success').hide('slide', {direction: 'right'}, 10000);

} );
    </script>
</x-app-layout>
