<x-app-layout>
   <x-slot name="header">
      <div class="row">
         <div class="col-md-10">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
               {{ __('Edit Party') }}
            </h2>
         </div>
         <div class="col-md-2">
            <a class="btn btn-primary" href="{{ route('party') }}">
            {{ __('Party List') }}
            </a>
         </div>
      </div>
   </x-slot>
   <div class="py-12">
      <div class="max-w-5xl mx-auto sm:px-6 lg:px-8">
         <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
            <div class="p-6 bg-white border-b border-gray-200">
            @if (session('status') == 'success')
            <div class="mb-4 font-medium text-sm text-green-600">
               <h5> {{ __('.Data Save Succesfully') }}</h5>
            </div>
        @endif
               <x-auth-validation-errors class="mb-4" :errors="$errors" />
               <fieldset class="scheduler-border">
                  <legend class="scheduler-border">Group Name:</legend>
                  <form method="POST" action="{{ route('party.update',$party->id) }}">
                  @method('PUT')
                     @csrf
                     <div class="form-row">
                        <!-- Name -->
                        <div class="mt-4 col">
                           <x-label for="name" :value="__('Name')" />
                           <x-input id="name" class="block mt-1 w-full" type="text" name="name" :value="old('name',$party->name)" required autofocus />
                        </div>
                        <!-- Party type id -->
                        <div class="mt-4 col">
                           <x-label for="party_types_id" :value="__('Party Type')" />
                           <select id="party_types_id" class="form-control" name="party_types_id" required>
                              <option value="">Choose...</option>
                              @foreach($partyTypes as $partyType)
                              @if($partyType->id==$party->party_types_id)
                          @php $selected = "selected='selcted'"; @endphp
                          @else
                          @php $selected=""; @endphp
                          @endif
                              <option {{$selected}} value="{{$partyType->id}}">{{$partyType->type}}</option>
                              @endforeach
                           </select>
                           <!-- <x-input id="party_types_id" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required /> -->
                        </div>
                     </div>
                     <div class="form-row">
                        <!-- Email -->
                        <div class="mt-4 col">
                           <x-label for="email" :value="__('Email')" />
                           <x-input id="email" class="block mt-1 w-full"
                              type="email" :value="old('email',$party->email)" name="email" required  />
                        </div>
                        <!-- Address -->
                        <div class="mt-4 col">
                           <x-label for="address" :value="__('Address')" />
                           <textarea id="address"  class="block mt-1 w-full form-control"
                              type="textarea"
                              name="address" required >{{old('address',$party->address)}}</textarea>
                        </div>
                     </div>
                     <div class="flex items-center justify-end mt-4">
                        <x-button class="ml-4">
                           {{ __('Save') }}
                        </x-button>
                     </div>
                  </form>
               </fieldset>
            </div>
         </div>
      </div>
   </div>
</x-app-layout>