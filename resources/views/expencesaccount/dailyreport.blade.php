<x-app-layout>
   <x-slot name="header">
      <div class="row">
         <div class="col-md-10">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
               {{ __('Daily Account') }}
            </h2>
         </div>
         <div class="col-md-2">
            <a class="btn btn-primary" href="{{ route('expensesaccount') }}">
            {{ __('Back') }}
            </a>
         </div>
      </div>
   </x-slot>
   <div class="col-md-12">
      <div class="row">
         <div class="col-md-9"></div>
         <div class="col-md-3" style="position:absolute; right:0">
            @if (session('status'))
            <div class="alert alert-success">
               <p class="msg"> <?php echo  session("status"); ?></p>
            </div>
            @endif
         </div>
      </div>
   </div>
   <div class="col-md-12">
   <div class="py-12">
      <div class="col-md-12">
         <div class="row">
            <div class="col-md-10">
            </div>
            <div class="col-md-2 mb-2">
            </div>
         </div>
      </div>
      <div class="py-12 bg-white max-w-7xl mx-auto sm:px-6 lg:px-8">
         <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg table-responsive">
            <div class="row">
               <div class="col-md-3">
                 
               </div>
             
            </div>
            <table class="table" id="table">
               <thead>
                  <tr>
                     <th scope="col">#</th>
                     <th scope="col">Date</th>
                     <th scope="col">Cash</th>
                     <th scope="col">Bank</th>
                     <th scope="col">Online</th>
                     <th scope="col">Settlement</th>
                     <th scope="col">Credit</th>
                     <th scope="col">Total</th>
                     <th scope="col">Action</th>
                 </tr>
               </thead>
               <tbody>
                  @php $i=1; @endphp
                  @if(!isset($payments['today']))
                  <tr>
                     <th scope="row">{{$i++}}</th>
                     <td>{{date('d-m-Y')}}</td>
                     <td>{{$payments['cash']}}</td>
                         <td>{{$payments['cheque']}}</td>
                     <td>{{$payments['online']}}</td>
                     <td>{{$payments['settlement']}}</td>
                     <td>{{$payments['credit']}}</td>
                     <td>{{array_sum($payments)}}</td>
                     <td>@if(isset($payments['today']))
                     <i class="fa fa-exclamation-circle" aria-hidden="true"></i>
                        @else <a href="{{ route('counter.create') }}" onclick="return confirm('Are you sure?')" ><i class="fa fa-check" aria-hidden="true"></i></a> </td>
                        @endif
                     </tr>
                     @endif
                  @foreach($dailyreports as $dailyreport)
                  <tr>
                     <th scope="row">{{$i++}}</th>
                     <td>{{$dailyreport->created_at}}</td>
                     <td>{{$dailyreport->cash}}</td>
                         <td>{{$dailyreport->bank}}</td>
                     <td>{{$dailyreport->online}}</td>
                     <td>{{$dailyreport->settlement}}</td>
                     <td>{{$dailyreport->credit}}</td>
                     <td>{{$dailyreport->amount}}</td>
                     <td><i class="fa fa-exclamation-circle" aria-hidden="true"></i></td>
                  </tr>
                
                  @endforeach
               </tbody>
            </table>
         </div>
      </div>
   </div>
   <script>
      // jQuery.noConflict();
      jQuery(document).ready(function(e) {
      e.noConflict();
      jQuery('#table').DataTable();
      jQuery('.alert-success').hide('slide', {direction: 'right'}, 10000);
      
      } );
   </script>
</x-app-layout>