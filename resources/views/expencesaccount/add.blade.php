<x-app-layout>
   <x-slot name="header">
      <div class="row">
         <div class="col-md-10">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
               {{ __('Expences Account') }}
            </h2>
         </div>
         <div class="col-md-2">
            <a class="btn btn-primary" href="{{ route('expensesaccount') }}">
            {{ __('Expences Account List') }}
            </a>
         </div>
      </div>
   </x-slot>
   <div class="py-12">
      <div class="max-w-5xl mx-auto sm:px-6 lg:px-8">
         <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
            <div class="p-6 bg-white border-b border-gray-200">
               <x-auth-validation-errors class="mb-4" :errors="$errors" />
               <fieldset class="scheduler-border">
                  <legend class="scheduler-border">Expences Account:</legend>
                  <form method="POST" action="{{ route('expensesaccount.store') }}">
                     @csrf
                     <div class="form-row">
                        <!-- Name -->
                        <div class="mt-4 col">
                           <x-label for="name" :value="__('Name')" />
                           <x-input id="name" class="block mt-1 w-full" type="text" name="name" :value="old('name')" required autofocus />
                        </div>
                     <div class="flex items-center justify-end mt-4">
                        <x-button class="ml-4">
                           {{ __('Save') }}
                        </x-button>
                     </div>
                  </form>
               </fieldset>
            </div>
         </div>
      </div>
   </div>
</x-app-layout>