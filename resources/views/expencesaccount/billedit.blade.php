<x-app-layout>
   <x-slot name="header">
      <div class="row">
         <div class="col-md-10">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
               {{ __('Expences Account') }}
            </h2>
         </div>
         <div class="col-md-2">
            <a class="btn btn-primary" href="{{ route('expensesaccount') }}">
            {{ __('Expences Account List') }}
            </a>
         </div>
      </div>
   </x-slot>
   <div class="py-12">
      <div class="max-w-5xl mx-auto sm:px-6 lg:px-8">
         <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
            <div class="p-6 bg-white border-b border-gray-200">
               <x-auth-validation-errors class="mb-4" :errors="$errors" />
               <fieldset class="scheduler-border">
                  <legend class="scheduler-border">Edit Bill:</legend>
                  <form method="POST" action="{{ route('expencesbill.update',$expencesBill->id) }}">
                  @method('PUT')
                     @csrf
                     <div class="form-row">
                        <!-- Name -->
                        <div class="mt-4 col-md-4">
                           <x-label for="bill_no" :value="__('Bill No')" />
                           <x-input id="bill_no" class="block mt-1 w-full" type="text" name="bill_no" :value="old('bill_no',$expencesBill->bill_no)" required autofocus />
                        </div>
                        <div class="mt-4 col-md-4">
                           <x-label for="amount" :value="__('Amount')" />
                           <x-input id="amount" class="block mt-1 w-full" type="number" name="amount" :value="old('amount',$expencesBill->amount)" required  />
                        </div>
                        <div class="mt-4 col-md-4">
                        <x-label for="paid_by" :value="__('Paid Through')" />
                        <select id="paid_by" class="form-control" name="paid_by" required>
                        @if($expencesBill->paid_by=='cash') @php $cash = 'selected' @endphp @else @php $cash = '' @endphp @endif
                        @if($expencesBill->paid_by=='cheque') @php $cheque = 'selected' @endphp @else @php $cheque = '' @endphp @endif

                           <option value="">Choose...</option>
                           <option {{$cash}} value="cash">Cash</option>
                           <option {{$cheque}} value="cheque">Cheque</option>
                        </select>
                      </div>
                      <div class="mt-4 col">
                        <x-label for="check_no" :value="__('Cheque No')" />
                        <x-input id="check_no" class="block mt-1 w-full"
                           type="number" disabled   name="check_no" :value="old('check_no',$expencesBill->check_no)"    />
                     </div>
                        <div class="mt-4 col-md-8">
                           <x-label for="description" :value="__('Description')" />
                           <textarea id="description" class="block mt-1 w-full form-control"  name="description"  required  >{{old('description',$expencesBill->description)}}</textarea>
                        </div>
                     <div class="flex items-center justify-end mt-4">
                        <x-button class="ml-4">
                           {{ __('Save') }}
                        </x-button>
                     </div>
                  </form>
               </fieldset>
            </div>
         </div>
      </div>
   </div>
   <script>
         $('#paid_by').change(function(){
var unit =$(this).val();
if(unit=='cheque'){
   $("#check_no").removeAttr("disabled"); 

}else{
   $("#check_no").attr('disabled','disabled');

}
});
   </script>
</x-app-layout>