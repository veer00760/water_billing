<x-app-layout>
   <x-slot name="header">
      <div class="row">
         <div class="col-md-10">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
               {{ __('Expances Records') }}
            </h2>
         </div>
         <div class="col-md-2">
            <a class="btn btn-primary" href="{{ route('expensesaccount') }}">
            {{ __('Expances') }}
            </a>
         </div>
      </div>
   </x-slot>
   <div class="py-12">
   <div class="max-w-5xl mx-auto sm:px-6 lg:px-8">
         <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
            <div class="p-6 bg-white border-b border-gray-200">
     
         <div class="form-row">
            <!--Connection  Name -->
            <div class="mt-4 col-md-3">
               <x-label for="party" :value="__('Select Party')" />
               <select id="party" class="form-control" name="party" >
                              <option value="">Choose...</option>
                              @foreach($accounts as $account)
                              <option value="{{$account->id}}">{{$account->name}}</option>
                              @endforeach
</select>
            </div>
            <div class="mt-4 col-md-3">
               <x-label for="month" :value="__('Select Month')" />
               <select id="month" class="form-control" name="month" >
                              <option value="">Choose...</option>
                              <option value="1">Jan</option>
                              <option value="2">Feb</option>
                              <option value="3">Mar</option>
                              <option value="4">Apr</option>
                              <option value="5">May</option>
                              <option value="6">Jun</option>
                              <option value="7">Jul</option>
                              <option value="8">Aug</option>
                              <option value="9">Sep</option>
                              <option value="10">Oct</option>
                              <option value="11">Nov</option>
                              <option value="12">Dec</option>
                      </select>
            </div>

            <div class="mt-4 col-md-3">
               <x-label for="year" :value="__('Select Year')" />
               <select id="year" class="form-control" name="year" >
                              <option value="">Choose...</option>
                              <option value="2021">2021</option>
                              <option value="2022">2022</option>
                      </select>
            </div>

            <div class="flex items-center justify-end mt-5">
                        <x-button id="search" class="ml-4 btn-primary ">
                           {{ __('Search') }}
                        </x-button>
                     </div>

            </div>




         </div>
      </div>

   <div class="col-md-12  mt-5">
      <div id="hide" style="display:none;" class="bg-white overflow-hidden shadow-sm sm:rounded-lg table-responsive">
         <table class="table mt-50" id="table" >
            <thead>
               <tr>
                  <th scope="col">#</th>
                  <!-- <th scope="col">Voucher No</th> -->
                  <th scope="col">Date</th>
                  <th scope="col">Party Name</th>
                  <th scope="col">Amount</th>
               </tr>
            </thead>
            <tbody id="data">
            </tbody>
            <tfoot>
            <tr>
               <td colspan='3'>Sum</td>
               <td id="sum"></td>
            </tr>
         </tfoot>
         </table>
      </div>

   </div>
   </div>
   <script>
      // jQuery.noConflict();
      // jQuery(document).ready(function(e) {
      //   e.noConflict();
      //   jQuery('#table').DataTable();
      // } );
      $(document).ready(function() {
     
      } );
     
      $("#search").click(function() {
 var party = $("#party").val();
var month = $("#month").val();
 var year = $("#year").val();

 getdata(party,month,year);
      } );
      
      function getdata(party,month,year){
      $.ajax({
        url: "/admin/getallexpances",
        data: { "party": party,"month":month,"year": year },
        dataType:"Json",
        type: "post",
        headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
        success: function(data){
           var html = '';
           var i =0;
           var sum =0;
                  data.map(function(item){
                   i = parseInt(i+1);
                    sum = parseInt(sum + item.amount);
                 var mysdate =  $.datepicker.formatDate( "dd-M-yy", new Date(item.created_at))
                        html=html+`<tr><td>${i}</td> <td> ${mysdate}</td><td>${item.acc.name}</td><td>${item.amount}</td></tr>`
                        })
                        console.log(html);
          $("#hide").show();
          $("#table").dataTable().fnDestroy()
          $("#data").html(html);
          $("#sum").text(sum);
          $('#table').DataTable({
  
      });
          return false;
  
        }
      });
      }


   </script>
</x-app-layout>