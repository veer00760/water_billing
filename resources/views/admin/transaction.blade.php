<x-app-layout>
   <x-slot name="header">
      <div class="row">
         <div class="col-md-10">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
               {{ __('All Tranactions') }}
            </h2>
         </div>
         <div class="col-md-2">
         
         </div>
      </div>
   </x-slot>
   <div class="col-md-12">
    <div class="row">
    <div class="col-md-9"></div>
    <div class="col-md-3" style="position:absolute; right:0">
    @if (session('status'))
      <div class="alert alert-success">
          <p class="msg"> <?php echo  session("status"); ?></p>
      </div>
    @endif
     </div>
     </div></div>
   <div class="py-12">
      <div class="py-12 bg-white max-w-7xl mx-auto sm:px-6 lg:px-8">
         <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg table-responsive">
            <table class="table" id="table">
               <thead>
                  <tr>
                  <th scope="col">Trans No</th>
                     <th scope="col">V.no</th>
                     <th scope="col">Date/Time</th>
                     <th scope="col">Type</th>
                     <th scope="col">Party</th>
                     <th scope="col">Conn no</th>
                     <th scope="col">Bill Amount</th>
                     <th scope="col">Amount paid</th>
                     <th scope="col">Status</th>
                     <th scope="col">Bank Recipt No</th>
                     <th scope="col">Credit</th>
                     <th scope="col">Debit</th>
 
                  </tr>
               </thead>
               <tbody>
                  @php $i=1; @endphp

                  @foreach($trans as $tran)
                  @if($tran->id<=999)
                  @php 
                  $num = $tran->id; 
                  $str_length = 4; 

                  // Left padding if number < $str_length 
                  $str = substr("0000{$num}", -$str_length); 
                  $vno =   sprintf($str); 
                  @endphp
                 @else
                   @php $vno = $tran->id; @endphp
                @endif
                  <tr>
                  @if($tran->pay_by=='online')
                     <td>@if(isset($tran->trans_no->payu_money_id)){{$tran->trans_no->payu_money_id}}@else --- @endif</td>
                     @else
                     <td>Txn-{{$vno}}</td>
                     @endif
                     <th scope="row">{{$tran->id}}</th>
                     @if($tran->pay_by=='online')
                     <td>{{date('d-m-Y-H:i',strtotime($tran->updated_at))}}</td>
                     @else
                     <td>{{date('d-m-Y-H:i',strtotime($tran->created_at))}}</td>
                     @endif
                     <td>{{$tran->pay_by}}</td>                    
                     <td>{{$tran->conn->party->name}}</td>
                   <td>{{$tran->conn->conn_no}}</td>
                     <td>{{$tran->bill_to_pay}}</td>
                     <td><b>{{$tran->paid_amount}}</b></td>
                     @if($tran->paid_status=='paid')
                     <td style="color:purple"><b>Success</b></td>
                     @else
                     <td style="color:red"><b>Failed</b></td>
                     @endif
                     <td>{{$tran->check_no}}</td>
                     <td style="color:purple"><b>{{$tran->bill_credit}}</b></td>
                     <td style="color:red"><b>{{$tran->bill_debit}}</b></td>
                  </tr>
                  @endforeach

                  @foreach($payments as $payment)
                  <tr>
                  <td>{{$payment->payu_money_id}}</td>
                     <th scope="row">{{$payment->bill_id}}</th>
                     <td>{{date('d-m-Y-H:i',strtotime($payment->created_at))}}</td>
                     <td>Online</td>
                     <td>{{$payment->conn->party->name}}</td>
                   <td>{{$payment->conn->conn_no}}</td>
                     <td>{{$payment->amount}}</td>
                     <td><b>0</b></td>                    
                     <td style="color:red"><b>{{$payment->paystatus}}</b></td>
                     <td>-----</td>
                     <td style="color:purple"><b>0</b></td>
                     <td style="color:red"><b>0</b></td>
 
                  </tr>

                  @endforeach


                  @foreach($settlements as $sett)
                  <tr>
                     <th scope="row">Settlement</th>
                     <td>{{date('d-m-Y-h:i',strtotime($sett->created_at))}}</td>
                     <td>{{$sett->paid_by}}</td>
                     <td>---</td>
                     <td>{{$sett->party->name}}</td>
                      <td>-----</td>
                      <td>{{$sett->amount}}</td>
                     <td><b>{{$sett->amount}}</b></td>
                     
                     <td style="color:purple"><b>Success</b></td>
                    
                     <td>{{$sett->cheque_no}}</td>
                     <td style="color:purple"><b>0</b></td>
                     <td style="color:red"><b>0</b></td>
 
                  </tr>

                  @endforeach
               </tbody>
            </table>
         </div>
      </div>
   </div>
   <script>
      // jQuery.noConflict();
      jQuery(document).ready(function(e) {
      e.noConflict();
      jQuery('#table').DataTable({ 
       "order": [[ 1, "dsc" ]] 
    }); 
      jQuery('.alert-success').hide('slide', {direction: 'right'}, 10000);

      } );
   </script>
</x-app-layout>