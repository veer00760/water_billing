<x-app-layout>
   <x-slot name="header">
      <div class="row">
         <div class="col-md-10">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
               {{ __('Profile') }}
            </h2>
         </div>
         <div class="col-md-2">
            <a class="btn btn-primary" href="{{ route('dashboard') }}">
            {{ __('Dashboard') }}
            </a>
         </div>
      </div>
   </x-slot>
   <div class="col-md-12">
    <div class="row">
    <div class="col-md-9"></div>
    <div class="col-md-3" style="position:absolute; right:0">
    @if (session('status'))
      <div class="alert alert-success">
          <p class="msg"> <?php echo  session("status"); ?></p>
      </div>
    @endif
     </div>
     </div></div>

   <div class="py-12">
   
      <div class="max-w-5xl mx-auto sm:px-6 lg:px-8">
         <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
            <div class="p-6 bg-white border-b border-gray-200">
               <x-auth-validation-errors class="mb-4" :errors="$errors" />
               <fieldset class="scheduler-border">
                  <legend class="scheduler-border">Profile:</legend>
                  <form method="POST" action="{{ route('updateprofile') }}">
                     @csrf
                 
                           <div class="form-row">

                           <div class="mt-4 col">
                                 <x-label for="name" :value="__('Name')" />
                                 <x-input id="name" class="block mt-1 w-full"
                                    type="text" name="name" value="{{old('name',$user->name) }}"  required  />
                              </div>
                              <div class="mt-4 col">
                                 <x-label for="email" :value="__('Email')" />
                                 <x-input id="email" class="block mt-1 w-full"
                                    type="email" name="email" value="{{old('email',$user->email) }}"  required  />
                              </div>

                              <div class="mt-4 col">
                                 <x-label for="role" :value="__('Role')" />
                                 <x-input id="role" class="block mt-1 w-full"
                                    type="text"  name="roles" value="{{old('roles',Auth::user()->role->name) }}"   readonly />
                              </div>
                         </div>
                         <div class="form-row">
                              <div class="mt-4 col-md-4">
                                 <x-label for="cur_password" :value="__('Current Password')" />
                                 <x-input id="cur_password" class="block mt-1 w-full"
                                    type="password"  name="cur_password"  />
                              </div>
                              <div class="mt-4 col-md-4">
                                 <x-label for="password" :value="__('New password')" />
                                 <x-input id="password" class="block mt-1 w-full"
                                    type="password"  name="password"  />
                              </div>
                              <div class="mt-4 col-md-4">
                                 <x-label for="c_password" :value="__('Confirm password')" />
                                 <x-input id="c_password" class="block mt-1 w-full"
                                    type="password"  name="c_password"/>
                              </div>

                              <div>
                        </fieldset>
                     </fieldset>
                     <div class="row">
                     <div class="col-md-10">
                     <!-- <div class="flex items-center justify-end mt-4">
                        <a href="#" class="ml-4 btn btn-primary"  data-toggle="modal" data-target="#changepass">
                           {{ __('Change Password') }}
                        </a>
                     </div> -->
                     </div>
                     <div class="col-md-2">
                     <div class="flex items-center justify-end mt-4">
                        <x-button id="update" class="ml-4">
                           {{ __('Update') }}
                        </x-button>
                     </div>
                     </div>
                     </div>
                  </form>
               </fieldset>

  


            </div>
         </div>
      </div>
   </div>
<script>
$(document).ready(function() {
    $('#update').click(function(event){
    
        data = $('#password').val();
         //   var len = data.length;
        
      //   if(len < 1) {
      //       alert("Password cannot be blank");
      //       // Prevent form submission
      //       event.preventDefault();
      //   }
         
        if($('#password').val() != $('#c_password').val()) {
            alert("Password and Confirm Password don't match");
            // Prevent form submission
            event.preventDefault();
        }
         
    });
    jQuery('.alert-success').hide('slide', {direction: 'right'}, 10000);

});
</script>



</x-app-layout>