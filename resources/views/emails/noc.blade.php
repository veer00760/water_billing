@component('mail::message')
{{-- Greeting --}}

# Hello!

{{-- Intro Lines --}}

 <br/>
{{$message}}


{{-- Action Button --}}


@component('mail::button', ['url' => 'http://admin.valsadindustrialwaterservices.in/', 'color' => 'green'])
Visit
@endcomponent


{{-- Outro Lines --}}


{{-- Salutation --}}

Regards,
{{ config('app.name') }}


{{-- Subcopy --}}
@component('mail::subcopy')
If you’re having trouble clicking the "'Visit'" button, copy and paste the URL below
into your web browser: ['VIA']('http://admin.valsadindustrialwaterservices.in')
<br/>
<tr><td style="text-align: center;"><img style="max-width:23% !important;sss" src="https://admin.valsadindustrialwaterservices.in/img/black.png" class="img img-responsive myclass logo" alt="logo"></td></tr>
@endcomponent
@endcomponent