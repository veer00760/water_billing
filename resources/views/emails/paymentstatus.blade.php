@component('mail::message')
{{-- Greeting --}}

# Hello!

{{-- Intro Lines --}}

@if($status =='success')
<tr><td style="text-align: center;">
<img style="max-width:26% !important;" src="https://admin.valsadindustrialwaterservices.in/img/success.png" class="img img-responsive myclass logo" alt="Success Logo">
</td></tr>
<h2>You have succesfully paid for the amount of ₹{{$amt}}. payment id is {{$payid}}</h2>
<h4>Please visit below link for further details.</h4>
@else
<tr><td style="text-align: center;">
<img style="max-width:26% !important;" src="https://admin.valsadindustrialwaterservices.in/img/fail.png" class=" img img-responsive myclass logo" alt="Failed Logo">
</td></tr>
<tr><td>
<h2>Your payment has been declined due to some technical issue</h2>
</tr></td><tr><td>
<h4>Please visit below link for further details.</h4>
</tr></td>
@endif

{{-- Action Button --}}


@component('mail::button', ['url' => 'http://admin.valsadindustrialwaterservices.in/', 'color' => 'green'])
Visit
@endcomponent


{{-- Outro Lines --}}


{{-- Salutation --}}

Regards,
{{ config('app.name') }}


{{-- Subcopy --}}
@component('mail::subcopy')
If you’re having trouble clicking the "'Visit'" button, copy and paste the URL below
into your web browser: ['VIA']('http://admin.valsadindustrialwaterservices.in/')
<br/>
<tr><td style="text-align: center;"><img style="max-width:23% !important;" src="https://admin.valsadindustrialwaterservices.in/img/black.png" class="img img-responsive myclass logo" alt="logo"></td></tr>
@endcomponent
@endcomponent