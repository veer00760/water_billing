@component('mail::message')
{{-- Greeting --}}

# Hello!

{{-- Intro Lines --}}

Your bill for the month of {{$month}} has been generated 
of amount <h1 class="text-center">₹ {{$total}}</h1>
<h1>Connection No:  {{$conn}}</h1>
<h1>Customer Name:  {{$name}}</h1>
<h1>Consumed Unit:  {{$unit}}</h1>
<h1>Payment Status:  {{$status}}</h1>


{{-- Action Button --}}


@component('mail::button', ['url' => 'http://admin.valsadindustrialwaterservices.in/', 'color' => 'green'])
Pay Now
@endcomponent


{{-- Outro Lines --}}
Please pay before {{$paydate}}


{{-- Salutation --}}

Regards,
{{ config('app.name') }}


{{-- Subcopy --}}
@component('mail::subcopy')
If you’re having trouble clicking the "'Pay Now'" button, copy and paste the URL below
into your web browser: ['google.com']('google.com')
<br/>
<tr><td style="text-align: center;"><img style="max-width:23% !important;sss" src="https://admin.valsadindustrialwaterservices.in/img/black.png" class="img img-responsive myclass logo" alt="logo"></td></tr>

@endcomponent
@endcomponent